if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_INVOICE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_INVOICE]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_MASTER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_MASTER]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Auto]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Auto]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Auto_Import]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Auto_Import]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Auto_import_old]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Auto_import_old]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Auto_old]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Auto_old]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Chq]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Chq]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Chq_One]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Chq_One]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[CIPL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIPL]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Code]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Code]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Error]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Error]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Error_List]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Error_List]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Final_Out]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Final_Out]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[ID_Code]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ID_Code]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[INV_TYPE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[INV_TYPE]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[INVCODE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[INVCODE]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_18-Jan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_18-Jan]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_37]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_37]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec_B4_Convrtng]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec_B4_Convrtng]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_DEC_Old]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_DEC_Old]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec_old_B4_Feb]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec_old_B4_Feb]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec_Sale]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec_Sale]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_DEC1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_DEC1]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Main]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Main]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[KNOCK_prev]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[KNOCK_prev]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Knock1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Knock1]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[NEW_CAR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[NEW_CAR]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Payment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Payment]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Pending]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Pending]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Product]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Product]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Ship_to]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ship_to]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally_MIS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally_MIS]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally_MIS_IIIrdOld]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally_MIS_IIIrdOld]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally_MIS_IInd]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally_MIS_IInd]
GO

CREATE TABLE [dbo].[AP_INVOICE] (
	[INVOICE_NO] [varchar] (50) NULL ,
	[INV_TYPE] [nvarchar] (255) NULL ,
	[VENDOR_NAME] [nvarchar] (255) NULL ,
	[V_ID] [nvarchar] (255) NULL ,
	[SITE] [nvarchar] (255) NULL ,
	[AMOUNT] [float] NULL ,
	[INV_DATE] [datetime] NULL ,
	[TERMS_NAME] [numeric](18, 0) NULL ,
	[DESCRIPTION] [nvarchar] (255) NULL ,
	[GL_DATE] [datetime] NULL ,
	[SITE_CODE] [numeric](18, 0) NULL ,
	[CAR_NO] [nvarchar] (255) NULL ,
	[NATURAL_ACCOUNT] [nvarchar] (255) NULL ,
	[NA_CODE] [numeric](18, 0) NULL ,
	[PRODUCT] [nvarchar] (255) NULL ,
	[P_CODE] [numeric](18, 0) NULL ,
	[TAX_NAME] [float] NULL ,
	[TAX RATE] [float] NULL ,
	[TAX ID] [float] NULL ,
	[Branch_Name] [varchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AP_MASTER] (
	[Sl#No] [float] NULL ,
	[Vendor_Name] [nvarchar] (255) NULL ,
	[Alternate_Name] [nvarchar] (255) NULL ,
	[Location] [nvarchar] (255) NULL ,
	[vendor_Type] [nvarchar] (255) NULL ,
	[Code] [nvarchar] (255) NULL ,
	[Site] [nvarchar] (255) NULL ,
	[City] [nvarchar] (255) NULL ,
	[Location1] [nvarchar] (255) NULL ,
	[Address1] [nvarchar] (255) NULL ,
	[PIN_CODE] [nvarchar] (255) NULL ,
	[PAN ] [nvarchar] (255) NULL ,
	[LOCATION2] [nvarchar] (255) NULL ,
	[NATURAL_ACCOUNTS] [float] NULL ,
	[PRODUCT] [nvarchar] (255) NULL ,
	[LOCATION3] [nvarchar] (255) NULL ,
	[Car_No] [nvarchar] (255) NULL ,
	[Natural_AccountP] [float] NULL ,
	[PRODUCT1] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Auto] (
	[CODE] [nvarchar] (255) NULL ,
	[CIPL_CUST] [nvarchar] (255) NULL ,
	[PAYMENT_METHOD] [nvarchar] (255) NULL ,
	[PROFILE] [nvarchar] (255) NULL ,
	[Category] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Auto_Import] (
	[RMS_Cust] [nvarchar] (255) NULL ,
	[CIPL_CUST] [nvarchar] (255) NULL ,
	[ID] [float] NULL ,
	[CATEGORY] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Auto_import_old] (
	[RMS_Cust] [nvarchar] (255) NULL ,
	[CIPL_CUST] [nvarchar] (255) NULL ,
	[ID] [float] NULL ,
	[CATEGORY] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Auto_old] (
	[CODE] [nvarchar] (255) NULL ,
	[CIPL_CUST] [nvarchar] (255) NULL ,
	[PAYMENT_METHOD] [nvarchar] (255) NULL ,
	[PROFILE] [nvarchar] (255) NULL ,
	[Category] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Chq] (
	[Vdate] [datetime] NULL ,
	[Deposit] [nvarchar] (255) NULL ,
	[Payment_method] [nvarchar] (255) NULL ,
	[Cheque_no] [nvarchar] (255) NULL ,
	[Gl_Date] [datetime] NULL ,
	[Amount] [float] NULL ,
	[Lockbox] [float] NULL ,
	[Location] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Description] [nvarchar] (255) NULL ,
	[Additional Remark] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Chq_One] (
	[Vdate] [smalldatetime] NULL ,
	[Deposit] [nvarchar] (255) NULL ,
	[Payment_method] [nvarchar] (255) NULL ,
	[CHQ] [nvarchar] (255) NULL ,
	[Cheque_no] [nvarchar] (255) NULL ,
	[Gl_Date] [smalldatetime] NULL ,
	[Amount] [float] NULL ,
	[Lockbox] [float] NULL ,
	[Location] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Description] [nvarchar] (255) NULL ,
	[Additional Remark] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CIPL] (
	[Cipl_Cust] [nvarchar] (255) NULL ,
	[RMS_Cust] [nvarchar] (255) NULL ,
	[Id] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Code] (
	[Customer_Name1] [nvarchar] (255) NULL ,
	[Code1] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Error] (
	[Bill_No] [varchar] (50) NULL ,
	[City_Name] [varchar] (50) NULL ,
	[Company] [varchar] (266) NULL ,
	[Amount] [float] NULL ,
	[DS_No] [varchar] (50) NULL ,
	[Vdate] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Error_List] (
	[Company] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Final_Out] (
	[Branch] [nvarchar] (255) NULL ,
	[company] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Vdate1] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pick_Up] [nvarchar] (255) NULL ,
	[KM_Out] [float] NULL ,
	[KM_IN] [float] NULL ,
	[Reson_Delay] [nvarchar] (255) NULL ,
	[Reson_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [float] NULL ,
	[Remark] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ID_Code] (
	[ID] [float] NULL ,
	[ID1] [float] NULL ,
	[ID2] [float] NULL ,
	[V_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[INV_TYPE] (
	[BRANCH_NAME] [nvarchar] (255) NULL ,
	[INV_TYPE] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[INVCODE] (
	[TRX_NUMBER] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice] (
	[Branch] [nvarchar] (255) NULL ,
	[company] [nvarchar] (255) NULL ,
	[Vdate] [datetime] NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pick_Up] [nvarchar] (255) NULL ,
	[KM_Out] [float] NULL ,
	[KM_IN] [float] NULL ,
	[Reson_Delay] [nvarchar] (255) NULL ,
	[Reson_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[uploaded] [bit] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_18-Jan] (
	[Branch] [nvarchar] (255) NULL ,
	[company] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pick_Up] [nvarchar] (255) NULL ,
	[KM_Out] [float] NULL ,
	[KM_IN] [float] NULL ,
	[Reson_Delay] [nvarchar] (255) NULL ,
	[Reson_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_37] (
	[Branch] [varchar] (200) NULL ,
	[Company1] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pick_Up] [varchar] (255) NULL ,
	[KM_Out] [varchar] (50) NULL ,
	[KM_IN] [varchar] (50) NULL ,
	[Reson_Delay] [varchar] (255) NULL ,
	[Reson_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[SHIP_TO] [varchar] (100) NULL ,
	[LOCATION_CODE] [varchar] (100) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec_B4_Convrtng] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_DEC_Old] (
	[Branch_Bill_To] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pickup] [varchar] (255) NULL ,
	[CITY_NAME] [varchar] (200) NULL ,
	[Remark] [varchar] (200) NULL ,
	[SHIP_TO] [varchar] (200) NULL ,
	[INVOICE_TYPE] [varchar] (200) NULL ,
	[New_Car] [varchar] (50) NULL ,
	[Uploaded] [bit] NULL ,
	[Ship_to1] [varchar] (200) NULL ,
	[Term_Id] [decimal](18, 0) NULL ,
	[Recipt_ID] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec_old_B4_Feb] (
	[Branch_Bill_to] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [float] NULL ,
	[Reason_Cancel] [float] NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [float] NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec_Sale] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [bit] NOT NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_DEC1] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pickup] [varchar] (255) NULL ,
	[CITY_NAME] [varchar] (200) NULL ,
	[Remark] [varchar] (200) NULL ,
	[SHIP_TO] [varchar] (200) NULL ,
	[INVOICE_TYPE] [varchar] (200) NULL ,
	[New_Car] [varchar] (50) NULL ,
	[Uploaded] [bit] NULL ,
	[Ship_to1] [varchar] (200) NULL ,
	[Term_Id] [decimal](18, 0) NULL ,
	[Recipt_ID] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Main] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pickup] [varchar] (255) NULL ,
	[CITY_NAME] [varchar] (200) NULL ,
	[Remark] [varchar] (200) NULL ,
	[SHIP_TO] [varchar] (200) NULL ,
	[LOCATION_CODE] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[KNOCK_prev] (
	[TRX_NUMBER1] [nvarchar] (255) NULL ,
	[TRX_NUMBER] [nvarchar] (255) NULL ,
	[TRX_TYPE] [nvarchar] (255) NULL ,
	[BALANCE_DUE_FUNCTIONAL] [float] NULL ,
	[CUSTOMER_NAME] [nvarchar] (255) NULL ,
	[TRX_DATE] [smalldatetime] NULL ,
	[GL_DATE] [smalldatetime] NULL ,
	[cash_id] [float] NULL ,
	[amount] [float] NULL ,
	[flag] [nvarchar] (255) NULL ,
	[F11] [numeric](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Knock1] (
	[TRX_NUMBER1] [nvarchar] (255) NULL ,
	[TRX_NUMBER] [nvarchar] (255) NULL ,
	[TRX_TYPE] [nvarchar] (255) NULL ,
	[BALANCE_DUE_FUNCTIONAL] [float] NULL ,
	[CUSTOMER_NAME] [nvarchar] (255) NULL ,
	[TRX_DATE] [smalldatetime] NULL ,
	[TRX_GL_DATE] [smalldatetime] NULL ,
	[cash_id] [float] NULL ,
	[amount] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[NEW_CAR] (
	[Branch] [nvarchar] (255) NULL ,
	[Veh_No] [nvarchar] (255) NULL ,
	[Car_No] [nvarchar] (255) NULL ,
	[code] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Payment] (
	[Payment_Method] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Pending] (
	[Inv_No] [varchar] (500) NULL ,
	[Company] [varchar] (100) NULL ,
	[Branch] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Product] (
	[FLEX_VALUE] [varchar] (53) NULL ,
	[PRODUCT] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ship_to] (
	[City] [varchar] (222) NULL ,
	[State] [varchar] (100) NULL ,
	[Postal_Code] [varchar] (100) NULL ,
	[Country] [varchar] (50) NULL ,
	[Address1] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally_MIS] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally_MIS_IIIrdOld] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [varchar] (50) NULL ,
	[km_in] [varchar] (50) NULL ,
	[Reason_Dealy] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[vat] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally_MIS_IInd] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [varchar] (50) NULL ,
	[km_in] [varchar] (50) NULL ,
	[Reason_Dealy] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pickup] [varchar] (255) NULL 
) ON [PRIMARY]
GO

