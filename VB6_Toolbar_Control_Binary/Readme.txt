***********************************************************************
vbAccelerator VB6 CoolMenu Toolbar and Rebar Control 
Copyright � 1998-2002 Steve McMahon (steve@vbaccelerator.com)
-----------------------------------------------------------------------
      Visit vbAccelerator - the VB programmer's resource - at
	             http://vbaccelerator.com
***********************************************************************

About the vbAccelerator CoolMenu Toolbar and Rebar Control
----------------------------------------------------------
This control provides a small and light (80k) all VB replacement for the 
toolbar control in COMCTL32.OCX.  It provides all the features required
to implement IE4/IE5 style toolbars.

Some of the great features of this implementation are:
1) Toolbars
-----------
a) CoolMenu support in toolbars.
b) Swap between List Style and Standard toolbar at runtime.
c) Hide text for all or selected buttons.
d) Toolbars support hot and disabled image lists as well as the standard
   type.  This allows you to set up different images to display when the
   mouse is over a button.
e) Toolbar buttons can autosize to fit the supplied text.
f) Dropdown toolbar buttons are fully supported.
g) Toolbar Buttons can be made invisible.
h) Smoother and more efficient methods to enable, change image and press
   buttons that the VB implementation.
i) Provides access to the default system image lists.  Get all the 
   standard toolbar button images for free!

2) Rebars
---------
a) Allows any type of control to be hosted
b) Chevron support - if the rebar band is too narrow, a chevron will appear
c) Rebar bands can be hidden (provided you have COMMCTRL.DLL v4.71 or above)
d) A background bitmap can be shown behind the control.  Toolbars added
   to the Rebar are transparent and the bitmap shows through.
e) Specify minimum band widths in the rebar
f) Automatically mimimise the arrangement of bands to fit a specified 
   area.

Any bugs or problems should be reported to the author 
(steve@vbaccelerator.com) for incorporation into future releases.

Installation Requirements
-------------------------
vbalTbar6 requires Visual Basic 6 with at least Service Pack 3 applied
and SSubTmr.DLL (available from http://vbaccelerator.com/)

**********************************************************************
Distribution notice:
You are free to distribute this zip in it's original state to any
public WWW site, online service or BBS without explicitly obtaining
the authors permission. (Notification would be greatly appreciated
though!).
You are also free to use and distribute the compiled vbalTbar.ocx file, 
provided it is unmodified from the version supplied in this package.

If you wish to distribute vbaltbar.zip by any other means (i.e. if 
you want to include it on a CD or any other software media) then the
EXPRESS PERMISSION of the author is REQUIRED.
***********************************************************************
