VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form revs 
   Caption         =   "Reveue Sharing"
   ClientHeight    =   8490
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15045
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form10"
   MDIChild        =   -1  'True
   ScaleHeight     =   8490
   ScaleWidth      =   15045
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Date"
      Height          =   495
      Left            =   4680
      TabIndex        =   8
      Top             =   0
      Width           =   2415
      Begin VB.OptionButton Option1 
         Caption         =   "Single"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Between"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1200
         TabIndex        =   9
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.CheckBox Check1 
      Caption         =   "&All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   4200
      TabIndex        =   2
      Top             =   1200
      Width           =   615
   End
   Begin VB.CommandButton Command3 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   375
      Left            =   8400
      TabIndex        =   5
      Top             =   960
      Width           =   1095
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Print"
      Height          =   375
      Left            =   8400
      TabIndex        =   4
      Top             =   600
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Preview"
      Default         =   -1  'True
      Height          =   375
      Left            =   8400
      TabIndex        =   3
      Top             =   240
      Width           =   1095
   End
   Begin VB.ComboBox Combo1 
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   960
      TabIndex        =   1
      Top             =   720
      Width           =   3735
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   0
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5295
      Left            =   0
      TabIndex        =   19
      Top             =   2280
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   9340
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   11786751
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   15
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Car No"
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "DATE"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "DS No."
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Category Alloted"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Revenue Sharing"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Category Booked"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Category Alloted"
         Object.Width           =   3351
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Time Out"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "Time In"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Bill Amount"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Parking/Toll"
         Object.Width           =   2205
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "S/Tax/DST/Cess"
         Object.Width           =   2470
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Car Booked"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Height          =   855
      Index           =   0
      Left            =   4680
      TabIndex        =   11
      Top             =   480
      Width           =   3615
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   325
         Left            =   840
         TabIndex        =   12
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CalendarForeColor=   0
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   20185091
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Date"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   13
         Top             =   240
         Width           =   345
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Height          =   855
      Index           =   1
      Left            =   4680
      TabIndex        =   14
      Top             =   480
      Width           =   3495
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   600
         TabIndex        =   15
         Top             =   255
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   20185091
         CurrentDate     =   37865
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   330
         Left            =   2160
         TabIndex        =   16
         Top             =   250
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   20185091
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "From"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   360
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "To"
         Height          =   195
         Index           =   2
         Left            =   1900
         TabIndex        =   17
         Top             =   300
         Width           =   180
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Click on the column to sort "
      ForeColor       =   &H80000010&
      Height          =   210
      Index           =   2
      Left            =   120
      TabIndex        =   7
      Top             =   1920
      Width           =   2055
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Reveue Sharing"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000011&
      Height          =   270
      Index           =   12
      Left            =   120
      TabIndex        =   6
      Top             =   60
      Width           =   1770
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vendor"
      Height          =   210
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   720
      Width           =   540
   End
End
Attribute VB_Name = "revs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim comprec3 As ADODB.Recordset
Dim per_sh As Double

Private Sub Command1_Click()
Dim comprec15 As ADODB.Recordset
ListView1.ListItems.Clear
'Set comprec = con1.Execute("delete from vCarRep")
Set repcomprec = New ADODB.Recordset
   strcmd = "select * from h_unit"
   Set NEWcomprec1 = con.Execute(strcmd)
   If Not NEWcomprec1.EOF Then
    CITY_NAME = NEWcomprec1!city & ""
   Else
    CITY_NAME = ""
   End If
If Check1.Value = 0 Then
If Option1.Value = True Then
    strcmd = "select * from APINTERFACE WHERE Date_in=Convert(DateTime, '" & DTPicker1.Value & "', 103)"
    strcmd = strcmd & " AND vendor='" & Combo1.Text & "'"
    strcmd = strcmd & " and invoice =1"
    strcmd = strcmd & " and void_duty=0"
    strcmd = strcmd & " and Owned=0"
    strcmd = strcmd & " order by Date_in"
    str1 = DTPicker1.Value
End If

If Option2.Value = True Then
    strcmd = "select * from APINTERFACE where Date_in >=Convert(DateTime, '" & DTPicker2.Value & "', 103)"
    strcmd = strcmd & " AND Date_in <=Convert(DateTime, '" & DTPicker3.Value & "', 103)"
    strcmd = strcmd & " AND vendor='" & Combo1.Text & "'"
    strcmd = strcmd & " and invoice =1"
    strcmd = strcmd & " and Owned=0"
    strcmd = strcmd & " and void_duty=0"
    strcmd = strcmd & " order by Date_in"
    str1 = "From " & DTPicker2.Value & " To " & DTPicker3.Value
End If
End If

If Check1.Value = 1 Then
If Option1.Value = True Then
    strcmd = "select * from APINTERFACE WHERE Date_in =Convert(DateTime, '" & DTPicker1.Value & "', 103)"
    strcmd = strcmd & " and invoice =1"
    strcmd = strcmd & " and void_duty=0"
    strcmd = strcmd & " and Owned=0"
    strcmd = strcmd & " order by Date_in"
    str1 = DTPicker1.Value
End If

If Option2.Value = True Then
    strcmd = "select * from APINTERFACE where Date_in >=Convert(DateTime, '" & DTPicker2.Value & "', 103)"
    strcmd = strcmd & " AND Date_in <=Convert(DateTime, '" & DTPicker3.Value & "', 103)"
    strcmd = strcmd & " and invoice =1"
    strcmd = strcmd & " and void_duty=0"
    strcmd = strcmd & " and Owned=0"
    strcmd = strcmd & " order by Date_in"
    str1 = "From " & DTPicker2.Value & " To " & DTPicker3.Value
End If
End If

Set comprec1 = con.Execute(strcmd)
If Not comprec1.EOF Then
    DoEvents
'    Status.Show
    DoEvents
    repcomprec.Open "vCarrep", con1, adOpenKeyset, adLockPessimistic
    Do While Not comprec1.EOF
 '           ListView1.Refresh
            Set lt = ListView1.ListItems.Add(, , comprec1!vehicle_no & " " & comprec1!Vendor)
            lt.SubItems(1) = comprec1!Date_out & ""
            lt.SubItems(2) = comprec1!Physical_Duty & ""
            FUEL1 = comprec1!Fuel
            If IsNull(fue11) = True Then
                FUEL1 = 0
            End If
            If IsNull(dst) = True Then
                dst = 0
            End If
            If IsNull(CESS) = True Then
                CESS = 0
            End If
            a = comprec1!package_Amount
            b = comprec1!extra_Km_Amount
            C = comprec1!EXTRA_HRS_AMOUNT
            d = comprec1!COMPANY_DISCOUNT
            E = 0 'comprec1!SPL_DISCOUNT
            f = comprec1!PARKING_TAX
            g = comprec1!Others
            k = Val(comprec1!Qutstation_Allow) + Val(comprec1!night_billed_amount)
            i = (a + b + C + k - d - E)
            lt.SubItems(9) = Round(i, 2)
            lt.SubItems(10) = Round((f + g), 2)
           ' TAX_AMT = comprec1!service_Tax
            service_Tax = Round(((i + g + f) * TAX_AMT) / 100, 2)
            lt.SubItems(11) = service_Tax + dst + CESS
            GROSS = i + f + g + service_Tax + dst + CESS
            lt.SubItems(12) = Round(GROSS, 2)
            lt.SubItems(13) = comprec1!Car_Type
            lt.SubItems(5) = comprec1!car_category
            lt.SubItems(6) = comprec1!Category
            
            per_sh = 0
            strcmd = "select * from revenue where branch='" & CITY_NAME & "'"
            strcmd = strcmd & " and category='" & comprec1!car_category & "'"
            strcmd = strcmd & " and VENDOR='" & Combo4.Text & "'"
            Set comprec2 = con1.Execute(strcmd)
            If Not comprec2.EOF Then
                 per_sh = comprec2!S_aMOUNT
                 lt.SubItems(4) = per_sh
            Else
                per_sh = 0
                lt.SubItems(4) = 0
            End If
            
            repcomprec.AddNew
            repcomprec!Ds_No = comprec1!Physical_Duty & ""
            repcomprec!VDate = comprec1!Date_out & ""
            repcomprec!S_Amt = per_sh & ""
            repcomprec!Category = comprec1!car_category & ""
            repcomprec!vehicle_no = comprec1!vehicle_no & " "
            repcomprec!Vendor = comprec1!Vendor & ""
            repcomprec!basic = Round(a, 2)
            repcomprec!Parking = Round(f + g, 2)
            repcomprec!Extra_KM = C + b + FUEL1
            repcomprec!Outstation = Round(k, 2)
            repcomprec!Strlbl = str1 & ""
            revamt = a + C + b + FUEL1 + k
            revamt = (revamt * per_sh / 100)
            repcomprec!Rev_Amt = Round(revamt, 0)
            repcomprec!branch = CITY_NAME & ""
            
            
            a = 0
            b = 0
            C = 0
            d = 0
            E = 0
            f = 0
            g = 0
            i = 0
            k = 0
            per_sh = 0
            service_Tax = 0
            GROSS = 0
            CESS = 0
            dst = 0
            comprec1.MoveNext
    Loop
            repcomprec.UpdateBatch
'Unload Status
Else
        MsgBox "No Transaction is there is the Database !", vbCritical, "Error"
End If
'Unload Status
End Sub

Private Sub Command2_Click()
'If UCase(globeuser1) = "ADMIN" Then
    cryctrl.WindowTitle = "Vendor Wise Report"
    cryctrl.ReportFileName = App.Path & "\reportS\Rev_Sha.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
'Else
'MsgBox "Please Logged As Admin", vbCritical, "Error"
'Exit Sub
'End If
End Sub

Private Sub Command3_Click()
Unload Me
End Sub

Private Sub Form_Load()
'adjust Me

'Me.Width = Screen.Width
'Me.Left = frmmain.Picture1.Width
'Me.Height = Screen.Height - 100
'Me.Top = frmmain.tb1.Height - 200
ListView1.Left = 0
ListView1.Height = Screen.Height - ListView1.Top - 1500
ListView1.Refresh
ListView1.Width = Me.Width ' - frmmain.Picture1.Width
addcomp
DTPicker1.Value = Format(Date, "dd-mm-yyyy")
DTPicker2.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker3.Value = Format(Date, "dd-mm-yyyy")
'Sorting a Listview Control
'Set m_cHdrIcons.ListView = ListView1
'Call m_cHdrIcons.SetHeaderIcons(0, soAscending)
End Sub


Public Sub addcomp()
Combo1.Clear
strcmd = "select Name from H_Vendor group by Name order by Name "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!Name & ""
        comprec.MoveNext
    Loop
End If
Combo1.Text = Combo1.List(0)
End Sub



'Sorting The Listview
Private Sub ListView1_ColumnClick(ByVal ColumnHeader As ColumnHeader)
  Dim i As Integer
  ' Toggle the clicked column's sort order only if the active colum is clicked
  ' (iow, don't reverse the sort order when different columns are clicked).
  If (ListView1.SortKey = ColumnHeader.Index - 1) Then
    ColumnHeader.Tag = Not Val(ColumnHeader.Tag)
  End If
  ' Set sort order to that of the respective ListSortOrderConstants value
  ListView1.SortOrder = Abs(Val(ColumnHeader.Tag))
  ' Get the zero-based index of the clicked column.
  ' (ColumnHeader.Index is one-based).
  ListView1.SortKey = ColumnHeader.Index - 1
  ' Sort the ListView
  ListView1.Sorted = True
  ' Set the header icons
'  Call m_cHdrIcons.SetHeaderIcons(ListView1.SortKey, ListView1.SortOrder)
End Sub


Private Sub Option1_Click()
If Option1.Value = True Then
    Frame2(0).ZOrder 0
Else
    Frame2(0).ZOrder 1
End If
End Sub

Private Sub Option2_Click()
If Option2.Value = True Then
    Frame2(1).ZOrder 0
Else
    Frame2(1).ZOrder 1
End If
End Sub
