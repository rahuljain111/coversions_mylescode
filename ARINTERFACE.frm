VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form3 
   Caption         =   "Auto Invoice Import System"
   ClientHeight    =   8490
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   8565
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   8565
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command5 
      Caption         =   "Update Invoices"
      Height          =   495
      Left            =   4440
      TabIndex        =   36
      Top             =   7680
      Width           =   1575
   End
   Begin VB.ComboBox Combo6 
      BackColor       =   &H00C0FFFF&
      Height          =   330
      Left            =   6960
      TabIndex        =   35
      Top             =   3000
      Width           =   975
   End
   Begin VB.ComboBox Combo5 
      BackColor       =   &H00C0FFFF&
      Height          =   330
      Left            =   5880
      TabIndex        =   34
      Top             =   3000
      Width           =   975
   End
   Begin VB.ComboBox Combo4 
      BackColor       =   &H00C0FFFF&
      Height          =   330
      Left            =   1920
      TabIndex        =   30
      Top             =   4320
      Width           =   2535
   End
   Begin VB.ComboBox Combo3 
      BackColor       =   &H00C0FFFF&
      Height          =   330
      Left            =   1920
      TabIndex        =   29
      Top             =   3840
      Width           =   2535
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Purge All Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   210
      Left            =   5160
      TabIndex        =   27
      Top             =   360
      Width           =   1815
   End
   Begin VB.ComboBox Combo2 
      Height          =   330
      Left            =   1920
      TabIndex        =   26
      Top             =   3120
      Width           =   2535
   End
   Begin VB.CommandButton Command4 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   495
      Left            =   6000
      TabIndex        =   23
      Top             =   7680
      Width           =   1575
   End
   Begin VB.ComboBox Combo1 
      Height          =   330
      Left            =   1920
      TabIndex        =   21
      Top             =   1920
      Width           =   2535
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Count Total Records"
      Height          =   495
      Left            =   2520
      TabIndex        =   18
      Top             =   7680
      Width           =   1935
   End
   Begin VB.ListBox List2 
      Height          =   900
      Left            =   -1440
      TabIndex        =   17
      Top             =   3360
      Width           =   975
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   16
      Top             =   8115
      Width           =   8565
      _ExtentX        =   15108
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14579
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Skinner1 
      Height          =   480
      Left            =   -1080
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   19
      Top             =   2400
      Width           =   1200
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Count"
      Height          =   375
      Left            =   9120
      TabIndex        =   15
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.ListBox List1 
      Height          =   1320
      Left            =   7440
      TabIndex        =   14
      Top             =   240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Timer Timer1 
      Left            =   4680
      Top             =   3960
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1200
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   2415
      Begin VB.OptionButton Option2 
         Caption         =   "Between"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Single"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Upload AR Data"
      Height          =   495
      Left            =   480
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   7680
      Width           =   2055
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   1095
      Left            =   -2160
      TabIndex        =   12
      Top             =   2640
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   1931
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   11786751
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   35
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   24
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   25
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   26
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   27
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   28
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(30) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   29
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(31) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   30
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(32) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   31
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(33) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   32
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(34) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   33
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(35) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   34
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Index           =   1
      Left            =   1200
      TabIndex        =   7
      Top             =   600
      Width           =   3495
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   600
         TabIndex        =   8
         Top             =   255
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   56885251
         CurrentDate     =   37865
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   330
         Left            =   2160
         TabIndex        =   9
         Top             =   250
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   56885251
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   1900
         TabIndex        =   11
         Top             =   300
         Width           =   180
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Top             =   300
         Width           =   360
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Index           =   0
      Left            =   1200
      TabIndex        =   4
      Top             =   600
      Visible         =   0   'False
      Width           =   3615
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   325
         Left            =   840
         TabIndex        =   5
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CalendarForeColor=   0
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   56885251
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   345
      End
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "End"
      Height          =   210
      Index           =   2
      Left            =   7200
      TabIndex        =   33
      Top             =   2760
      Width           =   270
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Start"
      Height          =   210
      Index           =   1
      Left            =   6000
      TabIndex        =   32
      Top             =   2760
      Width           =   345
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Remark"
      Height          =   210
      Index           =   0
      Left            =   240
      TabIndex        =   31
      Top             =   4320
      Width           =   540
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Batches in Apps"
      Height          =   210
      Left            =   240
      TabIndex        =   28
      Top             =   3840
      Width           =   1200
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Batch Source Name"
      Height          =   210
      Left            =   240
      TabIndex        =   25
      Top             =   3120
      Width           =   1440
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   225
      Left            =   3840
      TabIndex        =   24
      Top             =   7200
      Width           =   420
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Branch"
      Height          =   210
      Left            =   1320
      TabIndex        =   22
      Top             =   1920
      Width           =   525
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   255
      Left            =   1920
      TabIndex        =   20
      Top             =   2520
      Width           =   1335
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Height          =   210
      Left            =   1920
      TabIndex        =   13
      Top             =   3600
      Width           =   45
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim ctr As Variant
Dim new_car As Variant

Private Sub Command1_Click()
If Check1.Value = 1 Then
    Command5_Click
    StatusBar1.Panels(1).Text = "Purging Old data From System ------------------!"
    STRCMD = "delete from RA_INTERFACE_LINES_ALL" ' where interface_status='" & "P" & "'"
    Set comprec = con.Execute(STRCMD)
    Set comprec = con.Execute("Delete from RA_INTERFACE_DISTRIBUTIONS_ALL")
    StatusBar1.Panels(1).Text = "Data Purged------------------!"
End If

STRCMD = "select * from ra_interface_lines_all where batch_source_name='" & Combo2.Text & "'"
Set comprec = con.Execute(STRCMD)
If Not comprec.EOF Then
    MsgBox "Batch Source Already in Used ! Purge All The Previous data to Use The Batch Source ", vbInformation, "Batch Source Name"
    Exit Sub
End If
        STRCMD = "select count(*) as net from Invoice_dec " 'where invoice_no ='" & "011-DS00031704-CHK" & "'"
        'strcmd = strcmd & " WHERE invoice_no in(select distinct trx_number from invimp where UPLOADED =0)"
        'STRCMD = STRCMD & "  where company  IN(SELECT company FROM Error_List)"
        'strcmd = strcmd & "   remark='" & Combo4.Text & "'"
        STRCMD = STRCMD & " WHERE id >='" & Combo5.Text & "'"
        STRCMD = STRCMD & " AND id <='" & Combo6.Text & "'"
        STRCMD = STRCMD & " and uploaded=0"
        'STRCMD = STRCMD & "  AND PRODUCT ='" & "Self Drive Spot" & "'"
        'STRCMD = STRCMD & "  AND CITY_NAME='" & Combo1.Text & "'"
        'STRCMD = STRCMD & "  AND service_tax>=0"
        'STRCMD = STRCMD & "  AND service_tax<=10"
        'STRCMD = STRCMD & "  AND vehicle_no='" & "9351" & "'"
        
        Set comprec = con1.Execute(STRCMD)
        If Not comprec.EOF Then
            MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
        End If
add_data
End Sub

Public Sub add_data()
Dim repcomprec As ADODB.Recordset
Dim repcomprec1 As ADODB.Recordset
C = 1
Dim comprec15 As ADODB.Recordset
ListView1.ListItems.Clear


Set repcomprec = New ADODB.Recordset
Set repcomprec1 = New ADODB.Recordset
countctr = 1
inv_ctr = 1
STRCMD = "select * from ID_Code "
Set comprec = con1.Execute(STRCMD)
ctr = comprec!ID '9909720 x
    STRCMD = "select * from Invoice_dec " 'WHERE invoice_no ='" & "011-DS00031704-CHK" & "'"
    'strcmd = strcmd & "  WHERE invoice_no in(select distinct trx_number from invimp where UPLOADED =0)"
    STRCMD = STRCMD & " where id >='" & Combo5.Text & "'"
    STRCMD = STRCMD & " AND id <='" & Combo6.Text & "'"
    STRCMD = STRCMD & " and uploaded=0"
    'STRCMD = STRCMD & "  AND PRODUCT ='" & "Self Drive Spot" & "'"
    'STRCMD = STRCMD & "  AND CITY_NAME='" & Combo1.Text & "'"
    'strcmd = strcmd & "  AND REMARK ='" & "Uploaded on Current Date 16-05-2006" & "'"
    'STRCMD = STRCMD & "  AND service_tax>=0"
    'STRCMD = STRCMD & "  AND service_tax<=10"
    'STRCMD = STRCMD & "  AND vehicle_no='" & "9351" & "'"
    
Set comprec15 = con1.Execute(STRCMD)
If Not comprec15.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "RA_INTERFACE_LINES_ALL", con, adOpenKeyset, adLockPessimistic
    Set repcomprec2 = New ADODB.Recordset
    repcomprec2.Open "RA_INTERFACE_DISTRIBUTIONS_ALL", con, adOpenKeyset, adLockPessimistic
    Do While Not comprec15.EOF
            ctr = ctr + 1
            DoEvents
            status.Show
            DoEvents
            
            'Code To Map Car No
            If comprec15!owned = True Then
                STRCMD = "select * from new_car where vehicle_no='" & comprec15!vehicle_no & "'"
                STRCMD = STRCMD & " AND CITY='" & comprec15!CITY_NAME & "'"
                Set newcomprec = con1.Execute(STRCMD)
                If Not newcomprec.EOF Then
                    new_car = newcomprec!car_no
                Else
                    new_car = 0
                End If
            End If
            
            If comprec15!owned = False Then
                new_car = "Vendor Cateogory"
            End If
            
'            new_car = "Common"
            
            STRCMD = "select distinct(id) from auto_Import where RMS_Cust='" & comprec15!company & "'"
            DoEvents
            StatusBar1.Panels(1).Text = ctr
            Label3.Caption = "Total :" & inv_ctr
            DoEvents
            Set comprec1 = con1.Execute(STRCMD)
             If Not comprec1.EOF Then
                repcomprec1.AddNew
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "LINE"
                repcomprec1!Description = "Standard Package"
                repcomprec1!AMOUNT = comprec15!basic
                repcomprec1!Term_Id = comprec15!Term_Id    'For 30 Net
                repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!ID
                repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!ID  'comprec15!SHIP_TO & "-" & comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!ID
                repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                repcomprec1!TRX_DATE = comprec15!VDATE
                repcomprec1!GL_DATE = comprec15!VDATE 'CDate("01-12-2005")
                repcomprec1!QUANTITY = 1
                repcomprec1!UNIT_SELLING_PRICE = comprec15!basic
                repcomprec1!UNIT_STANDARD_PRICE = comprec15!basic
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!UOM_CODE = "EA"
                repcomprec1!UOM_NAME = "Each"
                repcomprec1!ORG_ID = 82
                repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                repcomprec1!HEADER_ATTRIBUTE6 = Trim(comprec15!Car_Used_Date)
                repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                If comprec15!Outstation_Amt > 0 Then
                    repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                Else
                    repcomprec1!HEADER_ATTRIBUTE14 = "No"
                End If
                
                If comprec15!direct = True Then
                    repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                Else
                    repcomprec1!HEADER_ATTRIBUTE15 = "No"
                End If
                
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                
                '*****************************************
                'Program Batch Name to be change for different batches
                repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                '*****************************************
                
                repcomprec1!TRX_NUMBER = Left(comprec15!invoice_no, 20) '-& "-RR"
                repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                repcomprec1!SET_OF_BOOKS_ID = 2
                repcomprec1!CURRENCY_CODE = "INR"
                
                '*****************************************
                'Program Invoice Name to be change for different branches
                STRCMD = "select * from inv_type where branch_Name='" & comprec15!branch_bill_to & "'"
                Set comprec7 = con1.Execute(STRCMD)
                If Not comprec7.EOF Then
                    Cipl_Type = comprec7!inv_type
                Else
                    Cipl_Type = ""
                End If
                repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                
                '*****************************************
                
                repcomprec1.Update
                
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008074"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec15!Ship_To1 & "'"
                Set comprec3 = con.Execute(strcmd2)
                       
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008075"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND FVTB.Description='" & new_car & "'"
                Set comprec4 = con.Execute(strcmd2)
            
               
               '*&***************************************
                'if product is drive from the system
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008077"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec15!Product & "'"
                Set comprec5 = con.Execute(strcmd2)
                '**************************************************
                'Distribution Table to Be insert Here
                    
                
                repcomprec2.AddNew
                'repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "REV"
                repcomprec2!AMOUNT = comprec15!basic
                repcomprec2!Percent = 100
                
                If Not comprec3.EOF Then
                    repcomprec2!SEGMENT1 = comprec3!flex_valuE  'Location Flexfield
                End If
                
                If Not comprec4.EOF Then
                    repcomprec2!SEGMENT2 = comprec4!flex_valuE      'Car Number Flexfield
                End If
                
                If Not comprec5.EOF Then
                    repcomprec2!SEGMENT4 = comprec5!flex_valuE    'Product
                End If
                 
                
                If comprec15!owned = True Then
                    repcomprec2!segment3 = "30001"  'Natural Acct For Revenue Owned Cars
                Else
                    repcomprec2!segment3 = "30002"  'Natural Acct For Revenue Hired Cars
                End If
                
                
                repcomprec2!ORG_ID = 82   'Org Id
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2.Update
                
                '-Distribution Line For Receivale Entry
                repcomprec2.AddNew
                'repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "REC"
                repcomprec2!AMOUNT = comprec15!basic
                repcomprec2!Percent = 100
                
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008074"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec15!branch_bill_to & "'"
                Set comprec6 = con.Execute(strcmd2)
                
                If Not comprec6.EOF Then
                    repcomprec2!SEGMENT1 = comprec6!flex_valuE  'Location Flexfield
                End If
                
                If Not comprec4.EOF Then
                     repcomprec2!SEGMENT2 = comprec4!flex_valuE      'Car Number Flexfield
                End If
                
                If Not comprec5.EOF Then
                    repcomprec2!SEGMENT4 = comprec5!flex_valuE    'Product
                End If
                 
                
                repcomprec2!segment3 = "13001"  'Natural Acct For Revenue Owned Cars
                repcomprec2!ORG_ID = 82   'Org Id
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2.Update
                ctr = ctr + 1
'                Else
 '                   List2.AddItem comprec17!Company
  '              End If
                
                
                '**************************NOT INCLUDEDE IN OUTSTANDING*********************
                
                '--------Line2-------------------------------------------------------
                If (comprec15!Parking_Toll + comprec15!other_taxes) > 0 Then
               ' If countctr = 0 Then
                repcomprec1.AddNew
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "LINE"
                repcomprec1!Description = "Parking & Toll Charges"
                repcomprec1!AMOUNT = (comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec1!Term_Id = comprec15!Term_Id
                repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!ID
                repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!ID  'comprec15!SHIP_TO & "-" & comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!ID
                repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                repcomprec1!TRX_DATE = comprec15!VDATE
                repcomprec1!GL_DATE = comprec15!VDATE 'CDate("01-12-2005")
                repcomprec1!QUANTITY = 1
                repcomprec1!UNIT_SELLING_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec1!UNIT_STANDARD_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec1!ATTRIBUTE1 = ctr - 1
                repcomprec1!UOM_CODE = "EA"
                repcomprec1!UOM_NAME = "Each"
                repcomprec1!ORG_ID = 82
                repcomprec1!TRX_NUMBER = Left(comprec15!invoice_no, 20) '& "-RR"
                repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                repcomprec1!HEADER_ATTRIBUTE6 = comprec15!Car_Used_Date
                repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                
                If comprec15!Outstation_Amt > 0 Then
                    repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                Else
                    repcomprec1!HEADER_ATTRIBUTE14 = "No"
                End If
                
                If comprec15!direct = True Then
                    repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                Else
                    repcomprec1!HEADER_ATTRIBUTE15 = "No"
                End If
                
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                repcomprec1!SET_OF_BOOKS_ID = 2
                repcomprec1!CURRENCY_CODE = "INR"
                repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                
                p_Ctr = ctr
                repcomprec1.Update
                
                '----------------For Distribution
                repcomprec2.AddNew
                'repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "REV"
                repcomprec2!AMOUNT = (comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec2!Percent = 100
                
                If Not comprec3.EOF Then
                    repcomprec2!SEGMENT1 = comprec3!flex_valuE  'Location Flexfield
                End If
                
                If Not comprec4.EOF Then
                    repcomprec2!SEGMENT2 = comprec4!flex_valuE      'Car Number Flexfield
                End If
                
                If Not comprec5.EOF Then
                    repcomprec2!SEGMENT4 = comprec5!flex_valuE
                End If
                repcomprec2!ORG_ID = 82   'Org Id
                repcomprec2!segment3 = "40301"    'Natural Acct For Car Parking
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2.Update
                End If
                
                '---------------------------fOR TAX LINE---------------------------------------
                If comprec15!STAX <> 0 Then
                ctr = ctr + 1
                repcomprec1.AddNew
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "TAX"
                repcomprec1!Description = "Tax line"
                
                '-----------------System Will Pickup the Tax Value from System
                repcomprec1!AMOUNT = Round((comprec15!basic * comprec15!Service_Tax) / 100, 2)
                '--------------------------------------------------------------
                
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                repcomprec1!TRX_DATE = comprec15!VDATE
                
                '-----------------System Will Pickup the Tax Value from System
                repcomprec1!TAX_RATE = comprec15!Service_Tax
                '--------------------------------------------------------------
                
                '-----------------System Will Pickup the Tax Value from System
                repcomprec1!TAX_CODE = "Service Tax @ " & comprec15!Service_Tax & "%"
                '------------------------------------------------------------
                
                repcomprec1!TAX_PRECEDENCE = 0
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!ORG_ID = 82
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                repcomprec1!SET_OF_BOOKS_ID = 2
                repcomprec1!CURRENCY_CODE = "INR"
                repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1.Update
                
                '----------------For Distribution
                repcomprec2.AddNew
                'repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "TAX"
                
                '---System Will Pickup the Tax Value from System
                repcomprec2!AMOUNT = Round((comprec15!basic * comprec15!Service_Tax) / 100, 2)
                '-----------------------------------------------
                
                repcomprec2!Percent = 100
'                repcomprec2!CODE_COMBINATION_ID = 1153
                repcomprec2!SEGMENT1 = "000"
                repcomprec2!SEGMENT2 = "00000"
                repcomprec2!segment3 = "25004"
                repcomprec2!SEGMENT4 = "00"
                repcomprec2!ORG_ID = 82
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2.Update
               '--------------------------------fOR cESS cALCULATION---------------------------
                ctr = ctr + 1
                repcomprec1.AddNew
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "TAX"
                repcomprec1!Description = "Tax line"
                
                '------------System Will Pickup the Tax Value from System
                repcomprec1!AMOUNT = Round((comprec15!basic * comprec15!cess_Tax) / 100, 2)
                '----------------------------------------------------------
                
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                repcomprec1!TRX_DATE = comprec15!VDATE
                
                '-------System Will Pickup the Tax Value from System
                repcomprec1!TAX_RATE = 2
                
                repcomprec1!TAX_CODE = "Service Tax Education Cess @ 2%"
                '------------------------------------------
                repcomprec1!TAX_PRECEDENCE = 1
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!ORG_ID = 82
                
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text)
                repcomprec1!SET_OF_BOOKS_ID = 2
                repcomprec1!CURRENCY_CODE = "INR"
                repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1.Update
                '----------------For Distribution
                repcomprec2.AddNew
                'repcomprec2!INTERFACE_LINE_ID = ctr
                
                repcomprec2!ACCOUNT_CLASS = "TAX"
                
                '------System Will Pickup the Tax Value from System
                repcomprec2!AMOUNT = Round((comprec15!basic * comprec15!cess_Tax) / 100, 2)
                '------------------------------------------
                repcomprec2!Percent = 100
'                repcomprec2!CODE_COMBINATION_ID = 1153
                repcomprec2!SEGMENT1 = "000"
                repcomprec2!SEGMENT2 = "00000"
                repcomprec2!segment3 = "25004"
                repcomprec2!SEGMENT4 = "00"
                repcomprec2!ORG_ID = 82
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2.Update
                End If
               '--------------------------------fOR vat cALCULATION---------------------------
            
             If comprec15!CITY_NAME = "DELHI" And comprec15!VAT > 0 Then
             ctr = ctr + 1
             repcomprec1.AddNew
             'repcomprec1!INTERFACE_LINE_ID = ctr
             repcomprec1!LINE_TYPE = "TAX"
             repcomprec1!Description = "Tax line"
             repcomprec1!AMOUNT = Round((comprec15!basic * 12.5) / 100, 2)
             repcomprec1!CONVERSION_TYPE = "User"
             repcomprec1!CONVERSION_RATE = 1
             repcomprec1!TRX_DATE = comprec15!VDATE
             repcomprec1!TAX_RATE = 12.5
             repcomprec1!TAX_CODE = "VAT @ 12.5%"
             repcomprec1!TAX_PRECEDENCE = 0
             repcomprec1!ATTRIBUTE1 = ctr
             repcomprec1!ORG_ID = 82
             repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
             repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
             repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
             repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
             repcomprec1!SET_OF_BOOKS_ID = 2
             repcomprec1!CURRENCY_CODE = "INR"
             repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
             repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
             repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
             repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
             repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
             repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
             repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
             repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
             repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
             repcomprec1.Update
                '----------------For Distribution
             repcomprec2.AddNew
             'repcomprec2!INTERFACE_LINE_ID = ctr
             repcomprec2!ACCOUNT_CLASS = "TAX"
             repcomprec2!AMOUNT = Round((comprec15!basic * 12.5) / 100, 2)
             repcomprec2!Percent = 100
             'repcomprec2!CODE_COMBINATION_ID = 1157
             repcomprec2!SEGMENT1 = "000"
             repcomprec2!SEGMENT2 = "00000"
             repcomprec2!segment3 = "25007"
             repcomprec2!SEGMENT4 = "00"
             repcomprec2!ORG_ID = 82
             repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
             repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
             repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
             repcomprec2.Update
            End If
           
            If (comprec15!Parking_Toll + comprec15!other_taxes) > 0 And comprec15!STAX <> 0 Then
            
                ' ---------------------------Tax on Parking
                 ctr = ctr + 1
                 repcomprec1.AddNew
                 'repcomprec1!INTERFACE_LINE_ID = ctr
                 repcomprec1!LINE_TYPE = "TAX"
                 repcomprec1!Description = "Tax line"
                 repcomprec1!AMOUNT = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!Service_Tax) / 100, 2)
                 repcomprec1!CONVERSION_TYPE = "User"
                 repcomprec1!CONVERSION_RATE = 1
                 repcomprec1!TRX_DATE = comprec15!VDATE
                 
                 '------------System Will Pickup the Tax Value from System
                 repcomprec1!TAX_RATE = comprec15!Service_Tax
                 repcomprec1!TAX_CODE = "Service Tax @ " & comprec15!Service_Tax & "%"
                 '--------------------------------------------------------
                 repcomprec1!TAX_PRECEDENCE = 0
                 repcomprec1!ATTRIBUTE1 = ctr
                 repcomprec1!ORG_ID = 82
                 repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                 repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                 repcomprec1!SET_OF_BOOKS_ID = 2
                 repcomprec1!CURRENCY_CODE = "INR"
                 repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                 
                 repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                 repcomprec1.Update
                 '----------------For Distribution
                 repcomprec2.AddNew
                 'repcomprec2!INTERFACE_LINE_ID = ctr
                 repcomprec2!ACCOUNT_CLASS = "TAX"
                 
                 '------------System Will Pickup the Tax Value from System
                 repcomprec2!AMOUNT = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!Service_Tax) / 100, 2)
                 '----------------------------------------------------------
                 repcomprec2!Percent = 100
                 repcomprec2!SEGMENT1 = "000"
                 repcomprec2!SEGMENT2 = "00000"
                 repcomprec2!segment3 = "25004"
                 repcomprec2!SEGMENT4 = "00"
                 repcomprec2!ORG_ID = 82
                 repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                 repcomprec2.Update
            'End If
                '--------------------------------fOR cESS cALCULATION---------------------------
                 ctr = ctr + 1
                 repcomprec1.AddNew
                 'repcomprec1!INTERFACE_LINE_ID = ctr
                 repcomprec1!LINE_TYPE = "TAX"
                 repcomprec1!Description = "Tax line"
                 repcomprec1!AMOUNT = Round((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!cess_Tax / 100)
                 repcomprec1!CONVERSION_TYPE = "User"
                 repcomprec1!CONVERSION_RATE = 1
                 repcomprec1!TRX_DATE = comprec15!VDATE
                 
                 '------------System Will Pickup the Tax Value from System
                 repcomprec1!TAX_RATE = 2
                 repcomprec1!TAX_CODE = "Service Tax Education Cess @ 2%"
                 '--------------------------------------------------------
                 repcomprec1!TAX_PRECEDENCE = 1
                 repcomprec1!ATTRIBUTE1 = ctr
                 repcomprec1!ORG_ID = 82
                 repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                 repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                 repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                 repcomprec1!SET_OF_BOOKS_ID = 2
                 repcomprec1!CURRENCY_CODE = "INR"
                 repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                 repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                 repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1.Update
                'For Distribution---------
                 
                 repcomprec2.AddNew
                 'repcomprec2!INTERFACE_LINE_ID = ctr
                 repcomprec2!ACCOUNT_CLASS = "TAX"
                 repcomprec2!AMOUNT = Round(comprec15!STAX, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 0.08) / 100, 2)
                 repcomprec2!Percent = 100
                 'repcomprec2!CODE_COMBINATION_ID = 1153
                 repcomprec2!SEGMENT1 = "000"
                 repcomprec2!SEGMENT2 = "00000"
                 repcomprec2!segment3 = "25004"
                 repcomprec2!SEGMENT4 = "00"
                 repcomprec2!ORG_ID = 82
                 repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                 repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                 repcomprec2.Update
                '--------------------------------fOR vat cALCULATION---------------------------
                
                If comprec15!CITY_NAME = "DELHI" And comprec15!VAT > 0 Then
                ctr = ctr + 1
                repcomprec1.AddNew
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "TAX"
                repcomprec1!Description = "Tax line"
                repcomprec1!AMOUNT = Round(comprec15!VAT, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 12.5) / 100, 2)
                
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                repcomprec1!TRX_DATE = comprec15!VDATE
                
                repcomprec1!TAX_RATE = 12.5
                repcomprec1!TAX_CODE = "VAT @ 12.5%"
                repcomprec1!TAX_PRECEDENCE = 0
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!ORG_ID = 82
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                repcomprec1!SET_OF_BOOKS_ID = 2
                repcomprec1!CURRENCY_CODE = "INR"
                repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1.Update
                '---------------FOR dISTRIBUTION
                
                repcomprec2.AddNew
                'repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "TAX"
                repcomprec2!AMOUNT = Round(comprec15!VAT, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 12.5) / 100, 2)
                repcomprec2!Percent = 100
                repcomprec2!CODE_COMBINATION_ID = 1157
                repcomprec2!SEGMENT1 = "000"
                repcomprec2!SEGMENT2 = "00000"
                repcomprec2!segment3 = "25007"
                repcomprec2!SEGMENT4 = "00"
                repcomprec2!ORG_ID = 82
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2.Update
                End If
                End If
            Else
                
                Set repcomprec4 = New ADODB.Recordset
                repcomprec4.Open "Error_List", con1, adOpenKeyset, adLockPessimistic
                repcomprec4.AddNew
                repcomprec4!company = comprec15!company
                repcomprec4.Update
                List2.AddItem comprec15!company & "---" & comprec15!invoice_no
            End If
            comprec15.MoveNext
            inv_ctr = inv_ctr + 1
    Loop
End If
        
       STRCMD = "update id_code set id='" & ctr & "'"
       Set repcomprec = con1.Execute(STRCMD)

        comprec15.Close
        STRCMD = "select count(*) as net from Invoice_dec " 'WHERE invoice_no ='" & "011-DS00031704-CHK" & "'"
        STRCMD = STRCMD & " where id >='" & Combo5.Text & "'"
        STRCMD = STRCMD & " AND id <='" & Combo6.Text & "'"
        STRCMD = STRCMD & " and uploaded=0"
        STRCMD = STRCMD & "  AND PRODUCT ='" & "Self Drive Spot" & "'"
        'STRCMD = STRCMD & "  AND CITY_NAME='" & Combo1.Text & "'"
        STRCMD = STRCMD & "  AND service_tax>=0"
        'STRCMD = STRCMD & "  AND service_tax<=10"
        'STRCMD = STRCMD & "  AND vehicle_no='" & "9351" & "'"
        
    
        Set comprec = con1.Execute(STRCMD)
        If Not comprec.EOF Then
            MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
        End If
        Unload status
'        Form_Load
Exit Sub
End Sub

Private Sub Command2_Click()
MsgBox List1.ListCount
End Sub

Private Sub Command3_Click()
    STRCMD = "select count(*) as gr from Invoice_dec " 'where invoice_no='" & "011-DS00031704-CHK" & "'"
    'strcmd = strcmd & " WHERE invoice_no in(select distinct trx_number from invimp where UPLOADED =0)"
    STRCMD = STRCMD & " WHERE id >='" & Combo5.Text & "'"
    STRCMD = STRCMD & " AND id <='" & Combo6.Text & "'"
    STRCMD = STRCMD & " and uploaded=0"
    'STRCMD = STRCMD & "  AND PRODUCT ='" & "Self Drive Spot" & "'"
'   STRCMD = STRCMD & "  AND CITY_NAME='" & Combo1.Text & "'"
    'STRCMD = STRCMD & "  AND service_tax>=0"
    'STRCMD = STRCMD & "  AND service_tax<=10"
    'STRCMD = STRCMD & "  AND vehicle_no='" & "9351" & "'"
    
    
    Set comprec = con1.Execute(STRCMD)
    If Not comprec.EOF Then
        Label5.Caption = "Total " & comprec!gr
    Else
        Label5.Caption = "Total " & 0
    End If
End Sub

Private Sub Command4_Click()
Unload Me
End Sub

Private Sub Command5_Click()
STRCMD = "select distinct trx_number from ra_interface_lines_all where interface_status='P'"
Set comprec = con.Execute(STRCMD)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Set repcomprec1 = New ADODB.Recordset
'        a = Mid(comprec!TRX_NUMBER, 0, Len(comprec!TRX_NUMBER) - 3)
        strcmd1 = "select * from Invoice_dec where invoice_no='" & comprec!TRX_NUMBER & "'"
        repcomprec1.Open strcmd1, con1, adOpenKeyset, adLockPessimistic
        If Not repcomprec1.EOF Then
            repcomprec1!Uploaded = 1
            DoEvents
            repcomprec1.Update
            StatusBar1.Panels(1).Text = "Updating Invoice Number:" & comprec!TRX_NUMBER
            DoEvents
        End If
    comprec.MoveNext
    Loop
    
End If
    STRCMD = "select distinct count(trx_number) as net from ra_interface_lines_all where interface_status='P'"
    Set comprec = con.Execute(STRCMD)
    If Not comprec.EOF Then
        MsgBox comprec!Net & " Invoices Gets Updated in SQL DATABASE !", vbInformation, "UPDATED"
        Exit Sub
    End If
End Sub

Private Sub Command6_Click()

End Sub

Private Sub Option1_Click()
If Option1.Value = True Then
    Frame2(0).ZOrder 0
Else
    Frame2(0).ZOrder 1
End If
End Sub

Private Sub Option2_Click()
If Option2.Value = True Then
    Frame2(1).ZOrder 0
Else
    Frame2(1).ZOrder 1
End If
End Sub


Public Sub add_brn()
Combo1.Clear
STRCMD = "select CITY_name from INVOICE_DEC group by CITY_NAME"
Set comprec = con1.Execute(STRCMD)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!CITY_NAME
        comprec.MoveNext
    Loop
    Combo1.Text = Combo1.List(0)
End If
End Sub
Public Sub add_rem()
Combo4.Clear
STRCMD = "select distinct remark from Invoice_dec"
Set comprec = con1.Execute(STRCMD)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo4.AddItem comprec!REMARK & ""
        comprec.MoveNext
    Loop
End If
Combo4.Text = Combo4.List(0)
End Sub

Private Sub Form_Load()
Combo5.Clear
Combo6.Clear
For i = 1 To 10000
    Combo5.AddItem i
    Combo6.AddItem i
Next
Combo5.Text = Combo5.List(0)
Combo6.Text = Combo6.List(0)

add_rem
Combo3.Clear
STRCMD = "select distinct batch_source_name from ra_interface_lines_all"
Set comprec = con.Execute(STRCMD)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo3.AddItem comprec!BATCH_SOURCE_NAME
        comprec.MoveNext
    Loop
End If
Combo3.Text = Combo3.List(0)
Combo2.Clear
With Combo2
.AddItem "CIPL Import1"
.AddItem "CIPL Import2"
.AddItem "CIPL Import3"
.AddItem "CIPL Import4"
.AddItem "CIPL Import5"
.AddItem "CIPL Import6"
.AddItem "CIPL Import7"
.AddItem "CIPL Import8"
End With
Combo2.Text = Combo2.List(0)
connection1
add_brn
Timer1.Interval = 1000
add_brn
'strconn = ("Driver={Microsoft ODBC for Oracle};Server=CIPL;Uid=apps;Pwd=apps;Persist Security Info=False")
'Set con = New ADODB.Connection
'con.Open strconn
DTPicker1.Value = Format(Date, "dd-mm-yyyy")
DTPicker2.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker3.Value = Format(Date, "dd-mm-yyyy")
End Sub

