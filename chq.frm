VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form2 
   BackColor       =   &H00000000&
   Caption         =   "Cheque Printing"
   ClientHeight    =   11010
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "chq.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   11010
   ScaleWidth      =   15240
   WindowState     =   2  'Maximized
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   3000
      TabIndex        =   1
      Top             =   360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      Format          =   16580609
      CurrentDate     =   39524
   End
   Begin VB.CommandButton Command5 
      BackColor       =   &H00C0FFFF&
      Caption         =   "&Voucher"
      Height          =   375
      Left            =   8160
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      BackColor       =   &H00C0FFFF&
      Caption         =   "&Print List"
      Height          =   375
      Left            =   6840
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1200
      Width           =   1335
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00000000&
      Caption         =   "Select All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   0
      TabIndex        =   12
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CommandButton Command3 
      BackColor       =   &H00C0FFFF&
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   375
      Left            =   9480
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      BackColor       =   &H00C0FFFF&
      Caption         =   "&Print"
      Height          =   375
      Left            =   5640
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   1200
      Width           =   1215
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1320
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   1200
      Width           =   3015
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00C0FFFF&
      Caption         =   "View"
      Height          =   375
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1200
      Width           =   1215
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   8175
      Left            =   0
      TabIndex        =   11
      Top             =   2040
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   14420
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   12648447
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Chq. No"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Vendor"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Date"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "Chq. Amount"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   1200
      TabIndex        =   0
      Top             =   360
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   16580609
      CurrentDate     =   39479
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   8040
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Chq Date"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   3
      Left            =   6120
      TabIndex        =   14
      Top             =   360
      Width           =   1140
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Total Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   2
      Left            =   4800
      TabIndex        =   13
      Top             =   360
      Width           =   1380
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "ID"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   1410
      TabIndex        =   10
      Top             =   960
      Width           =   885
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Bank Name"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Chq Date"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   360
      Width           =   780
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim chk As Boolean

Private Sub Check1_Click()
If Check1.Value = 1 Then
Dim lt As ListItem
i = 1
For i = 1 To ListView1.ListItems.Count
    Set lt = ListView1.ListItems(i)
    ListView1.ListItems(i).Checked = True
Next
Else
For i = 1 To ListView1.ListItems.Count
    Set lt = ListView1.ListItems(i)
    ListView1.ListItems(i).Checked = False
Next
End If
End Sub

Private Sub Combo1_Click()
strcmd = "select * from ap_bank_accounts_all where bank_account_name ='" & Combo1.Text & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Label2.Caption = comprec!bank_account_id
End If
End Sub

Private Sub Combo1_KeyDown(KeyCode As Integer, Shift As Integer)
strcmd = "select * from ap_bank_accounts_all where bank_account_name ='" & Combo1.Text & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Label2.Caption = comprec!bank_account_id
End If
End Sub

Private Sub Command1_Click()
arraydec
'aa = num2str(101010)
'MsgBox aa

Set comprec = con1.Execute("UPDATE Chq_Prn SET UPDATE_PRN=0")
Set comprec = con1.Execute("truncate table Chq_Prn")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)

p_start_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker2.Value, "dd-mmm-yyyy")
strcmd = " select  distinct apv.amount apvamt,A3.INVOICE_ID,a3.attribute1,apv.INVOICE_ID,apv.INVOICE_NUM,apv.INVOICE_DATE,apv.INVOICE_AMOUNT,a1.amount,"
strcmd = strcmd & " a1.vendor_name,a1.vendor_site_code,a1.address_line1,"
strcmd = strcmd & " a1.city,a1.check_id,a1.check_date,a1.check_number,"
strcmd = strcmd & "a1.bank_account_name , a3.Source from ap_checks_all a1, Ap_Invoice_Payments_All a2, ap_invoices_all a3 ,ap_invoice_payments_V apv"
strcmd = strcmd & " Where a2.invoice_id = a3.invoice_id "
strcmd = strcmd & " and a1.vendor_id=a3.vendor_id"
'strcmd = strcmd & " and a1.check_date >='" & p_start_date & "'"
strcmd = strcmd & " and a1.check_date between '" & p_start_date & "'" & " and '" & p_end_date & "'"

'strcmd = strcmd & " and a1.check_date <='" & p_End_date & "'"
strcmd = strcmd & " and a1.bank_account_name  ='" & Combo1.Text & "'"
strcmd = strcmd & " and a1.bank_account_id  =" & Label2.Caption & ""
'strcmd = strcmd & " or a1.bank_account_id  =" & "10003" & ""
strcmd = strcmd & " and a1.check_id=apv.CHECK_ID "
strcmd = strcmd & " and a2.invoice_id=apv.INVOICE_ID"
strcmd = strcmd & " and a1.status_lookup_code <>'" & "VOIDED" & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Set repcomprec2 = New ADODB.Recordset
    'repcomprec2.Open "CHQ_PRN", con1, adOpenKeyset, adLockPessimistic
    repcomprec2.Open "CHQ_PRN", con1, adOpenDynamic, adLockOptimistic
    
    
    Do While Not comprec.EOF
    repcomprec2.AddNew
    repcomprec2!Vendor_Name = comprec!Vendor_Name & ""
    repcomprec2!Address = comprec!ADDRESS_LINE1 & ""
    repcomprec2!CITY = comprec!CITY & ""
    repcomprec2!site_Code = comprec!vendor_site_code & ""
    repcomprec2!invoice_no = comprec!invoice_num & ""
    repcomprec2!VDATE = comprec!INVOICE_DATE & ""
    repcomprec2!INV_AMOUNT = comprec!apvamt & ""
    repcomprec2!chq_amount = comprec!Amount & ""
    repcomprec2!bank_name = Combo1.Text & ""
    repcomprec2!chq_date = comprec!check_date & ""
    'repcomprec2!chq_date = DTPicker1.Value & ""
    repcomprec2!BANK_ID = Label2.Caption & ""
    repcomprec2!Source = comprec!Source & ""
    repcomprec2!Inv_id = comprec!INVOICE_ID & ""
    repcomprec2!tds_ID = comprec!ATTRIBUTE1 & ""
    repcomprec2!voucher_ID = comprec!check_id & ""
    repcomprec2!Check_no = comprec!CHeck_Number & ""
    comprec.MoveNext
    repcomprec2.Update
    Loop
End If


    chq_amt = 0
    ListView1.ListItems.Clear
    strcmd = "select check_no from chq_prn group by check_no ORDER BY CHECK_NO"
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        Do While Not comprec.EOF
        strcmd = "select *  from chq_prn where check_no='" & comprec!Check_no & "'"
        Set comprec1 = con1.Execute(strcmd)
        If Not comprec1.EOF Then
            Set lt = ListView1.ListItems.Add(, , comprec!Check_no)
            lt.SubItems(1) = comprec1!Vendor_Name
            lt.SubItems(2) = comprec1!chq_date
            lt.SubItems(3) = comprec1!chq_amount
            chq_amt = chq_amt + comprec1!chq_amount
        End If
        comprec.MoveNext
        Loop
        Label1(3).Caption = Round(chq_amt, 2)
    End If


'
'strcmd = "select * from chq_prn where source='" & "TDS" & "'"
'Set comprec2 = con1.Execute(strcmd)
'If Not comprec2.EOF Then
'Set comprec2 = New ADODB.Recordset
'comprec2.Open strcmd, con1, adOpenDynamic, adLockOptimistic
'    Do While Not comprec2.EOF
'        comprec2!tax_ded = -(comprec2!INV_AMOUNT)
'        comprec2!INV_AMOUNT = 0
'        comprec2.Update
'        comprec2.MoveNext
'    Loop
'End If


'
'strcmd = "select * from chq_prn where source='" & "TDS" & "'"
'Set comprec2 = con1.Execute(strcmd)
'If Not comprec2.EOF Then
'Do While Not comprec2.EOF
'    strcmd = "select * from chq_prn where inv_id='" & comprec2!tds_ID & "'"
'    Set comprec3 = New ADODB.Recordset
'    comprec3.Open strcmd, con1, adOpenDynamic, adLockOptimistic
'    If Not comprec3.EOF Then
'        comprec3!tax_ded = -(comprec2!tax_ded)
'        comprec3.Update
'    End If
'    tds = comprec2!tds_ID
'    strcmd = "delete from chq_prn where tds_id='" & comprec2!tds_ID & "'"
'    Set comprec1 = con1.Execute(strcmd)
'    comprec2.MoveNext
'Loop
'End If

'strcmd = "update chq_prn set tax_ded = tax_ded * -1 "
'Set comprec4 = con1.Execute(strcmd)

'strcmd = "update chq_prn Set tax_ded = 0 Where tax_ded Is Null"
'Set comprec4 = con1.Execute(strcmd)

Set comprec = con1.Execute("update chq_prn set tax_ded = tax_ded * -1")
Set comprec = con1.Execute("update chq_prn Set tax_ded = 0 Where tax_ded Is Null")



strcmd = "select * from chq_prn order by check_no"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
     strcmd = "update chq_prn set to_words='" & num2str(comprec!chq_amount) & "'"
     strcmd = strcmd & " where check_no='" & comprec!Check_no & "'"
     Set comprec1 = con1.Execute(strcmd)
     
     comprec.MoveNext
    Loop
End If
MsgBox "Done"
Exit Sub

End Sub

Private Sub Command2_Click()
find_code
End Sub

Private Sub Command3_Click()
Unload Me
End Sub

Private Sub Command4_Click()
find_code1
End Sub

Private Sub Command5_Click()
find_code2
End Sub

Private Sub Form_Activate()
Set comprec = con1.Execute("UPDATE Chq_Prn SET UPDATE_PRN=0")
strcmd = "select * from ap_bank_accounts_all where bank_account_name ='" & Combo1.Text & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Label2.Caption = comprec!bank_account_id
End If
End Sub

Private Sub Form_Load()
add_apps1
connection1
add_brn
DTPicker1.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker2.Value = Format(Date, "dd-MMM-yyyy")
ListView1.Width = Screen.Width

End Sub

Public Sub add_brn()
Combo1.Clear
strcmd = "select bank_account_name from ap_bank_accounts_all "
'strcmd = strcmd & " where bank_branch_id='" & "10001" & "'"
'strcmd = strcmd & " or bank_branch_id='" & "10002" & "'"
'strcmd = strcmd & " or bank_branch_id='" & "10656" & "'"
'strcmd = strcmd & " or bank_branch_id='" & "10847" & "'"
'strcmd = strcmd & " or bank_branch_id='" & "10908" & "'"

strcmd = strcmd & " where bank_account_name like '%" & "HDFC" & "%'"
strcmd = strcmd & " or bank_account_name like '%" & "ICICI" & "%'"
strcmd = strcmd & " or bank_account_name like '%" & "Kotak" & "%'"
strcmd = strcmd & " or bank_account_name like '%" & "KOTAK" & "%'"
strcmd = strcmd & " or bank_account_name like '%" & "Icici" & "%'"
strcmd = strcmd & " or bank_account_name like '%" & "Canara" & "%'"
strcmd = strcmd & " or bank_account_name like '%" & "Bank" & "%'"

'strcmd = strcmd & " or (bank_branch_id>='" & "13201" & "'"
'strcmd = strcmd & " and bank_branch_id<='" & "13225" & "')"
strcmd = strcmd & " group by bank_account_name"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!bank_account_name
        comprec.MoveNext
    Loop
    Combo1.Text = "HDFC CORP 3746 CASH CREDIT"
End If
End Sub

Public Sub find_code()
Dim lt As ListItem
i = 1
For i = 1 To ListView1.ListItems.Count
        Set lt = ListView1.ListItems(i)
        If ListView1.ListItems(i).Checked = True Then
            a = ListView1.ListItems(i).Text
            strcmd = "UPDATE CHQ_PRN SET UPDATE_PRN=1 WHERE CHECK_NO='" & a & "'"
            chk = True
            Set comprec = con1.Execute(strcmd)
        End If
        
Next
    a = MsgBox("Want to Print/View (Yes->Print,No->View", vbYesNo, "Print")
    If a = vbYes Then
        prnagn
    Else
        prnview
    End If
    Set comprec = con1.Execute("UPDATE Chq_Prn SET UPDATE_PRN=0")
End Sub

Public Sub find_code1()
Dim lt As ListItem
i = 1
For i = 1 To ListView1.ListItems.Count
        Set lt = ListView1.ListItems(i)
        If ListView1.ListItems(i).Checked = True Then
            a = ListView1.ListItems(i).Text
            strcmd = "UPDATE CHQ_PRN SET UPDATE_PRN=1 WHERE CHECK_NO='" & a & "'"
            chk = True
            Set comprec = con1.Execute(strcmd)
        End If
        
Next
    a = MsgBox("Want to Print/View (Yes->Print,No->View", vbYesNo, "Print")
    If a = vbYes Then
        prnagn1
    Else
        prnview1
    End If
    Set comprec = con1.Execute("UPDATE Chq_Prn SET UPDATE_PRN=0")
End Sub
Public Sub prnagn()
        'cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\CHQ1.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToPrinter
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnview()
        'cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\CHQ1.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub prnagn1()
        'cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\CHQ12.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToPrinter
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnview1()
        'cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\CHQ12.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Public Sub find_code2()
Dim lt As ListItem
i = 1
For i = 1 To ListView1.ListItems.Count
        Set lt = ListView1.ListItems(i)
        If ListView1.ListItems(i).Checked = True Then
            a = ListView1.ListItems(i).Text
            strcmd = "UPDATE CHQ_PRN SET UPDATE_PRN=1 WHERE CHECK_NO='" & a & "'"
            chk = True
            Set comprec = con1.Execute(strcmd)
        End If
        
Next
    a = MsgBox("Want to Print/View (Yes->Print,No->View", vbYesNo, "Print")
    If a = vbYes Then
        prnagn2
    Else
        prnview2
    End If
    Set comprec = con1.Execute("UPDATE Chq_Prn SET UPDATE_PRN=0")
End Sub

Public Sub prnagn2()
        'cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\voucher.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToPrinter
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnview2()
        'cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\voucher.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


