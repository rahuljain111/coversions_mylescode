if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_INVOICES_Header]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_INVOICES_Header]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_INVOICES_Header1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_INVOICES_Header1]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_INVOICES_LINES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_INVOICES_LINES]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_INVOICES_LINES1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_INVOICES_LINES1]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_MASTER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_MASTER]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AP_MASTERa]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AP_MASTERa]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[AUTO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AUTO]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Auto_Import]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Auto_Import]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Cars]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Cars]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[chq]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[chq]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[CIPL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIPL]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Code]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Code]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Error]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Error]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Error_List]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Error_List]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Final_Out]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Final_Out]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[GOne]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[GOne]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[ID_Code]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ID_Code]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[INV_TYPE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[INV_TYPE]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[INVCODE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[INVCODE]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_08APR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_08APR]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[INVOICE_08APR_BLR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[INVOICE_08APR_BLR]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_16_Jan_Uploaded]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_16_Jan_Uploaded]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_18-Jan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_18-Jan]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_23]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_23]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec_B4_Convrtng]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec_B4_Convrtng]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[INVOICE_DEC_Last_Hotels]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[INVOICE_DEC_Last_Hotels]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec_Lease]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec_Lease]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec_Sale]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec_Sale]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Dec-Shangrila]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Dec-Shangrila]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Invoice_Main]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Invoice_Main]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[knock]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[knock]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[NEW_CAR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[NEW_CAR]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[NEW_CAR1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[NEW_CAR1]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Payment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Payment]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Pending]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Pending]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Product]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Product]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Ship_to]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ship_to]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally_MIS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally_MIS]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally_MIS_IIIrdOld]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally_MIS_IIIrdOld]
GO

if exists (select * from sysobjects where id = object_id(N'[dbo].[Tally_MIS_IInd]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Tally_MIS_IInd]
GO

CREATE TABLE [dbo].[AP_INVOICES_Header] (
	[id] [float] NULL ,
	[INVOICE NUM] [nvarchar] (255) NULL ,
	[invoice_no] [nvarchar] (255) NULL ,
	[ INVOICE_TYPE] [nvarchar] (255) NULL ,
	[VENDOR_NAME] [nvarchar] (255) NULL ,
	[SITE] [nvarchar] (255) NULL ,
	[INV_AMOUNT] [float] NULL ,
	[INV_DATE] [smalldatetime] NULL ,
	[TERMS_name] [float] NULL ,
	[DESCRIPTION] [nvarchar] (255) NULL ,
	[GL_DATE] [smalldatetime] NULL ,
	[inv_cur] [nvarchar] (255) NULL ,
	[F13] [nvarchar] (255) NULL ,
	[F14] [nvarchar] (255) NULL ,
	[F15] [nvarchar] (255) NULL ,
	[F16] [nvarchar] (255) NULL ,
	[F17] [nvarchar] (255) NULL ,
	[F18] [nvarchar] (255) NULL ,
	[F19] [nvarchar] (255) NULL ,
	[F20] [nvarchar] (255) NULL ,
	[F21] [nvarchar] (255) NULL ,
	[F22] [float] NULL ,
	[PREPAYMENT] [nvarchar] (255) NULL ,
	[A ASHOK] [nvarchar] (255) NULL ,
	[CHANDIGARH] [nvarchar] (255) NULL ,
	[F26] [float] NULL ,
	[F27] [float] NULL ,
	[IMMEDAITE] [float] NULL ,
	[F29] [nvarchar] (255) NULL ,
	[F30] [smalldatetime] NULL ,
	[F31] [smalldatetime] NULL ,
	[F32] [nvarchar] (255) NULL ,
	[F33] [nvarchar] (255) NULL ,
	[F34] [nvarchar] (255) NULL ,
	[F35] [nvarchar] (255) NULL ,
	[F36] [smalldatetime] NULL ,
	[F37] [nvarchar] (255) NULL ,
	[Corporate] [nvarchar] (255) NULL ,
	[001] [float] NULL ,
	[Vendor Cateogory] [nvarchar] (255) NULL ,
	[Motor cars-HP] [nvarchar] (255) NULL ,
	[F42] [float] NULL ,
	[CHAUFFER DRIVE] [nvarchar] (255) NULL ,
	[F44] [float] NULL ,
	[F45] [float] NULL ,
	[F46] [float] NULL ,
	[Sec 194 A - Interest- Individual] [nvarchar] (255) NULL ,
	[F48] [float] NULL ,
	[F49] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AP_INVOICES_Header1] (
	[id] [float] NULL ,
	[INVOICE NUM] [nvarchar] (255) NULL ,
	[invoice_no] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[VENDOR_NAME] [nvarchar] (255) NULL ,
	[SITE] [nvarchar] (255) NULL ,
	[INV_AMOUNT] [float] NULL ,
	[INV_DATE] [smalldatetime] NULL ,
	[TERMS_name] [float] NULL ,
	[DESCRIPTION] [nvarchar] (255) NULL ,
	[GL_DATE] [smalldatetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AP_INVOICES_LINES] (
	[id] [float] NULL ,
	[INVOICE NUM] [nvarchar] (255) NULL ,
	[AMOUNT] [float] NULL ,
	[ACCOUNTING_DATE] [smalldatetime] NULL ,
	[DESCRIPTION] [nvarchar] (255) NULL ,
	[BRANCH_NAME] [nvarchar] (255) NULL ,
	[LOCATION ] [nvarchar] (255) NULL ,
	[CAR_NO] [nvarchar] (255) NULL ,
	[CAR CODE] [nvarchar] (255) NULL ,
	[NATURAL_ACCOUNT] [nvarchar] (255) NULL ,
	[NATURAL ACCOUNT CODE] [float] NULL ,
	[PRODUCT NAME] [nvarchar] (255) NULL ,
	[PRODUCT CODE] [nvarchar] (255) NULL ,
	[TAX NAME] [nvarchar] (255) NULL ,
	[TAX RATE] [nvarchar] (255) NULL ,
	[tax_id] [float] NULL ,
	[F17] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AP_INVOICES_LINES1] (
	[id] [float] NULL ,
	[INVOICE NUM] [nvarchar] (255) NULL ,
	[AMOUNT] [float] NULL ,
	[ACCOUNTING_DATE] [smalldatetime] NULL ,
	[DESCRIPTION] [nvarchar] (255) NULL ,
	[BRANCH_NAME] [nvarchar] (255) NULL ,
	[LOCATION] [float] NULL ,
	[CAR_NO] [nvarchar] (255) NULL ,
	[NATURAL_ACCOUNT] [nvarchar] (255) NULL ,
	[NATURAL ACCOUNT CODE] [float] NULL ,
	[PRODUCT] [nvarchar] (255) NULL ,
	[PRODUCT CODE] [nvarchar] (255) NULL ,
	[tax_id] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AP_MASTER] (
	[Sl#No] [float] NULL ,
	[Vendor_Name] [nvarchar] (255) NULL ,
	[Alternate_Name] [nvarchar] (255) NULL ,
	[Location] [nvarchar] (255) NULL ,
	[vendor_Type] [nvarchar] (255) NULL ,
	[Code] [nvarchar] (255) NULL ,
	[Site] [nvarchar] (255) NULL ,
	[City] [nvarchar] (255) NULL ,
	[Location1] [nvarchar] (255) NULL ,
	[Address1] [nvarchar] (255) NULL ,
	[Pin_code] [nvarchar] (255) NULL ,
	[PAN ] [nvarchar] (255) NULL ,
	[TDS Section] [nvarchar] (255) NULL ,
	[TDS Type] [nvarchar] (255) NULL ,
	[LOCATION2] [float] NULL ,
	[Car_No] [nvarchar] (255) NULL ,
	[Natural_AccountS] [float] NULL ,
	[PRODUCT] [nvarchar] (255) NULL ,
	[LOCATION3] [float] NULL ,
	[CAR_no1] [nvarchar] (255) NULL ,
	[Natural_AccountP] [float] NULL ,
	[PRODUCT1] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AP_MASTERa] (
	[Sl#No] [float] NULL ,
	[Vendor_Name] [nvarchar] (255) NULL ,
	[Alternate_Name] [nvarchar] (255) NULL ,
	[Location] [nvarchar] (255) NULL ,
	[vendor_Type] [nvarchar] (255) NULL ,
	[Code] [nvarchar] (255) NULL ,
	[Site] [nvarchar] (255) NULL ,
	[City] [nvarchar] (255) NULL ,
	[Location1] [nvarchar] (255) NULL ,
	[Address1] [nvarchar] (255) NULL ,
	[Pin_code] [nvarchar] (255) NULL ,
	[PAN ] [nvarchar] (255) NULL ,
	[TDS Section] [nvarchar] (255) NULL ,
	[TDS Type] [nvarchar] (255) NULL ,
	[LOCATION2] [float] NULL ,
	[Car_No] [nvarchar] (255) NULL ,
	[Natural_AccountS] [float] NULL ,
	[PRODUCT] [nvarchar] (255) NULL ,
	[LOCATION3] [float] NULL ,
	[CAR NO] [nvarchar] (255) NULL ,
	[Natural_AccountP] [float] NULL ,
	[PRODUCT1] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AUTO] (
	[CODE] [nvarchar] (255) NULL ,
	[CIPL_CUST] [nvarchar] (255) NULL ,
	[PAYMENT_METHOD] [nvarchar] (255) NULL ,
	[PROFILE] [nvarchar] (255) NULL ,
	[Category] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Auto_Import] (
	[RMS_Cust] [nvarchar] (255) NULL ,
	[CIPL_CUST] [nvarchar] (255) NULL ,
	[ID] [float] NULL ,
	[CATEGORY] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Cars] (
	[   ] [nvarchar] (255) NULL ,
	[DESCRIPTION] [nvarchar] (255) NULL ,
	[veh_no] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[chq] (
	[Vdate] [smalldatetime] NULL ,
	[Deposit] [nvarchar] (255) NULL ,
	[Payment_method] [nvarchar] (255) NULL ,
	[CHQ] [float] NULL ,
	[Cheque_no] [nvarchar] (255) NULL ,
	[Gl_Date] [smalldatetime] NULL ,
	[Amount] [float] NULL ,
	[Lockbox] [float] NULL ,
	[Location] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Description] [nvarchar] (255) NULL ,
	[Additional Remark] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CIPL] (
	[Cipl_Cust] [nvarchar] (255) NULL ,
	[RMS_Cust] [nvarchar] (255) NULL ,
	[Id] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Code] (
	[Customer_Name1] [nvarchar] (255) NULL ,
	[Code1] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Error] (
	[Bill_No] [varchar] (50) NULL ,
	[City_Name] [varchar] (50) NULL ,
	[Company] [varchar] (266) NULL ,
	[Amount] [float] NULL ,
	[DS_No] [varchar] (50) NULL ,
	[Vdate] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Error_List] (
	[Company] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Final_Out] (
	[Branch] [nvarchar] (255) NULL ,
	[company] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Vdate1] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pick_Up] [nvarchar] (255) NULL ,
	[KM_Out] [float] NULL ,
	[KM_IN] [float] NULL ,
	[Reson_Delay] [nvarchar] (255) NULL ,
	[Reson_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [float] NULL ,
	[Remark] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[GOne] (
	[INV] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ID_Code] (
	[ID] [float] NULL ,
	[ID1] [float] NULL ,
	[ID2] [float] NULL ,
	[V_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[INV_TYPE] (
	[BRANCH_NAME] [nvarchar] (255) NULL ,
	[INV_TYPE] [nvarchar] (255) NULL ,
	[CREDIT_TDS_CC] [varchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[INVCODE] (
	[TRX_NUMBER] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice] (
	[Branch] [nvarchar] (255) NULL ,
	[company] [nvarchar] (255) NULL ,
	[Vdate] [datetime] NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pick_Up] [nvarchar] (255) NULL ,
	[KM_Out] [float] NULL ,
	[KM_IN] [float] NULL ,
	[Reson_Delay] [nvarchar] (255) NULL ,
	[Reson_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[uploaded] [bit] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_08APR] (
	[id] [float] NULL ,
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [nvarchar] (255) NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL ,
	[Service_Tax] [float] NULL ,
	[Cess_Tax] [float] NULL ,
	[Vat_Tax] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[INVOICE_08APR_BLR] (
	[ID] [float] NULL ,
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [nvarchar] (255) NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL ,
	[Service_Tax] [float] NULL ,
	[Cess_Tax] [float] NULL ,
	[Vat_Tax] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_16_Jan_Uploaded] (
	[id] [float] NULL ,
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [float] NULL ,
	[New_Car] [float] NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_18-Jan] (
	[Branch] [nvarchar] (255) NULL ,
	[company] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pick_Up] [nvarchar] (255) NULL ,
	[KM_Out] [float] NULL ,
	[KM_IN] [float] NULL ,
	[Reson_Delay] [nvarchar] (255) NULL ,
	[Reson_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_HRS] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_23] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [varchar] (50) NULL ,
	[New_Car] [float] NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL ,
	[Service_Tax] [float] NULL ,
	[Cess_Tax] [float] NULL ,
	[Vat_Tax] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec_B4_Convrtng] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[INVOICE_DEC_Last_Hotels] (
	[Branch_Bill_to] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [float] NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [varchar] (50) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [float] NULL ,
	[Reason_Cancel] [float] NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [nvarchar] (255) NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec_Lease] (
	[ID] [float] NULL ,
	[Branch_BILL_TO] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [nvarchar] (255) NULL ,
	[Other_Taxes] [nvarchar] (255) NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [float] NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [float] NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [float] NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [float] NULL ,
	[Driver] [float] NULL ,
	[Assignment] [float] NULL ,
	[Pickup] [float] NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [float] NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec_Sale] (
	[Branch_Bill_To] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [nvarchar] (255) NULL ,
	[Vdate] [smalldatetime] NULL ,
	[Actual_Invoice] [nvarchar] (255) NULL ,
	[Car_Used_Date] [smalldatetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [nvarchar] (255) NULL ,
	[Reason_Cancel] [nvarchar] (255) NULL ,
	[Outstation] [nvarchar] (255) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [nvarchar] (255) NULL ,
	[Uploaded] [bit] NOT NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Dec-Shangrila] (
	[id] [float] NULL ,
	[Branch_Bill_to] [nvarchar] (255) NULL ,
	[Company] [nvarchar] (255) NULL ,
	[Client_Name] [nvarchar] (255) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [float] NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [float] NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [nvarchar] (255) NULL ,
	[Duty_Slip_No] [nvarchar] (255) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [nvarchar] (255) NULL ,
	[Branch_Booked] [nvarchar] (255) NULL ,
	[Direct] [bit] NOT NULL ,
	[Cat] [nvarchar] (255) NULL ,
	[Owned] [bit] NOT NULL ,
	[Category] [nvarchar] (255) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [float] NULL ,
	[Reason_Cancel] [float] NULL ,
	[Outstation] [float] NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [nvarchar] (255) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [nvarchar] (255) NULL ,
	[Driver] [nvarchar] (255) NULL ,
	[Assignment] [nvarchar] (255) NULL ,
	[Pickup] [nvarchar] (255) NULL ,
	[CITY_NAME] [nvarchar] (255) NULL ,
	[Remark] [nvarchar] (255) NULL ,
	[SHIP_TO] [nvarchar] (255) NULL ,
	[INVOICE_TYPE] [nvarchar] (255) NULL ,
	[New_Car] [float] NULL ,
	[Uploaded] [float] NULL ,
	[Ship_to1] [nvarchar] (255) NULL ,
	[Term_Id] [float] NULL ,
	[Recipt_ID] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Invoice_Main] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [float] NULL ,
	[KM_in] [float] NULL ,
	[Reason_Delay] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[Vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pickup] [varchar] (255) NULL ,
	[CITY_NAME] [varchar] (200) NULL ,
	[Remark] [varchar] (200) NULL ,
	[SHIP_TO] [varchar] (200) NULL ,
	[LOCATION_CODE] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[knock] (
	[customer_name] [nvarchar] (255) NULL ,
	[cash_id] [float] NULL ,
	[TRX_NUMBER] [nvarchar] (255) NULL ,
	[amount] [float] NULL ,
	[gl_date] [smalldatetime] NULL ,
	[FLAG] [nvarchar] (255) NULL ,
	[ERR_MESSAGE] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[NEW_CAR] (
	[Branch] [nvarchar] (255) NULL ,
	[Veh_No] [nvarchar] (255) NULL ,
	[Car_No] [nvarchar] (255) NULL ,
	[code] [float] NULL ,
	[CITY] [varchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[NEW_CAR1] (
	[Branch] [nvarchar] (255) NULL ,
	[Veh_No] [nvarchar] (255) NULL ,
	[Car_No] [nvarchar] (255) NULL ,
	[code] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Payment] (
	[Payment_Method] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Pending] (
	[Inv_No] [varchar] (500) NULL ,
	[Company] [varchar] (100) NULL ,
	[Branch] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Product] (
	[FLEX_VALUE] [varchar] (53) NULL ,
	[PRODUCT] [nvarchar] (255) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ship_to] (
	[City] [varchar] (222) NULL ,
	[State] [varchar] (100) NULL ,
	[Postal_Code] [varchar] (100) NULL ,
	[Country] [varchar] (50) NULL ,
	[Address1] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally] (
	[vDATE] [nvarchar] (255) NULL ,
	[chq_NO] [varchar] (50) NULL ,
	[d_Amount] [float] NULL ,
	[c_AMOUNT] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally_MIS] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally_MIS_IIIrdOld] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [varchar] (50) NULL ,
	[km_in] [varchar] (50) NULL ,
	[Reason_Dealy] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[vat] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Tally_MIS_IInd] (
	[Branch] [varchar] (200) NULL ,
	[Company] [varchar] (200) NULL ,
	[Client_Name] [varchar] (200) NULL ,
	[Basic] [float] NULL ,
	[Parking_Toll] [float] NULL ,
	[Other_Taxes] [float] NULL ,
	[Vehicle_No] [varchar] (100) NULL ,
	[Vdate] [datetime] NULL ,
	[Actual_Invoice] [varchar] (100) NULL ,
	[Car_Used_Date] [datetime] NULL ,
	[Invoice_No] [varchar] (100) NULL ,
	[Duty_Slip_No] [varchar] (100) NULL ,
	[STax] [float] NULL ,
	[Car_Booked_by] [varchar] (250) NULL ,
	[Branch_Booked] [varchar] (200) NULL ,
	[Direct] [bit] NULL ,
	[Cat] [varchar] (200) NULL ,
	[Owned] [bit] NULL ,
	[Category] [varchar] (200) NULL ,
	[KM_OUT] [varchar] (50) NULL ,
	[km_in] [varchar] (50) NULL ,
	[Reason_Dealy] [varchar] (255) NULL ,
	[Reason_Cancel] [varchar] (233) NULL ,
	[Outstation] [varchar] (244) NULL ,
	[Extra_Km] [float] NULL ,
	[Extra_Hrs] [float] NULL ,
	[Outstation_Amt] [float] NULL ,
	[Fuel] [float] NULL ,
	[Night] [float] NULL ,
	[Other_Charges] [float] NULL ,
	[Product] [varchar] (200) NULL ,
	[vat] [float] NULL ,
	[Package_Name] [varchar] (200) NULL ,
	[Driver] [varchar] (200) NULL ,
	[Assignment] [varchar] (255) NULL ,
	[Pickup] [varchar] (255) NULL 
) ON [PRIMARY]
GO

