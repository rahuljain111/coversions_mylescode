VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form3 
   BackColor       =   &H00FF8080&
   Caption         =   "Auto Invoice Import System"
   ClientHeight    =   7980
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   10815
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form3"
   MDIChild        =   -1  'True
   ScaleHeight     =   7980
   ScaleWidth      =   10815
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.CheckBox Check1 
      BackColor       =   &H00FF8080&
      Caption         =   "Rebate In OL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   1
      Left            =   7560
      TabIndex        =   39
      Top             =   5280
      Width           =   1815
   End
   Begin VB.ComboBox Combo7 
      Height          =   330
      ItemData        =   "Interface.frx":0000
      Left            =   5520
      List            =   "Interface.frx":0002
      TabIndex        =   2
      Text            =   "Revenue"
      Top             =   4920
      Width           =   1695
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Update Invoices"
      Height          =   495
      Left            =   4320
      TabIndex        =   6
      Top             =   6720
      Width           =   1575
   End
   Begin VB.ComboBox Combo6 
      BackColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   3240
      TabIndex        =   1
      Top             =   4920
      Width           =   1215
   End
   Begin VB.ComboBox Combo5 
      BackColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   1320
      TabIndex        =   0
      Top             =   4920
      Width           =   1215
   End
   Begin VB.ComboBox Combo4 
      BackColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   1560
      TabIndex        =   34
      Top             =   2520
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.ComboBox Combo3 
      BackColor       =   &H00FFFFFF&
      Height          =   330
      Left            =   2160
      TabIndex        =   33
      Top             =   4440
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00FF8080&
      Caption         =   "Purge All Data"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   0
      Left            =   7560
      TabIndex        =   3
      Top             =   4920
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.ComboBox Combo2 
      Height          =   330
      Left            =   2160
      TabIndex        =   31
      Top             =   3960
      Width           =   2535
   End
   Begin VB.CommandButton Command4 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   495
      Left            =   5880
      TabIndex        =   7
      Top             =   6720
      Width           =   1575
   End
   Begin VB.ComboBox Combo1 
      Height          =   330
      Left            =   1560
      Locked          =   -1  'True
      TabIndex        =   27
      Top             =   2040
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Count Total Records"
      Height          =   495
      Left            =   2400
      TabIndex        =   4
      Top             =   6720
      Width           =   1935
   End
   Begin VB.ListBox List2 
      Height          =   900
      Left            =   -1440
      TabIndex        =   24
      Top             =   3360
      Width           =   975
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   23
      Top             =   7605
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18547
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox Skinner1 
      Height          =   480
      Left            =   -1680
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   25
      Top             =   2400
      Width           =   1200
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Count"
      Height          =   495
      Left            =   4680
      TabIndex        =   22
      Top             =   6000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox List1 
      Height          =   1530
      Left            =   7320
      TabIndex        =   21
      Top             =   240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Timer Timer1 
      Left            =   7680
      Top             =   3840
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   10
      Top             =   360
      Visible         =   0   'False
      Width           =   2415
      Begin VB.OptionButton Option2 
         Caption         =   "Between"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1320
         TabIndex        =   12
         Top             =   120
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Single"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   360
         TabIndex        =   11
         Top             =   120
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Upload AR Data"
      Height          =   495
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6720
      Width           =   2055
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   1095
      Left            =   -2160
      TabIndex        =   19
      Top             =   2640
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   1931
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   11786751
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   35
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(25) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   24
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(26) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   25
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(27) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   26
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(28) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   27
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(29) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   28
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(30) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   29
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(31) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   30
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(32) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   31
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(33) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   32
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(34) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   33
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(35) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   34
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   1
      Left            =   240
      TabIndex        =   16
      Top             =   1320
      Visible         =   0   'False
      Width           =   3975
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   600
         TabIndex        =   8
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   16646147
         CurrentDate     =   37865
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   330
         Left            =   2400
         TabIndex        =   9
         Top             =   120
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   16646147
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   2
         Left            =   1900
         TabIndex        =   18
         Top             =   300
         Width           =   180
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   300
         Width           =   360
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1440
      TabIndex        =   13
      Top             =   480
      Visible         =   0   'False
      Width           =   1575
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Left            =   0
         TabIndex        =   14
         Top             =   120
         Visible         =   0   'False
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   16646147
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   15
         Top             =   0
         Visible         =   0   'False
         Width           =   345
      End
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Label9"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   4800
      TabIndex        =   38
      Top             =   4920
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "End"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   2
      Left            =   2760
      TabIndex        =   37
      Top             =   5040
      Width           =   540
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   1
      Left            =   600
      TabIndex        =   36
      Top             =   5040
      Width           =   630
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Remark"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   0
      Left            =   480
      TabIndex        =   35
      Top             =   2520
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Batches in Apps"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   600
      TabIndex        =   32
      Top             =   4560
      Visible         =   0   'False
      Width           =   1200
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Batch Source Name"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   600
      TabIndex        =   30
      Top             =   3960
      Width           =   1440
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   225
      Left            =   3720
      TabIndex        =   29
      Top             =   6240
      Width           =   420
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Branch"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   480
      TabIndex        =   28
      Top             =   2160
      Visible         =   0   'False
      Width           =   525
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   4440
      TabIndex        =   26
      Top             =   1320
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Height          =   210
      Left            =   1560
      TabIndex        =   20
      Top             =   4320
      Width           =   45
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ctr As Variant
Dim RevenuePart As Variant
Dim new_car As Variant

Private Sub Command1_Click()
    
If Check1(0).Value = 1 Then
   'Command5_Click
    StatusBar1.Panels(1).Text = "Purging Old data From System ------------------!"
    strcmd = "delete from RA_INTERFACE_LINES_ALL where batch_source_name='" & Combo2.Text & "'"
    'where interface_status='" & "P" & "'"
    Set comprec = con.Execute(strcmd)
    strcmd = "delete from RA_INTERFACE_DISTRIBUTIONS_ALL where INTERIM_TAX_SEGMENT30 ='" & Combo2.Text & "'"
    Set comprec = con.Execute(strcmd)
    StatusBar1.Panels(1).Text = "Data Purged------------------!"
End If

strcmd = "select * from ra_interface_lines_all where batch_source_name='" & Combo2.Text & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    MsgBox "Batch Source Already in Used ! Purge All The Previous data to Use The Batch Source ", vbInformation, "Batch Source Name"
    Exit Sub
End If

If Combo7.Text = "OL" Then
    strcmd = "select count(*) as net from   Invoice_Dec_OL "
    strcmd = strcmd & " where  id >='" & Combo5.Text & "'"
    strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
    strcmd = strcmd & " and uploaded=0"
ElseIf Combo7.Text = "EasyCabs" Then
    strcmd = "select count(*) as net from   Invoice_Dec_EasyCabs "
    strcmd = strcmd & " where  id >='" & Combo5.Text & "'"
    strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
    strcmd = strcmd & " and uploaded=0"
Else
    strcmd = "select count(*) as net from   Invoice_Dec "
    strcmd = strcmd & " where  id >='" & Combo5.Text & "'"
    strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
    strcmd = strcmd & " and uploaded=0"
End If
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
End If

add_data
End Sub

Public Sub add_data()
Dim repcomprec As ADODB.Recordset
Dim repcomprec1 As ADODB.Recordset
Dim repcomprec2 As ADODB.Recordset
Dim comprec5 As ADODB.Recordset

c = 1
Dim comprec15 As ADODB.Recordset
ListView1.ListItems.Clear
Dim Cipl_Type As String

Set repcomprec = New ADODB.Recordset
Set repcomprec1 = New ADODB.Recordset
countctr = 1
inv_ctr = 1
strcmd = "select * from ID_Code "
Set comprec = con1.Execute(strcmd)
ctr = comprec!Id   '9909720 x
RevenuePart = comprec!Id   '9909720 x
    If Combo7.Text = "OL" Then
        strcmd = "select * from Invoice_Dec_OL "
        strcmd = strcmd & " where id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0  order by ID"
    ElseIf Combo7.Text = "EasyCabs" Then
        strcmd = "select * from Invoice_Dec_EasyCabs "
        strcmd = strcmd & " where id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0  order by ID"
    Else
        strcmd = "select * from Invoice_Dec "
        strcmd = strcmd & " where id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0  order by ID"
    End If
        
Set comprec15 = con1.Execute(strcmd)
If Not comprec15.EOF Then

    If Combo7.Text = "OL" And Check1(1).Value = 1 And Combo2.Text <> "CIPL Import3" Then
        MsgBox "Please select CIPL Import3"
        Exit Sub
    ElseIf Combo7.Text = "Rent A Car" And comprec15!basic < 0 And Combo2.Text = "CIPL Import1" Then
       MsgBox "Please do not select CIPL Import1"
       Exit Sub
    End If

    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "RA_INTERFACE_LINES_ALL", con, adOpenDynamic, adLockOptimistic
    Set repcomprec2 = New ADODB.Recordset
    repcomprec2.Open "RA_INTERFACE_DISTRIBUTIONS_ALL", con, adOpenDynamic, adLockOptimistic
    Do While Not comprec15.EOF
            ctr = ctr + 1
            RevenuePart = ctr
            DoEvents
            status.Show
            DoEvents
            
            'Code To Map Car No
            If comprec15!owned = True Then
                strcmd = "select * from new_car where vehicle_no='" & CStr(comprec15!vehicle_no) & "'"
                strcmd = strcmd & " AND CITY='" & comprec15!CITY_NAME & "'"
                Set newcomprec = con1.Execute(strcmd)
                If Not newcomprec.EOF Then
                    new_car = newcomprec!car_no
                Else
                    new_car = 0
                End If
            End If
                
            If comprec15!owned = False Then
                If Combo7.Text = "OL" Then
                    If Check1(1).Value = 0 Then
                        'new_car = "Vendor Cateogory"
                        'code added by Rahul
                        strcmd = "select * from new_car where vehicle_no='" & CStr(comprec15!vehicle_no) & "'"
                        strcmd = strcmd & " AND CITY='" & comprec15!CITY_NAME & "'"
                        Set newcomprec = con1.Execute(strcmd)
                        If Not newcomprec.EOF Then
                            new_car = newcomprec!car_no
                        Else
                            new_car = 0
                        End If
                    Else
                        new_car = "Common"
                    End If
                    'Added By Rahul
                ElseIf comprec15!invoice_no Like "*CC*" Or comprec15!invoice_no Like "*TDS*" Then
                    new_car = "Common"
                Else
                    new_car = "Vendor Cateogory"
                End If
             End If
            
            'new_car = "Common"
            'Replace(Combo1.Text, "'", "''")
            strcmd = "select distinct(id) from auto_Import where RMS_Cust='" & Replace(comprec15!company, "'", "''") & "'"
            DoEvents
            StatusBar1.Panels(1).Text = ctr
            Label3.Caption = "Total :" & inv_ctr
            DoEvents
            Set comprec1 = con1.Execute(strcmd)
             If Not comprec1.EOF Then
                      
'******************************Begin Of Main IF Condition added By rahul****************************************
             If (comprec15!basic <> 0 Or (comprec15!Parking_Toll + comprec15!other_taxes) <> 0) Then
                repcomprec1.AddNew
                repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "LINE"
                    If comprec15!invoice_no Like "*TDS*" Then
                        repcomprec1!Description = "TDS Recoverable"
                    ElseIf comprec15!invoice_no Like "*CC*" Then
                        repcomprec1!Description = "Credit Card Charges"
                    Else
                        repcomprec1!Description = "Standard Package"
                    End If
                repcomprec1!Amount = comprec15!basic
                
                'Code By RAHUL
                'repcomprec1!amount = -(comprec15!basic)
                
                repcomprec1!Term_Id = comprec15!Term_Id    'For 30 Net
                repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                repcomprec1!TRX_DATE = comprec15!VDATE
                repcomprec1!gl_DATE = comprec15!VDATE 'CDate("01-12-2005")
                repcomprec1!QUANTITY = 1
                repcomprec1!UNIT_SELLING_PRICE = comprec15!basic
                repcomprec1!UNIT_STANDARD_PRICE = comprec15!basic
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!UOM_CODE = "EA"
                repcomprec1!UOM_NAME = "Each"
                repcomprec1!ORG_ID = 101
                repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                repcomprec1!HEADER_ATTRIBUTE6 = Trim(comprec15!Car_Used_Date)
                repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                    If comprec15!Outstation_Amt > 0 Then
                        repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE14 = "No"
                    End If
                    
                    If comprec15!direct = True Then
                        repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE15 = "No"
                    End If
                
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id) 'Added By Rahul
                                '*****************************************
                'Program Batch Name to be change for different batches
                repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                '*****************************************
                
                repcomprec1!trx_number = Left(comprec15!invoice_no, 20) '& "-RR"
                repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                repcomprec1!SET_OF_BOOKS_ID = 1001
                repcomprec1!CURRENCY_CODE = "INR"
                
                '*****************************************
                'Program Invoice Name to be change for different branches
                'Combo2.Text
                    If repcomprec1!Amount < 0 And Combo2.Text = "CIPL Import3" Then
                        strcmd = "select * from INV_TYPE2 where branch_Name='" & comprec15!branch_bill_to & "'"
                    Else
                        strcmd = "select * from INV_TYPE where branch_Name='" & comprec15!branch_bill_to & "'"
                    End If
                Set comprec7 = con1.Execute(strcmd)
                    If Not comprec7.EOF Then
                        Cipl_Type = comprec7!Inv_Type
                    Else
                        Cipl_Type = ""
                    End If
                '---------------------------------------------------------addition by Rahul---
                repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                
                '------------------------------------------------------------end of code------
                '*****************************************
                'new_car = "Common"
                repcomprec1.Update
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009931"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND LOWER(Trim(FVTB.Description))='" & LCase(Trim(comprec15!Ship_To1)) & "'"
                
                'By Rahul
                'strcmd2 = strcmd2 & " AND ENABLED_FLAG = Y "
                
                Set comprec3 = con.Execute(strcmd2)
                If comprec3.EOF Then
                    Unload status
                    MsgBox comprec15!Ship_To1 & " Location is not matching in the Oracle for the Client."
                    Exit Sub
                End If
                

                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009932"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND FVTB.Description='" & new_car & "'"
                Set comprec4 = con.Execute(strcmd2)
            
               
               '*&***************************************
                'if product is drive from the system
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009934"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec15!product & "'"
                Set comprec5 = con.Execute(strcmd2)
                '**************************************************
                'Distribution Table to Be insert Here
                    
                
                repcomprec2.AddNew
                repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "REV"
                
                repcomprec2!Amount = comprec15!basic
                'Code by RAHUL
                'repcomprec2!amount = -(comprec15!basic)
                repcomprec2!Percent = 100
                
                If Not comprec3.EOF Then
                    repcomprec2!segment1 = comprec3!FLEX_VALUE  'Location Flexfield
                End If
                
                If Not comprec4.EOF Then
                    repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                End If
            
                If Not comprec5.EOF Then
                    repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE   'Product
                End If
                
                   If Combo7.Text = "OL" Then           'For Operating Lease Uploading
                        If Check1(1).Value = 0 Then
                            If comprec15!owned = True Then
                                repcomprec2!SEGMENT3 = "30005"  'Natural Acct For Revenue Owned Cars
                            Else
                                repcomprec2!SEGMENT3 = "30006"  'Natural Acct For Revenue Hired Cars
                            End If
                        Else
                            repcomprec2!SEGMENT3 = "42126"
                        End If
                   ElseIf Combo7.Text = "EasyCabs" Then
                        If comprec15!owned = True Then
                            repcomprec2!SEGMENT3 = "30003"  'Natural Acct For Revenue Owned Cars (Easy Cabs)
                        Else
                            repcomprec2!SEGMENT3 = "30003"  'Natural Acct For Revenue Hired Cars (Easy Cabs)
                        End If
                   Else                                   'For Revenue Uploading (Insta)
                        If comprec15!BusinessType = "EasyCabs" Then
                            repcomprec2!SEGMENT3 = "30014"  'Natural Acct For Easy Cabs
                        Else
                            If comprec15!DamageInvoice > 0 Then  'Damage Invoice add by Bk Sharma
                                repcomprec2!SEGMENT3 = "40211"  'Natural Acct For Damage Invoice
                            Else
                                If comprec15!VDPYN = True Then
                                    repcomprec2!SEGMENT3 = "30009"  'Natural Acct For Revenue Owned Cars
                                Else
                                    If comprec15!owned = True Then
                                        repcomprec2!SEGMENT3 = "30001"  'Natural Acct For Revenue Owned Cars
                                    Else
                                        repcomprec2!SEGMENT3 = "30002"  'Natural Acct For Revenue Hired Cars
                                    End If
                                End If
                            End If
                        End If
                   End If
                
                If comprec15!invoice_no Like "*TDS*" Then
                    'repcomprec2!SEGMENT3 = "14268"
                    'Code changed by rahul on 26 apr 2010
                    'repcomprec2!SEGMENT3 = "14269" 'Commented on behalf of Ajay Sharma on 30-09-2011
                    repcomprec2!SEGMENT3 = "14270"
                End If
                If comprec15!invoice_no Like "*CC*" Then
                    repcomprec2!SEGMENT3 = "43102"
                End If
                
                repcomprec2!ORG_ID = 101   'Org Id
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                repcomprec2.Update
                
                '-Distribution Line For Receivale Entry
                repcomprec2.AddNew
                repcomprec2!INTERFACE_LINE_ID = ctr
                repcomprec2!ACCOUNT_CLASS = "REC"
                repcomprec2!Amount = comprec15!basic
                'Code by Rahul
                'repcomprec2!amount = -(comprec15!basic)
                repcomprec2!Percent = 100
                
                strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009931"
                strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                strcmd2 = strcmd2 & " AND LOWER(Trim(FVTB.Description))='" & LCase(Trim(comprec15!branch_bill_to)) & "'"
                
                'By Rahul
                'strcmd2 = strcmd2 & " AND ENABLED_FLAG = Y "
                
                Set comprec6 = con.Execute(strcmd2)
                
                If Not comprec6.EOF Then
                    repcomprec2!segment1 = comprec6!FLEX_VALUE  'Location Flexfield
                End If
                
                If Not comprec4.EOF Then
                     repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
                End If
                
                If Not comprec5.EOF Then
                    repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE    'Product
                End If
                
                If Combo7.Text = "EasyCabs" Then
                    repcomprec2!SEGMENT3 = "13004"  'Natural Acct For Revenue Radio Taxi Drivers
                Else
                    repcomprec2!SEGMENT3 = "13001"  'Natural Acct For Revenue Owned Cars
                End If
                repcomprec2!ORG_ID = 101   'Org Id
                repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                repcomprec2.Update
                ctr = ctr + 1
                'Else
                'List2.AddItem comprec17!Company
                'End If
                
                
                '**************************NOT INCLUDEDE IN OUTSTANDING*********************
                
                '--------Begin Parking & Toll Taxes-------------------------------------------------------
                If (comprec15!Parking_Toll + comprec15!other_taxes) <> 0 Then
                    ' If countctr = 0 Then
                    repcomprec1.AddNew
                    repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "LINE"
                    repcomprec1!Description = "Parking & Toll Charges"
                    repcomprec1!Amount = (comprec15!Parking_Toll + comprec15!other_taxes)
                    
                    'Code by Rahul
                    'repcomprec1!amount = -(comprec15!Parking_Toll + comprec15!other_taxes)
                    
                    repcomprec1!Term_Id = comprec15!Term_Id
                    repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                    repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                    repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!gl_DATE = comprec15!VDATE
                    repcomprec1!QUANTITY = 1
                    
                    repcomprec1!UNIT_SELLING_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                    repcomprec1!UNIT_STANDARD_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                    repcomprec1!ATTRIBUTE1 = ctr - 1
                    repcomprec1!UOM_CODE = "EA"
                    repcomprec1!UOM_NAME = "Each"
                    repcomprec1!ORG_ID = 101
                    'repcomprec1!trx_number = Replace(Left(comprec15!invoice_no, 20), "'", "''") '& "-RR"
                    repcomprec1!trx_number = Left(comprec15!invoice_no, 20) '& "-RR"
                    repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                    repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                    repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                    repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                    repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                    repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                    repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                    repcomprec1!HEADER_ATTRIBUTE6 = comprec15!Car_Used_Date
                    repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                    repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                    repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                    repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                    repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                    repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                    repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                    
                    If comprec15!Outstation_Amt <> 0 Then
                        repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE14 = "No"
                    End If
                    
                    If comprec15!direct = True Then
                        repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE15 = "No"
                    End If
                    
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    '*************************************Changed by Rahul on 12 Nov 2009
                    'repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) & "Z"
                    '*************************************
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                   
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                                  
                    
                    p_Ctr = ctr
                    repcomprec1.Update
                    
                    '----------------For Distribution
                    repcomprec2.AddNew
                    repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "REV"
                    repcomprec2!Amount = (comprec15!Parking_Toll + comprec15!other_taxes)
                    
                    'Code by Rahul
                    'repcomprec2!amount = -(comprec15!Parking_Toll + comprec15!other_taxes)
                    
                    repcomprec2!Percent = 100
                    If Not comprec3.EOF Then
                        repcomprec2!segment1 = comprec3!FLEX_VALUE  'Location Flexfield
                    End If
                    
                    If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
                    End If
                    
                    If Not comprec5.EOF Then
                        repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE
                    End If
                    repcomprec2!ORG_ID = 101   'Org Id
                    repcomprec2!SEGMENT3 = "40301"    'Natural Acct For Car Parking
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec2.Update
                    ctr = ctr + 1
                End If
                '-----------------------End Parking & Toll Taxes------------------------------
                
                
                '--------Begin COR Tax-------------------------------------------------------
                If (comprec15!CORTax) <> 0 Then
                    ' If countctr = 0 Then
                    repcomprec1.AddNew
                    repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "LINE"
                    repcomprec1!Description = "COR TAX"
                    repcomprec1!Amount = (comprec15!CORTax)
                    
                    'Code by Rahul
                    'repcomprec1!amount = -(comprec15!CORTax)
                    
                    repcomprec1!Term_Id = comprec15!Term_Id
                    repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                    repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                    repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!gl_DATE = comprec15!VDATE
                    repcomprec1!QUANTITY = 1
                    
                    repcomprec1!UNIT_SELLING_PRICE = (comprec15!CORTax)
                    repcomprec1!UNIT_STANDARD_PRICE = (comprec15!CORTax)
                    repcomprec1!ATTRIBUTE1 = RevenuePart 'ctr - 1
                    repcomprec1!UOM_CODE = "EA"
                    repcomprec1!UOM_NAME = "Each"
                    repcomprec1!ORG_ID = 101
                    'repcomprec1!trx_number = Replace(Left(comprec15!invoice_no, 20), "'", "''") '& "-RR"
                    repcomprec1!trx_number = Left(comprec15!invoice_no, 20) '& "-RR"
                    repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                    repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                    repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                    repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                    repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                    repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                    repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                    repcomprec1!HEADER_ATTRIBUTE6 = comprec15!Car_Used_Date
                    repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                    repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                    repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                    repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                    repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                    repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                    repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                    
                    If comprec15!Outstation_Amt <> 0 Then
                        repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE14 = "No"
                    End If
                    
                    If comprec15!direct = True Then
                        repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE15 = "No"
                    End If
                    
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    '*************************************Changed by Rahul on 12 Nov 2009
                    'repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) & "Z"
                    '*************************************
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                   
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                                  
                    
                    p_Ctr = ctr
                    repcomprec1.Update
                    
                    '----------------For Distribution
                    repcomprec2.AddNew
                    repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "REV"
                    repcomprec2!Amount = (comprec15!CORTax)
                    
                    'Code by Rahul
                    'repcomprec2!amount = -(comprec15!Parking_Toll + comprec15!other_taxes)
                    
                    repcomprec2!Percent = 100
                    If Not comprec3.EOF Then
                        repcomprec2!segment1 = comprec3!FLEX_VALUE  'Location Flexfield
                    End If
                    
                    If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
                    End If
                    
                    If Not comprec5.EOF Then
                        repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE
                    End If
                    repcomprec2!ORG_ID = 101   'Org Id
                    repcomprec2!SEGMENT3 = "31016"    'Natural Acct For Car Parking
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec2.Update
                End If
                '-----------------------End COR Tax------------------------------
                
                '***********************Extra Amout for Easy Cabs start*******************************************************
                If comprec15!BusinessType = "EasyCabs" Then
                    Call add_data_Extra(comprec15!invoice_no, ctr, comprec15, repcomprec1, repcomprec2, comprec5, Cipl_Type)
                End If
                '***********************Extra Amout for Easy Cabs end  *******************************************************
              
                
                '--------------------Begin Service Tax Calculation : FOR TAX LINE----------------------------
                If comprec15!STAX <> 0 Then
                     'service tax calculation for Basic start*******************************************************************************
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!Amount = Round((comprec15!basic * comprec15!Service_Tax) / 100, 2)
                     
                     'Code by Rahul for negative invoices
                     'repcomprec1!amount = -(Round((comprec15!basic * comprec15!Service_Tax) / 100, 2))
                                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!Service_Tax
                     '--------------------------------------------------------------
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_CODE = "Service Tax @ " & comprec15!Service_Tax & "%"
                     '------------------------------------------------------------
                     
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   'require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '---System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round((comprec15!basic * comprec15!Service_Tax) / 100, 2)
                     '-----------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     'repcomprec2!segment1 = "000"
                     
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     Label9.Caption = getLocationcode(comprec15!Ship_To1) ' written for location code by Rajesh K Thanua
                     'repcomprec2!SEGMENT2 = "00000"
                     
                     If Not comprec4.EOF Then
                         repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     
                     'repcomprec2!SEGMENT4 ="000"
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                                 
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                    'service tax calculation for Basic end  *******************************************************************************
                    
                    '1 Start Swachh Bharat taxes for Basic ************************************************************************
                    
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!Amount = Round((comprec15!basic * comprec15!SwachhBharatTaxPercent) / 100, 2)
                     
                     'Code by Rahul for negative invoices
                     'repcomprec1!amount = -(Round((comprec15!basic * comprec15!SwachhBharatTaxPercent) / 100, 2))
                                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!SwachhBharatTaxPercent
                     '--------------------------------------------------------------
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_CODE = "Swachh Bharat Cess @ " & comprec15!SwachhBharatTaxPercent & "%"
                     '------------------------------------------------------------
                     
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   'require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '---System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round((comprec15!basic * comprec15!SwachhBharatTaxPercent) / 100, 2)
                     '-----------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     'repcomprec2!segment1 = "000"
                     
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     Label9.Caption = getLocationcode(comprec15!Ship_To1) ' written for location code by Rajesh K Thanua
                     'repcomprec2!SEGMENT2 = "00000"
                     
                     If Not comprec4.EOF Then
                         repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     
                     'repcomprec2!SEGMENT4 ="000"
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                                 
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                    
                    'End of swacch bharat taxes end  for Basic ***************************************************************************
                    
                    '1 Start Krishi kalyan taxes  for Basic ************************************************************************
                    
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!Amount = Round((comprec15!basic * comprec15!KrishiKalyanTaxPercent) / 100, 2)
                     
                     'Code by Rahul for negative invoices
                     'repcomprec1!amount = -(Round((comprec15!basic * comprec15!KrishiKalyanTaxPercent) / 100, 2))
                                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!KrishiKalyanTaxPercent
                     '--------------------------------------------------------------
                     '-----------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_CODE = "Krishi Kalyan Cess @ " & comprec15!KrishiKalyanTaxPercent & "%"
                     '------------------------------------------------------------
                     
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   'require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '---System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round((comprec15!basic * comprec15!KrishiKalyanTaxPercent) / 100, 2)
                     '-----------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     'repcomprec2!segment1 = "000"
                     
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     Label9.Caption = getLocationcode(comprec15!Ship_To1) ' written for location code by Rajesh K Thanua
                     'repcomprec2!SEGMENT2 = "00000"
                     
                     If Not comprec4.EOF Then
                         repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     
                     'repcomprec2!SEGMENT4 ="000"
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                                 
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                    
                    'End of Krishi kalyan taxes end for Basic *******************
                    
                    
                    
                    '--------------------------------fOR CESS cALCULATION---------------------------
                     ' start cess calculation for Basic
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!Amount = Round((comprec15!basic * comprec15!cess_Tax) / 100, 2)
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '-------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = 3
                     repcomprec1!TAX_CODE = "Service Tax Education Cess @ 3%"
                     '------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 1
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text)
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '------System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round((comprec15!basic * comprec15!cess_Tax) / 100, 2)
                                   
                     '------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                         repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                'End If
                ' end cess calculation for Basic
                '--------------------End Service Tax Calculation----------------------------
                
                
                 '------------------------Begin for Stax Calculation----------------------------
                'If (comprec15!Parking_Toll + comprec15!other_taxes) <> 0 And comprec15!STAX <> 0 Then
             If (comprec15!Parking_Toll + comprec15!other_taxes) <> 0 Then
                     ' ---------------------------Tax on Parking
                     'service tax calculation start for Parking*******************************************************************************
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!Service_Tax) / 100, 2)
                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!Service_Tax
                     repcomprec1!TAX_CODE = "Service Tax @ " & comprec15!Service_Tax & "%"
                     '--------------------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!Service_Tax) / 100, 2)
                     '----------------------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                     'End If
                     'service tax calculation end for Parking *******************************************************************************
                     
                     
                     '2 Start of Swachh Bharat tax start for Parking ************************************************************************************
                      ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!SwachhBharatTaxPercent) / 100, 2)
                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!SwachhBharatTaxPercent
                     repcomprec1!TAX_CODE = "Swachh Bharat Cess @ " & comprec15!SwachhBharatTaxPercent & "%"
                     '--------------------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!SwachhBharatTaxPercent) / 100, 2)
                     '----------------------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                     'End of Swachh Bharat tax end for Parking ************************************************************************************
                     
                     '2 Start of Krishi Kalyan tax start for Parking ************************************************************************************
                      ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!KrishiKalyanTaxPercent) / 100, 2)
                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!KrishiKalyanTaxPercent
                     repcomprec1!TAX_CODE = "Krishi Kalyan Cess @ " & comprec15!KrishiKalyanTaxPercent & "%"
                     '--------------------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!KrishiKalyanTaxPercent) / 100, 2)
                     '----------------------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                     'End of Krishi Kalyan tax end for Parking ********************************************************************
                     
                     '--------------------------------fOR cESS cALCULATION for Parking---------------------------
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     
                     repcomprec1!Amount = Round((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!cess_Tax / 100, 2)
                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = 3
                     repcomprec1!TAX_CODE = "Service Tax Education Cess @ 3%"
                     '--------------------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 1
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     '---------------------------------------------------------------------------
                    
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     repcomprec2!Amount = Round(comprec15!STAX, 2)
                     'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 0.08) / 100, 2)
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                End If
                'End If
                '------------------------End for Cess Calculation for Parking----------------------------
                
                '------------------------Begin for Stax Calculation for CORTax----------------------------
                If (comprec15!CORTax) <> 0 Then
                     ' ---------------------------Tax on Parking for CORTax
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round(((comprec15!CORTax) * comprec15!Service_Tax) / 100, 2)
                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = comprec15!Service_Tax
                     repcomprec1!TAX_CODE = "Service Tax @ " & comprec15!Service_Tax & "%"
                     '--------------------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     
                     '----------------For Distribution
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec2!Amount = Round(((comprec15!CORTax) * comprec15!Service_Tax) / 100, 2)
                     '----------------------------------------------------------
                     repcomprec2!Percent = 100
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                     'End If
                     '--------------------------------fOR cESS cALCULATION for CORTax---------------------------
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     
                     repcomprec1!Amount = Round((comprec15!CORTax) * comprec15!cess_Tax / 100, 2)
                     
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     
                     '------------System Will Pickup the Tax Value from System
                     repcomprec1!TAX_RATE = 3
                     repcomprec1!TAX_CODE = "Service Tax Education Cess @ 3%"
                     '--------------------------------------------------------
                     repcomprec1!TAX_PRECEDENCE = 1
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     '---------------------------------------------------------------------------
                    
                     repcomprec2.AddNew
                     repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     repcomprec2!Amount = Round(comprec15!STAX, 2)
                     'Round(((comprec15!CORTax) * 0.08) / 100, 2)
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     'repcomprec2!segment1 = "000"
                     repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                     'repcomprec2!SEGMENT2 = "00000"
                     If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                     End If
                     'repcomprec2!SEGMENT3 = "25004"
                     If comprec15!branch_bill_to Like "Limo*" Then
                        repcomprec2!SEGMENT3 = "25019"
                     Else
                        'repcomprec2!SEGMENT3 = "25004"
                        If Combo7.Text = "OL" Then
                            repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
                        Else
                            repcomprec2!SEGMENT3 = "25004" 'service tax code for others
                        End If
                     End If
                     repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     'repcomprec2!SEGMENT4 = "12"
                     repcomprec2!ORG_ID = 101
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                     repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                     repcomprec2.Update
                End If
                End If
                '------------------------End for Cess Calculation COR TAX----------------------------
                
               
                '********************************Vat Start*******************************************
                '--------------------------------Begin FOR vat cALCULATION---------------------------
               
                'If (comprec15!CITY_NAME = "DELHI" Or comprec15!company = "Self Drive") And comprec15!VAT <> 0 Then
                'begin Vat basic
                If comprec15!VAT <> 0 Then
                    ctr = ctr + 1
                    repcomprec1.AddNew
                    repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "TAX"
                    repcomprec1!Description = "Tax line"
                                 
                    repcomprec1!Amount = Round((comprec15!basic * comprec15!Vat_Tax) / 100, 2)
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!TAX_RATE = comprec15!Vat_Tax
                    repcomprec1!TAX_CODE = "VAT @ " & comprec15!Vat_Tax & "%"
                    repcomprec1!TAX_PRECEDENCE = 0
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!ORG_ID = 101
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    
                    
                    repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    '----------------For Distribution
                    repcomprec2.AddNew
                    repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "TAX"
                    repcomprec2!Amount = Round((comprec15!basic * comprec15!Vat_Tax) / 100, 2)
                    repcomprec2!Percent = 100
                    'repcomprec2!CODE_COMBINATION_ID = 1157
                    'repcomprec2!segment1 = "000"
                    repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                    'repcomprec2!SEGMENT2 = "00000"
                    
                    If Not comprec4.EOF Then
                            repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                    End If
                    
                    If Combo7.Text = "OL" Then
                        repcomprec2!SEGMENT3 = comprec15!Package_Name
                    Else
                        repcomprec2!SEGMENT3 = "25007"
                    End If
                    'Updated by Rahul on 30-APR-2008
                    'repcomprec2!SEGMENT3 = comprec15!Package_Name
                                    
                    repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                    'repcomprec2!SEGMENT4 = "12"
                    repcomprec2!ORG_ID = 101
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec2.Update
                'End If
                'end Vat basic
                '--------------------------------End FOR vat cALCULATION---------------------------

                '--------------------------------fOR vat cALCULATION---------------------------
                'If (comprec15!CITY_NAME = "DELHI" Or comprec15!company = "Self Drive") And comprec15!VAT <> 0 And (comprec15!Parking_Toll + comprec15!other_taxes) <> 0 Then
                'begin Vat parking
                If (comprec15!Parking_Toll + comprec15!other_taxes) <> 0 Then
                    ctr = ctr + 1
                    repcomprec1.AddNew
                    repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "TAX"
                    repcomprec1!Description = "Tax line"
                    'repcomprec1!Amount = Round(comprec15!VAT, 2)
                    repcomprec1!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * comprec15!Vat_Tax) / 100, 2)
                                    
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    
                    repcomprec1!TAX_RATE = comprec15!Vat_Tax
                    repcomprec1!TAX_CODE = "VAT @ " & comprec15!Vat_Tax & "%"
                    repcomprec1!TAX_PRECEDENCE = 0
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!ORG_ID = 101
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    
                    '---------------FOR dISTRIBUTION
                    
                    repcomprec2.AddNew
                    repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "TAX"
                    repcomprec2!Amount = Round(comprec15!VAT, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 12.5) / 100, 2)
                    
                    repcomprec2!Percent = 100
                    'repcomprec2!CODE_COMBINATION_ID = 1157
                    'repcomprec2!segment1 = "000"
                    repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                    'repcomprec2!SEGMENT2 = "00000"
                    
                    If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                    End If
                    
                    If Combo7.Text = "OL" Then
                        repcomprec2!SEGMENT3 = comprec15!Package_Name
                    Else
                        repcomprec2!SEGMENT3 = "25007"
                    End If
                    'repcomprec2!SEGMENT3 = "25007"
                    
                    'Updated By Rahul on 30-APR-2008
                    'repcomprec2!SEGMENT3 = comprec15!Package_Name
                    repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                    'repcomprec2!SEGMENT4 = "12"
                    repcomprec2!ORG_ID = 101
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec2.Update
                End If
                'end Vat parking
                'Start Vat Cor TAx
          
                '--------------------------------fOR vat cALCULATION---------------------------
                'begin Vat cortax
                If (comprec15!CORTax) <> 0 Then
                    ctr = ctr + 1
                    repcomprec1.AddNew
                    repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "TAX"
                    repcomprec1!Description = "Tax line"
                    'repcomprec1!Amount = Round(comprec15!VAT, 2)
                    repcomprec1!Amount = Round(((comprec15!CORTax) * comprec15!Vat_Tax) / 100, 2)
                                    
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    
                    repcomprec1!TAX_RATE = comprec15!Vat_Tax
                    repcomprec1!TAX_CODE = "VAT @ " & comprec15!Vat_Tax & "%"
                    repcomprec1!TAX_PRECEDENCE = 0
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!ORG_ID = 101
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    
                    '---------------FOR dISTRIBUTION
                    
                    repcomprec2.AddNew
                    repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "TAX"
                    repcomprec2!Amount = Round(comprec15!VAT, 2) 'Round(((comprec15!CORTax) * 12.5) / 100, 2)
                    
                    repcomprec2!Percent = 100
                    'repcomprec2!CODE_COMBINATION_ID = 1157
                    'repcomprec2!segment1 = "000"
                    repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
                    'repcomprec2!SEGMENT2 = "00000"
                    
                    If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
                    End If
                    
                    If Combo7.Text = "OL" Then
                        repcomprec2!SEGMENT3 = comprec15!Package_Name
                    Else
                        repcomprec2!SEGMENT3 = "25007"
                    End If
                    'repcomprec2!SEGMENT3 = "25007"
                    
                    'Updated By Rahul on 30-APR-2008
                    'repcomprec2!SEGMENT3 = comprec15!Package_Name
                    repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                    'repcomprec2!SEGMENT4 = "12"
                    repcomprec2!ORG_ID = 101
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
                    repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
                    repcomprec2.Update
                End If
                'end Vat cortax
                'End vat Cor TAx
                '********************************Vat End  *******************************************
                
                End If
                End If
            Else
                
                Set repcomprec4 = New ADODB.Recordset
                repcomprec4.Open "Error_List", con1, adOpenDynamic, adLockOptimistic
                repcomprec4.AddNew
                repcomprec4!company = comprec15!company
                repcomprec4.Update
                List2.AddItem comprec15!company & "---" & comprec15!invoice_no
            End If
            comprec15.MoveNext
            inv_ctr = inv_ctr + 1
    Loop
End If
        
       strcmd = "update id_code set id='" & ctr & "'"
       Set repcomprec = con1.Execute(strcmd)

        comprec15.Close
    If Combo7.Text = "OL" Then
        strcmd = "select count(*) as net from   Invoice_Dec_OL "
        strcmd = strcmd & " where  id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0"
    ElseIf Combo7.Text = "EasyCabs" Then
        strcmd = "select count(*) as net from Invoice_Dec_EasyCabs "
        strcmd = strcmd & " where  id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0"
    Else
        strcmd = "select count(*) as net from Invoice_Dec "
        strcmd = strcmd & " where  id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0"
    End If
        Set comprec = con1.Execute(strcmd)
        If Not comprec.EOF Then
            MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
        End If
        Unload status

Exit Sub
End Sub

Public Sub add_data_Extra(ByRef InvoiceNo As String, ByRef ctr As Variant, ByRef comprec15 As ADODB.Recordset, ByRef repcomprec1 As ADODB.Recordset, ByRef repcomprec2 As ADODB.Recordset, ByRef comprec5 As ADODB.Recordset, ByRef Cipl_Type As String)
    '***********************Extra Amout for Easy Cabs start*******************************************************
    Dim comprecExtraAmt As ADODB.Recordset
    
    strExtraAmt = "select * from CorIntCarBookingExtraAmount where replace(replace(Invoice_No, ' AA',''),' BB','') = 'replace(replace(" & comprec15!invoice_no & ", ' AA',''),' BB','')'"
    Set comprecExtraAmt = con1.Execute(strExtraAmt)

    If Not comprecExtraAmt.EOF Then
        Do While Not comprecExtraAmt.EOF
        'Code here for uploading
             ctr = ctr + 1
             repcomprec1.AddNew
             repcomprec1!INTERFACE_LINE_ID = ctr
             repcomprec1!LINE_TYPE = "LINE"
             repcomprec1!Description = comprecExtraAmt!ExtrasDesc  'added the description dynamically on 19-Oct-2016
             repcomprec1!Amount = (comprecExtraAmt!ExtraAmount)  'added the amount dynamically on 19-Oct-2016

             repcomprec1!Term_Id = comprec15!Term_Id
             repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
             repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
             repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
             repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
             repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
             repcomprec1!CONVERSION_TYPE = "User"
             repcomprec1!CONVERSION_RATE = 1
             repcomprec1!TRX_DATE = comprec15!VDATE
             repcomprec1!gl_DATE = comprec15!VDATE
             repcomprec1!QUANTITY = 1

             repcomprec1!UNIT_SELLING_PRICE = (comprecExtraAmt!ExtraAmount) 'added the amount dynamically on 19-Oct-2016
             repcomprec1!UNIT_STANDARD_PRICE = (comprecExtraAmt!ExtraAmount) 'added the amount dynamically on 19-Oct-2016
             repcomprec1!ATTRIBUTE1 = RevenuePart 'ctr - 1
             repcomprec1!UOM_CODE = "EA"
             repcomprec1!UOM_NAME = "Each"
             repcomprec1!ORG_ID = 101

             repcomprec1!trx_number = Left(comprec15!invoice_no, 20)
             repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
             repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
             repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
             repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
             repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
             repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
             repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
             repcomprec1!HEADER_ATTRIBUTE6 = comprec15!Car_Used_Date
             repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
             repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
             repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
             repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
             repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
             repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
             repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)

             If comprec15!Outstation_Amt <> 0 Then
                 repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
             Else
                 repcomprec1!HEADER_ATTRIBUTE14 = "No"
             End If

             If comprec15!direct = True Then
                 repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
             Else
                 repcomprec1!HEADER_ATTRIBUTE15 = "No"
             End If

             repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
             repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
             '*************************************Changed by Rahul on 12 Nov 2009
             'repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) & "Z"
             '*************************************
             repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
             repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
             repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
             repcomprec1!SET_OF_BOOKS_ID = 1001
             repcomprec1!CURRENCY_CODE = "INR"

             repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type

             p_Ctr = ctr
             repcomprec1.Update

             '----------------For Distribution
             repcomprec2.AddNew
             repcomprec2!INTERFACE_LINE_ID = ctr
             repcomprec2!ACCOUNT_CLASS = "REV"
             repcomprec2!Amount = (comprecExtraAmt!ExtraAmount) 'added the amount dynamically on 19-Oct-2016

             repcomprec2!Percent = 100
             If Not comprec3.EOF Then
                 repcomprec2!segment1 = comprec3!FLEX_VALUE  'Location Flexfield
             End If

             If Not comprec4.EOF Then
                 repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
             End If

             If Not comprec5.EOF Then
                 repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE
             End If
             repcomprec2!ORG_ID = 101   'Org Id

             If (comprecExtraAmt!ExtrasDesc = "Call center convenience charges") Then
                 repcomprec2!SEGMENT3 = "30008"    'Natural Acct call center convenience charges
             ElseIf (comprecExtraAmt!ExtrasDesc = "NCR Entry Charges") Then
                 repcomprec2!SEGMENT3 = "40301"    'Natural Acct For NCR Entry Charges
             ElseIf (comprecExtraAmt!ExtrasDesc = "Airport Parking") Then
                 repcomprec2!SEGMENT3 = "30004"    'Natural Acct For Airport Parking
             Else
                 repcomprec2!SEGMENT3 = "30004"    'Natural Acct For Airport Pickup
             End If

             repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
             repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
             repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
             repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
             repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
             repcomprec2.Update
             
            'Calling service tax sub method start
            Call add_data_ExtraTaxes(comprecExtraAmt, ctr, comprec15, repcomprec1, repcomprec2, comprec5, Cipl_Type)
            'Calling service tax sub method end
             
        comprecExtraAmt.MoveNext
        Loop
    End If
    '***********************Extra Amout for Easy Cabs end  *******************************************************
End Sub

Public Sub add_data_ExtraTaxes(ByRef comprecExtraAmt As ADODB.Recordset, ByRef ctr As Variant, ByRef comprec15 As ADODB.Recordset, ByRef repcomprec1 As ADODB.Recordset, ByRef repcomprec2 As ADODB.Recordset, ByRef comprec5 As ADODB.Recordset, ByRef Cipl_Type As String)
    If comprec15!STAX <> 0 Then
         'service tax calculation for Basic start*******************************************************************************
         ctr = ctr + 1
         repcomprec1.AddNew
         repcomprec1!INTERFACE_LINE_ID = ctr
         repcomprec1!LINE_TYPE = "TAX"
         repcomprec1!Description = "Tax line"
         
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!Service_Tax) / 100, 2)
         
         'Code by Rahul for negative invoices
         'repcomprec1!amount = -(Round((comprec15!basic * comprec15!Service_Tax) / 100, 2))
                         
         repcomprec1!CONVERSION_TYPE = "User"
         repcomprec1!CONVERSION_RATE = 1
         repcomprec1!TRX_DATE = comprec15!VDATE
         
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!TAX_RATE = comprec15!Service_Tax
         '--------------------------------------------------------------
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!TAX_CODE = "Service Tax @ " & comprec15!Service_Tax & "%"
         '------------------------------------------------------------
         
         repcomprec1!TAX_PRECEDENCE = 0
         repcomprec1!ATTRIBUTE1 = ctr
         repcomprec1!ORG_ID = 101
         repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec1!SET_OF_BOOKS_ID = 1001
         repcomprec1!CURRENCY_CODE = "INR"
         
         
         repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
         
         repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   'require from tax
         repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
         repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1.Update
         
         '----------------For Distribution
         repcomprec2.AddNew
         repcomprec2!INTERFACE_LINE_ID = ctr
         repcomprec2!ACCOUNT_CLASS = "TAX"
         
         '---System Will Pickup the Tax Value from System
         repcomprec2!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!Service_Tax) / 100, 2)
         '-----------------------------------------------
         repcomprec2!Percent = 100
         'repcomprec2!CODE_COMBINATION_ID = 1153
         'repcomprec2!segment1 = "000"
         
         repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
         Label9.Caption = getLocationcode(comprec15!Ship_To1) ' written for location code by Rajesh K Thanua
         'repcomprec2!SEGMENT2 = "00000"
         
         If Not comprec4.EOF Then
             repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
         End If
         
         If comprec15!branch_bill_to Like "Limo*" Then
            repcomprec2!SEGMENT3 = "25019"
         Else
            If Combo7.Text = "OL" Then
                repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
            Else
                repcomprec2!SEGMENT3 = "25004" 'service tax code for others
            End If
         End If
         
         'repcomprec2!SEGMENT4 ="000"
         repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     
         repcomprec2!ORG_ID = 101
         repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec2.Update
        'service tax calculation for Basic end  *******************************************************************************
        
        '1 Start Swachh Bharat taxes for Basic ************************************************************************
        
         ctr = ctr + 1
         repcomprec1.AddNew
         repcomprec1!INTERFACE_LINE_ID = ctr
         repcomprec1!LINE_TYPE = "TAX"
         repcomprec1!Description = "Tax line"
         
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!SwachhBharatTaxPercent) / 100, 2)
         
         'Code by Rahul for negative invoices
         'repcomprec1!amount = -(Round((comprecExtraAmt!ExtraAmount * comprec15!SwachhBharatTaxPercent) / 100, 2))
                         
         repcomprec1!CONVERSION_TYPE = "User"
         repcomprec1!CONVERSION_RATE = 1
         repcomprec1!TRX_DATE = comprec15!VDATE
         
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!TAX_RATE = comprec15!SwachhBharatTaxPercent
         '--------------------------------------------------------------
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!TAX_CODE = "Swachh Bharat Cess @ " & comprec15!SwachhBharatTaxPercent & "%"
         '------------------------------------------------------------
         
         repcomprec1!TAX_PRECEDENCE = 0
         repcomprec1!ATTRIBUTE1 = ctr
         repcomprec1!ORG_ID = 101
         repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec1!SET_OF_BOOKS_ID = 1001
         repcomprec1!CURRENCY_CODE = "INR"
         
         
         repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
         
         repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   'require from tax
         repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
         repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1.Update
         
         '----------------For Distribution
         repcomprec2.AddNew
         repcomprec2!INTERFACE_LINE_ID = ctr
         repcomprec2!ACCOUNT_CLASS = "TAX"
         
         '---System Will Pickup the Tax Value from System
         repcomprec2!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!SwachhBharatTaxPercent) / 100, 2)
         '-----------------------------------------------
         repcomprec2!Percent = 100
         'repcomprec2!CODE_COMBINATION_ID = 1153
         'repcomprec2!segment1 = "000"
         
         repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
         Label9.Caption = getLocationcode(comprec15!Ship_To1) ' written for location code by Rajesh K Thanua
         'repcomprec2!SEGMENT2 = "00000"
         
         If Not comprec4.EOF Then
             repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
         End If
         
         If comprec15!branch_bill_to Like "Limo*" Then
            repcomprec2!SEGMENT3 = "25019"
         Else
            If Combo7.Text = "OL" Then
                repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
            Else
                repcomprec2!SEGMENT3 = "25004" 'service tax code for others
            End If
         End If
         
         'repcomprec2!SEGMENT4 ="000"
         repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     
         repcomprec2!ORG_ID = 101
         repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec2.Update
        
        'End of swacch bharat taxes end  for Basic ***************************************************************************
        
        '1 Start Krishi kalyan taxes  for Basic ************************************************************************
        
         ctr = ctr + 1
         repcomprec1.AddNew
         repcomprec1!INTERFACE_LINE_ID = ctr
         repcomprec1!LINE_TYPE = "TAX"
         repcomprec1!Description = "Tax line"
         
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!KrishiKalyanTaxPercent) / 100, 2)
         
         'Code by Rahul for negative invoices
         'repcomprec1!amount = -(Round((comprec15!basic * comprec15!KrishiKalyanTaxPercent) / 100, 2))
                         
         repcomprec1!CONVERSION_TYPE = "User"
         repcomprec1!CONVERSION_RATE = 1
         repcomprec1!TRX_DATE = comprec15!VDATE
         
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!TAX_RATE = comprec15!KrishiKalyanTaxPercent
         '--------------------------------------------------------------
         '-----------------System Will Pickup the Tax Value from System
         repcomprec1!TAX_CODE = "Krishi Kalyan Cess @ " & comprec15!KrishiKalyanTaxPercent & "%"
         '------------------------------------------------------------
         
         repcomprec1!TAX_PRECEDENCE = 0
         repcomprec1!ATTRIBUTE1 = ctr
         repcomprec1!ORG_ID = 101
         repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec1!SET_OF_BOOKS_ID = 1001
         repcomprec1!CURRENCY_CODE = "INR"
         
         
         repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
         
         repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   'require from tax
         repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
         repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1.Update
         
         '----------------For Distribution
         repcomprec2.AddNew
         repcomprec2!INTERFACE_LINE_ID = ctr
         repcomprec2!ACCOUNT_CLASS = "TAX"
         
         '---System Will Pickup the Tax Value from System
         repcomprec2!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!KrishiKalyanTaxPercent) / 100, 2)
         '-----------------------------------------------
         repcomprec2!Percent = 100
         'repcomprec2!CODE_COMBINATION_ID = 1153
         'repcomprec2!segment1 = "000"
         
         repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
         Label9.Caption = getLocationcode(comprec15!Ship_To1) ' written for location code by Rajesh K Thanua
         'repcomprec2!SEGMENT2 = "00000"
         
         If Not comprec4.EOF Then
             repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
         End If
         
         If comprec15!branch_bill_to Like "Limo*" Then
            repcomprec2!SEGMENT3 = "25019"
         Else
            If Combo7.Text = "OL" Then
                repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
            Else
                repcomprec2!SEGMENT3 = "25004" 'service tax code for others
            End If
         End If
         
         'repcomprec2!SEGMENT4 ="000"
         repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
                     
         repcomprec2!ORG_ID = 101
         repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec2.Update
        
        'End of Krishi kalyan taxes end for Basic *******************
        
        
        
        '--------------------------------fOR CESS cALCULATION---------------------------
         ' start cess calculation for Basic
         ctr = ctr + 1
         repcomprec1.AddNew
         repcomprec1!INTERFACE_LINE_ID = ctr
         repcomprec1!LINE_TYPE = "TAX"
         repcomprec1!Description = "Tax line"
         
         '------------System Will Pickup the Tax Value from System
         repcomprec1!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!cess_Tax) / 100, 2)
         repcomprec1!CONVERSION_TYPE = "User"
         repcomprec1!CONVERSION_RATE = 1
         repcomprec1!TRX_DATE = comprec15!VDATE
         
         '-------System Will Pickup the Tax Value from System
         repcomprec1!TAX_RATE = 3
         repcomprec1!TAX_CODE = "Service Tax Education Cess @ 3%"
         '------------------------------------------
         repcomprec1!TAX_PRECEDENCE = 1
         repcomprec1!ATTRIBUTE1 = ctr
         repcomprec1!ORG_ID = 101
         
         repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec1!BATCH_SOURCE_NAME = Trim(Combo2.Text)
         repcomprec1!SET_OF_BOOKS_ID = 1001
         repcomprec1!CURRENCY_CODE = "INR"
         
         repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
         
         repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-RR"
         repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec1.Update
         
         '----------------For Distribution
         repcomprec2.AddNew
         repcomprec2!INTERFACE_LINE_ID = ctr
         repcomprec2!ACCOUNT_CLASS = "TAX"
         
         '------System Will Pickup the Tax Value from System
         repcomprec2!Amount = Round((comprecExtraAmt!ExtraAmount * comprec15!cess_Tax) / 100, 2)
                       
         '------------------------------------------
         repcomprec2!Percent = 100
         'repcomprec2!CODE_COMBINATION_ID = 1153
         'repcomprec2!segment1 = "000"
         repcomprec2!segment1 = getLocationcode(comprec15!Ship_To1)
         'repcomprec2!SEGMENT2 = "00000"
         If Not comprec4.EOF Then
             repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE   'Car Number Flexfield
         End If
         
         'repcomprec2!SEGMENT3 = "25004"
         If comprec15!branch_bill_to Like "Limo*" Then
            repcomprec2!SEGMENT3 = "25019"
         Else
            'repcomprec2!SEGMENT3 = "25004"
            If Combo7.Text = "OL" Then
                repcomprec2!SEGMENT3 = "25024" 'service tax code for operating lease
            Else
                repcomprec2!SEGMENT3 = "25004" 'service tax code for others
            End If
         End If
         
         repcomprec2!SEGMENT4 = getProductCode(comprec15!product)
         'repcomprec2!SEGMENT4 = "12"
         repcomprec2!ORG_ID = 101
         repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
         repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
         repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
         repcomprec2!INTERFACE_LINE_ATTRIBUTE8 = Trim(comprec15!Id)  'Added By Rahul
         repcomprec2!INTERIM_TAX_SEGMENT30 = Trim(Combo2.Text) '"CIPL Import4"
         repcomprec2.Update
    End If
End Sub


'-----------------------------------------------------------------
Private Function getLocationcode(tShipto As String) As String
Dim tRS As New Recordset

tRS.Open "select id from location where name='" & tShipto & "'", con1, adOpenDynamic, adLockOptimistic
Label9.Caption = tRS.Fields("id")
getLocationcode = tRS.Fields("id")
End Function

Private Function getProductCode(tShipto As String) As String
Dim tRS As New Recordset

tRS.Open "select flex_value from product where product='" & tShipto & "'", con1, adOpenDynamic, adLockOptimistic
getProductCode = tRS.Fields("flex_value")
End Function
'------------------------------------------------------------------


Private Sub Command2_Click()
    MsgBox List1.ListCount
End Sub


Private Sub Command3_Click()
    If Combo7.Text = "OL" Then
        strcmd = "select count(*) as gr from Invoice_Dec_OL "
        strcmd = strcmd & " where id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0"
    ElseIf Combo7.Text = "EasyCabs" Then
        strcmd = "select count(*) as gr from Invoice_Dec_EasyCabs "
        strcmd = strcmd & " where id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0"
    Else
        strcmd = "select count(*) as gr from Invoice_Dec "
        strcmd = strcmd & " where id >='" & Combo5.Text & "'"
        strcmd = strcmd & " AND id <='" & Combo6.Text & "'"
        strcmd = strcmd & " and uploaded=0"
    End If
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        Label5.Caption = "Total " & comprec!gr
    Else
        Label5.Caption = "Total " & 0
    End If
End Sub

Private Sub Command4_Click()
Unload Me
End Sub

Private Sub Command5_Click()
strcmd = "select distinct trx_number from ra_interface_lines_all where interface_status='P' and batch_source_name = '" & Combo2.Text & "'"
'where batch_source_name='" & Combo2.Text & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Set repcomprec1 = New ADODB.Recordset
        'a = Mid(comprec!TRX_NUMBER, 0, Len(comprec!TRX_NUMBER) - 3)
        If Combo7.Text = "OL" Then
            strcmd1 = "select * from Invoice_Dec_OL where invoice_no='" & comprec!trx_number & "'"
        ElseIf Combo7.Text = "EasyCabs" Then
            strcmd1 = "select * from Invoice_Dec_EasyCabs where invoice_no='" & comprec!trx_number & "'"
        Else
            strcmd1 = "select * from Invoice_Dec where invoice_no='" & comprec!trx_number & "'"
        End If
        repcomprec1.Open strcmd1, con1, adOpenDynamic, adLockOptimistic
        If Not repcomprec1.EOF Then
            repcomprec1!Uploaded = 1
            DoEvents
            repcomprec1.Update
            StatusBar1.Panels(1).Text = "Updating Invoice Number:" & comprec!trx_number
            DoEvents
        End If
    comprec.MoveNext
    Loop
    
End If
    strcmd = "select distinct count(trx_number) as net from ra_interface_lines_all where interface_status='P' and batch_source_name = '" & Combo2.Text & "'"
    Set comprec = con.Execute(strcmd)
    If Not comprec.EOF Then
        MsgBox comprec!Net & " Invoices Gets Updated in SQL DATABASE !", vbInformation, "UPDATED"
        Exit Sub
    End If
End Sub

'-------------------------------------------------------------------------------
Private Sub Option1_Click()
If Option1.Value = True Then
    Frame2(0).ZOrder 0
Else
    Frame2(0).ZOrder 1
End If
End Sub

Private Sub Option2_Click()
If Option2.Value = True Then
    Frame2(1).ZOrder 0
Else
    Frame2(1).ZOrder 1
End If
End Sub

Public Sub add_brn()
   Combo1.Clear
   If Combo7.Text = "OL" Then
    strcmd = "select CITY_name from Invoice_Dec_OL group by CITY_NAME"
   ElseIf Combo7.Text = "EasyCabs" Then
    strcmd = "select CITY_name from Invoice_Dec_EasyCabs group by CITY_NAME"
   Else
    strcmd = "select CITY_name from Invoice_Dec group by CITY_NAME"
   End If
  Set comprec = con1.Execute(strcmd)
   If Not comprec.EOF Then
       Do While Not comprec.EOF
           Combo1.AddItem comprec!CITY_NAME
           comprec.MoveNext
       Loop
   Combo1.Text = Combo1.List(0)
   End If
   End Sub

Public Sub add_rem()
Combo4.Clear
If Combo7.Text = "OL" Then
    strcmd = "select distinct remark from Invoice_Dec_OL"
ElseIf Combo7.Text = "EasyCabs" Then
    strcmd = "select distinct remark from Invoice_Dec_EasyCabs"
Else
    strcmd = "select distinct remark from Invoice_Dec"
End If
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo4.AddItem comprec!REMARK & ""
        comprec.MoveNext
    Loop
End If
Combo4.Text = Combo4.List(0)
End Sub

Private Sub Form_Load()
Combo7.Clear
Combo5.Clear
Combo6.Clear
Combo7.AddItem "Rent A Car"
Combo7.AddItem "OL"
Combo7.AddItem "EasyCabs"
Combo7.Text = Combo7.List(0)
For i = 1 To 10000
    Combo5.AddItem i
    Combo6.AddItem i
Next

'strcmd = "select distinct ID from Invoice_Dec  "
'strcmd = strcmd & " where Uploaded = 0 "
'Set comprec = con1.Execute(strcmd)
'
'If Not comprec.EOF Then
'    Do While Not comprec.EOF
'        Combo5.AddItem comprec!Id
'        Combo6.AddItem comprec!Id
'        comprec.MoveNext
'    Loop
'End If

Combo5.Text = Combo5.List(0)
Combo6.Text = Combo6.List(0)


'Combo5.Text = Combo5.List(0)
'Combo6.Text = Combo6.List(0)
'add_rem
Combo3.Clear
strcmd = "select distinct batch_source_name from ra_interface_lines_all"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo3.AddItem comprec!BATCH_SOURCE_NAME
        comprec.MoveNext
    Loop
End If

Combo3.Text = Combo3.List(0)
Combo2.Clear

With Combo2
.AddItem "CIPL Import1"
.AddItem "CIPL Import2"
.AddItem "CIPL Import3"
'.AddItem "CIPL Import4"
'.AddItem "CIPL Import5"
'.AddItem "CIPL Import6"
'.AddItem "CIPL Import7"
'.AddItem "CIPL Import8"
End With

Combo2.Text = Combo2.List(0)
connection1
add_brn

Timer1.Interval = 1000
DTPicker1.Value = Format(Date, "dd-mm-yyyy")
DTPicker2.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker3.Value = Format(Date, "dd-mm-yyyy")
End Sub
