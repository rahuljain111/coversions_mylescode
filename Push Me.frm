VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Form2 
   Caption         =   "Push Me"
   ClientHeight    =   8895
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10080
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   ScaleHeight     =   11115
   ScaleWidth      =   15240
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Combo7 
      Height          =   330
      Left            =   11280
      TabIndex        =   19
      Top             =   3360
      Width           =   1935
   End
   Begin VB.TextBox Text1 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   325
      Index           =   1
      Left            =   9960
      TabIndex        =   18
      Top             =   3360
      Width           =   1335
   End
   Begin VB.ComboBox Combo6 
      Height          =   330
      Left            =   8040
      TabIndex        =   13
      Top             =   3360
      Width           =   1935
   End
   Begin VB.ComboBox Combo5 
      Height          =   330
      Left            =   6000
      TabIndex        =   12
      Top             =   3360
      Width           =   1935
   End
   Begin VB.ComboBox Combo4 
      Height          =   330
      Left            =   3720
      TabIndex        =   10
      Top             =   3360
      Width           =   2175
   End
   Begin VB.ComboBox Combo3 
      Height          =   330
      Left            =   1320
      TabIndex        =   9
      Top             =   3360
      Width           =   2295
   End
   Begin VB.ComboBox Combo2 
      Height          =   330
      Left            =   1320
      TabIndex        =   6
      Top             =   1080
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1320
      TabIndex        =   5
      Top             =   1680
      Width           =   1455
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   1320
      TabIndex        =   4
      Top             =   2280
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd-MM-yyyy"
      Format          =   55508995
      CurrentDate     =   38694
   End
   Begin VB.ComboBox Combo1 
      Height          =   330
      Left            =   1320
      TabIndex        =   1
      Top             =   480
      Width           =   2535
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "DFF"
      Height          =   210
      Index           =   7
      Left            =   11280
      TabIndex        =   20
      Top             =   3000
      Width           =   285
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Bill Amount"
      Height          =   210
      Index           =   5
      Left            =   10080
      TabIndex        =   17
      Top             =   3000
      Width           =   795
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Product"
      Height          =   210
      Index           =   3
      Left            =   8040
      TabIndex        =   16
      Top             =   3000
      Width           =   555
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Branch"
      Height          =   210
      Index           =   9
      Left            =   3840
      TabIndex        =   15
      Top             =   3000
      Width           =   525
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Natural  Acct"
      Height          =   210
      Index           =   8
      Left            =   480
      TabIndex        =   14
      Top             =   2760
      Width           =   945
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Car No."
      Height          =   210
      Index           =   6
      Left            =   6000
      TabIndex        =   11
      Top             =   3000
      Width           =   540
   End
   Begin VB.Label ID 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "ID"
      Height          =   210
      Left            =   4080
      TabIndex        =   8
      Top             =   480
      Width           =   135
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Site Name"
      Height          =   210
      Index           =   4
      Left            =   240
      TabIndex        =   7
      Top             =   1080
      Width           =   720
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Bill No."
      Height          =   210
      Index           =   2
      Left            =   720
      TabIndex        =   3
      Top             =   1680
      Width           =   480
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Inv. Date"
      Height          =   210
      Index           =   1
      Left            =   600
      TabIndex        =   2
      Top             =   2280
      Width           =   630
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Name"
      Height          =   210
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   405
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Combo1_Click()
strcmd = "select vendor_ID from PO_VENDORS where vendor_name='" & Combo1.Text & "'"
Set comprec1 = con.Execute(strcmd)
If Not comprec1.EOF Then
ID.Caption = comprec1!Vendor_Id
Else
ID.Caption = ""
End If
add_site
End Sub

Private Sub Form_Load()
strconn = ("Driver={Microsoft ODBC for Oracle};Server=CIPL;Uid=apps;Pwd=apps;Persist Security Info=False")
Set con = New ADODB.Connection
con.Open strconn
add_vendor
add_cct
add_brn
add_prod
add_car
End Sub

Public Sub add_head()
Combo2.Clear
Combo2.AddItem "CAR FUEL"
Combo2.AddItem "CAR HIRE"
Combo2.AddItem "VENDOR CASH"
Combo2.AddItem "CEDIT MEMO"
Combo2.Text = Combo2.List(0)
End Sub

Public Sub add_vendor()
Combo1.Clear
strcmd = "select vendor_name from PO_VENDORS group by Vendor_Name"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!vendor_name & ""
        comprec.MoveNext
    Loop
End If
Combo1.Text = Combo1.List(0)
'add_site
End Sub

Public Sub add_site()
Combo2.Clear
strcmd = "select * from PO_VENDOR_SITES_ALL where vendor_id=" & ID.Caption & ""
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo2.AddItem comprec!VENDOR_SITE_CODE & ""
        comprec.MoveNext
    Loop
End If
Combo2.Text = Combo2.List(0)
End Sub

Public Sub add_cct()
Combo3.Clear
strcmd = "select * from FND_FLEX_VALUES FVA,FND_FLEX_VALUES_TL FVTA where FVA.FLEX_VALUE_SET_ID = '" & "1007955" & "'"
strcmd = strcmd & " and FVA.FLEX_VALUE_ID =  FVTA.FLEX_VALUE_ID"
strcmd = strcmd & " and FVA.ENABLED_FLAG ='" & "Y" & "'"
Set comprec3 = con.Execute(strcmd)
If Not comprec3.EOF Then
    Do While Not comprec3.EOF
        Combo3.AddItem comprec3!Description & ""
        comprec3.MoveNext
    Loop
End If
Combo3.Text = Combo3.List(0)
End Sub


Public Sub add_brn()
Combo4.Clear
strcmd = "select * from FND_FLEX_VALUES FVA,FND_FLEX_VALUES_TL FVTA where FVA.FLEX_VALUE_SET_ID = '" & "1007953" & "'"
strcmd = strcmd & " and FVA.FLEX_VALUE_ID =  FVTA.FLEX_VALUE_ID"
strcmd = strcmd & " and FVA.ENABLED_FLAG ='" & "Y" & "'"
Set comprec3 = con.Execute(strcmd)
If Not comprec3.EOF Then
    Do While Not comprec3.EOF
        Combo4.AddItem comprec3!Description
        comprec3.MoveNext
    Loop
End If
Combo4.Text = Combo4.List(0)
End Sub

Public Sub add_car()
Combo5.Clear
strcmd = "select * from FND_FLEX_VALUES FVA,FND_FLEX_VALUES_TL FVTA where FVA.FLEX_VALUE_SET_ID = '" & "1007954" & "'"
strcmd = strcmd & " and FVA.FLEX_VALUE_ID =  FVTA.FLEX_VALUE_ID"
strcmd = strcmd & " and FVA.ENABLED_FLAG ='" & "Y" & "'"
Set comprec3 = con.Execute(strcmd)
If Not comprec3.EOF Then
    Do While Not comprec3.EOF
        Combo5.AddItem comprec3!Description
        comprec3.MoveNext
    Loop
End If
Combo5.Text = Combo5.List(0)
End Sub

Public Sub add_prod()
Combo6.Clear
strcmd = "select * from FND_FLEX_VALUES FVA,FND_FLEX_VALUES_TL FVTA where FVA.FLEX_VALUE_SET_ID = '" & "1007956" & "'"
strcmd = strcmd & " and FVA.FLEX_VALUE_ID =  FVTA.FLEX_VALUE_ID"
strcmd = strcmd & " and FVA.ENABLED_FLAG ='" & "Y" & "'"
Set comprec3 = con.Execute(strcmd)
If Not comprec3.EOF Then
    Do While Not comprec3.EOF
        Combo6.AddItem comprec3!Description
        comprec3.MoveNext
    Loop
End If
Combo6.Text = Combo6.List(0)
End Sub

