VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form Form7 
   BackColor       =   &H00FF8080&
   Caption         =   "AR Collection Interface"
   ClientHeight    =   6720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13455
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RECEIPT.frx":0000
   LinkTopic       =   "Form7"
   MDIChild        =   -1  'True
   ScaleHeight     =   6720
   ScaleWidth      =   13455
   WindowState     =   2  'Maximized
   Begin VB.OptionButton dtChq 
      BackColor       =   &H00FF8080&
      Caption         =   "Chq"
      Height          =   350
      Left            =   4320
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   9
      Top             =   240
      Value           =   -1  'True
      Width           =   915
   End
   Begin VB.OptionButton reEasyCabs 
      BackColor       =   &H00FF8080&
      Caption         =   "Easy Cabs"
      Height          =   350
      Left            =   5280
      TabIndex        =   8
      Top             =   240
      Width           =   1275
   End
   Begin VB.ListBox List1 
      Height          =   4050
      Left            =   11280
      TabIndex        =   7
      Top             =   600
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00FF8080&
      Caption         =   "Check Customer"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   11280
      TabIndex        =   6
      Top             =   360
      Value           =   1  'Checked
      Width           =   1575
   End
   Begin VB.ComboBox Combo1 
      Height          =   330
      Left            =   8640
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   7920
      TabIndex        =   2
      Top             =   6000
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Generate Padding"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   5880
      TabIndex        =   1
      Top             =   6000
      Width           =   2055
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5055
      Left            =   0
      TabIndex        =   3
      Top             =   720
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   8916
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   16771515
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   13
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "BANK"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "IDENTIFIER"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "CHQ NO"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "AMOUNT"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "DATE"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "GL DATE"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "ITEM"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "CUSTOMER ID"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "LOCATION ID"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "LOCATION"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "REMARK"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Company Name"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Payment Type"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "LockBox No."
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   7680
      TabIndex        =   5
      Top             =   360
      Width           =   915
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "AR Collection Interface"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   3675
   End
End
Attribute VB_Name = "Form7"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim w As WshShell
Dim CHQ_NO As Variant
Dim chq_amt As Variant
Dim loc1 As Variant
Dim CHQ_PAD As Variant
Dim Amt_PAD As Variant
Dim loc_PAD As Variant
Dim PAY_PAD As Variant
Dim Payment_Method As Variant
Public Sub lockb()

Combo1.Clear
If (dtChq.Value = True) Then
strcmd = "select lockbox from chq  where lockbox is not null and upload_status=0 group by lockbox order by lockbox"
ElseIf (reEasyCabs.Value = True) Then
strcmd = "select lockbox from Chq_EasyCabs  where lockbox is not null and upload_status=0 group by lockbox order by lockbox"
End If

Set comprec = con1.Execute(strcmd)
'Set comprec = New Recordset
'comprec.Open "select lockbox from chq  where lockbox is not null and location is not null group by lockbox", con1, adOpenDynamic, adLockOptimistic

'MsgBox (comprec.RecordCount)
    'Combo1.Text = ""
If comprec.EOF = False Then
    Do While Not comprec.EOF
        'if comprec!lockbox
        Combo1.AddItem comprec!lockbox
        comprec.MoveNext
    Loop
    Combo1.Text = Combo1.List(0)
End If
End Sub

Public Sub chkcust()

If (dtChq.Value = True) Then
strcmd = "select * from chq where lockbox is not null and location is not null and upload_status=0 order by company  "
'Else
ElseIf (reEasyCabs.Value = True) Then
strcmd = "select * from Chq_EasyCabs where lockbox is not null and location is not null and upload_status=0 order by company  "
End If

'vdate=Convert(DateTime, '" & DTPicker1.Value & "', 103)"
'strcmd = strcmd & " where remark='" & "Uploaded on 01-03-2006" & "'"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    DoEvents
    
    'ListView1.Refresh
    List1.Refresh
    
    'List1.ListItems.Clear
    status.Show
    DoEvents
    Do While Not comprec.EOF
            strcmd = "select * from auto_import where CIPL_cust like '" & Replace(comprec!company, "'", "''") & "'"
            Set comprec1 = con1.Execute(strcmd)
            List1.Refresh
            'Set repcomprec = New ADODB.Recordset
            If Not comprec1.EOF Then
            
            Else
                List1.AddItem comprec!lockbox & "-" & comprec!company
                'repcomprec.AddNew
                'repcomprec.AddNew comprec!Company
            End If
            'repcomprec.Update
            comprec.MoveNext
    Loop
End If
Unload status
MsgBox "Done"
End Sub

Private Sub Check1_Click()
If Check1.Value = 1 Then
    List1.Visible = True
Else
    List1.Visible = False
    List1.Clear
End If
End Sub


Private Sub Command1_Click()
    'Code changed by rahul chkcust
    
    chkcust
    If List1.ListCount > 0 Then
        'Code changed by rahul Exit Sub
        'Exit Sub
End If

'c_ctr = Combo1.Text
c_ctr = 1
c = 1
Do While c <= Combo1.ListCount
ctr = 1
item = 1000

If (dtChq.Value = True) Then
strcmd = "select * from chq WHERE lockbox IS NOT NULL and location is not null and upload_status=0 and LOCKBOX='" & Trim(Combo1.Text) & "'"
strcmd = strcmd & " and amount >=0"
'Else
ElseIf (reEasyCabs.Value = True) Then
strcmd = "select * from Chq_EasyCabs WHERE lockbox IS NOT NULL and location is not null and upload_status=0 and LOCKBOX='" & Trim(Combo1.Text) & "'"
strcmd = strcmd & " and amount >=0"
End If

Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
        CHQ_PAD = ""
        cust = ""
        Amt_PAD = 0
        loc_PAD = ""
        
        Do While Not comprec.EOF
        
            CHQ_PAD = ""
            cust = ""
            Amt_PAD = 0
            loc_PAD = ""
        
        
        '*************************************
        strcmd = "select orig_system_reference from ra_customers r"
        strcmd = strcmd & " where r.STATUS = 'A'"
        strcmd = strcmd & " and trim(r.customer_name)='" & Replace(Trim(comprec!company), "'", "''") & "'"
        Set comprec2 = con.Execute(strcmd)
        If Not comprec2.EOF Then
            Id = comprec2!orig_system_reference
        Else
            Id = "''"
        End If
        
        strcmd = "select CUSTOMER_NAME,LOCATION, CUSTOMER_ID FROM LOCATIONMAPPING where Location_ID ='" & Trim(comprec!Location_ID) & "'"
        strcmd = strcmd & " and CUSTOMER_ID='" & Trim(comprec!Company_ID) & "'"
        Set comprec3 = con1.Execute(strcmd)
        
        
        strcmd = "BEGIN"
        strcmd = strcmd & " dbms_application_info.set_client_info(101)" & ";"
        strcmd = strcmd & " END" & ";"
        Set comprec1 = con.Execute(strcmd)
        
        strcmd = "select distinct b.ACCOUNT_NUMBER as Customer_Number,d.location as Location,r.CUSTOMER_NAME as Party_name,v.city as City"
        strcmd = strcmd & " From hz_parties a, hz_cust_accounts b, hz_cust_acct_sites_all c,"
        strcmd = strcmd & " hz_cust_site_uses_all d,ra_customers r, AR_ADDRESSES_V v"
        strcmd = strcmd & " Where a.PARTY_ID = b.PARTY_ID and b.PARTY_ID=r.PARTY_ID and a.party_id = r.party_id   and r.PARTY_ID=v.PARTY_ID(+)"
        strcmd = strcmd & " And b.cust_account_id = c.cust_account_id  and c.CUST_ACCT_SITE_ID = d.cust_acct_site_id "
        strcmd = strcmd & "  and d.SITE_USE_CODE = 'BILL_TO' "
        strcmd = strcmd & " AND B.STATUS='A' and c.STATUS='A' and d.STATUS='A' and v.STATUS='A' and r.STATUS = 'A' and a.status='A' "
        strcmd = strcmd & " and b.CUST_ACCOUNT_ID=v.CUSTOMER_ID and  a.VALIDATED_FLAG is null "
        strcmd = strcmd & " and v.party_site_id = c.PARTY_SITE_ID "
        'strcmd = strcmd & " and r.CUSTOMER_NAME='" & Trim(comprec!company) & "'"
        'strcmd = strcmd & " and v.city='" & UCase(Trim(comprec!Location)) & "'"
        strcmd = strcmd & " and b.CUST_ACCOUNT_ID = " & Trim(comprec3!CUSTOMER_ID)
        strcmd = strcmd & " and a.PARTY_NAME like '" & Replace(Trim(comprec3!CUSTOMER_NAME), "'", "''") & "%'"
        strcmd = strcmd & " and v.city='" & UCase(Trim(comprec3!Location)) & "'"
        strcmd = strcmd & " order by d.LOCATION asc "
        
        'file_name = "c:\Lockbox\" & "vendor.txt"
        'Open file_name For Append Access Read Write Shared As #1
        'LOCKPAD = strcmd
        'Print #1, Trim(LOCKPAD)
        'Close #1
       
       
        'strcmd = "select distinct r.CUSTOMER_NUMBER,d.location,a.PARTY_NAME From hz_parties a, hz_cust_accounts b, hz_cust_acct_sites_all c, hz_cust_site_uses_all d,ra_customers r"
        'strcmd = strcmd & " Where a.PARTY_ID = b.PARTY_ID And b.cust_account_id = c.cust_account_id "
        'strcmd = strcmd & " and c.CUST_ACCT_SITE_ID = d.cust_acct_site_id and d.SITE_USE_CODE = 'BILL_TO'"
        'strcmd = strcmd & " and r.STATUS = 'A'"
        'strcmd = strcmd & " and a.party_id = r.party_id"
        'strcmd = strcmd & " and a.party_name='" & Trim(comprec!company) & "'"
        'strcmd = strcmd & " and D.ORIG_SYSTEM_REFERENCE='" & UCase(Trim(comprec!Location)) & "-" & ID & "'"
        
        'Dim cntr As Integer
        'cntr = 1
        
        Set comprec1 = con.Execute(strcmd)
        If Not comprec1.EOF Then
            CHQ_NO = Mid(comprec!CHEQUE_NO, 1, Len(comprec!CHEQUE_NO) - 1)
                
        ADDCHQ
        
        Payment_Method = comprec!Payment_Method
        ADDPay
        Set lt = ListView1.ListItems.Add(, , comprec!DEPOSIT)
        lt.SubItems(1) = c_ctr
        lt.SubItems(2) = CHQ_PAD
        chq_amt = Round(comprec!Amount, 2) * 100
        ADDamt
        lt.SubItems(3) = Amt_PAD 'Round(comprec!AMOUNT, 2) * 100
        lt.SubItems(4) = Format(comprec!VDATE, "DD-MMM-YYYY")
        lt.SubItems(5) = Format(comprec!gl_DATE, "DD-MMM-YYYY")
        lt.SubItems(6) = item
        ctr = ctr + 1
        
        cust = comprec1!CUSTOMER_NUMBER
        loc1 = comprec1!Location
        'cust = comprec!Company_ID
        'loc1 = comprec!Location_ID
        
        lt.SubItems(7) = cust
        lt.SubItems(8) = loc1
        Else
            lt.SubItems(7) = cust
            lt.SubItems(8) = loc1
        End If
        ADDloc
        'If comprec!company = "BLUESCOPE STEEL" Then
        'Print ""
        'End If
        lt.SubItems(9) = comprec!Location
        lt.SubItems(10) = comprec!Description & ""
        lt.SubItems(11) = comprec!company & ""
        lt.SubItems(12) = comprec!Payment_Method
        
        file_name = "C:\Lockbox\" & Trim(Combo1.Text) & ".txt"
        'file_name = "\data\erpp\" & Trim(Combo1.Text) & ".txt"
        Open file_name For Append Access Read Write Shared As #1
        LOCKPAD = "1" & cust & CHQ_PAD & Amt_PAD & Format(comprec!VDATE, "DD-MMM-YYYY") & "INR" & item & loc_PAD & Format(comprec!gl_DATE, "DD-MMM-YYYY") & PAY_PAD
        Print #1, Trim(LOCKPAD) & Trim(comprec!Description)
        Close #1
         item = item + 1
        ListView1.Refresh
        comprec.MoveNext
        c_ctr = c_ctr + 1
    Loop
End If
    
    Combo1.Text = Combo1.List(c)
    c = c + 1
    MsgBox Combo1.Text & " LOCKBOX Started Now. Total Collections : " & c_ctr - 1
    Loop

    'strcmd = "select count(*)as tot from chq WHERE CHEQUE_NO IS NOT NULL and LOCKBOX='" & Trim(Combo1.Text) & "'"
    'Set comprec = con1.Execute(strcmd)
    'If Not comprec.EOF Then
    'MsgBox comprec!tot & " Records Transfered", vbInformation, "Done"
    'MsgBox comprec!tot & " Records Transfered"
    'End If

End Sub

Public Sub ADDPay()
    TOTLEN = Len(Payment_Method)
    ctr = 30 - TOTLEN
    PAY_PAD = Payment_Method
    For i = 1 To ctr
        PAY_PAD = "0" & PAY_PAD
    Next
End Sub

Public Sub ADDCHQ()
    TOTLEN = Len(CHQ_NO)
    ctr = 30 - TOTLEN
    CHQ_PAD = CHQ_NO
    For i = 1 To ctr
        CHQ_PAD = "0" & CHQ_PAD
    Next
End Sub

Public Sub ADDloc()
    TOTLEN = Len(loc1)
    'ctr = 5 - TOTLEN  ' 2 July 2014
    ctr = 7 - TOTLEN
    loc_PAD = loc1
    For i = 1 To ctr
        loc_PAD = "0" & loc_PAD
    Next
End Sub

Public Sub ADDamt()
    TOTLEN = Len(chq_amt)
    ctr = 10 - TOTLEN
    Amt_PAD = chq_amt
    For i = 1 To ctr
        Amt_PAD = "0" & Amt_PAD
    Next
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub dtChq_Click()
    lockb
End Sub

'Private Sub Command3_Click()
'r = Shell(App.Path & "\plink.exe -ssh -P 22 192.168.2.155 -l root -pw root123 cd /erpp/ ")
's = Shell("sftp 192.168.2.155 -l root")
'End Sub

Private Sub Form_Load()
    'Dim FSO As New FileSystemObject
    'FSO.DeleteFolder ("C:\lockbox")
    'FSO.CreateFolder ("C:\LockBox")
    'Kill "C:\Lockbox\*.txt"
    'connection1
    'add_apps
    
    ListView1.Width = Screen.Width
    lockb
End Sub

Private Sub reEasyCabs_Click()
    lockb
End Sub
