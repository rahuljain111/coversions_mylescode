VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmsetup 
   Caption         =   "Hertz Setup"
   ClientHeight    =   5805
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5520
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmsetup.frx":0000
   LinkTopic       =   "Form5"
   MaxButton       =   0   'False
   ScaleHeight     =   5805
   ScaleWidth      =   5520
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton command2 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2640
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   5280
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Settings"
      Height          =   4215
      Left            =   360
      TabIndex        =   14
      Top             =   480
      Width           =   5055
      Begin VB.TextBox Text2 
         Height          =   375
         Index           =   1
         Left            =   1200
         TabIndex        =   5
         Top             =   3000
         Width           =   1215
      End
      Begin VB.TextBox Text2 
         Height          =   375
         Index           =   0
         Left            =   1200
         TabIndex        =   4
         Top             =   2520
         Width           =   1215
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Parking With Service Tax"
         Height          =   195
         Left            =   480
         TabIndex        =   3
         Top             =   2160
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.CheckBox Check1 
         Caption         =   "&Default"
         Height          =   195
         Left            =   3960
         TabIndex        =   27
         Top             =   -1200
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Frame Frame2 
         Caption         =   "Others Settings"
         Height          =   2415
         Left            =   5880
         TabIndex        =   18
         Top             =   600
         Width           =   2775
         Begin VB.ComboBox Combo1 
            Height          =   315
            Left            =   1365
            TabIndex        =   13
            Top             =   1920
            Width           =   975
         End
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   1365
            TabIndex        =   12
            Top             =   1450
            Width           =   975
         End
         Begin MSComCtl2.DTPicker dtptimein 
            Height          =   330
            Left            =   1365
            TabIndex        =   10
            Top             =   480
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   582
            _Version        =   393216
            Format          =   60751874
            CurrentDate     =   37822
         End
         Begin MSComCtl2.DTPicker dtptimeout 
            Height          =   330
            Left            =   1365
            TabIndex        =   11
            Top             =   965
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   582
            _Version        =   393216
            CustomFormat    =   "HH:MM"
            Format          =   60751874
            CurrentDate     =   37822
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            Height          =   195
            Index           =   7
            Left            =   2400
            TabIndex        =   24
            Top             =   1960
            Width           =   165
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Service Tax"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   23
            Top             =   1920
            Width           =   840
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "TDS Deduction"
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   22
            Top             =   1440
            Width           =   1050
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Time In"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   21
            Top             =   480
            Width           =   525
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Time Out"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   20
            Top             =   960
            Width           =   645
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "%"
            Height          =   195
            Index           =   3
            Left            =   2400
            TabIndex        =   19
            Top             =   1500
            Width           =   165
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Accept Entry"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1455
         Left            =   480
         TabIndex        =   17
         Top             =   480
         Width           =   2055
         Begin VB.OptionButton Option3 
            Caption         =   "Sentence Case"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   240
            TabIndex        =   2
            Top             =   1080
            Width           =   1575
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Title Case"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   240
            TabIndex        =   1
            Top             =   720
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.OptionButton Option1 
            Caption         =   "&Ucase"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   240
            TabIndex        =   0
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.TextBox Text1 
         Height          =   325
         Index           =   1
         Left            =   1080
         TabIndex        =   9
         Top             =   -1140
         Width           =   2775
      End
      Begin VB.TextBox Text1 
         Height          =   325
         Index           =   0
         Left            =   1080
         TabIndex        =   8
         Top             =   -720
         Width           =   2775
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Diesel Price"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   29
         Top             =   3000
         Width           =   810
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Petrol Price"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   28
         Top             =   2520
         Width           =   810
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "All The Changes Made in The Setting Detail Will be Automatically Reflected in the Application."
         ForeColor       =   &H00808080&
         Height          =   435
         Index           =   9
         Left            =   240
         TabIndex        =   26
         Top             =   3720
         Width           =   4920
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Unit Name"
         Height          =   195
         Index           =   1
         Left            =   195
         TabIndex        =   16
         Top             =   -600
         Width           =   735
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "City Name"
         Height          =   195
         Index           =   0
         Left            =   195
         TabIndex        =   15
         Top             =   -1080
         Width           =   735
      End
   End
   Begin VB.CommandButton command1 
      Caption         =   "&Save"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1680
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   5280
      Width           =   975
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Setup Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000011&
      Height          =   375
      Index           =   8
      Left            =   120
      TabIndex        =   25
      Top             =   0
      Width           =   5895
   End
End
Attribute VB_Name = "frmsetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim match2(300) As Variant
Dim str4 As Variant
Dim txt As String

Public Function test1(s As String, Index As Integer)
'Static prev As Variant
'txt = ""
'prev = 0
'ST = 0
'str1 = Trim(Text1(Index))
'len1 = Len(Trim(Text1(Index)))
'Text1(Index) = Text1(Index) & Space(1)
'For i = 1 To len1 + 1
'match2(i) = Mid(Text1(Index), i, 1)
'If match2(i) = Space(1) Then
'    fil = Mid(Text1(Index), prev + 1, i - prev - 1)
'    f = Mid(fil, 1, 1)
'    f = UCase(f)
'    l = Mid(fil, 2, Len(fil))
'    txt = txt & f & l & Space(1)
'    prev = i
'    i = i + 1
'End If
'Next
End Function

Public Function test2(s As String, Index As Integer)
'Static prev As Variant
'txt = ""
'prev = 0
'ST = 0
'str1 = Trim(Text1(Index))
'len1 = Len(Trim(Text1(Index)))
'Text1(Index) = Text1(Index) & Space(1)
'For i = 1 To len1 + 1
'match2(i) = Mid(Text1(Index), i, 1)
'If match2(i) = Space(1) Then
'    fil = Mid(Text1(Index), prev + 1, i - prev - 1)
'    txt = txt & UCase(fil) & Space(1)
'    prev = i
'    i = i + 1
'End If
'Next
End Function

Public Function test3(s As String, Index As Integer)
str2 = ""
str3 = ""
str1 = Text1(Index)
str2 = Mid(Text1(Index), 1, 1)
str3 = Mid(Text1(Index), 2, Len(Text1(Index)))
str4 = UCase(str2) & str3
End Function


'Private Sub Check1_Click()
'If Check1.Value = 1 Then
'    Check1.Caption = "Default"
'Else
'    Check1.Caption = "New Unit"
'    Text1(0).SetFocus
'End If
'
'strcmd = "select * from h_unit "
'Set comprec = con.Execute(strcmd)
'If Not comprec.EOF Then
'    Text1(0) = comprec!City & ""
'    Text1(1) = comprec!unit & ""
'End If
'Else
'    Set comprec = New ADODB.Recordset
'    Set comprec = con.Execute("h_Setup")
'    If Not comprec.EOF Then
'        Text1(0) = comprec!City & ""
'        Text1(1) = comprec!unit & ""
'    End If
'End If
'End Sub
'
Private Sub Command1_Click()
Set comprec = New ADODB.Recordset
a = MsgBox("Want to Save The Changes", vbYesNo, "Save")
If a = vbYes Then
    Set comprec = con.Execute("truncate table h_setup")
    comprec.Open "H_Setup", con, adOpenDynamic, adLockOptimistic
    comprec.AddNew
    comprec!CITY = Text1(0)
    comprec!Unit = Text1(1)
    comprec!TAX_RATE = Combo1.Text
    comprec!Time_In = dtptimein.Value
    comprec!time_Out = dtptimeout.Value
    comprec!TDS_Rate = Combo2.Text
    If Option1.Value = True Then
        comprec!Accept = "Upper Case"
    End If

    If Option2.Value = True Then
        comprec!Accept = "Title Case"
    End If

    If Option3.Value = True Then
        comprec!Accept = "Sentence Case"
    End If
    
    If Check2.Value = 1 Then
        comprec!PARKING_TAX = 1
    Else
        comprec!PARKING_TAX = 0
    End If
    comprec!Petrol = Text2(0)
    comprec!diesel = Text2(1)
    
    comprec.Update
    MsgBox "Saved Suceessfully", vbExclamation, "Saved"
Else
    Exit Sub
End If
End Sub

Private Sub Text1_LostFocus(Index As Integer)
If frmsetup.Option1.Value = True Then
    a = test2(Text1(Index), Index)
    Text1(Index) = Trim(txt)
End If

If frmsetup.Option2.Value = True Then
    a = test1(Text1(Index), Index)
    Text1(Index) = Trim(txt)
End If

If frmsetup.Option3.Value = True Then
    a = test3(Text1(Index), Index)
    Text1(Index) = Trim(str4)
End If
Text1(Index).BackColor = vbWhite
End Sub


Private Sub Command2_Click()
Unload Me
End Sub


Private Sub Form_Load()
'Check1.Value = 1
connection1
Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2
For i = 1 To 50
    Combo1.AddItem Val(i)
Next
Combo1.Text = Combo1.List(0)

For i = 1 To 50
    Combo2.AddItem Val(i)
Next
Combo2.Text = Combo2.List(0)
Set comprec = New ADODB.Recordset
'Set comprec = con1.Execute("h_Unit")
'If Not comprec.EOF Then
'Text1(0) = comprec!city & ""
'Text1(1) = comprec!Unit & ""
'End If

'Set comprec = New ADODB.Recordset
'Set comprec = con1.Execute("h_Setup")
'If Not comprec.EOF Then
'Text1(0) = comprec!city & ""
'Text1(1) = comprec!Unit & ""
'Text2(0) = comprec!Petrol & ""
'Text2(1) = comprec!diesel & ""
'Combo1.Text = comprec!TAX_RATE
'Combo2.Text = comprec!TDS_Rate
'dtptimein.Value = comprec!Time_In
'dtptimeout.Value = comprec!time_Out
'If comprec!Accept = "Upper Case" Then
 '   Option1.Value = True
'End If
'If comprec!Accept = "Title Case" Then
 '   Option2.Value = True
'End If
'If comprec!Accept = "Sentence Case" Then
 '   Option3.Value = True
'End If
End Sub

