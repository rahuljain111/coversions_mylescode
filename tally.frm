VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form tally 
   BackColor       =   &H00FF8080&
   Caption         =   "Invoice Report"
   ClientHeight    =   11010
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   11400
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form11"
   ScaleHeight     =   550.5
   ScaleMode       =   0  'User
   ScaleWidth      =   570
   WindowState     =   2  'Maximized
   Begin VB.ListBox List1 
      Height          =   450
      Left            =   7560
      TabIndex        =   19
      Top             =   120
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Caption         =   "Date"
      Height          =   615
      Left            =   120
      TabIndex        =   12
      Top             =   360
      Width           =   2535
      Begin VB.OptionButton Option2 
         BackColor       =   &H00FF8080&
         Caption         =   "Between"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   1200
         TabIndex        =   1
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FF8080&
         Caption         =   "Single"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   240
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   6735
      Left            =   0
      TabIndex        =   10
      Top             =   2040
      Width           =   1.30000e5
      _ExtentX        =   229314
      _ExtentY        =   11880
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   16767927
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   24
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Inv. No."
         Object.Width           =   2293
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Invoice Date"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Company"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Guest"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Package "
         Object.Width           =   2011
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Extra Km Amt"
         Object.Width           =   1834
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Extra HRS Amt"
         Object.Width           =   1834
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "Comp. Discount"
         Object.Width           =   2187
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "spl discount"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Parking"
         Object.Width           =   2011
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Others Taxes"
         Object.Width           =   2011
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Outstation"
         Object.Width           =   2011
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Night Allow"
         Object.Width           =   1746
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Fuel"
         Object.Width           =   1658
      EndProperty
      BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   15
         Text            =   "Sale Amount"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   16
         Text            =   "Service Tax"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(18) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   17
         Text            =   "Cess Amount"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(19) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   18
         Text            =   "STAX WITH CESS"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(20) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   19
         Text            =   "DST On RUG"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(21) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   20
         Text            =   "Net Amount"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(22) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   21
         Text            =   "Car NO"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(23) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   22
         Text            =   "Branch"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(24) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   23
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.CommandButton Command5 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   375
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   840
      Width           =   1455
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Update Database"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1200
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Print"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   840
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Restore Data"
      Height          =   375
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   480
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Preview"
      Height          =   375
      Left            =   8520
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   120
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   855
      Index           =   1
      Left            =   2760
      TabIndex        =   14
      Top             =   360
      Width           =   3495
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   600
         TabIndex        =   2
         Top             =   255
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   60751875
         CurrentDate     =   37865
      End
      Begin MSComCtl2.DTPicker DTPicker3 
         Height          =   330
         Left            =   2160
         TabIndex        =   3
         Top             =   250
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   60751875
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   2
         Left            =   1900
         TabIndex        =   17
         Top             =   300
         Width           =   180
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   300
         Width           =   360
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FF8080&
      BorderStyle     =   0  'None
      Height          =   855
      Index           =   0
      Left            =   2760
      TabIndex        =   13
      Top             =   360
      Width           =   3495
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   325
         Left            =   840
         TabIndex        =   8
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         CalendarBackColor=   16777215
         CalendarForeColor=   0
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   60751875
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Date"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   15
         Top             =   240
         Width           =   345
      End
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Height          =   195
      Left            =   4920
      TabIndex        =   21
      Top             =   1560
      Width           =   885
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   4440
      TabIndex        =   20
      Top             =   1560
      Width           =   405
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Click on the column to sort "
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   1
      Left            =   120
      TabIndex        =   18
      Top             =   1320
      Width           =   1935
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Invoice Report"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   330
      Index           =   12
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   3975
   End
End
Attribute VB_Name = "tally"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Dst_tax As Variant
Dim sale_amt As Variant
Dim CITY_NAME As String
Dim CITY_Code As String

Private Sub Command1_Click()
Dim comprec15 As ADODB.Recordset
ListView1.ListItems.Clear
Set repcomprec = New ADODB.Recordset
ctr = 0

If Option1.Value = True Then
    strcmd = "select * from ARP WHERE Inv_Date =Convert(DateTime, '" & DTPicker1.Value & "', 103)"
    'strcmd = strcmd & " AND  LEN(INV_NO)>1"
    strcmd = strcmd & " AND LEN(NEW_INVOICE)>1"
    'strcmd = strcmd & " and actual_invoice='" & "INV00015027" & "'"
    strcmd = strcmd & " AND void_invoice=0"
    strcmd = strcmd & " order by inv_date"
    str1 = DTPicker1.Value
End If

If Option2.Value = True Then
    strcmd = "select * from ARP where Inv_Date >=Convert(DateTime, '" & DTPicker2.Value & "', 103)"
    strcmd = strcmd & " AND Inv_Date <=Convert(DateTime, '" & DTPicker3.Value & "', 103)"
    'strcmd = strcmd & " AND LEN(INV_NO)>1"
    strcmd = strcmd & " AND LEN(NEW_INVOICE)>1"
    strcmd = strcmd & " AND void_invoice=0"
    'strcmd = strcmd & " and new_invoice='" & "INV00015027" & "'"
    strcmd = strcmd & " order by inv_date"
    str1 = "From " & DTPicker2.Value & " To " & DTPicker3.Value
End If
Set comprec15 = con2.Execute(strcmd)
If Not comprec15.EOF Then
    DoEvents
    status.Show
    DoEvents
   strcmd = "select * from h_unit"
   Set NEWcomprec1 = con2.Execute(strcmd)
   If Not NEWcomprec1.EOF Then
        CITY_NAME = NEWcomprec1!CITY & ""
        CITY_Code = Left(NEWcomprec1!Phone, 3) & ""
   Else
        CITY_NAME = ""
        CITY_Code = ""
   End If
   
   Set repcomprec = New ADODB.Recordset
   repcomprec.Open "invoice_dec", con1, adOpenDynamic, adLockOptimistic
   Do While Not comprec15.EOF
            ListView1.Refresh
            Label4.Caption = ctr
            strcmd = "select * from invoice_dec where actual_invoice ='" & comprec15!new_invoice & "'"
            strcmd = strcmd & " and city_name='" & CITY_NAME & "'"
            Set comprec5 = con1.Execute(strcmd)
            '-----------------------------------------------------
            If Not comprec5.EOF Then
            Else
                Set lt = ListView1.ListItems.Add(, , comprec15!new_invoice)
                    If comprec15!direct = True Then
                            lt.SubItems(1) = comprec15!Physical_Duty & ""
                    Else
                            lt.SubItems(1) = Val(Mid(comprec15!new_invoice, 4, Len(comprec15!new_invoice))) & "INV"
                    End If
                    lt.SubItems(2) = comprec15!inv_DATE & ""
                    lt.SubItems(3) = comprec15!company & ""
                    lt.SubItems(4) = comprec15!guest & ""
                    a = comprec15!package_Amount
                    b = comprec15!extra_Km_Amount
                    c = comprec15!EXTRA_HRS_AMOUNT
                    Netent = a + b + c
                    d = comprec15!COMPANY_DISCOUNT
                    If IsNull(comprec15!Qutstation_Allow) = True Then
                        k = 0
                    Else
                        k = Val(comprec15!Qutstation_Allow) & ""
                   End If
                   l = Val(comprec15!Night_Billed_Amount & "")
                   If comprec15!direct = True Then
                        h = 0
                   Else
                        h = comprec15!Fuel
                   End If
                   
                     
                    If IsNull(comprec15!Fuel) = True Then
                    h = 0
                    End If
                    
                    f = comprec15!PARKING_TAX
                    g = comprec15!others
                    
                    If IsNull(f) = True Then
                    f = 0
                    End If
                    
                    
                    If IsNull(g) = True Then
                    g = 0
                    End If
                    sale_amt = a + b + c + h + k + l - d - e
                    i = (a + b + c - d - e + k + l + h + g + f)
                    strcmd = "select * from h_unit"
                    Set comprec3 = con2.Execute(strcmd)
                    If Not comprec3.EOF Then
                        TAX_AMT = comprec3!Service_Tax
                        Cess_Amt = comprec3!CESS
                    Else
                        TAX_AMT = 0
                        Cess_Amt = 0
                    End If
                    Service_Tax = (i * TAX_AMT) / 100
                    cess_Tax = (i * Cess_Amt) / 100
                    GROSS = i + Service_Tax + cess_Tax
            
                    If comprec3!dst = True Then
                        Dst_tax = (i * 12.5) / 100
                    Else
                        Dst_tax = 0
                    End If
            
                    If IsNull(a) = True Or IsNull(b) = True Or IsNull(c) = True Or IsNull(d) = True Then
                        a = 0
                        b = 0
                        c = 0
                        d = 0
                        e = 0
                        f = 0
                        g = 0
                        k = 0
                        sale_amt = 0
                        Service_Tax = 0
                        cess_Tax = 0
                    End If
            
                    If IsNull(Dst_tax) = True Then
                        Dst_tax = 0
                    End If
                    lt.SubItems(5) = a
                    lt.SubItems(6) = b
                    lt.SubItems(7) = c
                    lt.SubItems(8) = d
                    lt.SubItems(9) = e
                    lt.SubItems(10) = f
                    lt.SubItems(11) = g
                    lt.SubItems(12) = k
                    lt.SubItems(13) = l
                    lt.SubItems(14) = h
                    lt.SubItems(15) = Round(sale_amt, 2)
                    lt.SubItems(16) = Round(Service_Tax, 2)
                    lt.SubItems(17) = Round(cess_Tax, 2)
                    lt.SubItems(18) = Round((Service_Tax + cess_Tax), 2)
                    lt.SubItems(19) = 0
                    lt.SubItems(20) = sale_amt + f + g + Service_Tax + cess_Tax + Dst_tax
                    If comprec15!owned = True Then
                    Else
                        lt.SubItems(21) = comprec15!Vendor & "-" & comprec15!vehicle_no
                    End If
                     lt.SubItems(22) = comprec15!branch & ""
                     branch = comprec15!branch & ""
                     branch_from = comprec15!branch_from & ""
                If Netent > 0 Then
                    repcomprec.AddNew
                    repcomprec!vehicle_no = comprec15!vehicle_no & ""
                    repcomprec!Duty_Slip_No = CITY_Code & "-" & comprec15!Physical_Duty
                    repcomprec!STAX = Round(Service_Tax + cess_Tax, 2)
                    repcomprec!VAT = Round(Dst_tax, 2)
                    repcomprec!company = comprec15!company & ""
                    repcomprec!VDATE = comprec15!inv_DATE & ""
                  
                    If comprec15!direct = True Then
                        repcomprec!direct = 1
                        repcomprec!invoice_no = CITY_Code & "-" & comprec15!Physical_Duty & ""
                        repcomprec!Term_Id = 5
                    Else
                        repcomprec!invoice_no = CITY_Code & "-" & Val(Mid(comprec15!new_invoice, 4, Len(comprec15!new_invoice))) & "INV"
                        repcomprec!direct = 0
                        repcomprec!Term_Id = 5
                    End If
                    repcomprec!recipt_id = "1010"
                    repcomprec!Client_Name = comprec15!guest & ""
                    
                    BRN = UCase(comprec15!branch)
                    
                    Select Case BRN
                    
                    Case "BANGALORE"
                    repcomprec!Ship_To1 = "Bangalore-Hub"
                    
                    Case "MUMBAI"
                    repcomprec!Ship_To1 = "Mumbai Hub"
                    
                    Case "DELHI"
                    repcomprec!Ship_To1 = "Delhi Hub"
                                        
                    Case "HYDERABAD"
                    repcomprec!Ship_To1 = "Hyderabad-Hub"
                                        
                    Case "CHENNAI"
                    repcomprec!Ship_To1 = "Chennai-Hub"
                    
                    Case "PUNE"
                    repcomprec!Ship_To1 = "Pune-Hub"
                    
                    Case "CHANDIGARH"
                    repcomprec!Ship_To1 = "Chandigarh Hub"
                    
                    Case "JAIPUR"
                    repcomprec!Ship_To1 = "Jaipur Hub"
                    
                    Case "AHMEDABAD"
                    repcomprec!Ship_To1 = "Ahmedabad"
                    
                    Case "GURGAON"
                    repcomprec!Ship_To1 = "Gurgaon"
                    
                    End Select
                    
                    '------------------------
                    BRN1 = UCase(comprec15!branch_from)
                    Select Case BRN1
                    Case "BANGALORE"
                    repcomprec!branch_bill_to = "Bangalore-Hub"
                    
                    Case "MUMBAI"
                    repcomprec!branch_bill_to = "Mumbai Hub"
                    
                    Case "DELHI"
                    repcomprec!branch_bill_to = "Delhi Hub"
                                        
                    Case "HYDERABAD"
                    repcomprec!branch_bill_to = "Hyderabad-Hub"
                                        
                    Case "CHENNAI"
                    repcomprec!branch_bill_to = "Chennai-Hub"
                    
                    Case "PUNE"
                    repcomprec!branch_bill_to = "Pune-Hub"
                                        
                    Case "CHANDIGARH"
                    repcomprec!branch_bill_to = "Chandigarh Hub"
                                        
                    Case "JAIPUR"
                    repcomprec!branch_bill_to = "Jaipur Hub"
                    
                    Case "AHMEDABAD"
                    repcomprec!branch_bill_to = "Ahmedabad"
                    
                    Case "GURGAON"
                    repcomprec!branch_bill_to = "Gurgaon"
                    
                    End Select
                    
                    '------------------------
                    
                    If comprec15!company Like "SELF%" Then
                        repcomprec!product = "Self Drive Spot"
                    Else
                        repcomprec!product = "Rent A Car"
                    End If
                    
                    repcomprec!Branch_Booked = comprec15!branch_from & ""
                    repcomprec!ship_to = CITY_NAME & ""
                    repcomprec!actual_Invoice = comprec15!new_invoice
                    repcomprec!Car_Used_Date = comprec15!date_out
                    repcomprec!Car_Booked_By = comprec15!Car_Booked_By
                    repcomprec!Cat = comprec15!Car_Type
                    repcomprec!Category = comprec15!Car_Alloted
                    repcomprec!KM_out = comprec15!KM_out
                    repcomprec!KM_in = comprec15!KM_in
                    repcomprec!Extra_Km = comprec15!Extra_Km
                    repcomprec!Extra_Hrs = comprec15!Extra_Hrs
                    repcomprec!Outstation = 0
                    repcomprec!Outstation_Amt = comprec15!Qutstation_Allow
                    repcomprec!Fuel = comprec15!Fuel
                    repcomprec!Night = comprec15!Night_Billed_Amount
                    repcomprec!Assignment = comprec15!REquirement
                    repcomprec!Package_Name = comprec15!Package
                    repcomprec!Driver = comprec15!chaffeur_Code
                    repcomprec!CITY_NAME = CITY_NAME
                    repcomprec!Pickup = comprec15!Report_Address & ""
                    repcomprec!Reason_Delay = comprec15!Vendor & "-" & comprec15!vehicle_no
                    repcomprec!Reason_Cancel = "  "
                    repcomprec!other_Charges = comprec15!others
                    repcomprec!REMARK = "Uploaded on Current Date " & Date & ""
                    repcomprec!Parking_Toll = Round(f, 2)
                    repcomprec!other_taxes = Round(g, 2)
                    repcomprec!basic = Round(sale_amt, 2)
                    repcomprec!Uploaded = 0
                    repcomprec!Service_Tax = comprec15!Service_Tax
                    repcomprec!cess_Tax = comprec15!Cess_Amt
                    repcomprec!Vat_Tax = comprec15!Dst_amt
                    
                    Set comprec2 = New ADODB.Recordset
                    strcmd = "select * from invoice_dec order by id"
                    comprec2.Open strcmd, con1, adOpenDynamic, adLockOptimistic
                    If Not comprec2.EOF Then
                    comprec2.MoveLast
                    repcomprec!Id = comprec2!Id + 1
                    End If
                    If comprec15!owned = True Then
                        repcomprec!owned = 1
                    Else
                        repcomprec!owned = 0
                    End If
                    Netent = 0
                    Else
                        Set repcomprec1 = New ADODB.Recordset
                        repcomprec1.Open "Error", con1, adOpenDynamic, adLockOptimistic
                        repcomprec1.AddNew
                        repcomprec1!Bill_No = comprec15!new_invoice
                        repcomprec1!CITY_NAME = CITY_NAME
                        repcomprec1!VDATE = comprec15!inv_DATE
                        repcomprec1!company = comprec15!company
                        repcomprec1!Amount = Round(Netent, 2)
                        repcomprec1!DS_no = comprec15!Physical_Duty
                        repcomprec1.Update
                        repcomprec1.Close
                    End If
                    a = 0
                    b = 0
                    c = 0
                    d = 0
                    e = 0
                    f = 0
                    g = 0
                    i = 0
                    k = 0
                    i = 0
                    a1 = 0
                    a2 = 0
                    a3 = 0
                    Service_Tax = 0
                    cess_Tax = 0
                    Dst_tax = 0
                    sale_amt = 0
                    car_no = ""
                    ctr = ctr + 1
            End If
        comprec15.MoveNext
        Loop
        repcomprec.UpdateBatch
        comprec15.Close
Else
        MsgBox "No Transaction is there is the Database !", vbCritical, "Error"
End If
    MsgBox "Updated Sucessfully", vbExclamation, "Updated"
    Unload status
End Sub

Private Sub Command2_Click()
cryctrl.WindowTitle = "Date Wise Invoice Report"
    cryctrl.ReportFileName = App.Path & "\reportS\Tally.rpt"
                     With cryctrl
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Private Sub Command3_Click()
'update_data
End Sub

Private Sub Command4_Click()
back.Show
End Sub

Private Sub Command5_Click()
Unload Me
End Sub


Private Sub Command6_Click()

End Sub

Private Sub Form_Load()
connection1
connection2
Me.Width = Screen.Width
Me.Height = Screen.Height
ListView1.Left = 0

ListView1.Width = Screen.Width
ListView1.Refresh
ListView1.Width = Screen.Width
DTPicker1.Value = Format(Date, "dd-mm-yyyy")
DTPicker2.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker3.Value = Format(Date, "dd-mm-yyyy")
'Sorting a Listview Control
ListView1.Height = Screen.Height - ListView1.Top - 4200
'Set m_cHdrIcons.ListView = ListView1
'Call m_cHdrIcons.SetHeaderIcons(0, soAscending)
End Sub

Private Sub Option1_Click()
If Option1.Value = True Then
    Frame2(0).ZOrder 0
Else
    Frame2(0).ZOrder 1
End If
End Sub

Private Sub Option2_Click()
If Option2.Value = True Then
    Frame2(1).ZOrder 0
Else
    Frame2(1).ZOrder 1
End If
End Sub

'Sorting The Listview
Private Sub ListView1_ColumnClick(ByVal ColumnHeader As ColumnHeader)
  Dim i As Integer
  ' Toggle the clicked column's sort order only if the active colum is clicked
  ' (iow, don't reverse the sort order when different columns are clicked).
  If (ListView1.SortKey = ColumnHeader.Index - 1) Then
    ColumnHeader.Tag = Not Val(ColumnHeader.Tag)
  End If
  ' Set sort order to that of the respective ListSortOrderConstants value
  ListView1.SortOrder = Abs(Val(ColumnHeader.Tag))
  ' Get the zero-based index of the clicked column.
  ' (ColumnHeader.Index is one-based).
  ListView1.SortKey = ColumnHeader.Index - 1
  ' Sort the ListView
  ListView1.Sorted = True
  ' Set the header icons
'  Call m_cHdrIcons.SetHeaderIcons(ListView1.SortKey, ListView1.SortOrder)
End Sub



Public Sub update_data()
Dim comprec15 As ADODB.Recordset
ListView1.ListItems.Clear
Set repcomprec = New ADODB.Recordset
If Option1.Value = True Then
    strcmd = "select * from H_Invoice WHERE Inv_Date =Convert(DateTime, '" & DTPicker1.Value & "', 103)"
    strcmd = strcmd & " AND  LEN(INV_NO)>1"
'    strcmd = strcmd & " and company  not like  '" & "COMP" & "%'"
'    strcmd = strcmd & " and company  not like  '%" & "TAJ" & "%'"
    strcmd = strcmd & " AND LEN(NEW_INVOICE)>1"
    strcmd = strcmd & " AND void_invoice=0"
    strcmd = strcmd & " order by inv_date"
    str1 = DTPicker1.Value
End If

If Option2.Value = True Then
    strcmd = "select * from H_invoice where Inv_Date >=Convert(DateTime, '" & DTPicker2.Value & "', 103)"
    strcmd = strcmd & " AND Inv_Date <=Convert(DateTime, '" & DTPicker3.Value & "', 103)"
'    strcmd = strcmd & " and company  not like  '%" & "TAJ" & "%'"
'    strcmd = strcmd & " and company  not like  '" & "COMP" & "%'"
    strcmd = strcmd & " AND LEN(INV_NO)>1"
    strcmd = strcmd & " AND LEN(NEW_INVOICE)>1"
    strcmd = strcmd & " AND void_invoice=0"
    strcmd = strcmd & " order by inv_date"
    str1 = "From " & DTPicker2.Value & " To " & DTPicker3.Value
End If

Set comprec15 = con.Execute(strcmd)
If Not comprec15.EOF Then
    DoEvents
    status.Show
    DoEvents
   Set repcomprec1 = New ADODB.Recordset
   repcomprec1.Open "invoice_dec", con2, adOpenDynamic, adLockOptimistic
   strcmd = "select * from h_unit"
   Set NEWcomprec1 = con.Execute(strcmd)
   If Not NEWcomprec1.EOF Then
    CITY_NAME = NEWcomprec1!CITY & ""
   Else
    CITY_NAME = ""
   End If
    
    Do While Not comprec15.EOF
            ListView1.Refresh
            
            strcmd = "select * from invoice_dec where Actual_Invoice ='" & comprec15!new_invoice & "'"
            strcmd = strcmd & " and branch='" & CITY_NAME & "'"
            Set comprec5 = con2.Execute(strcmd)
            If Not comprec5.EOF Then
            
            Else
        
            strcmd = "SELECT * FROM H_DUTYSLIP WHERE DUTY_SLIP_NO='" & comprec15!Duty_Slip_No & "'"
            Set comprec1 = con.Execute(strcmd)
            If Not comprec1.EOF Then
                a = comprec1!package_Amount
                b = comprec1!extra_Km_Amount
                c = comprec1!EXTRA_HRS_AMOUNT
                Netent = a + b + c
                d = comprec1!COMPANY_DISCOUNT
                e = comprec1!SPL_DISCOUNT
               If IsNull(comprec1!Qutstation_Allow) = True Then
                    k = 0
               Else
                    k = Val(comprec1!Qutstation_Allow) & ""
               End If
                
                l = Val(comprec1!Night_Billed_Amount & "")
                If comprec15!direct = True Then
                    h = 0
                Else
                    h = comprec15!Fuel
                End If
                f = comprec1!PARKING_TAX
                g = comprec1!others
                sale_amt = a + b + c + h + k + l - d - e
                i = (a + b + c - d - e + k + l + h + g + f)
                
            strcmd = "select * from h_unit"
            Set comprec3 = con.Execute(strcmd)
            If Not comprec3.EOF Then
                 TAX_AMT = comprec3!Service_Tax
                 Cess_Amt = comprec3!CESS
             Else
                 TAX_AMT = 0
                 Cess_Amt = 0
             End If
                Service_Tax = (i * TAX_AMT) / 100
                cess_Tax = (i * Cess_Amt) / 100
                Dst_tax = (i * 12.5) / 100
                GROSS = i + Service_Tax + cess_Tax
            End If
            
            If IsNull(a) = True Or IsNull(b) = True Or IsNull(c) = True Or IsNull(d) = True Then
                a = 0
                b = 0
                c = 0
                d = 0
                e = 0
                f = 0
                g = 0
                k = 0
                sale_amt = 0
                Service_Tax = 0
                cess_Tax = 0
            End If
           ' lt.SubItems(5) = a
            'lt.SubItems(6) = b
            'lt.SubItems(7) = C
            'lt.SubItems(8) = d
            'lt.SubItems(9) = e
            'lt.SubItems(10) = f
            'lt.SubItems(11) = g
            'lt.SubItems(12) = k
            'lt.SubItems(13) = l
            'lt.SubItems(14) = h
            'lt.SubItems(15) = Round(sale_amt, 2)
            'lt.SubItems(16) = Round(service_Tax, 2)
            'lt.SubItems(17) = Round(cess_tax, 2)
            'lt.SubItems(18) = Round((service_Tax + cess_tax), 2)
            'lt.SubItems(19) = 0
            'lt.SubItems(20) = sale_amt + f + g + service_Tax + cess_tax + dst_tax
            
            If comprec1!owned = True Then
           '     lt.SubItems(21) = comprec1!Vehicle_no
                car_no = comprec1!vehicle_no
            Else
            '    lt.SubItems(21) = comprec1!vendor & "-" & comprec1!Vehicle_no
                car_no = comprec1!Vendor & "-" & comprec1!vehicle_no
            End If
            
            strcmd = "SELECT * FROM H_RESERVATION WHERE code ='" & comprec1!reservation_no & "'"
            Set comprec3 = con.Execute(strcmd)
            If Not comprec3.EOF Then
                branch = comprec3!branch & ""
            Else
              '  lt.SubItems(22) = ""
            End If
            
            strcmd = "select * from h_unit"
            Set NEWcomprec1 = con.Execute(strcmd)
            If Not NEWcomprec1.EOF Then
                CITY_NAME = NEWcomprec1!CITY & ""
            Else
                CITY_NAME = ""
            End If
            
            If Netent > 0 Then
            repcomprec1.AddNew
            repcomprec1!branch = CITY_NAME & ""
            repcomprec1!actual_Invoice = comprec15!new_invoice & ""
            strcmd = "select * from h_dutyslip where `No='" & comprec15!Duty_Slip_No & "'"
            Set newcomprec = con.Execute(strcmd)
            
            If Not newcomprec.EOF Then
                 repcomprec1!Car_Booked_By = newcomprec!Booked_By & ""
                 repcomprec1!Car_Used_Date = newcomprec!Duty_slip_date & ""
            End If
            
            If comprec15!direct = True Then
                repcomprec1!direct = 1
            Else
                repcomprec1!direct = 0
            End If
                
            'To Update Category
            repcomprec1!Cat = comprec1!Car_Type & ""
            repcomprec1!Category = comprec1!Category & ""
            
            'To Update Vendor
            If comprec1!owned = True Then
                repcomprec1!owned = 1
            Else
                repcomprec1!owned = 0
            End If
                                
                repcomprec1!Duty_Slip_No = comprec15!Physical_Duty & "-" & newcomprec!Booked_By & "-" & CStr(newcomprec!Duty_slip_date)
                repcomprec1!STAX = Round(Service_Tax + cess_Tax, 0)
                repcomprec1!company = comprec15!company & ""
                repcomprec1!VDATE = comprec15!inv_DATE & ""
                If comprec15!company = "I B M INDIA LTD." Then
                    branch = "Delhi"
                End If
                
                If comprec3!direct = True Then
                    repcomprec1!invoice_no = comprec15!Physical_Duty & ""
                Else
                    repcomprec1!invoice_no = Val(Mid(comprec15!new_invoice, 4, Len(comprec15!new_invoice))) & "INV"
                End If
                repcomprec1!Client_Name = comprec15!Client & ""
                repcomprec1!Branch_Booked = branch_from & ""
                repcomprec1!vehicle_no = car_no & ""
                If comprec1!owned = True Then
                    repcomprec1!Parking_Toll = Round(f, 0)
                    repcomprec1!other_taxes = Round(g, 0)
                    repcomprec1!basic = Round(sale_amt, 0)
                Else
                    a1 = Round(f, 0)
                    a2 = Round(g, 0)
                    a3 = Round(sale_amt, 0)
                    repcomprec1!basic = Round((a1 + a2 + a3), 0)
                    repcomprec1!other_taxes = 0
                    repcomprec1!Parking_Toll = 0
                End If
                    'repcomprec1!Net_Amount = GROSS
                    repcomprec1.Update
'                    repcomprec1.Close
                    Netent = 0
                    End If
                a = 0
                b = 0
                c = 0
                d = 0
                e = 0
                f = 0
                g = 0
                i = 0
                k = 0
                i = 0
                a1 = 0
                a2 = 0
                a3 = 0
                Service_Tax = 0
                cess_Tax = 0
                Dst_tax = 0
                sale_amt = 0
                car_no = ""
    End If
        comprec15.MoveNext

    Loop
    
        comprec15.Close
Else
        MsgBox "No Transaction is there is the Database !", vbCritical, "Error"
End If
Unload status
MsgBox "Data Updated SucessFully", vbInformation, "Updated"
End Sub
