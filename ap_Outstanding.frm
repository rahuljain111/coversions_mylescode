VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form6 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FF8080&
   Caption         =   "Creditors Outstanding"
   ClientHeight    =   7185
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7185
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "ap_Outstanding.frx":0000
   LinkTopic       =   "Form6"
   ScaleHeight     =   7185
   ScaleWidth      =   7185
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton AGING 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Aging"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   6120
      Width           =   1455
   End
   Begin VB.CheckBox Check2 
      BackColor       =   &H00FF8080&
      Caption         =   "All"
      Height          =   210
      Left            =   4800
      TabIndex        =   3
      Top             =   1440
      Width           =   615
   End
   Begin VB.ComboBox Combo2 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   1680
      TabIndex        =   2
      Top             =   1320
      Width           =   3135
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   9
      Top             =   6690
      Visible         =   0   'False
      Width           =   7185
      _ExtentX        =   12674
      _ExtentY        =   873
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00FF8080&
      Caption         =   "All"
      Height          =   210
      Left            =   4800
      TabIndex        =   1
      Top             =   720
      Width           =   615
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   1680
      TabIndex        =   0
      Top             =   720
      Width           =   3135
   End
   Begin VB.CommandButton Command2 
      BackColor       =   &H00FFFFFF&
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4200
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   6120
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Preview"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1320
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6120
      Width           =   1455
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   0
      Top             =   1560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
      WindowShowSearchBtn=   -1  'True
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   1680
      TabIndex        =   4
      Top             =   1920
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   16777215
      CustomFormat    =   "dd-MM-yyyy"
      Format          =   16580611
      CurrentDate     =   37865
   End
   Begin VB.Label LBL 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1800
      TabIndex        =   12
      Top             =   3360
      Width           =   45
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "As on Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   1
      Left            =   480
      TabIndex        =   11
      Top             =   1920
      Width           =   885
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Site"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   0
      Left            =   840
      TabIndex        =   10
      Top             =   1320
      Width           =   315
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Vendor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   2
      Left            =   840
      TabIndex        =   8
      Top             =   720
      Width           =   615
   End
End
Attribute VB_Name = "Form6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim chk As Boolean

Private Sub AGING_Click()
Set comprec = con1.Execute("truncate table ap_Aging")
Dim newcomprec As ADODB.Recordset
strcmd = "SELECT * FROM Creditors_Outstanding"
Set newcomprec = con1.Execute(strcmd)

If Not newcomprec.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "AP_aging", con1, adOpenDynamic, adLockOptimistic
    status.LBL.Caption = "Loading Aging ..........."
    Do While Not newcomprec.EOF
        status.Show
       a1 = newcomprec!gl_DATE
       a2 = DTPicker1.Value
       total_days = DateDiff("d", a1, a2)
       'MsgBox total_days
        repcomprec.AddNew
       
       If total_days >= 0 And total_days <= 30 Then
            netamt = newcomprec!invoice_AMOUNT
            repcomprec!upTO_30 = netamt
            repcomprec!upTO_60 = 0
            repcomprec!upTO_90 = 0
            repcomprec!upTO_180 = 0
            repcomprec!MORE_180 = 0
       End If
       
       If total_days > 30 And total_days <= 60 Then
              netamt = newcomprec!invoice_AMOUNT
              repcomprec!upTO_30 = 0
              repcomprec!upTO_60 = netamt
              repcomprec!upTO_90 = 0
              repcomprec!upTO_180 = 0
              repcomprec!MORE_180 = 0
              
       End If
       
       If total_days > 60 And total_days <= 90 Then
              netamt = newcomprec!invoice_AMOUNT
              repcomprec!upTO_30 = 0
              repcomprec!upTO_60 = 0
              repcomprec!upTO_90 = netamt
              repcomprec!upTO_180 = 0
              repcomprec!MORE_180 = 0
              
       End If
              
       If total_days >= 91 And total_days <= 180 Then
              netamt = newcomprec!invoice_AMOUNT
              repcomprec!upTO_30 = 0
              repcomprec!upTO_60 = 0
              repcomprec!upTO_90 = 0
              repcomprec!upTO_180 = netamt
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 181 Then
              netamt = newcomprec!invoice_AMOUNT
              repcomprec!upTO_30 = 0
              repcomprec!upTO_60 = 0
              repcomprec!upTO_90 = 0
              repcomprec!upTO_180 = 0
              repcomprec!MORE_180 = netamt
       End If
      
       repcomprec!Vendor = newcomprec!Vendor
       repcomprec!Site = newcomprec!Site
       repcomprec!Amount = newcomprec!invoice_AMOUNT
       repcomprec.UpdateBatch
        'StatusBar1.Panels(1).Text = "Loading ..................!" & newcomprec!invoice_num
        newcomprec.MoveNext
    Loop
   
Else
'Exit Sub
End If
Unload status
a = MsgBox("Aging Uploaded!", vbYesNo, "Print")
If a = vbYes Then
agn_prn
Else
Exit Sub
End If
End Sub

Public Sub agn_prn()
                    cryctrl.ReportFileName = App.Path & "\reportS\Ap_aging.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Private Sub Check1_Click()
If Check1.Value = 1 Then
Combo1.Text = "All"
End If
End Sub

Private Sub Command1_Click()
Dim D2 As Date
Dim D3 As Date
p_start_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
Set comprec = con1.Execute("truncate table Creditors_Outstanding")
strcmd = "BEGIN"
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & "END;"
Set comprec1 = con.Execute(strcmd)

If Check1.Value = 1 Then
    strcmd = "select distinct apd.invoice_id,APO.VENDOR_ID,PI.GL_DATE,po.vendor_name,po1.vendor_site_code,api.approval_status_lookup_code ,api.invoice_type_lookup_code,  api.invoice_date,"
    strcmd = strcmd & " api.invoice_num,api.prepay_amount_applied,api.invoice_amount,api.amount_paid,(api.invoice_amount-api.amount_paid) as Balance"
    strcmd = strcmd & " from ap_invoices_V api,ap_invoice_distributions_all apd,po_vendor_sites_all po1,po_vendors po where api.vendor_id=po.vendor_id "
    strcmd = strcmd & " and po.vendor_id=po1.vendor_id"
    strcmd = strcmd & " and api.gl_date <='" & p_start_date & "'"
    strcmd = strcmd & " and  api.vendor_site_id=po1.vendor_site_id"
    strcmd = strcmd & " and api.invoice_id=apd.invoice_id"
    If Check2.Value = 1 Then
        'strcmd = strcmd & " and PO1.vendor_site_code like '" & Combo2.Text & "%'"
    Else
        strcmd = strcmd & " and PO1.vendor_site_code = '" & Combo2.Text & "'"
    End If
Else
    strcmd = "select distinct apd.invoice_id,PO.VENDOR_ID,API.GL_DATE,po.vendor_name,po1.vendor_site_code,api.approval_status_lookup_code ,api.invoice_type_lookup_code,  api.invoice_date,"
    strcmd = strcmd & " api.invoice_num,api.prepay_amount_applied,api.invoice_amount,api.amount_paid,(api.invoice_amount-api.amount_paid) as Balance"
    strcmd = strcmd & " from ap_invoices_V api,ap_invoice_distributions_all apd, po_vendor_sites_all po1,po_vendors po where api.vendor_id=po.vendor_id "
    strcmd = strcmd & " and PO.vendor_name ='" & Combo1.Text & "'"
    strcmd = strcmd & " and api.gl_date <='" & p_start_date & "'"
    strcmd = strcmd & " and  api.vendor_site_id=po1.vendor_site_id"
    strcmd = strcmd & " and po.vendor_id=po1.vendor_id"
    strcmd = strcmd & " and api.invoice_id=apd.invoice_id"
    If Check2.Value = 1 Then
        'strcmd = strcmd & " and PO1.vendor_site_code like '" & Combo2.Text & "%'"
    Else
        strcmd = strcmd & " and PO1.vendor_site_code = '" & Combo2.Text & "'"
    End If
    
End If

Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "Creditors_Outstanding", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
    
    
    strcmd = "BEGIN"
    strcmd = strcmd & " dbms_application_info.set_client_info(101);"
    strcmd = strcmd & "END;"
    Set comprec2 = con.Execute(strcmd)
    
    'strcmd = "select * from ap_invoice_payment_history_v aa1,ap_invoices_all aa2,po_vendors p1"
    'strcmd = strcmd & " Where aa1.invoice_id = aa2.invoice_id"
    ''strcmd = strcmd & " and aa2.vendor_id='" & comprec!vendor_id & "'"
    'strcmd = strcmd & " and  AA1.invoice_id= '" & comprec!invoice_id & "'"
    'Set comprec1 = con.Execute(strcmd)
    
    'If Not comprec1.EOF Then
'        MsgBox comprec1!check_amount & "   " & comprec1!check_DATE
     '   chk = True
    'End If
    
    If comprec!invoice_num = "4579" Then
        MsgBox ""
    End If
    

    
    LBL.Caption = "UPloading : " & comprec!Vendor_Name & " --" & comprec!invoice_num
    repcomprec.AddNew
    repcomprec!Vendor = comprec!Vendor_Name
    repcomprec!invoice_num = comprec!invoice_num
    repcomprec!INVOICE_DATE = comprec!INVOICE_DATE
    repcomprec!INVOICE_ID = comprec!INVOICE_ID
    repcomprec!invoice_AMOUNT = comprec!invoice_AMOUNT
    repcomprec!Site = comprec!vendor_site_code
    
    'repcomprec!paid_amount = PAID 'comprec1!check_amount
        
    
    If IsNull(comprec!amount_paid) = True Then
        repcomprec!paid_amount = 0
    Else
        repcomprec!paid_amount = comprec!amount_paid
    End If
    
    
    repcomprec!Inv_Type = comprec!invoice_type_lookup_code
    repcomprec!status = comprec!approval_status_lookup_code
    
    If IsNull(comprec!amount_paid) = True Then
        PAID1 = 0
    Else
        PAID1 = comprec!amount_paid
    End If
    
    If comprec!invoice_type_lookup_code = "PREPAYMENT" And comprec!approval_status_lookup_code = "AVAILABLE" Then
        repcomprec!Balance = -(comprec!amount_paid - comprec!PREPAY_AMOUNT_APPLIED)
    Else
         repcomprec!Balance = comprec!invoice_AMOUNT - PAID1
         comprec.MoveNext
    End If
    Loop
    repcomprec.UpdateBatch
End If

        LBL.Caption = ""
        
        strcmd = "select * from Creditors_Outstanding"
        Set comprec2 = con1.Execute(strcmd)
        If Not comprec2.EOF Then
            Do While Not comprec2.EOF
                strcmd = "select * from  AP_UNAPPLY_PREPAYS_V where invoice_id='" & comprec2!INVOICE_ID & "'"
                Set newcomprec = con.Execute(strcmd)
                If Not newcomprec.EOF Then
                    Set repcomprec = New ADODB.Recordset
                    repcomprec.Open "Creditors_Outstanding", con1, adOpenDynamic, adLockOptimistic
                        D2 = newcomprec!accounting_date
                        D3 = comprec2!INVOICE_DATE
                        If D3 <= DTPicker1.Value And D2 > DTPicker1.Value Then
                            pd = newcomprec!PREPAY_AMOUNT_APPLIED
                            repcomprec!paid_amount = repcomprec!paid_amount - pd
                        End If
                   
                    End If
                      comprec2.MoveNext
                    Loop
                    repcomprec.UpdateBatch
                End If
    

a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    a1 = MsgBox("Want to Print The Detail/Summary (Y/N)? " & Code & "(Y/N?)", vbYesNo, "Print")
    If a1 = vbYes Then
        cr_led
    Else
        cr_led_Summ
    End If
End If

End Sub

Public Sub cr_led()
        'cryctrl.WindowTitle = CCODE & " Wise Report"
                    cryctrl.ReportFileName = App.Path & "\reportS\Ap_Out.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub cr_led_Summ()
                    cryctrl.ReportFileName = App.Path & "\reportS\Ap_Out_summ.rpt"
                     With cryctrl
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub





Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
'Me.Left = frmmain.ListView1.Width + 2000
'Me.Top = frmmain.ListView1.Height - 7500
ADDVEN
ADDSITE
DTPicker1.Value = Date
End Sub

Public Sub ADDVEN()
strcmd = "select DISTINCT vendor_name"
strcmd = strcmd & " from po_vendors "
'strcmd = strcmd & " Where q.vendor_id = w.vendor_id "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!Vendor_Name
        comprec.MoveNext
    Loop
End If
Combo1.Text = Combo1.List(0)
End Sub


Public Sub ADDSITE()
strcmd = "select DISTINCT vendor_site_code"
strcmd = strcmd & " from po_vendor_sites_all "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo2.AddItem comprec!vendor_site_code
        comprec.MoveNext
    Loop
End If
Combo2.Text = Combo2.List(0)
End Sub


