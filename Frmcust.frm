VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form5 
   BackColor       =   &H00FF8080&
   Caption         =   "AR Customer Uploading Interface"
   ClientHeight    =   7125
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8940
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   14.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Frmcust.frx":0000
   LinkTopic       =   "Form5"
   MDIChild        =   -1  'True
   ScaleHeight     =   7125
   ScaleWidth      =   8940
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command2 
      Caption         =   "U&pdate"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4800
      TabIndex        =   20
      Top             =   4920
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Find"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7080
      TabIndex        =   19
      Top             =   3360
      Width           =   1335
   End
   Begin VB.OptionButton oCust 
      BackColor       =   &H00FF8080&
      Caption         =   "Old Customer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Left            =   3960
      TabIndex        =   18
      Top             =   1680
      Width           =   1635
   End
   Begin VB.OptionButton nCust 
      BackColor       =   &H00FF8080&
      Caption         =   "New Customer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   350
      Left            =   1920
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   17
      Top             =   1680
      Value           =   -1  'True
      Width           =   1875
   End
   Begin VB.TextBox Text4 
      Height          =   405
      Left            =   3960
      TabIndex        =   0
      Top             =   2760
      Width           =   2895
   End
   Begin VB.TextBox Text3 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   1
      Top             =   3360
      Width           =   2895
   End
   Begin VB.CommandButton Command1 
      Caption         =   "I&nsert"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1920
      TabIndex        =   6
      Top             =   4920
      Width           =   1335
   End
   Begin VB.TextBox Text2 
      Height          =   405
      Left            =   5880
      TabIndex        =   3
      Top             =   3960
      Width           =   1815
   End
   Begin VB.TextBox Text1 
      Height          =   405
      Left            =   3120
      TabIndex        =   2
      Top             =   3960
      Width           =   1575
   End
   Begin VB.CommandButton JeweledButton2 
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton JeweledButton1 
      Caption         =   "&Upload Customer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3240
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4920
      Width           =   1575
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   11
      Top             =   6750
      Width           =   8940
      _ExtentX        =   15769
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6324
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   2
            TextSave        =   "NUM"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            AutoSize        =   1
            Object.Width           =   6324
            TextSave        =   "10:51 AM"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   10200
      TabIndex        =   10
      Top             =   1200
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      Left            =   8880
      TabIndex        =   9
      Top             =   1200
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "View"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7560
      TabIndex        =   8
      Top             =   7320
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   2295
      Left            =   12240
      TabIndex        =   7
      Top             =   840
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   11786751
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Code"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Name"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Address"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "City"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label5 
      Caption         =   "Maximum ID"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   16
      Top             =   2760
      Width           =   2055
   End
   Begin VB.Label Label4 
      Caption         =   "Enter Company Name"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   15
      Top             =   3360
      Width           =   2055
   End
   Begin VB.Label Label3 
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4680
      TabIndex        =   14
      Top             =   3960
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1920
      TabIndex        =   13
      Top             =   3960
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "AR Customer Uploading Interface"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   120
      TabIndex        =   12
      Top             =   0
      Width           =   4935
   End
End
Attribute VB_Name = "Form5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
If (Text3.Text = "") Then
    MsgBox ("Please Enter Company Name..")
    Exit Sub
End If

strcmd = "select CIPL_CUST, ID from auto_import where CIPL_CUST like '" & Replace(Text3.Text, "'", "''") & "' or ID = '" & Text1.Text & "'"
Set comprec = con1.Execute(strcmd)
If comprec.EOF Then

strcmd = "insert into auto_import (RMS_Cust,CIPL_CUST,ID,CATEGORY)"
strcmd = strcmd & "values ('" & Replace(Text3.Text, "'", "''") & "'"
strcmd = strcmd & ", '" & Replace(Text3.Text, "'", "''") & "'"
strcmd = strcmd & ", '" & Text1.Text & "'"
strcmd = strcmd & ", 'Hub')"
Set comprec = con1.Execute(strcmd)

strcmd = "insert into auto (code,CIPL_CUST,payment_method,profile,CATEGORY)"
strcmd = strcmd & "values ('" & Text1.Text & "'"
strcmd = strcmd & ", '" & Replace(Text3.Text, "'", "''") & "'"
strcmd = strcmd & ", 'CIPL ICICI 806'"
strcmd = strcmd & ", 'Standard'"
strcmd = strcmd & ", 'Hub')"
Set comprec = con1.Execute(strcmd)
Command1.Enabled = False
JeweledButton1.Enabled = True
MsgBox "Done"
Else
    If comprec!Id = Text1.Text Then
        MsgBox "Duplicate ID used."
        Text1.Text = Text1.Text + 1
        Text4.Text = Text1.Text
        Text2.Text = Text1.Text
    Else
        MsgBox "The Given Client is already exists in the Tables!"
    End If
End If

End Sub

Private Sub Command2_Click()
    strcmd = "select max(distinct orig_system_reference) as orig_system_reference from HZ_PARTIES WHERE STATUS ='A' and "
    strcmd = strcmd & "LENGTH(orig_system_reference) = 7 and PARTY_TYPE='ORGANIZATION' "
    strcmd = strcmd & "order by orig_system_reference desc"
    Set comprec = con.Execute(strcmd)
    Text4.Text = comprec!orig_system_reference + 1
    Text1.Text = comprec!orig_system_reference + 1
    Text2.Text = comprec!orig_system_reference + 1
    'JeweledButton1.Enabled = True
    Command1.Enabled = True
End Sub

Private Sub Command4_Click()
If Text3.Text = "" Then
    If Text1.Text <> "" Then
        strcmd = "select top 1 CIPL_CUST, ID from auto_import where ID = '" & Text1.Text & "'"
    Else
        If Text2.Text <> "" Then
           strcmd = "select top 1 CIPL_CUST, ID from auto_import where ID = '" & Text2.Text & "'"
        End If
    End If
Else
    strcmd = "select top 1 CIPL_CUST, ID from auto_import where RMS_CUST like '" & Replace(Text3.Text, "'", "''") & "%'"
End If

Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Text3.Text = comprec!CIPL_CUST
    Text1.Text = comprec!Id
    Text2.Text = comprec!Id
Else
    MsgBox ("No Record Found...")
End If
End Sub

Private Sub Label5_Click()
strcmd = "select max(id) as mid from auto_import"
Set comprec = con1.Execute(strcmd)
Text4.Text = comprec.Fields("mid")
End Sub

'Private Sub Command2_Click()
'If (Text3.Text = "") Then
'    MsgBox ("Please Enter Company Name..")
'    Exit Sub
'End If
'strcmd = "insert into auto (code,CIPL_CUST,payment_method,profile,CATEGORY)"
'strcmd = strcmd & "values ('" & Text1.Text & "'"
'strcmd = strcmd & ", '" & Text3.Text & "'"
'strcmd = strcmd & ", 'CIPL ICICI 806'"
'strcmd = strcmd & ", 'Standard'"
'strcmd = strcmd & ", 'Hub')"
'Set comprec = con1.Execute(strcmd)
'MsgBox "Done"
'End Sub

Private Sub JeweledButton1_Click()
Dim ctr As Variant
Dim c_code As Variant
Dim flag As String

If (nCust.Value = True) Then
    strcmd = "select max(distinct orig_system_reference) as orig_system_reference from HZ_PARTIES WHERE STATUS ='A' and "
    strcmd = strcmd & "LENGTH(orig_system_reference) = 7 and PARTY_TYPE='ORGANIZATION' "
    strcmd = strcmd & "order by orig_system_reference desc"
    Set comprec = con.Execute(strcmd)
    Text4.Text = comprec!orig_system_reference + 1
    Text1.Text = comprec!orig_system_reference + 1
    Text2.Text = comprec!orig_system_reference + 1
    
    If (Text3.Text = "") Then
        MsgBox ("Please Enter Company Name..")
        Exit Sub
    End If
End If
If (oCust.Value = True) Then
    If (Text1.Text = "" Or Text2.Text = "") Then
        MsgBox ("Please Enter ID..")
        Exit Sub
    End If
End If
Set repcomprec = con.Execute("delete from RA_Customers_INTERFACE_ALL")
Set repcomprec = con.Execute("delete from RA_Customer_Profiles_INT_ALL")
Set repcomprec = con.Execute("delete from RA_Cust_Pay_Method_int_ALL")

strcmd = "select DISTINCT(CIPL_CUST),CODE from Auto where "
'strcmd = strcmd & "  code ='" & "201628" & "'"
'strcmd = strcmd & "  code ='" & Val(Text1.Text) & "'"
strcmd = strcmd & "  code between " & "'" & Val(Text1.Text) & "'" & " and " & "'" & Val(Text2.Text) & "'"
strcmd = strcmd & " Order By Code"


Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "RA_Customers_INTERFACE_ALL", con, adOpenDynamic, adLockOptimistic
    Set repcomprec2 = New ADODB.Recordset
    repcomprec2.Open "RA_Customer_Profiles_INT_ALL", con, adOpenDynamic, adLockOptimistic
    Set REPcomprec3 = New ADODB.Recordset
    REPcomprec3.Open "RA_Cust_Pay_Method_int_ALL", con, adOpenDynamic, adLockOptimistic
    DoEvents
    status.Show
    DoEvents
    Do While Not comprec.EOF
        StatusBar1.Panels(1).Text = comprec!CIPL_CUST & "ID : " & comprec!Code
        c_code = 1
        '----------FOR SHIP TO ENTRY---------------
        strcmd = "SELECT * FROM SHIP_TO " 'WHERE CITY= '" & "OPERATING LEASE" & "'"
        'strcmd = "SELECT * FROM SHIP_TO where city='" & "NOIDA" & "'"
        Set comprec3 = con1.Execute(strcmd)
        If Not comprec3.EOF Then
            Do While Not comprec3.EOF
                If c_code <> 1 Then
                    flag = "N"
                Else
                    c_code = c_code + 1
                    flag = "Y"
                End If
                repcomprec.AddNew
                repcomprec!ORIG_SYSTEM_CUSTOMER_ref = comprec!Code
                repcomprec!site_use_code = "BILL_TO"
                repcomprec!orig_system_address_ref = comprec3!CITY & "-" & comprec!Code
                repcomprec!INSERT_UPDATE_FLAG = "I"
                repcomprec!CUSTOMER_NAME = comprec!CIPL_CUST
                repcomprec!Address1 = comprec3!Address1
                'repcomprec!Address1 = comprec!Address
                repcomprec!CITY = comprec3!CITY
                repcomprec!State = comprec3!State
                repcomprec!POstal_Code = comprec3!POstal_Code
                repcomprec!COUNTRY = comprec3!COUNTRY
                repcomprec!LAST_UPDATED_BY = -1
                repcomprec!LAST_UPDATE_date = Date
                repcomprec!CREATED_BY = -1
                repcomprec!CREATion_date = Date
                repcomprec!BILL_TO_ORIG_ADDRESS_REF = ""
                repcomprec!CUSTOMER_STATUS = "A"
                repcomprec!PRIMARY_SITE_USE_FLAG = flag
                
                repcomprec.AddNew
                repcomprec!ORIG_SYSTEM_CUSTOMER_ref = comprec!Code
                repcomprec!site_use_code = "SHIP_TO"
                repcomprec!orig_system_address_ref = comprec3!CITY & "-" & comprec!Code
                repcomprec!INSERT_UPDATE_FLAG = "I"
                repcomprec!CUSTOMER_NAME = comprec!CIPL_CUST
                repcomprec!Address1 = comprec3!Address1
                'repcomprec!Address1 = comprec!Address
                repcomprec!CITY = comprec3!CITY
                repcomprec!State = comprec3!State
                repcomprec!POstal_Code = comprec3!POstal_Code
                repcomprec!COUNTRY = comprec3!COUNTRY
                repcomprec!LAST_UPDATED_BY = -1
                repcomprec!LAST_UPDATE_date = Date
                repcomprec!CREATED_BY = -1
                repcomprec!CREATion_date = Date
                repcomprec!BILL_TO_ORIG_ADDRESS_REF = comprec3!CITY & "-" & comprec!Code
                repcomprec!CUSTOMER_STATUS = "A"
                repcomprec!PRIMARY_SITE_USE_FLAG = "N"
                comprec3.MoveNext
            Loop
        End If
        comprec.MoveNext
    Loop
' Val(Text1.Text)
        'strcmd = "select distinct(CODE),profile FROM AUTO where code ='" & "201628" & "'"
        'between " & "'" & Val(Text1.Text) & "'" & " and " & "'" & Val(Text2.Text) & "'"
        
        'strcmd = "select distinct(CODE),profile FROM AUTO where code ='" & Val(Text1.Text) & "'"
        
        strcmd = "select distinct(CODE),profile FROM AUTO where code between " & "'" & Val(Text1.Text) & "'" & " and " & "'" & Val(Text2.Text) & "'"
        
        Set comprec1 = con1.Execute(strcmd)
        If Not comprec1.EOF Then
            Do While Not comprec1.EOF
                    repcomprec2.AddNew
                    repcomprec2!INSERT_UPDATE_FLAG = "I"
                    repcomprec2!ORIG_SYSTEM_CUSTOMER_ref = comprec1!Code
                    repcomprec2!CUSTOMER_PROFILE_CLASS_NAME = comprec1!Profile
                    repcomprec2!CREDIT_HOLD = "N"
                    repcomprec2!LAST_UPDATED_BY = "-1"
                    repcomprec2!LAST_UPDATE_date = Date
                    repcomprec2!CREATED_BY = "-1"
                    repcomprec2!CREATion_date = Date
                    comprec1.MoveNext
        Loop
        End If
        repcomprec.UpdateBatch
        repcomprec2.UpdateBatch
        strcmd = "select distinct(CODE),(Payment_Method) from auto where "
        
        'strcmd = strcmd & "  code ='" & "201628" & "'"
        'between " & "'" & Val(Text1.Text) & "'" & " and " & "'" & Val(Text2.Text) & "'"
        'strcmd = strcmd & "  code ='" & Val(Text1.Text) & "'"
        
        strcmd = strcmd & "  code Between " & " '" & Val(Text1.Text) & "'" & " and " & "'" & Val(Text2.Text) & "'"
        
        Set comprec1 = con1.Execute(strcmd)
        If Not comprec1.EOF Then
            Do While Not comprec1.EOF
                strcmd = "SELECT * FROM SHIP_TO"  'where city='" & "NOIDA" & "'"
                'strcmd = "SELECT * FROM SHIP_TO where city='" & "NOIDA" & "'"
                Set comprec3 = con1.Execute(strcmd)
                If Not comprec3.EOF Then
                    Do While Not comprec3.EOF
                        strcmd = "SELECT * FROM PAYMENT ORDER BY PAYMENT_METHOD"
                        Set comprec4 = con1.Execute(strcmd)
                        If Not comprec4.EOF Then
                            Do While Not comprec4.EOF
                                REPcomprec3.AddNew
                                REPcomprec3!ORIG_SYSTEM_CUSTOMER_ref = comprec1!Code
                                REPcomprec3!PAYMENT_METHOD_NAME = comprec4!Payment_Method
                                If comprec1!Payment_Method = comprec4!Payment_Method Then
                                    REPcomprec3!PRIMARY_FLAG = "Y"
                                Else
                                    REPcomprec3!PRIMARY_FLAG = "N"
                                End If
                                REPcomprec3!orig_system_address_ref = comprec3!CITY & "-" & comprec1!Code
                                REPcomprec3!start_date = "01-01-2002"   'To Be Change
                                REPcomprec3!ORG_ID = "82"
                                REPcomprec3!LAST_UPDATED_BY = "-1"
                                REPcomprec3!LAST_UPDATE_date = Date
                                REPcomprec3!CREATED_BY = "-1"
                                REPcomprec3!CREATion_date = Date
                                comprec4.MoveNext
                            Loop
                        End If
                comprec3.MoveNext
                Loop
                End If
                comprec1.MoveNext
            Loop
        End If
        REPcomprec3.UpdateBatch
    If (nCust.Value = True) Then
        JeweledButton1.Enabled = False
        Command2.Enabled = True
    End If
    If (oCust.Value = True) Then
        JeweledButton1.Enabled = True
    End If
    MsgBox "Done"
    
    Unload status
End If
End Sub

Private Sub Command3_Click()
'strcmd = "SELECT * FROM TALLY_MIS ORDER BY COMPANY"
'Set comprec1 = con1.Execute(strcmd)
'If Not comprec1.EOF Then
'    Do While Not comprec1.EOF
'    strcmd = "SELECT * FROM AUTO_IMPORT WHERE RMS_CUST='" & comprec1!COMPANY & "'"
'    strcmd = strcmd & " AND BILL_TO='" & comprec1!BRANCH_BOOKED & "'"
'    Set comprec2 = con1.Execute(strcmd)
'    If Not comprec2.EOF Then
'        list1.AddItem comprec1!COMPANY & "---" & comprec1!BRANCH_BOOKED
''        comprec1.MoveNext
'    Else
'        List2.AddItem comprec1!COMPANY & "---" & comprec1!BRANCH_BOOKED
'    End If
'    comprec1.MoveNext
'    Loop
'    list1.Refresh
'    List2.Refresh
'End If
'MsgBox "DONE"
End Sub

Private Sub Form_Load()
'strconn = ("Driver={Microsoft ODBC for Oracle};Server=CIPL;Uid=apps;Pwd=apps;Persist Security Info=False")
'Set con = New ADODB.Connection
'con.Open strconn
'add_apps
'connection1
JeweledButton1.Enabled = False
Command2.Enabled = False
strcmd = "select max(distinct orig_system_reference) as orig_system_reference from HZ_PARTIES WHERE STATUS ='A' and "
strcmd = strcmd & "LENGTH(orig_system_reference) = 7 and PARTY_TYPE='ORGANIZATION' "
strcmd = strcmd & "order by orig_system_reference desc"
Set comprec = con.Execute(strcmd)
Text4.Text = comprec!orig_system_reference + 1
Text1.Text = comprec!orig_system_reference + 1
Text2.Text = comprec!orig_system_reference + 1
If (nCust.Value = True) Then
Text4.Enabled = False
Text1.Enabled = False
Text2.Enabled = False
Command4.Enabled = False
End If
Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2
End Sub

Private Sub JeweledButton2_Click()
Unload Me
End Sub

Private Sub nCust_Click()
strcmd = "select max(distinct orig_system_reference) as orig_system_reference from HZ_PARTIES WHERE STATUS ='A' and "
strcmd = strcmd & "LENGTH(orig_system_reference) = 7 and PARTY_TYPE='ORGANIZATION' "
strcmd = strcmd & "order by orig_system_reference desc"
Set comprec = con.Execute(strcmd)
Text4.Text = comprec!orig_system_reference + 1
Text1.Text = comprec!orig_system_reference + 1
Text2.Text = comprec!orig_system_reference + 1
    Command1.Enabled = True
    Command2.Enabled = False
    JeweledButton1.Enabled = False
    Command4.Enabled = False
    Text3.Enabled = True
    Text3.Text = ""
    Text4.Enabled = False
    Text1.Enabled = False
    Text2.Enabled = False
End Sub

Private Sub oCust_Click()
strcmd = "select max(distinct orig_system_reference) as orig_system_reference from HZ_PARTIES WHERE STATUS ='A' and "
strcmd = strcmd & "LENGTH(orig_system_reference) = 7 and PARTY_TYPE='ORGANIZATION' "
strcmd = strcmd & "order by orig_system_reference desc"
Set comprec = con.Execute(strcmd)
Text4.Text = comprec!orig_system_reference + 1
    
    JeweledButton1.Enabled = True
    Text4.Enabled = False
    'Text3.Enabled = False
    Text1.Enabled = True
    Text2.Enabled = True
    Command4.Enabled = True
    Command1.Enabled = False
    Command2.Enabled = False
    Text1.Text = ""
    Text2.Text = ""
End Sub
