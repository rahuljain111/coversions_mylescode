VERSION 5.00
Begin VB.Form Form6 
   Caption         =   "Customer Creation"
   ClientHeight    =   7530
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7590
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Customer_Creation.frx":0000
   LinkTopic       =   "Form6"
   MaxButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   7590
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   375
      Left            =   5640
      TabIndex        =   21
      Top             =   6960
      Width           =   975
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "&Exit"
      Height          =   375
      Left            =   3840
      TabIndex        =   10
      Top             =   6840
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Save"
      Height          =   375
      Left            =   2640
      TabIndex        =   9
      Top             =   6840
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Caption         =   "Choose Detail"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Left            =   480
      TabIndex        =   11
      Top             =   720
      Width           =   6615
      Begin VB.ComboBox Combo5 
         Height          =   330
         Left            =   2040
         TabIndex        =   8
         Top             =   4800
         Width           =   3255
      End
      Begin VB.TextBox Text3 
         Height          =   375
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   2640
         Width           =   3255
      End
      Begin VB.TextBox Text2 
         Height          =   1335
         Left            =   2040
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   7
         Top             =   3240
         Width           =   3255
      End
      Begin VB.ComboBox Combo4 
         Height          =   330
         Left            =   2040
         TabIndex        =   5
         Top             =   2130
         Width           =   3255
      End
      Begin VB.ComboBox Combo3 
         Height          =   330
         Left            =   2040
         TabIndex        =   4
         Top             =   1575
         Width           =   3255
      End
      Begin VB.ComboBox Combo1 
         Height          =   330
         Left            =   2040
         TabIndex        =   2
         Top             =   480
         Width           =   3255
      End
      Begin VB.TextBox Text1 
         Height          =   375
         Left            =   2040
         TabIndex        =   3
         Top             =   990
         Width           =   3255
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Payment Method"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   6
         Left            =   240
         TabIndex        =   20
         Top             =   4800
         Width           =   1395
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "RMS Customer"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   5
         Left            =   390
         TabIndex        =   19
         Top             =   1080
         Width           =   1245
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Address"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   4
         Left            =   915
         TabIndex        =   18
         Top             =   3240
         Width           =   720
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "State"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   3
         Left            =   1215
         TabIndex        =   17
         Top             =   2760
         Width           =   420
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "City"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   2
         Left            =   1320
         TabIndex        =   16
         Top             =   2280
         Width           =   315
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Bill To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   1
         Left            =   1140
         TabIndex        =   15
         Top             =   1680
         Width           =   495
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "-----"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5880
         TabIndex        =   14
         Top             =   240
         Width           =   300
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "ID"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5400
         TabIndex        =   13
         Top             =   240
         Width           =   150
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Customer Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   285
         TabIndex        =   12
         Top             =   480
         Width           =   1350
      End
   End
   Begin VB.OptionButton Option2 
      Caption         =   "New Customer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   3000
      TabIndex        =   1
      Top             =   240
      Width           =   1575
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Existing Customer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   600
      TabIndex        =   0
      Top             =   240
      Value           =   -1  'True
      Width           =   1935
   End
End
Attribute VB_Name = "Form6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub addcipl()
Combo1.Clear
strcmd = "select Customer_Name1 from Code order by Customer_Name1"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Combo1.AddItem comprec!Customer_Name1 & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
Combo1.Text = Combo1.List(0)
End Sub


Private Sub Combo1_Click()
strcmd = "select * from code where customer_name1='" & Combo1.Text & "'"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Label3.Caption = comprec!Code1
Else
    Label3.Caption = ""
End If
End Sub

Private Sub Combo4_Click()
strcmd = "select * from ship_to where city='" & Combo4.Text & "'"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
Text3 = comprec!State
Else
Text3 = ""
End If
End Sub

Public Sub clr()
Text1 = ""
Text2 = ""
Text3 = ""
Combo1.Text = ""
'Combo2.Text = ""
Combo3.Text = ""
Combo4.Text = ""
Combo5.Text = ""
End Sub
Private Sub Command1_Click()
a = MsgBox("Want to Add The Customer :" & Text1 & "-" & " Under " & vbCrLf & Combo1.Text & " (Y/N?", vbYesNo, "Save")
If a = vbYes Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "Auto_IMPort", con1, adOpenKeyset, adLockPessimistic
    repcomprec.AddNew
    repcomprec!Rms_Cust = UCase(Text1)
    repcomprec!CIPL_CUST = UCase(Combo1.Text)
    repcomprec!Bill_To = UCase(Combo3.Text)
    repcomprec!ID = UCase(Label3.Caption)  'BANGALORE    1000339
    repcomprec!Site_Code1 = UCase(Combo3.Text) & "    " & UCase(Label3.Caption)
    
    repcomprec!City1 = UCase(Combo4.Text)
    repcomprec!State1 = UCase(Combo4.Text)
    repcomprec!Address1 = UCase(Text2.Text)
    repcomprec!POstal_Code = "110011"
    repcomprec!Country = "IN"
    repcomprec!PAYMENT_METHOD = Combo5.Text & ""
    repcomprec.Update
    MsgBox "Save SucessFully", vbInformation, "Saved"
    clr
    Option1_Click
    Exit Sub
Else
    Exit Sub
End If
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Command3_Click()
strcmd = "SELECT CIPL_CUST FROM auto GROUP BY CIPL_CUST"
Set repcomprec = con1.Execute(strcmd)
ctr = 100001

If Not repcomprec.EOF Then
    Do While Not repcomprec.EOF
    Set repcomprec1 = New ADODB.Recordset
    strcmd = "SELECT * FROM auto WHERE CIPL_CUST='" & repcomprec!CIPL_CUST & "'"
    repcomprec1.Open strcmd, con1, adOpenKeyset, adLockPessimistic
    If Not repcomprec1.EOF Then

    strcmd = "UPDATE AUTO SET id=" & ctr & ""
    strcmd = strcmd & " WHERE CIPL_CUST='" & repcomprec!CIPL_CUST & "'"
    Set repcomprec1 = con1.Execute(strcmd)

    'Do While Not repcomprec1.EOF
     '       repcomprec1!cODE = ctr
      '      repcomprec1.Update
       '     repcomprec1.MoveNext
    'Loop
    'Repcomprec1.Close
    End If
    repcomprec.MoveNext
    ctr = ctr + 1
Loop
End If
MsgBox ":OK"







End Sub

Private Sub Form_Load()
connection1
Option1_Click
End Sub

Private Sub Option1_Click()
addcipl
'addrms
addcity
addbillto
End Sub

Public Sub addrms()
Combo2.Clear
strcmd = "select Rms_Cust from Auto_Import group by Rms_Cust"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Combo2.AddItem comprec!Rms_Cust & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
Combo2.Text = Combo2.List(0)
End Sub


Public Sub addcity()
Combo4.Clear
strcmd = "select city from ship_to order by City"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Combo4.AddItem comprec!city & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
Combo4.Text = Combo4.List(0)
End Sub


Public Sub addbillto()
Combo3.Clear
strcmd = "select city from ship_to order by City"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Combo3.AddItem comprec!city & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
Combo3.Text = Combo3.List(0)
End Sub

