VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Voucher 
   BackColor       =   &H00000000&
   Caption         =   "Vendor Voucher"
   ClientHeight    =   10500
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   15240
   LinkTopic       =   "Form13"
   MDIChild        =   -1  'True
   ScaleHeight     =   10500
   ScaleWidth      =   15240
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      BackColor       =   &H00C0FFFF&
      Caption         =   "View"
      Height          =   375
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   960
      Width           =   1215
   End
   Begin VB.CommandButton Command3 
      BackColor       =   &H00C0FFFF&
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   375
      Left            =   5040
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   960
      Width           =   1215
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00000000&
      Caption         =   "Select All"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   0
      TabIndex        =   3
      Top             =   1680
      Width           =   1215
   End
   Begin VB.CommandButton Command4 
      BackColor       =   &H00C0FFFF&
      Caption         =   "&Print List"
      Height          =   375
      Left            =   2400
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   960
      Width           =   1335
   End
   Begin VB.CommandButton Command5 
      BackColor       =   &H00C0FFFF&
      Caption         =   "&Voucher"
      Height          =   375
      Left            =   3720
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   960
      Width           =   1335
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   240
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      Format          =   16580609
      CurrentDate     =   39524
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   8175
      Left            =   0
      TabIndex        =   6
      Top             =   1920
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   14420
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   12648447
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Microsoft Sans Serif"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Invoice No"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Vendor Name"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Date"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "Amount"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Natural Code"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Distribution Amt"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   1800
      TabIndex        =   7
      Top             =   240
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   16580609
      CurrentDate     =   39479
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   8520
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Voucher Date"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   240
      Width           =   1260
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Total Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   2
      Left            =   5280
      TabIndex        =   9
      Top             =   240
      Width           =   1380
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Voucher Date"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   3
      Left            =   6600
      TabIndex        =   8
      Top             =   240
      Width           =   1140
   End
End
Attribute VB_Name = "Voucher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim chk As Boolean

Private Sub Check1_Click()
If Check1.Value = 1 Then
Dim lt As ListItem
i = 1
For i = 1 To ListView1.ListItems.Count
    Set lt = ListView1.ListItems(i)
    ListView1.ListItems(i).Checked = True
Next
Else
For i = 1 To ListView1.ListItems.Count
    Set lt = ListView1.ListItems(i)
    ListView1.ListItems(i).Checked = False
Next
End If
End Sub

Private Sub Command1_Click()
arraydec
Set comprec = con1.Execute("UPDATE Voucher_prn SET UPDATE_PRN=0")
Set comprec = con1.Execute("truncate table Voucher_prn")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)

p_start_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker2.Value, "dd-mmm-yyyy")

strcmd = "SELECT AI.DOC_SEQUENCE_VALUE VOUCHER_NUM, AI.GL_DATE, AI.INVOICE_NUM, AI.INVOICE_DATE, "
strcmd = strcmd & " (CASE WHEN AITDS.INVOICE_TYPE_LOOKUP_CODE = 'CREDIT' THEN AITDS.INVOICE_NUM ELSE 'NO TDS' END) AS VOUCHER_TDS_NUM, "
strcmd = strcmd & "(CASE WHEN AITDS.INVOICE_TYPE_LOOKUP_CODE = 'CREDIT' THEN ABS(AITDS.INVOICE_AMOUNT) ELSE 0.00 END) AS TDS_DEDUCTED, "
strcmd = strcmd & "PV.VENDOR_NAME, "
strcmd = strcmd & "AI.INVOICE_AMOUNT, "
strcmd = strcmd & "AI.INVOICE_AMOUNT + (CASE WHEN AITDS.INVOICE_TYPE_LOOKUP_CODE = 'CREDIT' THEN AITDS.INVOICE_AMOUNT ELSE 0.00 END) VENDOR_AMOUNT, "
strcmd = strcmd & "AI.DESCRIPTION NARRATION, "
strcmd = strcmd & "GCC.SEGMENT1||'.'||GCC.SEGMENT2||'.'||GCC.SEGMENT3||'.'||GCC.SEGMENT4 DIST_CODE_COMBINATION, "
strcmd = strcmd & "FVTA.DESCRIPTION||'.'||FVTB.DESCRIPTION||'.'||FVTC.DESCRIPTION||'.'||FVTD.DESCRIPTION DIST_CODE_DESCRIPTION, "
strcmd = strcmd & "AID.AMOUNT DISTRIBUTION_AMOUNT, "
strcmd = strcmd & "NVL(AI_PREPAY.INVOICE_NUM, 'NILL') ADJ_PREPAY_VOUCHER_NUM, "
strcmd = strcmd & "(CASE WHEN AID.LINE_TYPE_LOOKUP_CODE = 'PREPAY' THEN ABS(AID.AMOUNT) ELSE 0.00 END) AS ADJ_PREPAY_AMOUNT, "
strcmd = strcmd & "NVL(AI_PREPAY.INVOICE_AMOUNT, 0.00) -  (CASE WHEN AID.LINE_TYPE_LOOKUP_CODE = 'PREPAY' THEN ABS(AID.AMOUNT) ELSE 0.00 END) AS PREPAY_AMOUNT_REM "
strcmd = strcmd & "From "
strcmd = strcmd & "AP_INVOICES_ALL AI, "
strcmd = strcmd & "AP_INVOICES_ALL AI_PREPAY, "
strcmd = strcmd & "AP_INVOICES_ALL AITDS, "
strcmd = strcmd & "PO_VENDORS PV, "
strcmd = strcmd & "AP_INVOICE_DISTRIBUTIONS_ALL AID, "
strcmd = strcmd & "AP_INVOICE_DISTRIBUTIONS_ALL AID_PREPAY, "
strcmd = strcmd & "GL_CODE_COMBINATIONS GCC, "
strcmd = strcmd & "FND_FLEX_VALUES_TL FVTA, "
strcmd = strcmd & "FND_FLEX_VALUES FVA, "
strcmd = strcmd & "FND_FLEX_VALUES_TL FVTB, "
strcmd = strcmd & "FND_FLEX_VALUES FVB, "
strcmd = strcmd & "FND_FLEX_VALUES_TL FVTC, "
strcmd = strcmd & "FND_FLEX_VALUES FVC, "
strcmd = strcmd & "FND_FLEX_VALUES_TL FVTD, "
strcmd = strcmd & "FND_FLEX_VALUES FVD "
strcmd = strcmd & "Where "
'strcmd = strcmd & "AI.INVOICE_TYPE_LOOKUP_CODE = 'STANDARD' " 'Commented By Rahul
'strcmd = strcmd & "AND AITDS.VENDOR_ID(+) = AI.VENDOR_ID "
strcmd = strcmd & "AITDS.VENDOR_ID(+) = AI.VENDOR_ID "
strcmd = strcmd & "AND AI.INVOICE_ID = AITDS.ATTRIBUTE1(+) "
strcmd = strcmd & "AND PV.VENDOR_ID = AI.VENDOR_ID "
strcmd = strcmd & "AND AI.INVOICE_NUM NOT LIKE '%TDS%' "
strcmd = strcmd & "AND AI.GL_DATE BETWEEN '" & p_start_date & "'" & " and '" & p_end_date & "' "
'strcmd = strcmd & " and a1.check_date between '" & P_START_DATE & "'" & " and '" & p_end_date & "'"
'strcmd = strcmd & "AND PV.VENDOR_NAME = NVL(:P_VENDOR_NAME, PV.VENDOR_NAME) "
'strcmd = strcmd & "AND (AI.INVOICE_ID = NVL(:P_INVOICE_NUMBER, AI.INVOICE_ID) OR (AI.DOC_SEQUENCE_VALUE BETWEEN :P_VOUCHER_LOW AND :P_VOUCHER_HIGH)) "
strcmd = strcmd & "AND AI.INVOICE_ID = AID.INVOICE_ID "
strcmd = strcmd & "AND AI.CANCELLED_BY IS NULL "
strcmd = strcmd & "AND GCC.CODE_COMBINATION_ID = AID.DIST_CODE_COMBINATION_ID "
strcmd = strcmd & "AND GCC.SEGMENT1 = FVA.FLEX_VALUE "
strcmd = strcmd & "AND GCC.SEGMENT2 = FVB.FLEX_VALUE "
strcmd = strcmd & "AND GCC.SEGMENT3 = FVC.FLEX_VALUE "
strcmd = strcmd & "AND GCC.SEGMENT4 = FVD.FLEX_VALUE "
strcmd = strcmd & "AND FVA.FLEX_VALUE_SET_ID = 1009931 "
strcmd = strcmd & "AND FVB.FLEX_VALUE_SET_ID = 1009932 "
strcmd = strcmd & "AND FVC.FLEX_VALUE_SET_ID = 1009933 "
strcmd = strcmd & "AND FVD.FLEX_VALUE_SET_ID = 1009934 "
strcmd = strcmd & "AND FVTA.FLEX_VALUE_ID = FVA.FLEX_VALUE_ID "
strcmd = strcmd & "AND FVTB.FLEX_VALUE_ID = FVB.FLEX_VALUE_ID "
strcmd = strcmd & "AND FVTC.FLEX_VALUE_ID = FVC.FLEX_VALUE_ID "
strcmd = strcmd & "AND FVTD.FLEX_VALUE_ID = FVD.FLEX_VALUE_ID "
strcmd = strcmd & "AND AID.PREPAY_DISTRIBUTION_ID = AID_PREPAY.INVOICE_DISTRIBUTION_ID(+) "
strcmd = strcmd & "AND AID_PREPAY.INVOICE_ID = AI_PREPAY.INVOICE_ID(+) "
strcmd = strcmd & "ORDER BY AI.INVOICE_ID "

Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Set repcomprec2 = New ADODB.Recordset
    repcomprec2.Open "Voucher_prn", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
    repcomprec2.AddNew
    'If comprec!VOUCHER_NUM = "" Then
    '    repcomprec2!VoucherNo = 0
    'Else
    repcomprec2!VoucherNo = comprec!VOUCHER_NUM
    'End If
    repcomprec2!gl_DATE = comprec!gl_DATE
    repcomprec2!invoice_no = comprec!invoice_num
    repcomprec2!INVOICE_DATE = comprec!INVOICE_DATE
    repcomprec2!Natural_Acc_Code = comprec!DIST_CODE_COMBINATION
    repcomprec2!Natual_Acc_Name = comprec!DIST_CODE_DESCRIPTION
    repcomprec2!Amount = comprec!invoice_AMOUNT
    repcomprec2!TDS_Deduction = comprec!VOUCHER_TDS_NUM
    repcomprec2!Vendor_Name = comprec!Vendor_Name
    repcomprec2!Description = comprec!NARRATION
    repcomprec2!Adjusted_vch_No = comprec!ADJ_PREPAY_VOUCHER_NUM
    repcomprec2!Adjusted_vch_Amt = comprec!ADJ_PREPAY_AMOUNT
    repcomprec2!VENDOR_AMOUNT = comprec!VENDOR_AMOUNT
    repcomprec2!TDS_DEDUCTED = comprec!TDS_DEDUCTED
    repcomprec2!Distribution_Amt = comprec!DISTRIBUTION_AMOUNT
    comprec.MoveNext
    repcomprec2.Update
    Loop
End If

    chq_amt = 0
    ListView1.ListItems.Clear
    strcmd = "select invoice_no,Vendor_Name, Natural_Acc_Code, Distribution_Amt from Voucher_prn group by invoice_no,Vendor_Name, Natural_Acc_Code,Distribution_Amt ORDER BY invoice_no,Vendor_Name, Natural_Acc_Code"
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        Do While Not comprec.EOF
        strcmd = "select *  from Voucher_prn where invoice_no='" & Replace(comprec!invoice_no, "'", "''''") & "'"
        strcmd = strcmd & " and Vendor_Name ='" & comprec!Vendor_Name & "'"
        strcmd = strcmd & " and Natural_Acc_Code ='" & comprec!Natural_Acc_Code & "'"
        strcmd = strcmd & " and Distribution_Amt ='" & comprec!Distribution_Amt & "'"
        Set comprec1 = con1.Execute(strcmd)
        If Not comprec1.EOF Then
            Set lt = ListView1.ListItems.Add(, , comprec!invoice_no)
            lt.SubItems(1) = comprec1!Vendor_Name
            lt.SubItems(2) = comprec1!INVOICE_DATE
            lt.SubItems(3) = comprec1!Amount
            lt.SubItems(4) = comprec1!Natural_Acc_Code
            lt.SubItems(5) = comprec1!Distribution_Amt
            chq_amt = chq_amt + comprec1!Amount
        End If
        comprec.MoveNext
        Loop
        Label1(3).Caption = Round(chq_amt, 2)
    End If

strcmd = "select * from Voucher_prn order by invoice_no, Vendor_Name"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    strcmd = "update Voucher_prn set Amt_In_Words='" & num2str(comprec!Amount) & "'"
    strcmd = strcmd & " where invoice_no='" & Replace(comprec!invoice_no, "'", "''''") & "'"
    strcmd = strcmd & " and Vendor_Name ='" & comprec!Vendor_Name & "'"
    strcmd = strcmd & " and Natural_Acc_Code='" & comprec!Natural_Acc_Code & "'"
    strcmd = strcmd & " and Distribution_Amt=" & comprec!Distribution_Amt
     
     Set comprec1 = con1.Execute(strcmd)
     comprec.MoveNext
    Loop
End If
MsgBox "Done"
Exit Sub

End Sub

Private Sub Command3_Click()
Unload Me
End Sub

Private Sub Command4_Click()
find_code1
End Sub

Private Sub Command5_Click()
find_code2
End Sub

Private Sub Form_Activate()
Set comprec = con1.Execute("UPDATE Voucher_prn SET UPDATE_PRN=0")
End Sub

Private Sub Form_Load()
DTPicker1.Value = Format(Date, "dd-MMM-yyyy") 'Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker2.Value = Format(Date, "dd-MMM-yyyy")
ListView1.Width = Screen.Width

End Sub

Public Sub find_code1()
Dim lt As ListItem
i = 1
For i = 1 To ListView1.ListItems.Count
        Set lt = ListView1.ListItems(i)
        If ListView1.ListItems(i).Checked = True Then
            a = ListView1.ListItems(i).Text
            b = lt.SubItems(4)
            c = lt.SubItems(5)
            d = lt.SubItems(1)
            strcmd = "UPDATE Voucher_prn SET UPDATE_PRN=1 WHERE invoice_no='" & a & "'"
            strcmd = strcmd & "and Vendor_Name='" & d & "'"
            strcmd = strcmd & "and Natural_Acc_Code='" & b & "'"
            strcmd = strcmd & "and Distribution_Amt='" & c & "'"
            
            chk = True
            Set comprec = con1.Execute(strcmd)
        End If
        
Next
    a = MsgBox("Want to Print/View (Yes->Print,No->View", vbYesNo, "Print")
    If a = vbYes Then
        prnagn1
    Else
        prnview1
    End If
    Set comprec = con1.Execute("UPDATE Voucher_prn SET UPDATE_PRN=0")
End Sub
Public Sub prnagn1()
                cryctrl.ReportFileName = App.Path & "\reportS\vnd12.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToPrinter
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnview1()
        cryctrl.ReportFileName = App.Path & "\reportS\vnd12.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub find_code2()
Dim lt As ListItem
i = 1
'j = 5
For i = 1 To ListView1.ListItems.Count
        Set lt = ListView1.ListItems(i)

        If ListView1.ListItems(i).Checked = True Then
            a = ListView1.ListItems(i).Text
            b = lt.SubItems(4)
            c = lt.SubItems(5)
            d = lt.SubItems(1)
            strcmd = "UPDATE Voucher_Prn SET UPDATE_PRN=1 WHERE invoice_no='" & a & "'"
            strcmd = strcmd & "and Vendor_Name='" & d & "'"
            strcmd = strcmd & " and Natural_Acc_Code='" & b & "'"
            strcmd = strcmd & "and Distribution_Amt='" & c & "'"
            chk = True
            Set comprec = con1.Execute(strcmd)
        End If
        
Next
    a = MsgBox("Want to Print/View (Yes->Print,No->View", vbYesNo, "Print")
    If a = vbYes Then
        prnagn2
    Else
        prnview2
    End If
    Set comprec = con1.Execute("UPDATE Voucher_Prn SET UPDATE_PRN=0")
End Sub

Public Sub prnagn2()
        cryctrl.ReportFileName = App.Path & "\reportS\vvoucher.rpt" 'for voucher
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToPrinter
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnview2()
        cryctrl.ReportFileName = App.Path & "\reportS\vvoucher.rpt" 'for voucher
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
