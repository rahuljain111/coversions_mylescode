VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form9 
   BackColor       =   &H00FF8080&
   Caption         =   "Payable Vendor Ledger-Old"
   ClientHeight    =   8160
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7815
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form9"
   MDIChild        =   -1  'True
   ScaleHeight     =   8967.758
   ScaleMode       =   0  'User
   ScaleWidth      =   7815
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Combo3 
      ForeColor       =   &H000000FF&
      Height          =   345
      Left            =   2280
      TabIndex        =   17
      Top             =   2160
      Width           =   3135
   End
   Begin VB.ComboBox Combo2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Index           =   0
      Left            =   2280
      TabIndex        =   16
      Top             =   1560
      Width           =   3135
   End
   Begin VB.CheckBox Check3 
      BackColor       =   &H00FF8080&
      Caption         =   "All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5400
      TabIndex        =   14
      Top             =   2160
      Width           =   615
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00FF8080&
      Caption         =   "All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5400
      TabIndex        =   1
      Top             =   960
      Width           =   615
   End
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   2280
      TabIndex        =   0
      Top             =   960
      Width           =   3135
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3960
      TabIndex        =   6
      Top             =   6360
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Preview"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2640
      TabIndex        =   5
      Top             =   6360
      Width           =   1335
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FF8080&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   855
      Index           =   5
      Left            =   1800
      TabIndex        =   7
      Top             =   2760
      Width           =   3615
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   330
         Left            =   2160
         TabIndex        =   4
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   16515075
         CurrentDate     =   37865
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   330
         Left            =   600
         TabIndex        =   3
         Top             =   240
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   16515075
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   12
         Left            =   120
         TabIndex        =   9
         Top             =   300
         Width           =   360
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   11
         Left            =   1905
         TabIndex        =   8
         Top             =   300
         Width           =   180
      End
   End
   Begin VB.CheckBox Check2 
      BackColor       =   &H00FF8080&
      Caption         =   "All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5400
      TabIndex        =   2
      Top             =   1560
      Width           =   615
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   13
      Top             =   7665
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   873
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Vendor Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   1
      Left            =   1200
      TabIndex        =   15
      Top             =   2160
      Width           =   1065
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Site"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   0
      Left            =   1200
      TabIndex        =   12
      Top             =   1560
      Width           =   315
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Vendor"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   2
      Left            =   1200
      TabIndex        =   11
      Top             =   960
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2280
      TabIndex        =   10
      Top             =   4080
      Width           =   2295
   End
End
Attribute VB_Name = "Form9"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub ADDVEN()
strcmd = "select DISTINCT vendor_name from po_vendors "
'strcmd = strcmd & " Where q.vendor_id = w.vendor_id "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!Vendor_Name
        comprec.MoveNext
    Loop
End If
Combo1.Text = Combo1.List(0)
End Sub

Public Sub ADDVENDTYPE()
strcmd = "select distinct VENDOR_TYPE_LOOKUP_CODE from po_vendors where VENDOR_TYPE_LOOKUP_CODE is not null order by VENDOR_TYPE_LOOKUP_CODE "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo3.AddItem comprec!vendor_type_lookup_code
        comprec.MoveNext
    Loop
End If
End Sub

Public Sub ADDSITE()
strcmd = "select DISTINCT vendor_site_code from po_vendor_sites_all "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo2(0).AddItem comprec!vendor_site_code
        comprec.MoveNext
    Loop
End If
Combo2(0).Text = Combo2(0).List(0)
End Sub

Private Sub Check1_Click()
If Check1.Value = 1 Then
    Combo1.Text = "All"
End If
End Sub

Private Sub Check2_Click()
If Check2.Value = 1 Then
    Combo2(0).Text = "All"
End If
End Sub

Private Sub Check3_Click()
If Check3.Value = 1 Then
    Combo3.Text = "All"
End If
End Sub

Private Sub Command1_Click()
    add_vendor_Ledger
End Sub

Public Sub add_vendor_Ledger()
p_start_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker2.Value, "dd-mmm-yyyy")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)
Set comprec = con1.Execute("truncate table all_data")

If Check1.Value = 0 Then
strcmd = "SELECT  povs.vendor_site_code, "
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " apd.dist_code_combination_id ccid,"
strcmd = strcmd & " api.invoice_currency_code,"
strcmd = strcmd & " api.exchange_rate,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',abs(z.amt_val) - abs(nvl(api.discount_amount_taken,0) ) , 0)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',0,z.amt_val - nvl(api.discount_amount_taken,0) ) cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " to_char(api.doc_sequence_value) payment_num,"
strcmd = strcmd & " to_char(apd.accounting_date, 'dd-MON-yyyy') pay_accounting_date,"
strcmd = strcmd & " NULL check_number,"
strcmd = strcmd & " pov.segment1,"
strcmd = strcmd & " pov.vendor_name,"
strcmd = strcmd & " pov.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,"
strcmd = strcmd & " api.batch_id,"
strcmd = strcmd & " api.exchange_date,"
strcmd = strcmd & " api.invoice_id,"
strcmd = strcmd & " apd.accounting_date"
strcmd = strcmd & " FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs,"
strcmd = strcmd & " (SELECT NVL(SUM(apd.amount),0) amt_val,"
strcmd = strcmd & " api.invoice_id"
strcmd = strcmd & " FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs"
strcmd = strcmd & " Where api.invoice_id = apd.invoice_id"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " AND apd.match_status_flag='A'"
strcmd = strcmd & " AND apd.line_type_lookup_code <> 'PREPAY'"
strcmd = strcmd & " GROUP BY api.invoice_id) z"
strcmd = strcmd & " Where api.invoice_id = Z.invoice_id"
strcmd = strcmd & " and api.invoice_id = apd.invoice_id"
'strcmd = strcmd & " --and api.vendor_id=418"
strcmd = strcmd & " and apd.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND apd.accounting_date <='" & p_end_date & "'"
'strcmd = strcmd & " and pov.vendor_Name='" & Combo1.Text & "'"
strcmd = strcmd & " and pov.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "' "

If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and povs.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and pov.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

strcmd = strcmd & " AND apd.rowid = ( select  rowid"
strcmd = strcmd & " From ap_invoice_distributions_all"
strcmd = strcmd & " Where rownum = 1"
strcmd = strcmd & " and invoice_id=apd.invoice_id"
strcmd = strcmd & " AND match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND apd.match_status_flag = 'A'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " AND ((api.invoice_type_lookup_code  <> 'DEBIT')"
strcmd = strcmd & " or"
strcmd = strcmd & " ("
strcmd = strcmd & " (api.invoice_type_lookup_code  = 'DEBIT')"
strcmd = strcmd & " and"
strcmd = strcmd & " ( not exists"
strcmd = strcmd & " (Select '1'"
strcmd = strcmd & " from ap_invoice_payments_all  app,"
strcmd = strcmd & " ap_checks_all apc"
strcmd = strcmd & " Where App.check_id = apc.check_id"
strcmd = strcmd & " and app.invoice_id = api.invoice_id"
strcmd = strcmd & " and apc.payment_type_flag = 'R'"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
'----------------IInd
strcmd = strcmd & " Union All"
strcmd = strcmd & " SELECT povs.vendor_site_code,"
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " app.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " api.payment_currency_code,"
strcmd = strcmd & " apc.exchange_rate ,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',0,0),app.amount)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',app.amount,abs(app.amount)),0)  cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.doc_sequence_value), 'P',  to_char(apc.doc_sequence_value),  to_char(apc.doc_sequence_value), 'N', NULL) payment_num,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(app.accounting_date, 'dd-MON-yyyy'), 'P', to_char(app.accounting_date, 'dd-MON-yyyy')) pay_accounting_date,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.check_number), 'P', to_char(apc.check_number)) check_number,"
strcmd = strcmd & " pov.segment1,"
strcmd = strcmd & " pov.vendor_name,"
strcmd = strcmd & " pov.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id,"
strcmd = strcmd & " apc.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,apc.exchange_date,api.invoice_id,App.accounting_date"
strcmd = strcmd & " FROM ap_invoices_all api,ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,ap_invoice_payments_all app,ap_checks_all apc,"
strcmd = strcmd & " po_vendor_sites_all povs Where api.invoice_id = apd.invoice_id"
strcmd = strcmd & " AND apd.rowid = (select rowid from ap_invoice_distributions_all where rownum=1 and invoice_id=apd.invoice_id and match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND app.invoice_id = api.invoice_id"
strcmd = strcmd & " AND app.check_id = apc.check_id"
strcmd = strcmd & " AND apc.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')"
strcmd = strcmd & " AND apd.match_status_flag='A'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
'strcmd = strcmd & " and pov.vendor_Name='" & Combo1.Text & "'"
strcmd = strcmd & " and pov.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "' "
strcmd = strcmd & " and app.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND app.accounting_date <='" & p_end_date & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and povs.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and pov.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If
'strcmd = strcmd & " --AND API.VENDOR_ID=418"
'strcmd = strcmd & " --and app.reversal_flag='N'"
'strcmd = strcmd & "--d povs.vendor_site_code='"

'''--------------------------IIIrd
strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,d.ACCOUNTING_DATE INVOICE_DATE ,null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " 0 distribution_id,api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id,sysdate exchange_date,"
strcmd = strcmd & " api.invoice_id,d.ACCOUNTING_DATE from po_vendor_sites_all a,po_vendors b,"
strcmd = strcmd & " ap_ae_lines_all c,ap_ae_headers_all d,ap_invoices_all api,"
strcmd = strcmd & " po_vendor_sites_all povs where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICES' AND"
strcmd = strcmd & " c.source_id = api.invoice_id"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
'strcmd = strcmd & " and api.vendor_id=418"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
strcmd = strcmd & " and b.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "'"
If Check2.Value = 1 Then
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If
'--------------------------4th
strcmd = strcmd & " Union All"
strcmd = strcmd & " select"
strcmd = strcmd & " a.vendor_site_code,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,"
strcmd = strcmd & " to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,"
strcmd = strcmd & " b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & " d.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & " ap_invoices_all api,ap_invoice_distributions_all apd"
strcmd = strcmd & " where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND"
strcmd = strcmd & " a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND"
strcmd = strcmd & " c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICE_DISTRIBUTIONS' AND"
strcmd = strcmd & " c.source_id = apd.invoice_distribution_id"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

strcmd = strcmd & " and b.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "'"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
'-------------------------5th

strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description, a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & "  c.currency_code invoice_currency_code,"
strcmd = strcmd & "  c.currency_conversion_rate  rate ,"
strcmd = strcmd & "  0 dr_val,0 cr_val,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "  null payment_num,"
strcmd = strcmd & "  to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & "  to_char(ac.check_number) check_number,"
strcmd = strcmd & "  b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & "  0 distribution_id,api.exchange_rate_type,"
strcmd = strcmd & "  api.org_id,api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & "  d.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & "  ap_invoices_all api,"
strcmd = strcmd & "  ap_checks_all ac ,ap_invoice_payments_all App,"
strcmd = strcmd & " po_vendor_sites_all povs"
strcmd = strcmd & "  where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & "  a.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & "   b.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & "  api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & "  app.check_id = ac.check_id  AND"
strcmd = strcmd & "  c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & "  c.source_table = 'AP_CHECKS' AND"
strcmd = strcmd & "  c.source_id = ac.check_id  AND"
strcmd = strcmd & "  ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED') -- AND"
'strcmd = strcmd & " and api.vendor_id=418"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
strcmd = strcmd & " and b.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "'"
'strcmd = strcmd & "  and a.vendor_site_code='DELHI'"
' and app.reversal_flag='N'
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and povs.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If
'--------------------------------------------6th
strcmd = strcmd & "  Union All"
strcmd = strcmd & "  select a.vendor_site_code,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & "  api.invoice_num,d.ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & "  c.currency_code invoice_currency_code,"
strcmd = strcmd & "  c.currency_conversion_rate  rate ,"
strcmd = strcmd & "  0 dr_val,0 cr_val,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "  null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & "  to_char(ac.check_number) check_number,"
strcmd = strcmd & "  b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,0 distribution_id,"
strcmd = strcmd & "  api.exchange_rate_type,api.org_id,api.batch_id,sysdate exchange_date,"
strcmd = strcmd & "  api.invoice_id,d.accounting_date"
strcmd = strcmd & "  from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & "  ap_invoices_all api,ap_checks_all ac ,ap_invoice_payments_all App"
strcmd = strcmd & "  where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & "  a.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & "  b.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & "  api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & "  app.check_id = ac.check_id  AND"
strcmd = strcmd & "  c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & "  c.source_table = 'AP_INVOICE_PAYMENTS' AND"
strcmd = strcmd & "  c.source_id = app.invoice_payment_id  AND"
strcmd = strcmd & "  ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')  --AND"
'strcmd = strcmd & "  AND API.VENDOR_ID=418"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
strcmd = strcmd & " and b.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
'and app.reversal_flag='N'

'-----------------------7th
 strcmd = strcmd & "  Union All"
strcmd = strcmd & "  select DISTINCT a.vendor_site_code,"
strcmd = strcmd & "  NULL invoice_type_lookup_code,"
strcmd = strcmd & " null invoice_num,"
strcmd = strcmd & " SYSDATE INVOICE_DATE ,"
strcmd = strcmd & "  null description,"
strcmd = strcmd & "  a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " Null  invoice_currency_code,"
strcmd = strcmd & "  0  rate ,0 dr_val,0  cr_val,0 acct_dr,"
strcmd = strcmd & "  0 acct_cr,null payment_num,null  pay_accounting_date,"
strcmd = strcmd & "  null check_number,"
strcmd = strcmd & "  b.segment1 ,"
strcmd = strcmd & "  b.vendor_name,"
strcmd = strcmd & "  null,"
strcmd = strcmd & "  0 distribution_id,"
strcmd = strcmd & "  null,a.org_id,0 batch_id,sysdate exchange_date,0 invoice_id,"
strcmd = strcmd & " sysdate accounting_date"
strcmd = strcmd & "  from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_invoices_all c,"
strcmd = strcmd & "  AP_INVOICE_DISTRIBUTIONS_ALL D"
strcmd = strcmd & "  where a.vendor_id = b.vendor_id and"
strcmd = strcmd & "  a.vendor_id = C.vendor_id"
strcmd = strcmd & " AND A.VENDOR_SITE_ID = C.VENDOR_SITE_ID"
strcmd = strcmd & "  AND C.INVOICE_ID = D.INVOICE_ID"
'strcmd = strcmd & "  AND a.vendor_site_code='DELHI'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

strcmd = strcmd & " and b.vendor_Name='" & Replace(Combo1.Text, "'", "''") & "'"
'strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
'strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
strcmd = strcmd & "  ORDER BY 24,25"

Else
strcmd = "SELECT  povs.vendor_site_code, "
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " apd.dist_code_combination_id ccid,"
strcmd = strcmd & " api.invoice_currency_code,"
strcmd = strcmd & " api.exchange_rate,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',abs(z.amt_val) - abs(nvl(api.discount_amount_taken,0) ) , 0)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',0,z.amt_val - nvl(api.discount_amount_taken,0) ) cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " to_char(api.doc_sequence_value) payment_num,"
strcmd = strcmd & " to_char(apd.accounting_date, 'dd-MON-yyyy') pay_accounting_date,"
strcmd = strcmd & " NULL check_number,"
strcmd = strcmd & " pov.segment1,"
strcmd = strcmd & " pov.vendor_name,"
strcmd = strcmd & " pov.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,"
strcmd = strcmd & " api.batch_id,"
strcmd = strcmd & " api.exchange_date,"
strcmd = strcmd & " api.invoice_id,"
strcmd = strcmd & " apd.accounting_date"
strcmd = strcmd & " FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs,"
strcmd = strcmd & " (SELECT NVL(SUM(apd.amount),0) amt_val,"
strcmd = strcmd & " api.invoice_id"
strcmd = strcmd & " FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs"
strcmd = strcmd & " Where api.invoice_id = apd.invoice_id"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " AND apd.match_status_flag='A'"
strcmd = strcmd & " AND apd.line_type_lookup_code <> 'PREPAY'"
strcmd = strcmd & " GROUP BY api.invoice_id) z"
strcmd = strcmd & " Where api.invoice_id = Z.invoice_id"
strcmd = strcmd & " and api.invoice_id = apd.invoice_id"
'strcmd = strcmd & " --and api.vendor_id=418"
strcmd = strcmd & " and apd.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND apd.accounting_date <='" & p_end_date & "'"
'strcmd = strcmd & " and pov.vendor_Name='" & Combo1.Text & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and povs.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and pov.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

strcmd = strcmd & " AND apd.rowid = ( select  rowid"
strcmd = strcmd & " From ap_invoice_distributions_all"
strcmd = strcmd & " Where rownum = 1"
strcmd = strcmd & " and invoice_id=apd.invoice_id"
strcmd = strcmd & " AND match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND apd.match_status_flag = 'A'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " AND ((api.invoice_type_lookup_code  <> 'DEBIT')"
strcmd = strcmd & " or"
strcmd = strcmd & " ("
strcmd = strcmd & " (api.invoice_type_lookup_code  = 'DEBIT')"
strcmd = strcmd & " and"
strcmd = strcmd & " ( not exists"
strcmd = strcmd & " (Select '1'"
strcmd = strcmd & " from ap_invoice_payments_all  app,"
strcmd = strcmd & " ap_checks_all apc"
strcmd = strcmd & " Where App.check_id = apc.check_id"
strcmd = strcmd & " and app.invoice_id = api.invoice_id"
strcmd = strcmd & " and apc.payment_type_flag = 'R'"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
strcmd = strcmd & " )"

'----------------IInd

strcmd = strcmd & " Union All"
strcmd = strcmd & " SELECT povs.vendor_site_code,"
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " app.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " api.payment_currency_code,"
strcmd = strcmd & " apc.exchange_rate ,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',0,0),app.amount)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',app.amount,abs(app.amount)),0)  cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.doc_sequence_value), 'P',  to_char(apc.doc_sequence_value),  to_char(apc.doc_sequence_value), 'N', NULL) payment_num,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(app.accounting_date, 'dd-MON-yyyy'), 'P', to_char(app.accounting_date, 'dd-MON-yyyy')) pay_accounting_date,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.check_number), 'P', to_char(apc.check_number)) check_number,"
strcmd = strcmd & " pov.segment1,"
strcmd = strcmd & " pov.vendor_name,"
strcmd = strcmd & " pov.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id,"
strcmd = strcmd & " apc.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,apc.exchange_date,api.invoice_id,App.accounting_date"
strcmd = strcmd & " FROM ap_invoices_all api,ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,ap_invoice_payments_all app,ap_checks_all apc,"
strcmd = strcmd & " po_vendor_sites_all povs Where api.invoice_id = apd.invoice_id"
strcmd = strcmd & " AND apd.rowid = (select rowid from ap_invoice_distributions_all where rownum=1 and invoice_id=apd.invoice_id and match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND app.invoice_id = api.invoice_id"
strcmd = strcmd & " AND app.check_id = apc.check_id"
strcmd = strcmd & " AND apc.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')"
strcmd = strcmd & " AND apd.match_status_flag='A'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
'strcmd = strcmd & " and pov.vendor_Name='" & Combo1.Text & "'"
strcmd = strcmd & " and apd.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND apd.accounting_date <='" & p_end_date & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and povs.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and pov.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

'strcmd = strcmd & " --AND API.VENDOR_ID=418"
'strcmd = strcmd & " --and app.reversal_flag='N'"
'strcmd = strcmd & "--d povs.vendor_site_code='"

'''--------------------------IIIrd
strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,d.ACCOUNTING_DATE INVOICE_DATE ,null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " 0 distribution_id,api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id,sysdate exchange_date,"
strcmd = strcmd & " api.invoice_id,d.ACCOUNTING_DATE from po_vendor_sites_all a,po_vendors b,"
strcmd = strcmd & " ap_ae_lines_all c,ap_ae_headers_all d,ap_invoices_all api,"
strcmd = strcmd & " po_vendor_sites_all povs where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICES' AND"
strcmd = strcmd & " c.source_id = api.invoice_id"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
'strcmd = strcmd & " and api.vendor_id=418"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
'strcmd = strcmd & " and b.vendor_Name='" & Combo1.Text & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

''''--------------------4th
strcmd = strcmd & " Union All"
strcmd = strcmd & " select"
strcmd = strcmd & " a.vendor_site_code,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,"
strcmd = strcmd & " to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,"
strcmd = strcmd & " b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & " d.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & " ap_invoices_all api,ap_invoice_distributions_all apd"
strcmd = strcmd & " where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND"
strcmd = strcmd & " a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND"
strcmd = strcmd & " c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICE_DISTRIBUTIONS' AND"
strcmd = strcmd & " c.source_id = apd.invoice_distribution_id"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
'strcmd = strcmd & " and api.vendor_id=418"
'strcmd = strcmd & " and b.vendor_Name='" & Combo1.Text & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

'strcmd = strcmd & " and a.vendor_site_code='DELHI'"

'-------------------------5th
strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description, a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & "  c.currency_code invoice_currency_code,"
strcmd = strcmd & "  c.currency_conversion_rate  rate ,"
strcmd = strcmd & "  0 dr_val,0 cr_val,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "  null payment_num,"
strcmd = strcmd & "  to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & "  to_char(ac.check_number) check_number,"
strcmd = strcmd & "  b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & "  0 distribution_id,api.exchange_rate_type,"
strcmd = strcmd & "  api.org_id,api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & "  d.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & "  ap_invoices_all api,"
strcmd = strcmd & "  ap_checks_all ac ,ap_invoice_payments_all App,"
strcmd = strcmd & " po_vendor_sites_all povs"
strcmd = strcmd & "  where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & "  a.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & "   b.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & "  api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & "  app.check_id = ac.check_id  AND"
strcmd = strcmd & "  c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & "  c.source_table = 'AP_CHECKS' AND"
strcmd = strcmd & "  c.source_id = ac.check_id  AND"
strcmd = strcmd & "  ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED') -- AND"
'strcmd = strcmd & " and api.vendor_id=418"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

'strcmd = strcmd & " and b.vendor_Name='" & Combo1.Text & "'"
'strcmd = strcmd & "  and a.vendor_site_code='DELHI'"
' and app.reversal_flag='N'
'--------------------------------------------6th
strcmd = strcmd & "  Union All"
strcmd = strcmd & "  select a.vendor_site_code,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & "  api.invoice_num,d.ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & "  c.currency_code invoice_currency_code,"
strcmd = strcmd & "  c.currency_conversion_rate  rate ,"
strcmd = strcmd & "  0 dr_val,0 cr_val,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "  null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & "  to_char(ac.check_number) check_number,"
strcmd = strcmd & "  b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,0 distribution_id,"
strcmd = strcmd & "  api.exchange_rate_type,api.org_id,api.batch_id,sysdate exchange_date,"
strcmd = strcmd & "  api.invoice_id,d.accounting_date"
strcmd = strcmd & "  from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & "  ap_invoices_all api,ap_checks_all ac ,ap_invoice_payments_all App"
strcmd = strcmd & "  where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & "  a.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & "  b.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & "  api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & "  app.check_id = ac.check_id  AND"
strcmd = strcmd & "  c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & "  c.source_table = 'AP_INVOICE_PAYMENTS' AND"
strcmd = strcmd & "  c.source_id = app.invoice_payment_id  AND"
strcmd = strcmd & "  ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')  --AND"
'strcmd = strcmd & "  AND API.VENDOR_ID=418"
strcmd = strcmd & " and apd.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND apd.accounting_date <='" & p_end_date & "'"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and povs.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

'strcmd = strcmd & " and b.vendor_Name='" & Combo1.Text & "'"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
'and app.reversal_flag='N'
'-----------------------7th
strcmd = strcmd & "  Union All"
strcmd = strcmd & "  select DISTINCT a.vendor_site_code,"
strcmd = strcmd & "  NULL invoice_type_lookup_code,"
strcmd = strcmd & " null invoice_num,"
strcmd = strcmd & " SYSDATE INVOICE_DATE ,"
strcmd = strcmd & "  null description,"
strcmd = strcmd & "  a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " Null  invoice_currency_code,"
strcmd = strcmd & "  0  rate ,0 dr_val,0  cr_val,0 acct_dr,"
strcmd = strcmd & "  0 acct_cr,null payment_num,null  pay_accounting_date,"
strcmd = strcmd & "  null check_number,"
strcmd = strcmd & "  b.segment1 ,"
strcmd = strcmd & "  b.vendor_name,"
strcmd = strcmd & "  null,"
strcmd = strcmd & "  0 distribution_id,"
strcmd = strcmd & "  null,a.org_id,0 batch_id,sysdate exchange_date,0 invoice_id,"
strcmd = strcmd & " sysdate accounting_date"
strcmd = strcmd & "  from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_invoices_all c,"
strcmd = strcmd & "  AP_INVOICE_DISTRIBUTIONS_ALL D"
strcmd = strcmd & "  where a.vendor_id = b.vendor_id and"
strcmd = strcmd & "  a.vendor_id = C.vendor_id"
strcmd = strcmd & " AND A.VENDOR_SITE_ID = C.VENDOR_SITE_ID"
strcmd = strcmd & "  AND C.INVOICE_ID = D.INVOICE_ID"
If Check2.Value = 1 Then
    'like  '" & "COMP" & "%'"
    'strcmd = strcmd & " and povs.vendor_site_code ='DELHI'"
Else
    strcmd = strcmd & " and a.vendor_site_code ='" & Combo2(0).Text & "'"
End If

If Check3.Value = 1 Then
Else
    strcmd = strcmd & " and b.vendor_type_lookup_code = '" & Combo3.Text & "'"
End If

'strcmd = strcmd & "  AND a.vendor_site_code='DELHI'"
'strcmd = strcmd & " and b.vendor_Name='" & Combo1.Text & "'"
strcmd = strcmd & " and d.accounting_date >='" & p_start_date & "'"
strcmd = strcmd & " AND d.accounting_date <='" & p_end_date & "'"
strcmd = strcmd & "  ORDER BY 24,25"
End If

Set comprec2 = con.Execute(strcmd)

If Not comprec2.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "All_Data", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec2.EOF
If comprec2!dr_val <> 0 Then
    If Year(comprec2!INVOICE_DATE) > "0050" And Year(comprec2!INVOICE_DATE) < "2999" Then
    'If comprec2!Invoice_Date < "01-01-0006" And comprec2!Invoice_Date > "01-01-2999" Then
    repcomprec.AddNew
            repcomprec!vendor_site_code = comprec2!vendor_site_code
            repcomprec!invoice_type_lookup_code = comprec2!invoice_type_lookup_code
            repcomprec!invoice_num = comprec2!invoice_num
            repcomprec!INVOICE_DATE = comprec2!INVOICE_DATE
            repcomprec!Description = comprec2!Description
            repcomprec!dr_val = comprec2!dr_val
            repcomprec!cr_val = comprec2!cr_val
            repcomprec!segment1 = comprec2!segment1
            repcomprec!Vendor_Name = comprec2!Vendor_Name
            repcomprec!vendor_type_lookup_code = comprec2!vendor_type_lookup_code
            repcomprec!INVOICE_ID = comprec2!INVOICE_ID
            repcomprec!accounting_date = comprec2!accounting_date
            repcomprec!CHeck_Number = comprec2!CHeck_Number
        
    End If
'code started by Rahul
ElseIf comprec2!cr_val <> 0 Then
        If Year(comprec2!INVOICE_DATE) > "0050" And Year(comprec2!INVOICE_DATE) < "2999" Then
        'If comprec2!INVOICE_DATE < "01-01-0006" And comprec2!INVOICE_DATE > "01-01-2999" Then
             repcomprec.AddNew
                repcomprec!vendor_site_code = comprec2!vendor_site_code
                repcomprec!invoice_type_lookup_code = comprec2!invoice_type_lookup_code
                repcomprec!invoice_num = comprec2!invoice_num
                repcomprec!INVOICE_DATE = comprec2!INVOICE_DATE
                repcomprec!Description = comprec2!Description
                repcomprec!dr_val = comprec2!dr_val
                repcomprec!cr_val = comprec2!cr_val
                repcomprec!segment1 = comprec2!segment1
                repcomprec!Vendor_Name = comprec2!Vendor_Name
                repcomprec!vendor_type_lookup_code = comprec2!vendor_type_lookup_code
                repcomprec!INVOICE_ID = comprec2!INVOICE_ID
                repcomprec!accounting_date = comprec2!accounting_date
                repcomprec!CHeck_Number = comprec2!CHeck_Number
        End If
End If
'End code by rahul
    comprec2.MoveNext
    'MsgBox "Yes"
    Loop
    repcomprec.UpdateBatch
  
Else
    MsgBox "No Transaction Is There is the Database !", vbInformation, "No Data"
    Exit Sub
End If
MsgBox CCODE & " Successfully Loaded in The System !"
StatusBar1.Panels(1).Text = ""
a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ANAREP
Else
    Exit Sub
End If
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Form_Load()
DTPicker1.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker2.Value = Format(Date, "dd-MMM-yyyy")
ADDSITE
ADDVEN
ADDVENDTYPE
End Sub

Public Sub ANAREP()
        cryctrl.WindowTitle = "ACCOUNT ANALYSIS REPORT"
        cryctrl.ReportFileName = App.Path & "\reportS\ap_Ledger.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                     .Action = 1
                End With
End Sub
