VERSION 5.00
Begin VB.Form Form10 
   BackColor       =   &H00FF8080&
   Caption         =   "TDS/CREDIT CHARGES"
   ClientHeight    =   8145
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7755
   LinkTopic       =   "Form10"
   MDIChild        =   -1  'True
   ScaleHeight     =   8145
   ScaleWidth      =   7755
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      Caption         =   "Update Invoice"
      Height          =   375
      Left            =   2280
      TabIndex        =   0
      Top             =   7320
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "&Exit"
      Height          =   375
      Left            =   3960
      TabIndex        =   1
      Top             =   7320
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "TDS-CC-Autoinvoice "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000011&
      Height          =   270
      Index           =   12
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   2310
   End
End
Attribute VB_Name = "Form10"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
strcmd = "select count(*) as net from Invoice_Dec " 'where " 'invoice_no ='" & "020-5070" & "'"
    strcmd = strcmd & " where TYPES='" & "TDS" & "'"
'strcmd = strcmd & " where ctr >=1551 and  Ctr <=1650)"
Set comprec = con1.Execute(strcmd)
        If Not comprec.EOF Then
            MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
        End If
add_data
End Sub

Private Sub Command2_Click()
Unload Me
End Sub


Public Sub add_data()
Dim repcomprec As ADODB.Recordset
Dim repcomprec1 As ADODB.Recordset
c = 1
Dim comprec15 As ADODB.Recordset

strcmd = "delete from RA_INTERFACE_LINES_ALL" ' where interface_status='" & "P" & "'"
Set comprec = con.Execute(strcmd)
Set comprec = con.Execute("Delete from RA_INTERFACE_DISTRIBUTIONS_ALL")
Set repcomprec = New ADODB.Recordset
Set repcomprec1 = New ADODB.Recordset
countctr = 1
strcmd = "select * from ID_Code "
Set comprec = con1.Execute(strcmd)
ctr = comprec!Id '9909720

'If Option1.Value = True Then
    strcmd = "select * from Invoice_Dec " 'where " ''invoice_no ='" & "011-23194" & "'"
    strcmd = strcmd & " where TYPES='" & "TDS" & "'"
'Else
'    strcmd = "select * from invoice where vdate >=Convert(DateTime, '" & DTPicker2.Value & "', 103)"
'    strcmd = strcmd & " AND vDate <=Convert(DateTime, '" & DTPicker3.Value & "', 103)"
'End If
Set comprec15 = con1.Execute(strcmd)
If Not comprec15.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "RA_INTERFACE_LINES_ALL", con, adOpenDynamic, adLockOptimistic
    Set repcomprec2 = New ADODB.Recordset
    repcomprec2.Open "RA_INTERFACE_DISTRIBUTIONS_ALL", con, adOpenDynamic, adLockOptimistic
    Do While Not comprec15.EOF
            ctr = ctr + 1
            DoEvents
            status.Show
            DoEvents
            
            'Code To Map Car No
            If comprec15!owned = True Then
                strcmd = "select * from new_car where veh_no='" & comprec15!vehicle_no & "'"
                Set newcomprec = con1.Execute(strcmd)
                If Not newcomprec.EOF Then
                    new_car = newcomprec!car_no
                Else
                    new_car = 0
                End If
            End If
            
            If comprec15!owned = False Then
                new_car = "Vendor Cateogory"
            End If
            
            'new_car = "Common" 'To Be change if required for Car No.
            
            strcmd = "select distinct(id) from auto_Import where RMS_Cust='" & comprec15!company & "'"
'            StatusBar1.Panels(1).Text = ctr
            Set comprec1 = con1.Execute(strcmd)
             If Not comprec1.EOF Then
                repcomprec1.AddNew
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "LINE"
                
                Select Case comprec15!types
                Case "STANDARD"
                    repcomprec1!Description = "Standard Packages"
                    repcomprec1!Amount = comprec15!basic
                    repcomprec1!QUANTITY = 1
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) 'chq no in case CC/TDS'& "-R"
                    repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                    repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                    repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                    repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                    repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                    repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                    repcomprec1!HEADER_ATTRIBUTE6 = Trim(comprec15!Car_Used_Date)
                    repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                    repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                    repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                    repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                    repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                    repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                    repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                    
                    If comprec15!Outstation_Amt > 0 Then
                        repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE14 = "No"
                    End If
                    
                    If comprec15!direct = True Then
                        repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE15 = "No"
                    End If
                    product = comprec15!product
                    
                Case "TDS"
                    repcomprec1!Description = "TDS Recoverable"  'TO BE CHANGED FOR TDS/CC INVOICES Credit Card Charges for CC
                    repcomprec1!Amount = -comprec15!basic
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) & "-" & ctr
                    repcomprec1!QUANTITY = -1
                    new_car = "Common"
                    product = "Common" 'comprec15!Product
                    
                Case "CC"
                    repcomprec1!Description = "Credit Card Charges"
                    repcomprec1!QUANTITY = -1
                    repcomprec1!Amount = -comprec15!basic
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) & "-" & ctr
                    new_car = "Common"
                    product = "Common"
                End Select
                
                    repcomprec1!Term_Id = comprec15!Term_Id    'For 30 Net
                    repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                    repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                    repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!gl_DATE = comprec15!VDATE 'CDate("01-12-2005")
                    repcomprec1!UNIT_SELLING_PRICE = comprec15!basic
                    repcomprec1!UNIT_STANDARD_PRICE = comprec15!basic
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!UOM_CODE = "EA"
                    repcomprec1!UOM_NAME = "Each"
                    repcomprec1!ORG_ID = 101 '82
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
    '                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) 'chq no in case CC/TDS'& "-R"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    
                    '*****************************************
                    'Program Batch Name to be change for different batches
                    repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                    '*****************************************
                    
                    repcomprec1!trx_number = Left(comprec15!invoice_no, 20) '& "-R"
                    repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    
                    '*****************************************
                    'Program Invoice Type Name to be change for different branches
                    strcmd = "select * from inv_type where branch_Name='" & comprec15!branch_bill_to & "'"
                    strcmd = strcmd & " and credit_tds_cc='" & comprec15!types & "'"   '*Done**To Be changed
                    Set comprec7 = con1.Execute(strcmd)
                    If Not comprec7.EOF Then
                        Cipl_Type = comprec7!Inv_Type
                    Else
                        Cipl_Type = ""
                    End If
                    
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    '*****************************************
                    repcomprec1.Update
                    
                    strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                    strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008036" '1009931"  '
                    strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                    strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec15!Ship_To1 & "'"
                    Set comprec3 = con.Execute(strcmd2)
                           
                    strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                    strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008037" '1009932"
                    strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                    strcmd2 = strcmd2 & " AND FVTB.Description='" & new_car & "'"
                    Set comprec4 = con.Execute(strcmd2)
                
                   '*&***************************************
                    'if product is drive from the system
                    strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                    strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008038" '1009934"
                    strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                    strcmd2 = strcmd2 & " AND FVTB.Description='" & product & "'"
                    Set comprec5 = con.Execute(strcmd2)
                    '**************************************************
                    'Distribution Table to Be insert Here
                    repcomprec2.AddNew
                    'repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "REV"
                    repcomprec2!Amount = comprec15!basic
                    repcomprec2!Percent = 100
                    
                    If Not comprec3.EOF Then
                        repcomprec2!segment1 = comprec3!FLEX_VALUE  'Location Flexfield
                    End If
                    
                    If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
                    End If
                    
                    If Not comprec5.EOF Then
                        repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE    'Product
                    End If
                     
                    Select Case comprec15!types
                    Case "STANDARD"
                        repcomprec2!SEGMENT3 = "30001"  'Natural Acct For Revenue Owned Cars
                        repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30))
                    Case "TDS"
                        repcomprec2!SEGMENT3 = "14221"  'Natural Acct For TDS Recoverable
                        repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) & "-" & ctr
                    Case "CC"
                        repcomprec2!SEGMENT3 = "43102"  'Natural Acct For CC
                        repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) & "-" & ctr
                    End Select
                    
                    repcomprec2!ORG_ID = 101 '82   'Org Id
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    'repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-R"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2.Update
                    
                    '-Distribution Line For Receivale Entry
                    repcomprec2.AddNew
                    'repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "REC"
                    repcomprec2!Amount = comprec15!basic
                    repcomprec2!Percent = 100
                    
                    Select Case comprec15!types
                    Case "STANDARD"
                        repcomprec2!SEGMENT3 = "30001"  'Natural Acct For Revenue Owned Cars
                        repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30))
                    Case "TDS"
                        repcomprec2!SEGMENT3 = "14221"  'Natural Acct For TDS Recoverable
                        repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) & "-" & ctr
                    Case "CC"
                        repcomprec2!SEGMENT3 = "43102"  'Natural Acct For CC
                        repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 15)) & "-" & ctr
                    End Select
    
                    
                    strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                    strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1008036" '1009931"
                    strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                    strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec15!branch_bill_to & "'"
                    Set comprec6 = con.Execute(strcmd2)
                    
                    If Not comprec6.EOF Then
                        repcomprec2!segment1 = comprec6!FLEX_VALUE  'Location Flexfield
                    End If
                    
                    If Not comprec4.EOF Then
                         repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
                    End If
                    
                    If Not comprec5.EOF Then
                        repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE    'Product
                    End If
                    'repcomprec2!SEGMENT3 = "13001"  'Natural Acct For Revenue Owned Cars
                    repcomprec2!ORG_ID = 101 '82   'Org Id
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    'repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-R"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2.Update
                    ctr = ctr + 1
    '                Else
     '                   List2.AddItem comprec17!Company
      '              End If
                
                    
                    If comprec15!types = "STANDARD" Then
                    '--------Line2-------------------------------------------------------
                    If (comprec15!Parking_Toll + comprec15!other_taxes) > 0 Then
                    repcomprec1.AddNew
                    'repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "LINE"
                    repcomprec1!Description = "Parking & Toll Charges"
                    repcomprec1!Amount = (comprec15!Parking_Toll + comprec15!other_taxes)
                    repcomprec1!Term_Id = comprec15!Term_Id
                    repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                    repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                    repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                    repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!gl_DATE = comprec15!VDATE 'CDate("01-12-2005")
                    repcomprec1!QUANTITY = 1
                    repcomprec1!UNIT_SELLING_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                    repcomprec1!UNIT_STANDARD_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                    repcomprec1!ATTRIBUTE1 = ctr - 1
                    repcomprec1!UOM_CODE = "EA"
                    repcomprec1!UOM_NAME = "Each"
                    repcomprec1!ORG_ID = 101 '82
                    repcomprec1!trx_number = Left(comprec15!invoice_no, 20) '& "-R"
                    repcomprec1!PURCHASE_ORDER = UCase(Left(comprec15!Client_Name, 50)) & ""
                    repcomprec1!HEADER_ATTRIBUTE_CATEGORY = "CIPL Invoice Header"
                    repcomprec1!HEADER_ATTRIBUTE1 = "Corporate"
                    repcomprec1!HEADER_ATTRIBUTE2 = UCase(Trim(comprec15!Client_Name))
                    repcomprec1!HEADER_ATTRIBUTE3 = UCase(Trim(comprec15!Cat))
                    repcomprec1!HEADER_ATTRIBUTE4 = UCase(Trim(comprec15!Category))
                    repcomprec1!HEADER_ATTRIBUTE5 = Trim(Left(comprec15!Pickup, 60))
                    repcomprec1!HEADER_ATTRIBUTE6 = comprec15!Car_Used_Date
                    repcomprec1!HEADER_ATTRIBUTE7 = Trim(UCase(comprec15!Driver) & "")
                    repcomprec1!HEADER_ATTRIBUTE8 = Trim(UCase(comprec15!Assignment) & "")
                    repcomprec1!HEADER_ATTRIBUTE9 = UCase(Trim(comprec15!Package_Name))
                    repcomprec1!HEADER_ATTRIBUTE10 = Trim(comprec15!KM_out)
                    repcomprec1!HEADER_ATTRIBUTE11 = Trim(comprec15!KM_in)
                    repcomprec1!HEADER_ATTRIBUTE12 = Trim(comprec15!Reason_Delay)
                    repcomprec1!HEADER_ATTRIBUTE13 = Trim(comprec15!Reason_Cancel)
                    
                    If comprec15!Outstation_Amt > 0 Then
                        repcomprec1!HEADER_ATTRIBUTE14 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE14 = "No"
                    End If
                    
                    If comprec15!direct = True Then
                        repcomprec1!HEADER_ATTRIBUTE15 = "Yes"
                    Else
                        repcomprec1!HEADER_ATTRIBUTE15 = "No"
                    End If
                    
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    
                    p_Ctr = ctr
                    repcomprec1.Update
                    
                    '----------------For Distribution
                    repcomprec2.AddNew
                    'repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "REV"
                    repcomprec2!Amount = (comprec15!Parking_Toll + comprec15!other_taxes)
                    repcomprec2!Percent = 100
                    
                    If Not comprec3.EOF Then
                        repcomprec2!segment1 = comprec3!FLEX_VALUE  'Location Flexfield
                    End If
                    
                    If Not comprec4.EOF Then
                        repcomprec2!SEGMENT2 = comprec4!FLEX_VALUE      'Car Number Flexfield
                    End If
                    
                    If Not comprec5.EOF Then
                        repcomprec2!SEGMENT4 = comprec5!FLEX_VALUE
                    End If
                    
        '            strcmd3 = "select CODE_COMBINATION_ID from gl_code_combinations r where r.SEGMENT1 ='" & comprec3!FLEX_VALUE & "'"
        '            strcmd3 = strcmd3 & " and r.SEGMENT2 ='" & comprec4!FLEX_VALUE & "'"
        '            strcmd3 = strcmd3 & " and r.SEGMENT3  = '" & "40301" & "'"
        '            strcmd3 = strcmd3 & " and r.SEGMENT4 ='" & comprec5!FLEX_VALUE & "'"
        '            Set comprec6 = con.Execute(strcmd3)
        
        '            If Not comprec6.EOF Then
        '                repcomprec2!CODE_COMBINATION_ID = comprec6!CODE_COMBINATION_ID
        '            End If
        
                    repcomprec2!ORG_ID = 101 '82   'Org Id
                    repcomprec2!SEGMENT3 = "40301"    'Natural Acct For Car Parking
                    'repcomprec2!ORG_ID = 101
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2.Update
                    End If
        '---------------------------fOR TAX LINE---------------------------------------
                    
                    ctr = ctr + 1
                    repcomprec1.AddNew
                    'repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "TAX"
                    repcomprec1!Description = "Tax line"
                    repcomprec1!Amount = Round((comprec15!basic * 4) / 100, 2)
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!TAX_RATE = 4
                    repcomprec1!TAX_CODE = "Service Tax @ 4%"
                    repcomprec1!TAX_PRECEDENCE = 0
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!ORG_ID = 101 '82
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-R"
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    
                    '----------------For Distribution
                    repcomprec2.AddNew
                    'repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "TAX"
                    repcomprec2!Amount = Round((comprec15!basic * 4) / 100, 2)
                    repcomprec2!Percent = 100
                    
    '                repcomprec2!CODE_COMBINATION_ID = 1153
                    repcomprec2!segment1 = "000"
                    repcomprec2!SEGMENT2 = "00000"
                    repcomprec2!SEGMENT3 = "25004"
                    repcomprec2!SEGMENT4 = "00"
                    repcomprec2!ORG_ID = 101 '82
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2.Update
                   '--------------------------------fOR cESS cALCULATION---------------------------
                    ctr = ctr + 1
                    repcomprec1.AddNew
                    'repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "TAX"
                    repcomprec1!Description = "Tax line"
                    repcomprec1!Amount = Round((comprec15!basic * 0.08) / 100, 2)
                    
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    repcomprec1!TAX_RATE = 2
                    repcomprec1!TAX_CODE = "Service Tax Education Cess @ 2%"
                    repcomprec1!TAX_PRECEDENCE = 1
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!ORG_ID = 101 '82
                    
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-R"
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    '----------------For Distribution
                    repcomprec2.AddNew
                    'repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "TAX"
                    repcomprec2!Amount = Round((comprec15!basic * 0.08) / 100, 2)
                    repcomprec2!Percent = 100
    '                repcomprec2!CODE_COMBINATION_ID = 1153
                    repcomprec2!segment1 = "000"
                    repcomprec2!SEGMENT2 = "00000"
                    repcomprec2!SEGMENT3 = "25004"
                    repcomprec2!SEGMENT4 = "00"
                    repcomprec2!ORG_ID = 101 '82
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2.Update
                   '--------------------------------fOR vat cALCULATION---------------------------
            
                    If comprec15!CITY_NAME = "DELHI" Then
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     'repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round((comprec15!basic * 12.5) / 100, 2)
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     repcomprec1!TAX_RATE = 12.5
                     repcomprec1!TAX_CODE = "VAT @ 12.5%"
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101 '82
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = Trim(Left(comprec15!Duty_Slip_No, 30)) '& "-R"
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                        '----------------For Distribution
                     repcomprec2.AddNew
                     'repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     repcomprec2!Amount = Round((comprec15!basic * 12.5) / 100, 2)
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1157
                     repcomprec2!segment1 = "000"
                     repcomprec2!SEGMENT2 = "00000"
                     repcomprec2!SEGMENT3 = "25007"
                     repcomprec2!SEGMENT4 = "00"
                     repcomprec2!ORG_ID = 101 '82
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2.Update
                    End If
           
                If (comprec15!Parking_Toll + comprec15!other_taxes) > 0 Then
                    ' ---------------------------Tax on Parking
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     'repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 4) / 100, 2)
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     repcomprec1!TAX_RATE = 4
                     repcomprec1!TAX_CODE = "Service Tax @ 4%"
                     repcomprec1!TAX_PRECEDENCE = 0
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101 '82
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1.Update
                     '----------------For Distribution
                     repcomprec2.AddNew
                     'repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     repcomprec2!Amount = Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 4) / 100, 2)
                     repcomprec2!Percent = 100
                     repcomprec2!segment1 = "000"
                     repcomprec2!SEGMENT2 = "00000"
                     repcomprec2!SEGMENT3 = "25004"
                     repcomprec2!SEGMENT4 = "00"
                     repcomprec2!ORG_ID = 101 '82
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2.Update
                'End If
                '--------------------------------fOR cESS cALCULATION---------------------------
                     ctr = ctr + 1
                     repcomprec1.AddNew
                     'repcomprec1!INTERFACE_LINE_ID = ctr
                     repcomprec1!LINE_TYPE = "TAX"
                     repcomprec1!Description = "Tax line"
                     repcomprec1!Amount = Round((comprec15!Parking_Toll + comprec15!other_taxes) * 0.08 / 100)
                     repcomprec1!CONVERSION_TYPE = "User"
                     repcomprec1!CONVERSION_RATE = 1
                     repcomprec1!TRX_DATE = comprec15!VDATE
                     repcomprec1!TAX_RATE = 2
                     repcomprec1!TAX_CODE = "Service Tax Education Cess @ 2%"
                     repcomprec1!TAX_PRECEDENCE = 1
                     repcomprec1!ATTRIBUTE1 = ctr
                     repcomprec1!ORG_ID = 101 '82
                     repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                     repcomprec1!SET_OF_BOOKS_ID = 1001
                     repcomprec1!CURRENCY_CODE = "INR"
                     repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                     repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    'For Distribution---------
                     
                     repcomprec2.AddNew
                     'repcomprec2!INTERFACE_LINE_ID = ctr
                     repcomprec2!ACCOUNT_CLASS = "TAX"
                     repcomprec2!Amount = Round(comprec15!STAX, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 0.08) / 100, 2)
                     repcomprec2!Percent = 100
                     'repcomprec2!CODE_COMBINATION_ID = 1153
                     repcomprec2!segment1 = "000"
                     repcomprec2!SEGMENT2 = "00000"
                     repcomprec2!SEGMENT3 = "25004"
                     repcomprec2!SEGMENT4 = "00"
                     repcomprec2!ORG_ID = 101 '82
                     repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                     repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                     repcomprec2.Update
                    '--------------------------------fOR vat cALCULATION---------------------------
                    
                    If comprec15!CITY_NAME = "DELHI" Then
                    ctr = ctr + 1
                    repcomprec1.AddNew
                    'repcomprec1!INTERFACE_LINE_ID = ctr
                    repcomprec1!LINE_TYPE = "TAX"
                    repcomprec1!Description = "Tax line"
                    repcomprec1!Amount = Round(comprec15!VAT, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 12.5) / 100, 2)
                    
                    repcomprec1!CONVERSION_TYPE = "User"
                    repcomprec1!CONVERSION_RATE = 1
                    repcomprec1!TRX_DATE = comprec15!VDATE
                    
                    repcomprec1!TAX_RATE = 12.5
                    repcomprec1!TAX_CODE = "VAT @ 12.5%"
                    repcomprec1!TAX_PRECEDENCE = 0
                    repcomprec1!ATTRIBUTE1 = ctr
                    repcomprec1!ORG_ID = 101 '82
                    repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                    repcomprec1!SET_OF_BOOKS_ID = 1001
                    repcomprec1!CURRENCY_CODE = "INR"
                    repcomprec1!CUST_TRX_TYPE_NAME = Cipl_Type
                    repcomprec1!LINK_TO_LINE_CONTEXT = "CIPL Transaction Line"   '' require from tax
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE1 = p_Ctr
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec1!LINK_TO_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec1.Update
                    '---------------FOR dISTRIBUTION
                    
                    repcomprec2.AddNew
                    'repcomprec2!INTERFACE_LINE_ID = ctr
                    repcomprec2!ACCOUNT_CLASS = "TAX"
                    repcomprec2!Amount = Round(comprec15!VAT, 2) 'Round(((comprec15!Parking_Toll + comprec15!other_taxes) * 12.5) / 100, 2)
                    repcomprec2!Percent = 100
                    repcomprec2!CODE_COMBINATION_ID = 1157
                    repcomprec2!segment1 = "000"
                    repcomprec2!SEGMENT2 = "00000"
                    repcomprec2!SEGMENT3 = "25007"
                    repcomprec2!SEGMENT4 = "00"
                    repcomprec2!ORG_ID = 101 '82
                    repcomprec2!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE1 = ctr
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE2 = Trim(comprec15!Extra_Km)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE3 = Trim(comprec15!Extra_Hrs)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE4 = Trim(comprec15!Outstation_Amt)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE5 = Trim(comprec15!Fuel)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE6 = Trim(comprec15!Night)
                    repcomprec2!INTERFACE_LINE_ATTRIBUTE7 = Trim(comprec15!other_Charges)
                    repcomprec2.Update
                    End If
                    End If
                    End If                              'endif If standard if
                Else
                    
                    Set repcomprec4 = New ADODB.Recordset
                    repcomprec4.Open "Error_List12", con1, adOpenDynamic, adLockOptimistic
                    repcomprec4.AddNew
                    repcomprec4!company = comprec15!company
                    repcomprec4.Update
                    List2.AddItem comprec15!company & "---" & comprec15!invoice_no
                End If
            comprec15.MoveNext
    Loop
End If
        
       strcmd = "update id_code set id='" & ctr & "'"
       Set repcomprec = con1.Execute(strcmd)

        comprec15.Close
        strcmd = "select count(*) as net from Invoice_Dec " 'where " 'invoice_no ='" & "011-23194" & "'"
        strcmd = strcmd & " where TYPES='" & "TDS" & "'"
        'strcmd = strcmd & "  invoice_no in(select distinct trx_number from invimp WHERE DONE=0)"        'strcmd = strcmd & "and uploaded=0"
        Set comprec = con1.Execute(strcmd)
        If Not comprec.EOF Then
            MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
        End If
        Unload status
Exit Sub
End Sub

Private Sub Form_Load()
Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2
End Sub
