VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Knock 
   BackColor       =   &H00FF8080&
   Caption         =   "Receipt Application Interface"
   ClientHeight    =   8025
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8610
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "KNCOME.frx":0000
   LinkTopic       =   "Form5"
   MaxButton       =   0   'False
   ScaleHeight     =   8025
   ScaleWidth      =   8610
   Begin VB.ComboBox Combo1 
      Height          =   330
      ItemData        =   "KNCOME.frx":000C
      Left            =   6120
      List            =   "KNCOME.frx":000E
      TabIndex        =   17
      Text            =   "Knocking"
      Top             =   5640
      Width           =   2175
   End
   Begin VB.ComboBox Combo7 
      Height          =   330
      ItemData        =   "KNCOME.frx":0010
      Left            =   3480
      List            =   "KNCOME.frx":0012
      TabIndex        =   15
      Text            =   "Knocking"
      Top             =   5640
      Width           =   2175
   End
   Begin VB.ComboBox CashIDTo 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3480
      TabIndex        =   4
      Text            =   "CashIDTo"
      Top             =   6720
      Width           =   1455
   End
   Begin VB.OptionButton reknock 
      BackColor       =   &H00FF8080&
      Caption         =   "Receipt Wise"
      Height          =   350
      Left            =   5040
      TabIndex        =   5
      Top             =   6720
      Width           =   1400
   End
   Begin VB.OptionButton dtknock 
      BackColor       =   &H00FF8080&
      Caption         =   "Date Wise"
      Height          =   350
      Left            =   5040
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   2
      Top             =   6120
      Width           =   1400
   End
   Begin VB.ComboBox CashIDFrom 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1200
      TabIndex        =   3
      Text            =   "CashIDFrom"
      Top             =   6720
      Width           =   1695
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   3480
      TabIndex        =   1
      Top             =   6120
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393216
      Format          =   15990785
      CurrentDate     =   39650
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   1200
      TabIndex        =   0
      Top             =   6120
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      Format          =   15990785
      CurrentDate     =   39650
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      Height          =   375
      Left            =   3480
      TabIndex        =   7
      Top             =   7320
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Upload"
      Height          =   375
      Left            =   2280
      TabIndex        =   6
      Top             =   7320
      Width           =   1215
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   5055
      Left            =   0
      TabIndex        =   8
      Top             =   360
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   8916
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   0
      BackColor       =   16771515
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Sr.No."
         Object.Width           =   1147
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "TRX_NO"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "CASH ID"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "AMOUNT"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "DATE"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label7 
      BackStyle       =   0  'Transparent
      Caption         =   "Label7"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1920
      TabIndex        =   16
      Top             =   5640
      Width           =   975
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   3120
      TabIndex        =   14
      Top             =   6840
      Width           =   210
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   720
      TabIndex        =   13
      Top             =   6840
      Width           =   435
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Cash ID"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   240
      TabIndex        =   12
      Top             =   6840
      Width           =   615
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   3120
      TabIndex        =   11
      Top             =   6195
      Width           =   210
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   720
      TabIndex        =   10
      Top             =   6195
      Width           =   435
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Receipt Application Interface"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   4590
   End
End
Attribute VB_Name = "Knock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
ListView1.ListItems.Clear
ListView1.Refresh
'CashIDFrom.AddItem comprec!CASH_ID
'CashIDTo.AddItem comprec!CASH_ID
'CashIDTo.Text >= CashIDFrom.Text
p_start_dt = Format(DTPicker1.Value, "yyyy-mm-dd")
P_end_dt = Format(DTPicker2.Value, "yyyy-mm-dd")
'Cast(floor(cast(p_end_dt as float)) as datetime)
Set comprec2 = con.Execute("truncate table PWC_RECEIPT_TL")
Set repcomprec1 = New ADODB.Recordset
repcomprec1.Open "PWC_RECEIPT_TL", con, adOpenDynamic, adLockOptimistic

If Combo7.Text = "Knocking Off" Then
If (dtknock.Value = True) Then
    strcmd = "SELECT * FROM knock where cash_id is not null and status=0 "
    strcmd = strcmd & " and gl_date between '" & p_start_dt & "'"
    strcmd = strcmd & " and '" & P_end_dt & "'"
Else
    strcmd = "SELECT * FROM knock where cash_id is not null and status=0 "
    strcmd = strcmd & " and cash_id >= '" & CashIDFrom.Text & "'"
    strcmd = strcmd & " and cash_id <= '" & CashIDTo.Text & "'"
    'Text1.Text = Receipt_no
    
    If CashIDFrom.Text > CashIDTo.Text Then
    MsgBox "From Value must be smaller than To value"
         Exit Sub
    End If
End If

Else
If (dtknock.Value = True) Then
    strcmd = "SELECT * FROM knockcopy where cash_id is not null and status=0 "
    strcmd = strcmd & " and gl_date between '" & p_start_dt & "'"
    strcmd = strcmd & " and '" & P_end_dt & "'"
Else
    strcmd = "SELECT * FROM knockcopy where cash_id is not null and status=0 "
    strcmd = strcmd & " and cash_id >= '" & CashIDFrom.Text & "'"
    strcmd = strcmd & " and cash_id <= '" & CashIDTo.Text & "'"
    'Text1.Text = Receipt_no
    
    If CashIDFrom.Text > CashIDTo.Text Then
    MsgBox "From Value must be smaller than To value"
         Exit Sub
    End If
End If
End If
'CAST(
'FLOOR( CAST( GETDATE() AS FLOAT ) )
'AS DATETIME
')
'where ds_num='" & "044-50980" & "'" 'WHERE amount > 0" 'where trx_type='" & "CIPL DELHI Invoice" & "'"
'strcmd = strcmd & " (select CIPL_ID from sh)" 'CASH_ID=12068 AND FLAG = 0"
'strcmd = strcmd & " and f11=0"
Dim cntr As Integer
cntr = 1
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        repcomprec1.AddNew
        Set lt = ListView1.ListItems.Add(, , cntr)
        'Set lt = ListView1.ListItems.Add(, , comprec!trx_number)
        'lt.SubItems(1) = Mid(comprec!trx_number, 1, Len(comprec!trx_number))
        ' Below is the new Code Tested by Rahul
        If Combo1.Text = "Normal" Then
            lt.SubItems(1) = comprec!trx_number
        Else
            lt.SubItems(1) = comprec!trx_number + " AA"
        End If
        lt.SubItems(2) = comprec!CASH_ID
        'Mid(comprec!trx_number, 1, Len(comprec!trx_number))
        
        lt.SubItems(3) = comprec!adjusted_amount
        
        'Testing Rahul
        'repcomprec1!V_CR_ID = Mid(comprec!CASH_ID, 1, Len(comprec!CASH_ID) - 2)
        
        lt.SubItems(4) = comprec!gl_DATE
        
        repcomprec1!V_CR_ID = comprec!CASH_ID
        If Combo1.Text = "Normal" Then
            repcomprec1!V_TRX_NUMBER = Mid(comprec!trx_number, 1, Len(comprec!trx_number))
        Else
            repcomprec1!V_TRX_NUMBER = Mid(comprec!trx_number, 1, Len(comprec!trx_number)) + " AA"
        End If
        repcomprec1!V_AMOUNT = comprec!adjusted_amount
        repcomprec1!flag = "NEW"
        repcomprec1!V_GL_DATE = comprec!gl_DATE
        
        'a1 = Mid(comprec!CASH_ID, 1, Len(comprec!CASH_ID) - 2)
        ' Below is the new Code Tested by Rahul
        ''a1 = comprec!CASH_ID
        
        ''strcmd = "select gl_date from ar_cash_receipt_history_all s"
        ''strcmd = strcmd & " Where s.cash_receipt_id = " & a1 & ""
        ''Set comprec1 = con.Execute(strcmd)
        ''If Not comprec1.EOF Then
        ''   repcomprec1!V_GL_DATE = comprec1!gl_date
        ''End If
        
        'strcmd = "select gl_date from ra_customer_trx_all s "
        ''strcmd = "select a.gl_date from ra_cust_trx_line_gl_dist_all a,ra_customer_trx_all b where a.customer_trx_id=b.customer_trx_id "
        ''strcmd = strcmd & " and b.trx_number = '" & Mid(comprec!trx_number, 1, Len(comprec!trx_number)) & "'"
        
        ''Set comprec3 = con.Execute(strcmd)
        
        ''If Not comprec3.EOF Then
        'Test By Rahul
        ''If Not comprec1.EOF Then
        ''If comprec3!gl_date > comprec1!gl_date Then
        ''repcomprec1!V_GL_DATE = comprec3!gl_date
        ''Else
        ''       repcomprec1!V_GL_DATE = DateValue(Now)
                ' "27-06-2008"
        ''End If
        ''End If
        'Test By Rahul
        ''End If
        
        comprec.MoveNext
        cntr = cntr + 1
    Loop
    repcomprec1.UpdateBatch
End If

MsgBox "Total Invoices : " & cntr - 1
End Sub

Private Sub Command2_Click()
Unload Me
End Sub
'Added by Rahul --------------------
Private Sub dtknock_Click()
    CashIDFrom.Enabled = False
    CashIDTo.Enabled = False
    DTPicker1.Enabled = True
    DTPicker2.Enabled = True
End Sub

Private Sub Form_Load()
Combo7.Clear
Combo7.AddItem "Knocking Off"
Combo7.AddItem "CC Charges"
Combo7.Text = Combo7.List(0)

Combo1.Clear
Combo1.AddItem "Normal"
Combo1.AddItem "Reversal"
Combo1.Text = Combo1.List(0)

DTPicker1.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker2.Value = Format(Date, "dd-MMM-yyyy")
Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2

'Added by Rahul --------------------
If Combo7.Text = "Knocking Off" Then
strcmd = "select distinct Cash_ID from knock  "
strcmd = strcmd & " where cash_id is not null and status = 0 "
strcmd = strcmd & "  and receipt_no is not null order by  cash_id "
Else
strcmd = "select distinct Cash_ID from knockcopy  "
strcmd = strcmd & " where cash_id is not null and status = 0 "
strcmd = strcmd & "  and receipt_no is not null order by  cash_id "
End If
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        'ReceiptNo.AddItem ("" & comprec!receipt_no & ", " & comprec!cash_id & "")
        CashIDFrom.AddItem comprec!CASH_ID
        CashIDTo.AddItem comprec!CASH_ID
        comprec.MoveNext
    Loop
End If

CashIDFrom.Text = CashIDFrom.List(0)
CashIDTo.Text = CashIDTo.List(0)
End Sub
'Added by Rahul --------------------
Private Sub reknock_Click()
    CashIDFrom.Enabled = True
    CashIDTo.Enabled = True
    DTPicker1.Enabled = False
    DTPicker2.Enabled = False
    CashIDFrom.Clear
    CashIDTo.Clear
'Added by Rahul --------------------
If Combo7.Text = "Knocking Off" Then
strcmd = "select distinct Cash_ID from knock  "
strcmd = strcmd & " where cash_id is not null and status = 0 "
strcmd = strcmd & "  and receipt_no is not null order by  cash_id "
Else
strcmd = "select distinct Cash_ID from knockcopy  "
strcmd = strcmd & " where cash_id is not null and status = 0 "
strcmd = strcmd & "  and receipt_no is not null order by  cash_id "
End If
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        'ReceiptNo.AddItem ("" & comprec!receipt_no & ", " & comprec!cash_id & "")
        CashIDFrom.AddItem comprec!CASH_ID
        CashIDTo.AddItem comprec!CASH_ID
        comprec.MoveNext
    Loop
End If

CashIDFrom.Text = CashIDFrom.List(0)
CashIDTo.Text = CashIDTo.List(0)
'-----------------------------------
End Sub
