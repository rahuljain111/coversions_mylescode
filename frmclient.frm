VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Begin VB.Form frmclient 
   Caption         =   "Vendor Detail"
   ClientHeight    =   8490
   ClientLeft      =   225
   ClientTop       =   900
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmclient.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8490
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Combo4 
      Height          =   315
      Left            =   7560
      TabIndex        =   23
      Top             =   360
      Visible         =   0   'False
      Width           =   3135
   End
   Begin MSComctlLib.ListView List1 
      Height          =   6615
      Left            =   7560
      TabIndex        =   24
      Top             =   720
      Visible         =   0   'False
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   11668
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Car No"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Vendor"
         Object.Width           =   4763
      EndProperty
   End
   Begin VB.CheckBox Check1 
      Caption         =   "&View"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   210
      Left            =   7560
      TabIndex        =   22
      Top             =   120
      Width           =   735
   End
   Begin RichTextLib.RichTextBox TTT 
      Height          =   135
      Left            =   0
      TabIndex        =   60
      Top             =   0
      Visible         =   0   'False
      Width           =   135
      _ExtentX        =   238
      _ExtentY        =   238
      _Version        =   393217
      BackColor       =   14548991
      Enabled         =   -1  'True
      TextRTF         =   $"frmclient.frx":000C
   End
   Begin VB.Frame frameeng 
      BorderStyle     =   0  'None
      Height          =   5895
      Left            =   0
      TabIndex        =   51
      Top             =   480
      Width           =   7335
      Begin VB.CheckBox chk1 
         Caption         =   "VDP"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4080
         TabIndex        =   14
         Top             =   4320
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Frame Frame1 
         Caption         =   "Vehicle Detail"
         Height          =   2895
         Left            =   4080
         TabIndex        =   61
         Top             =   360
         Visible         =   0   'False
         Width           =   3015
         Begin VB.TextBox txtFields 
            DataField       =   "Employee ID"
            DataSource      =   "Data1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   18
            Left            =   960
            TabIndex        =   13
            Top             =   2040
            Width           =   855
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Diesel"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   1800
            TabIndex        =   12
            Top             =   1680
            Width           =   855
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Petrol"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   840
            TabIndex        =   11
            Top             =   1680
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.ComboBox cmbmodel 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   960
            TabIndex        =   10
            Top             =   1200
            Width           =   1815
         End
         Begin VB.ComboBox cmbcolor 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H008080FF&
            Height          =   330
            Left            =   960
            TabIndex        =   9
            Top             =   720
            Width           =   1695
         End
         Begin VB.Frame Frame7 
            Caption         =   "Authorisation"
            Height          =   735
            Left            =   120
            TabIndex        =   81
            Top             =   7560
            Width           =   5535
            Begin VB.TextBox txtFields 
               DataField       =   "Employee ID"
               DataSource      =   "Data1"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Index           =   17
               Left            =   1080
               TabIndex        =   39
               Top             =   240
               Width           =   1695
            End
            Begin MSComCtl2.DTPicker DTPicker6 
               Height          =   315
               Left            =   3480
               TabIndex        =   40
               Top             =   240
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CustomFormat    =   "dd-MM-yyyy"
               Format          =   57278467
               CurrentDate     =   38046
            End
            Begin VB.Label lblLabels 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Exp. Dt."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   210
               Index           =   25
               Left            =   2880
               TabIndex        =   83
               Tag             =   "Order Amount:"
               Top             =   240
               Width           =   555
            End
            Begin VB.Label lblLabels 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Issue Place"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   210
               Index           =   24
               Left            =   120
               TabIndex        =   82
               Tag             =   "Order Amount:"
               Top             =   240
               Width           =   825
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Road Permit"
            Height          =   1335
            Left            =   120
            TabIndex        =   72
            Top             =   8160
            Width           =   1935
            Begin VB.TextBox txtFields 
               DataField       =   "Employee ID"
               DataSource      =   "Data1"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Index           =   13
               Left            =   720
               TabIndex        =   31
               Top             =   240
               Width           =   1095
            End
            Begin MSComCtl2.DTPicker DTPicker2 
               Height          =   315
               Left            =   720
               TabIndex        =   35
               Top             =   720
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CustomFormat    =   "dd-MM-yyyy"
               Format          =   57278467
               CurrentDate     =   38046
            End
            Begin VB.Label lblLabels 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Exp. Dt."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   210
               Index           =   17
               Left            =   120
               TabIndex        =   74
               Tag             =   "Order Amount:"
               Top             =   720
               Width           =   555
            End
            Begin VB.Label lblLabels 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue Place"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   450
               Index           =   16
               Left            =   120
               TabIndex        =   73
               Tag             =   "Order Amount:"
               Top             =   240
               Width           =   465
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "State Road Permit"
            Height          =   1335
            Left            =   2040
            TabIndex        =   71
            Top             =   7080
            Width           =   1935
            Begin VB.TextBox txtFields 
               DataField       =   "Employee ID"
               DataSource      =   "Data1"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Index           =   14
               Left            =   720
               TabIndex        =   32
               Top             =   240
               Width           =   1095
            End
            Begin MSComCtl2.DTPicker DTPicker3 
               Height          =   315
               Left            =   720
               TabIndex        =   36
               Top             =   720
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CustomFormat    =   "dd-MM-yyyy"
               Format          =   57278467
               CurrentDate     =   38046
            End
            Begin VB.Label lblLabels 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Exp. Dt."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   210
               Index           =   19
               Left            =   120
               TabIndex        =   76
               Tag             =   "Order Amount:"
               Top             =   720
               Width           =   555
            End
            Begin VB.Label lblLabels 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue Place"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   450
               Index           =   18
               Left            =   120
               TabIndex        =   75
               Tag             =   "Order Amount:"
               Top             =   240
               Width           =   465
            End
         End
         Begin VB.Frame Frame6 
            Caption         =   "Fitness Permit"
            Height          =   1335
            Left            =   3960
            TabIndex        =   70
            Top             =   7080
            Width           =   1935
            Begin VB.TextBox txtFields 
               DataField       =   "Employee ID"
               DataSource      =   "Data1"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Index           =   15
               Left            =   600
               TabIndex        =   33
               Top             =   240
               Width           =   1215
            End
            Begin MSComCtl2.DTPicker DTPicker4 
               Height          =   315
               Left            =   650
               TabIndex        =   37
               Top             =   720
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CustomFormat    =   "dd-MM-yyyy"
               Format          =   57278467
               CurrentDate     =   38046
            End
            Begin VB.Label lblLabels 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Exp. Dt."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   210
               Index           =   21
               Left            =   120
               TabIndex        =   78
               Tag             =   "Order Amount:"
               Top             =   720
               Width           =   555
            End
            Begin VB.Label lblLabels 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue Place"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   450
               Index           =   20
               Left            =   120
               TabIndex        =   77
               Tag             =   "Order Amount:"
               Top             =   240
               Width           =   465
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "State Road Permit"
            Height          =   1335
            Left            =   5880
            TabIndex        =   69
            Top             =   7080
            Width           =   2415
            Begin VB.TextBox txtFields 
               DataField       =   "Employee ID"
               DataSource      =   "Data1"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   315
               Index           =   16
               Left            =   840
               TabIndex        =   34
               Top             =   240
               Width           =   1335
            End
            Begin MSComCtl2.DTPicker DTPicker5 
               Height          =   315
               Left            =   840
               TabIndex        =   38
               Top             =   720
               Width           =   1215
               _ExtentX        =   2143
               _ExtentY        =   556
               _Version        =   393216
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               CustomFormat    =   "dd-MM-yyyy"
               Format          =   57278467
               CurrentDate     =   38046
            End
            Begin VB.Label lblLabels 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Exp. Dt."
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   210
               Index           =   23
               Left            =   120
               TabIndex        =   80
               Tag             =   "Order Amount:"
               Top             =   720
               Width           =   555
            End
            Begin VB.Label lblLabels 
               BackStyle       =   0  'Transparent
               Caption         =   "Issue Place"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   450
               Index           =   22
               Left            =   120
               TabIndex        =   79
               Tag             =   "Order Amount:"
               Top             =   240
               Width           =   465
            End
         End
         Begin VB.TextBox txtFields 
            DataField       =   "Employee ID"
            DataSource      =   "Data1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   12
            Left            =   -4200
            TabIndex        =   30
            Top             =   960
            Width           =   2895
         End
         Begin VB.TextBox txtFields 
            DataField       =   "Employee ID"
            DataSource      =   "Data1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   11
            Left            =   -4920
            TabIndex        =   28
            Top             =   600
            Width           =   1215
         End
         Begin VB.TextBox txtFields 
            DataField       =   "Employee ID"
            DataSource      =   "Data1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   10
            Left            =   -6720
            TabIndex        =   27
            Top             =   600
            Width           =   975
         End
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   315
            Left            =   -2640
            TabIndex        =   29
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   57278467
            CurrentDate     =   38046
         End
         Begin VB.ComboBox Combo2 
            Height          =   315
            Left            =   12360
            TabIndex        =   26
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox txtFields 
            DataField       =   "Employee ID"
            DataSource      =   "Data1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   9
            Left            =   10200
            TabIndex        =   25
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox txtFields 
            DataField       =   "Employee ID"
            DataSource      =   "Data1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   315
            Index           =   8
            Left            =   960
            TabIndex        =   8
            Top             =   240
            Width           =   855
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "KMPL"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   29
            Left            =   240
            TabIndex        =   86
            Tag             =   "Order Amount:"
            Top             =   2160
            Width           =   405
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Car Type"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   27
            Left            =   120
            TabIndex        =   85
            Tag             =   "Order Amount:"
            Top             =   1200
            Width           =   660
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Category"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   26
            Left            =   240
            TabIndex        =   84
            Tag             =   "Order Amount:"
            Top             =   720
            Width           =   660
         End
         Begin VB.Label lblLabels 
            BackStyle       =   0  'Transparent
            Caption         =   "Comp. IIIrd Party"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   450
            Index           =   15
            Left            =   -5280
            TabIndex        =   68
            Tag             =   "Order Amount:"
            Top             =   960
            Width           =   675
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Expiry Date"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   14
            Left            =   -3600
            TabIndex        =   67
            Tag             =   "Order Amount:"
            Top             =   600
            Width           =   825
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Company"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   13
            Left            =   -5640
            TabIndex        =   66
            Tag             =   "Order Amount:"
            Top             =   600
            Width           =   675
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Policy No."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   12
            Left            =   -7440
            TabIndex        =   65
            Tag             =   "Order Amount:"
            Top             =   600
            Width           =   705
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Regd. No."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   11
            Left            =   9480
            TabIndex        =   64
            Tag             =   "Order Amount:"
            Top             =   240
            Width           =   705
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Mfd Year"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   10
            Left            =   11400
            TabIndex        =   63
            Tag             =   "Order Amount:"
            Top             =   240
            Width           =   675
         End
         Begin VB.Label lblLabels 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Vehicle No"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Index           =   9
            Left            =   120
            TabIndex        =   62
            Tag             =   "Order Amount:"
            Top             =   240
            Width           =   780
         End
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Employee ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   325
         Index           =   7
         Left            =   1080
         TabIndex        =   1
         Top             =   987
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Employee ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   325
         Index           =   6
         Left            =   1080
         TabIndex        =   7
         Top             =   4320
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Employee ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   325
         Index           =   5
         Left            =   1080
         TabIndex        =   6
         Top             =   3812
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Order ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   325
         Index           =   0
         Left            =   1080
         TabIndex        =   0
         Top             =   480
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Order Amount"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   """$""#,##0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   2
         EndProperty
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Index           =   1
         Left            =   1080
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   1494
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Customer ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   325
         Index           =   2
         Left            =   1080
         TabIndex        =   3
         Top             =   2291
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Employee ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   325
         Index           =   3
         Left            =   1080
         TabIndex        =   4
         Top             =   2798
         Width           =   2775
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Employee ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   325
         Index           =   4
         Left            =   1080
         TabIndex        =   5
         Top             =   3305
         Width           =   2775
      End
      Begin VB.Frame frmadd 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   775
         Left            =   600
         TabIndex        =   50
         Top             =   4800
         Width           =   5055
         Begin VB.CommandButton cmdexit 
            Cancel          =   -1  'True
            Caption         =   "E&xit"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3060
            Style           =   1  'Graphical
            TabIndex        =   21
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton Command3 
            Caption         =   "&Search"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2085
            Style           =   1  'Graphical
            TabIndex        =   20
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton cmddele 
            Caption         =   "&Delete"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2070
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   240
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.CommandButton cmdmodi 
            Caption         =   "&Modify"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1095
            Style           =   1  'Graphical
            TabIndex        =   18
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton cmdadd 
            Caption         =   "&Add"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            Style           =   1  'Graphical
            TabIndex        =   17
            Top             =   240
            Width           =   900
         End
      End
      Begin VB.Frame frmsave 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   775
         Left            =   2040
         TabIndex        =   49
         Top             =   4800
         Width           =   2535
         Begin VB.CommandButton cmdcancel 
            Caption         =   "&Cancel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   16
            Top             =   240
            Width           =   1095
         End
         Begin VB.CommandButton cmdsave 
            Caption         =   "&Save"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Label lblcode 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Code"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   210
         Left            =   3240
         TabIndex        =   58
         Tag             =   "Order ID:"
         Top             =   150
         Width           =   1035
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Site"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   8
         Left            =   600
         TabIndex        =   57
         Tag             =   "Employee ID:"
         Top             =   1080
         Width           =   270
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "EMail"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   7
         Left            =   630
         TabIndex        =   56
         Tag             =   "Employee ID:"
         Top             =   4320
         Width           =   360
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Fax No."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   5
         Left            =   435
         TabIndex        =   55
         Tag             =   "Employee ID:"
         Top             =   3771
         Width           =   555
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Code"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   4
         Left            =   2640
         TabIndex        =   54
         Tag             =   "Order ID:"
         Top             =   150
         Width           =   375
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   " Name"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   0
         Left            =   540
         TabIndex        =   44
         Tag             =   "Order ID:"
         Top             =   495
         Width           =   450
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Address"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   1
         Left            =   360
         TabIndex        =   45
         Tag             =   "Order Amount:"
         Top             =   1587
         Width           =   630
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "City"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   2
         Left            =   720
         TabIndex        =   46
         Tag             =   "Customer ID:"
         Top             =   2250
         Width           =   270
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Phone"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   3
         Left            =   540
         TabIndex        =   47
         Tag             =   "Employee ID:"
         Top             =   2805
         Width           =   450
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Mobile"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   6
         Left            =   540
         TabIndex        =   48
         Tag             =   "Employee ID:"
         Top             =   3345
         Width           =   450
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   0
      TabIndex        =   52
      Top             =   2400
      Width           =   6975
      Begin VB.CommandButton Command2 
         Caption         =   "&Cancel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5280
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Ok"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5280
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   240
         Width           =   975
      End
      Begin VB.ComboBox Combo1 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         Left            =   1680
         TabIndex        =   41
         Top             =   480
         Width           =   2895
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Vendor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   960
         TabIndex        =   53
         Top             =   480
         Width           =   540
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vendor Detail"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000011&
      Height          =   270
      Index           =   12
      Left            =   240
      TabIndex        =   59
      Top             =   0
      Width           =   1530
   End
End
Attribute VB_Name = "frmclient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strcmd As String
Dim strcode As String
Dim match2(300) As Variant
Dim str4 As Variant
Dim txt As String

Public Function test1(s As String, Index As Integer)
Static prev As Variant
txt = ""
prev = 0
ST = 0
str1 = Trim(txtFields(Index))
len1 = Len(Trim(txtFields(Index)))
txtFields(Index) = txtFields(Index) & Space(1)
For i = 1 To len1 + 1
match2(i) = Mid(txtFields(Index), i, 1)
If match2(i) = Space(1) Then
    fil = Mid(txtFields(Index), prev + 1, i - prev - 1)
    f = Mid(fil, 1, 1)
    f = UCase(f)
    l = Mid(fil, 2, Len(fil))
    txt = txt & f & l & Space(1)
    prev = i
    i = i + 1
End If
Next
End Function

Public Function test2(s As String, Index As Integer)
Static prev As Variant
txt = ""
prev = 0
ST = 0
str1 = Trim(txtFields(Index))
len1 = Len(Trim(txtFields(Index)))
txtFields(Index) = txtFields(Index) & Space(1)
For i = 1 To len1 + 1
match2(i) = Mid(txtFields(Index), i, 1)
If match2(i) = Space(1) Then
    fil = Mid(txtFields(Index), prev + 1, i - prev - 1)
    txt = txt & UCase(fil) & Space(1)
    prev = i
    i = i + 1
End If
Next
End Function

Public Function test3(s As String, Index As Integer)
str2 = ""
str3 = ""
str1 = txtFields(Index)
str2 = Mid(txtFields(Index), 1, 1)
str3 = Mid(txtFields(Index), 2, Len(txtFields(Index)))
str4 = UCase(str2) & str3
End Function


Private Sub cmbcolor_Click()
cmbmodel.Clear
strcmd = "select * from h_car_Type where category='" & cmbcolor.Text & "'"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    cmbmodel.AddItem comprec!Description & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
cmbmodel.Text = cmbmodel.List(0)
End Sub

Private Sub Combo4_Click()
'list1.ListItems.Clear
'strcmd = "select * from h_vendor where Name='" & Combo4.Text & "'"
'strcmd = strcmd & " Order By Name "
'Set comprec = con2.Execute(strcmd)
'If Not comprec.EOF Then
'    Do While Not comprec.EOF
'        Set lt = list1.ListItems.Add(, , comprec!Code)
'        lt.SubItems(1) = comprec!Veh_no
'        lt.SubItems(2) = comprec!Name
'        lt.ListSubItems(1).ForeColor = vbRed
'        comprec.MoveNext
'    Loop
'End If
End Sub

Private Sub List1_KeyUp(KeyCode As Integer, Shift As Integer)
List1_Click
End Sub

Public Sub addvendor()
list1.ListItems.Clear
strcmd = "select * from h_vendor order by  Name "  '='" & Combo4.Text & "'"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Set lt = list1.ListItems.Add(, , comprec!Code)
        lt.SubItems(1) = comprec!Veh_no
        lt.SubItems(2) = comprec!Name
        lt.ListSubItems(1).ForeColor = vbRed
        comprec.MoveNext
    Loop
End If
End Sub


Private Sub txtfields_LostFocus(Index As Integer)
If frmsetup.Option1.Value = True Then
    a = test2(txtFields(Index), Index)
    txtFields(Index) = Trim(txt)
End If

If frmsetup.Option2.Value = True Then
    a = test1(txtFields(Index), Index)
    txtFields(Index) = Trim(txt)
End If

If frmsetup.Option3.Value = True Then
    a = test3(txtFields(Index), Index)
    txtFields(Index) = Trim(str4)
End If
txtFields(Index).BackColor = vbWhite
End Sub


Private Sub Check1_Click()
'On Error GoTo errhandler
If Check1.Value = 1 Then
    addvendor
    'Combo4_Click
    list1.Visible = True
    'Combo4.Visible = True
Else
    list1.Visible = False
    'Combo4.Visible = False
    clr
End If
'errhandler:
'        If Err.Number <> 0 Then
'            MsgBox Err.Number & Err.Description, vbCritical, "Error"
'            Exit Sub
'        End If
End Sub
 
Private Sub cmdadd_Click()
On Error Resume Next
' proc to add the records to the customer tabel
'If UCase(globeuser1) = "ADMIN" Then
addcust
'addcolor
cmdcancel.Cancel = True
frmadd.Visible = False
frmsave.Visible = True
newflag = True
clr
Unlockctrl
Check1.Enabled = False
list1.Visible = False
'autoId
'ADDCAR_TYPE
'addYear
txtFields(0).SetFocus
cmbcolor_Click
txtFields(18) = 0
End Sub

Private Sub cmdCancel_Click()
On Error GoTo errhandler
cmdexit.Cancel = True
frmadd.Visible = True
frmsave.Visible = False
clr
lockctrl
Check1.Enabled = True
If Check1.Value = 1 Then
    addcust
    list1.Visible = True
    List1_Click
Else
    list1.Visible = False
    list1.ZOrder 1
End If
cmdexit.Cancel = True
newflag = False
editflag = False
errhandler:
        If Err.Number <> 0 Then
            MsgBox Err.Number & Err.Description, vbCritical, "Error"
            Exit Sub
        End If
End Sub

Private Sub cmddele_Click()
On Error Resume Next
If UCase(globeuser1) = "ADMIN" Then
Else
    MsgBox "Please Logged As Admin To Create A Company", vbInformation, "Admin"
Exit Sub
End If
addeditcombo
Command2.Cancel = True
delflag = True
frameeng.ZOrder 1
frameeng.Visible = False
Frame2.Visible = True
Frame2.ZOrder 0
If Check1.Value = 1 Then
    listtxt = list1.SelectedItem.Text
    Check1.Enabled = False
    list1.Visible = False
    Combo1.Text = listtxt
End If
Combo1.SetFocus
End Sub

Private Sub cmdexit_Click()
' proc to exit from the customer entry form
Unload Me
End Sub

Private Sub cmdmodi_Click()
On Error GoTo errhandler
'If UCase(globeuser1) = "ADMIN" Then
'Else
'    MsgBox "Please Logged As Admin To Create A Company", vbInformation, "Admin"
'Exit Sub
'End If
editflag = True
addeditcombo
frameeng.ZOrder 1
frameeng.Visible = False
Frame2.Visible = True
Frame2.ZOrder 0
If Check1.Value = 1 Then
    listtxt = list1.SelectedItem.Text
    Check1.Enabled = False
    list1.Visible = False
    Combo1.Text = listtxt
End If
Command2.Cancel = True
Combo1.SetFocus
errhandler:
        If Err.Number <> 0 Then
            MsgBox Err.Number & Err.Description, vbCritical, "Error"
            Exit Sub
        End If
End Sub

Private Sub cmdsave_Click()
'On Error GoTo errhandler
autoId
cmdexit.Cancel = True
If txtFields(0) = "" Then
    MsgBox "Vendor Name Should Not Be Blank ", vbCritical, "Error"
    txtFields(0).SetFocus
    Exit Sub
End If

If txtFields(0) = "" Then
    MsgBox "Site Should Not Be Blank ", vbCritical, "Error"
    txtFields(7).SetFocus
    Exit Sub
End If

If txtFields(2) = "" Then
    MsgBox "City Should Not Be Blank ", vbCritical, "Error"
    txtFields(2).SetFocus
    Exit Sub
End If


Set comprec = New ADODB.Recordset
If newflag = True Then
    Set comprec = New ADODB.Recordset
    comprec.Open "H_Vendor", con2, adOpenKeyset, adLockPessimistic
    comprec.AddNew
    letrec
    comprec.Update
    MsgBox "Saved Successfully", vbExclamation, "Saved"
    addvendor
'    Combo4_Click
 '   a = MsgBox("Want to Add More Vehicle To This Vendor (Y/N)?", vbYesNo, "More")
'    If a = vbYes Then
'        autoId
'        For i = 8 To 17
'            txtFields(i) = ""
'        Next
'        txtFields(8).SetFocus
'        Exit Sub
'    End If
    chkrec
    newflag = False
    comprec.Close
    clr
    lockctrl
End If

If editflag = True Then
    strcmd = "select * from H_Vendor where Code='" & Combo1.Text & "'"
    comprec.Open strcmd, con2, adOpenKeyset, adLockPessimistic
    letrec
    comprec.Update
    MsgBox "Saved Successfully", vbExclamation, "Saved"
    chkrec
    editflag = False
    comprec.Close
    clr
    lockctrl
    addvendor
    
End If
        frmadd.Visible = True
        frmsave.Visible = False
        Check1.Enabled = True
        If Check1.Value = 1 Then
            addcust
            list1.Visible = True
        Else
            addcust
            list1.Visible = False
        End If
'errhandler:
'        If Err.Number <> 0 Then
'            MsgBox Err.Number & Err.Description, vbCritical, "Error"
'            Exit Sub
'        End If
End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    Command1.SetFocus
End If
End Sub

Private Sub Command1_Click()
'On Error GoTo errhandler
If editflag = True Then
        cmdexit.Cancel = True
        strcmd = "select * from H_Vendor where code='" & Combo1.Text & "'"
        Set comprec = con2.Execute(strcmd)
        getrec
        frameeng.Visible = True
        Frame2.ZOrder 1
        frameeng.ZOrder 0
        a = MsgBox(" Want to Modify The Record ?", vbYesNo, "Modify")
        If a = vbYes Then
            'Check1.Value = 0
            oldcust = Combo1.Text
            frmadd.Visible = False
            frmsave.Visible = True
            Visible = True
            Frame2.Visible = False
            txtFields(0).SetFocus
            Unlockctrl
            ADDYEAR1
            'chkcar1
            frameeng.ZOrder 0
            Frame2.ZOrder 1
            Combo4_Click
'            txtFields(0).Locked = True
        Else
            clr
            lockctrl
            editflag = False
            frameeng.Visible = True
            Frame2.Visible = False
            Check1.Enabled = True
            If Check1.Value = 1 Then
                addcust
                getrec
                list1.Visible = True
            End If
        End If
End If
If delflag = True Then
    strcmd1 = "select * from H_Vendor where Code='" & Combo1.Text & "'"
    Set comprec = New ADODB.Recordset
    comprec.Open strcmd1, con2, adOpenKeyset, adLockPessimistic
    getrec
    Frame2.ZOrder 1
    frameeng.ZOrder 0
    frameeng.Visible = True
    a = MsgBox(" Want to Delete The Record ?", vbYesNo, "Delete")
    If a = vbYes Then
        delflag = False
        comprec.Delete
        MsgBox "Deleted  Successfully !", vbExclamation, "Deleted"
        comprec.Close
        chkrec
        clr
        lockctrl
        frameeng.ZOrder 0
        Frame2.ZOrder 1
        list1.Visible = False
        Combo4_Click
        addvendor
        frameeng.Visible = True
        Check1.Enabled = True
        If Check1.Value = 1 Then
            addcust
            list1.Visible = True
        End If
        clr
    Else
        lockctrl
        comprec.Close
        delflag = False
        frameeng.ZOrder 0
        Frame2.ZOrder 1
        frameeng.Visible = True
        Check1.Enabled = True
        If Check1.Value = 1 Then
            addcust
            getrec
            list1.Visible = True
        End If
        clr
    End If
End If

'errhandler:
 '       If Err.number <> 0 Then
  '          MsgBox Err.number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Private Sub Command1_KeyPress(KeyAscii As Integer)
If KeyPress = 13 Then
    Command1_Click
End If
End Sub

Private Sub Command2_Click()
On Error GoTo errhandler
cmdexit.Cancel = True
Frame2.ZOrder 1
frameeng.ZOrder 0
frameeng.Visible = True
editflag = False
delflag = False
Check1.Enabled = True
If Check1.Value = 1 Then
    addeditcombo
    list1.Visible = True
End If
errhandler:
        If Err.Number <> 0 Then
            MsgBox Err.Number & Err.Description, vbCritical, "Error"
            Exit Sub
        End If
End Sub

Public Sub addcolor()
cmbcolor.Clear
strcmd = "select * from H_Car_MASTER ORDER BY CATEGORY"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    cmbcolor.AddItem comprec!Category & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
cmbcolor.Text = cmbcolor.List(0)
End Sub

Private Sub Form_Activate()
'On Error GoTo errhandler
lockctrl
addvendor
'cmddele.Enabled = False
'cmdadd.Enabled = False
'cmdmodi.Enabled = False
'errhandler:
'        If Err.number <> 0 Then
'         MsgBox Err.number & Err.Description, vbCritical, "Error"
'         Exit Sub
'        End If
End Sub

' proc to show the data from recordset to txtboxes
Public Sub getrec()
lblcode.Caption = comprec!Code & ""
txtFields(0) = comprec!Name & ""
txtFields(1) = comprec!Address & ""
txtFields(2) = comprec!City & ""
txtFields(3) = comprec!Phone & ""
txtFields(4) = comprec!mobile & ""
txtFields(5) = comprec!Fax_No & ""
txtFields(6) = comprec!Email & ""
txtFields(7) = comprec!contact_Person & ""
txtFields(8) = comprec!Veh_no & ""
txtFields(9) = comprec!REGD_NO & ""
txtFields(10) = comprec!Policy_No & ""
txtFields(11) = comprec!company & ""
txtFields(12) = comprec!Third_Party & ""
Combo2.Text = comprec!Mfd_Year & ""
DTPicker1.Value = comprec!ins_exp_date & ""
txtFields(13) = comprec!Road_Permit_issue & ""
DTPicker2.Value = comprec!Road_Permit_Exp_Date & ""
txtFields(14) = comprec!State_Permit_issuE & ""
DTPicker3.Value = comprec!State_Permit_Exp_Date & ""
txtFields(15) = comprec!Fitness_Permit_issue & ""
DTPicker4.Value = comprec!Fitness_Permit_Exp_Date & ""
txtFields(16) = comprec!auth_Permit_issue & ""
DTPicker5.Value = comprec!Auth_Exp_Date & ""
txtFields(17) = comprec!Poll_Permit_issuE & ""
DTPicker6.Value = comprec!Poll_Exp_DATE
cmbmodel.Text = comprec!Car_Type & ""
cmbcolor.Text = comprec!car_category & ""
'If comprec!vdp = True Then
'chk1.Value = 1
'Else
'chk1.Value = 0
'End If
'If comprec!Petrol = True Then
'    Option1.Value = True
'End If
'
'If comprec!diesel = True Then
'    Option2.Value = True
'End If
'txtFields(18) = comprec!KMPL & ""
End Sub

' proc to send the data from txtboxes to the recordset
Public Sub letrec()
comprec!Code = lblcode.Caption & ""
comprec!Name = txtFields(0) & ""
comprec!Address = txtFields(1) & ""
comprec!City = txtFields(2) & ""
comprec!Phone = txtFields(3) & ""
comprec!mobile = txtFields(4) & ""
comprec!Fax_No = txtFields(5) & ""
comprec!Email = txtFields(6) & ""
comprec!contact_Person = txtFields(7) & ""
comprec!Veh_no = txtFields(8) & ""
comprec!REGD_NO = txtFields(9) & ""
comprec!Policy_No = txtFields(10) & ""
comprec!company = txtFields(11) & ""
comprec!Third_Party = txtFields(12) & ""
comprec!Mfd_Year = Combo2.Text & ""
comprec!ins_exp_date = DTPicker1.Value & ""
comprec!Road_Permit_issue = txtFields(13) & ""
comprec!Road_Permit_Exp_Date = DTPicker2.Value & ""
comprec!State_Permit_issuE = txtFields(14) & ""
comprec!State_Permit_Exp_Date = DTPicker3.Value & ""
comprec!Fitness_Permit_issue = txtFields(15) & ""
comprec!Fitness_Permit_Exp_Date = DTPicker4.Value & ""
comprec!auth_Permit_issue = txtFields(16) & ""
comprec!Auth_Exp_Date = DTPicker5.Value & ""
comprec!Poll_Permit_issuE = txtFields(17) & ""
comprec!Poll_Exp_DATE = DTPicker6.Value & ""

comprec!car_category = cmbcolor.Text & ""
comprec!Car_Type = cmbmodel.Text & ""

'If chk1.Value = 1 Then
'comprec!vdp = 0
'Else
'comprec!vdp = 1
'End If

'comprec!KMPL = Val(txtFields(18)) & ""
'If Option1.Value = True Then
 '   comprec!Petrol = 1
'End If

'If Option2.Value = True Then
 '   comprec!diesel = 1
'End If
End Sub

Private Sub Form_Load()
'connection2
'adjust Me
'Me.Width = Screen.Width
'Me.Left = frmmain.Picture1.Width
'Me.Height = Screen.Height - 10
'Me.Top = frmmain.tb1.Height - 200
chkrec
'addtype
addvendor
End Sub

Private Sub List1_Click()
'On Error GoTo errhandler
'If list1.ListIndex >= 0 Then
If list1.ListItems.Count <= 0 Then Exit Sub
If Check1.Value = 1 Then
    a = list1.SelectedItem.Text
    strcode = a
    strcmd = "select * from H_Vendor where code='" & a & "'"
    Set comprec = con2.Execute(strcmd)
    If Not comprec.EOF Then
    getrec
    End If
Else
    clr
    lockctrl
    Exit Sub
End If
'errhandler:
'        If Err.Number <> 0 Then
'            MsgBox Err.Number & Err.Description, vbCritical, "Error"
'            Exit Sub
'        End If
End Sub


Private Sub List1_DblClick()
List1_KeyPress 13
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    strcmd = "select * from H_Vendor where code='" & strcode & "'"
    Set comprec = con2.Execute(strcmd)
    If Not comprec.EOF Then
        cmdmodi_Click
        Combo1.Text = strcode
        Command1_Click
    End If
Else
    Exit Sub
End If
End Sub
Private Sub txtfields_GotFocus(Index As Integer)
Select Case txtFields(Index).Index
Case Index
        txtFields(Index).SelStart = 0
        txtFields(Index).SelLength = Len(txtFields(Index))
End Select
txtFields(Index).BackColor = TTT.BackColor
End Sub

Private Sub txtFields_KeyPress(Index As Integer, KeyAscii As Integer)
''On Error GoTo errhandler
''Select Case txtFields(Index).Index
''Case 3, 4, 5
''    If KeyAscii < 48 Or KeyAscii > 57 Then KeyAscii = 0
''End Select
'
'If KeyAscii = 13 Then
'Select Case txtFields(Index).Index
'
'Case 0
'        txtFields(1).SetFocus
'Case 1
'        txtFields(2).SetFocus
'
'Case 2
'        txtFields(3).SetFocus
'
'Case 3
'        txtFields(4).SetFocus
'Case 4
'        txtFields(5).SetFocus
'Case 5
'        txtFields(6).SetFocus
'Case 6
'        txtFields(7).SetFocus
'
'Case 7
'       ListView1.SetFocus
'        If newflag = True Or editflag = True Then
'            cmdsave.SetFocus
'        Else
'            Exit Sub
'        End If
'End Select
'End If
'errhandler:
 '       If Err.number <> 0 Then
  '          MsgBox Err.number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Public Sub clr()
For i = 0 To 18
    txtFields(i) = ""
Next
lblcode.Caption = ""
chk1.Value = 0
'ListView1.ListItems.Clear
End Sub

Public Sub lockctrl()
For i = 0 To 18
    txtFields(i).Locked = True
Next
chk1.Enabled = False
End Sub

Public Sub Unlockctrl()
For i = 0 To 18
    txtFields(i).Locked = False
Next
chk1.Enabled = True
End Sub

Public Sub addcust()
'List1.ListItems.Clear
'strcmd = "select * from H_Vendor ='" & Combo4.Text & "'"
'strcmd = strcmd & " Order By Name "
'Set comprec = con2.Execute(strcmd)
'If Not comprec.EOF Then
'Do While Not comprec.EOF
'    Set lt = List1.ListItems.add(, , comprec!Code)
'    lt.SubItems(1) = comprec!Veh_no & ""
'    comprec.MoveNext
'Loop
'Else
'    Exit Sub
'End If
'list1.Text = list1.list(0)
End Sub

Public Sub addeditcombo()
Combo1.Clear
strcmd = "select * from H_Vendor order by name"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Combo1.AddItem comprec!Code & ""
    comprec.MoveNext
Loop
Else
    MsgBox "No Vendor Available in The Vendor Master", vbCritical, "No User"
    Combo1.Visible = False
    Exit Sub
End If
Combo1.Text = Combo1.List(0)
End Sub

Private Sub txtFields_Validate(Index As Integer, Cancel As Boolean)
txtFields_KeyPress Index, 13
End Sub

Public Sub autoId()
Dim compauto As ADODB.Recordset
Dim strref As String
Set compauto = New ADODB.Recordset
strcmd = "select * from  H_Vendor order by code"
Set compauto = New ADODB.Recordset
compauto.Open strcmd, con2, adOpenStatic, adLockReadOnly

If Not compauto.EOF Then
    s = Left(txtFields(0), 1)
    compauto.MoveLast
    strref = compauto!Code & ""
    strref = Mid(strref, 2, Len(strref))
    strref = Val(strref) + 1
        
    If strref >= 0 And strref <= 9 Then
    lblcode.Caption = s & "000" & strref & "H"
    End If
    
    If strref >= 10 And strref <= 99 Then
    lblcode.Caption = s & "00" & strref & "H"
    End If
    
    If strref >= 100 And strref <= 999 Then
    lblcode.Caption = s & "0" & strref & "H"
    End If
    If strref >= 1000 And strref <= 9999 Then
    lblcode.Caption = s & "" & strref & "H"
    End If
    Else
        strref = "0001"
        
        lblcode.Caption = s & strref & "H"
    End If
    compauto.Close
End Sub

Public Function ValidateEmail(Email As String) As Boolean
If InStr(1, Email, "@", vbTextCompare) <> 0 Then
If InStr(1, Email, ".", vbTextCompare) <> 0 Then
Else
ValidateEmail = False
Exit Function
End If
Else
ValidateEmail = False
Exit Function
End If
ValidateEmail = True
End Function

                   
Public Sub chkrec()
strcmd = "select count(*) as tot from H_Vendor"
Set comprec = con2.Execute(strcmd)
If comprec!tot <= 0 Then
    cmdmodi.Enabled = False
    cmddele.Enabled = False
Else
    cmdmodi.Enabled = True
    cmddele.Enabled = True
End If
End Sub

'Public Sub ADDCAR_TYPE()
'strcmd = "select * from H_Car_Type order by category"
'Set comprec = con2.Execute(strcmd)
'If Not comprec.EOF Then
'    Do While Not comprec.EOF
'        Set lt = ListView1.ListItems.Add(, , comprec!code & "")
'        lt.SubItems(1) = comprec!code & ""
'        lt.SubItems(2) = comprec!Description & ""
'        comprec.MoveNext
'    Loop
'Else
'    Exit Sub
'End If
'End Sub
'
'Public Sub add_Vendor_Type()
'Set repcomprec = New ADODB.Recordset
'repcomprec.Open "H_Vendor_Type", con2, adOpenKeyset, adLockPessimistic
'For i = 1 To ListView1.ListItems.Count
'    Set lt = ListView1.ListItems(i)
'    If lt.Checked = True Then
'        repcomprec.AddNew
'        repcomprec!v_code = lblcode.Caption & ""
'        repcomprec!car_code = lt.SubItems(1) & ""
'        repcomprec.Update
'    End If
'Next
'repcomprec.Close
'End Sub

'Public Sub add_Vendor_Type1()
'Set repcomprec = New ADODB.Recordset
'strcmd = "select * from H_Vendor_Type where V_Code='" & lblcode.Caption & "'"
'repcomprec.Open strcmd, con2, adOpenKeyset, adLockPessimistic
'For i = 1 To ListView1.ListItems.Count
'    Set lt = ListView1.ListItems(i)
'    If lt.Checked = True Then
'        repcomprec.AddNew
'        repcomprec!v_code = lblcode.Caption & ""
'        repcomprec!car_code = lt.SubItems(1) & ""
'        repcomprec.Update
'    End If
'Next
'repcomprec.Close
'End Sub

'Public Sub chkcar()
'ListView1.ListItems.Clear
'strcmd = "select * from H_Vendor_Type where V_Code='" & lblcode.Caption & "'"
'Set repcomprec = con2.Execute(strcmd)
'If Not repcomprec.EOF Then
'    Do While Not repcomprec.EOF
'    strcmd = "select * from H_Car_type where code='" & repcomprec!car_code & "'"
'    Set comprec1 = con2.Execute(strcmd)
'    If Not comprec1.EOF Then
'                Set lt = ListView1.ListItems.Add()
'                lt.SubItems(1) = repcomprec!v_code & ""
'                lt.SubItems(2) = Trim(comprec1!Description) & ""
'                lt.Checked = True
'    End If
'    repcomprec.MoveNext
'    Loop
'End If
'End Sub
'
'Public Sub chkcar1()
'strcmd = "select * from H_Car_type ORDER BY CODE"
''select name from H_Vendor where name not in (select name from h_Vendor Where Name like 'M%')
'Set repcomprec = con2.Execute(strcmd)
'If Not repcomprec.EOF Then
'    Do While Not repcomprec.EOF
'                Set lt = ListView1.ListItems.Add()
'                lt.SubItems(1) = repcomprec!code & ""
'                lt.SubItems(2) = Trim(repcomprec!Description) & ""
'                repcomprec.MoveNext
'    Loop
'    End If
'End Sub

Private Sub Command3_Click()
a = InputBox("Enter Client Name :", "Find Client ")
If a = "" Then Exit Sub
strcmd = "select * from H_Vendor where code='" & Trim(a) & "'"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
    clr
    lockctrl
    getrec
Else
    MsgBox "Please Enter a Valid Client !", vbCritical, "Error"
    clr
    lockctrl
    Exit Sub
End If
End Sub

Public Sub addYear()
Combo2.Clear
For i = 1990 To 2100
    Combo2.AddItem i
Next
Combo2.Text = Combo2.List(0)
End Sub

Public Sub ADDYEAR1()
For i = 1990 To 2100
    Combo2.AddItem i
Next
End Sub

Private Sub addtype()
cmbmodel.Clear
strcmd = "select * from h_car_Type " 'where category='" & cmbcolor.Text & "'"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    cmbmodel.AddItem comprec!Description & ""
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
cmbmodel.Text = cmbmodel.List(0)
End Sub

'Public Sub addvendor()
'Combo4.Clear
'strcmd = "select Name from H_Vendor group by name order by name"
'Set comprec = con2.Execute(strcmd)
'If Not comprec.EOF Then
'Do While Not comprec.EOF
'    'Set lt = List1.ListItems.add(, , comprec!Code)
'    'lt.SubItems(1) = comprec!Name & ""
'    'lt.SubItems(2) = comprec!Veh_No & ""
'    Combo4.AddItem comprec!Name & ""
'    comprec.MoveNext
'Loop
'Else
'    Exit Sub
'End If
'Combo4.Text = Combo4.List(0)
'End Sub

