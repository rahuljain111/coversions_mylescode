VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form1 
   BackColor       =   &H00FF8080&
   Caption         =   "Invoice Cancellation"
   ClientHeight    =   6840
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6840
   ScaleWidth      =   6585
   WindowState     =   2  'Maximized
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "cancel.frx":0000
      Left            =   3240
      List            =   "cancel.frx":0002
      TabIndex        =   3
      Text            =   "Revenue"
      Top             =   2520
      WhatsThisHelpID =   3
      Width           =   1695
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   1200
      TabIndex        =   2
      Top             =   2520
      WhatsThisHelpID =   2
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   65470465
      CurrentDate     =   39661
   End
   Begin VB.ComboBox Combo5 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1320
      TabIndex        =   0
      Top             =   1200
      Width           =   1695
   End
   Begin VB.ComboBox Combo6 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   3240
      TabIndex        =   1
      Top             =   1200
      WhatsThisHelpID =   1
      Width           =   1695
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   6465
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "&Exit"
      Height          =   495
      Left            =   3360
      TabIndex        =   5
      Top             =   5640
      WhatsThisHelpID =   5
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Update Invoice"
      Height          =   495
      Left            =   1560
      TabIndex        =   4
      Top             =   5640
      WhatsThisHelpID =   4
      Width           =   1695
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Start"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   1
      Left            =   1320
      TabIndex        =   8
      Top             =   960
      Width           =   375
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "End"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Index           =   2
      Left            =   3240
      TabIndex        =   7
      Top             =   960
      Width           =   330
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
'strcmd = "select count(*) as net from Invoice_Dec_OL where invoice_no ='" & "InvCOR10305" & "'"
If Combo1.Text = "OL" Then
strcmd = "select count(*) as Net from Invoice_Dec_OL" ' where invoice_no ='" & "InvCOR10305" & "'"
strcmd = strcmd & " where invoice_no in(select distinct trx_number from invimp)"
strcmd = strcmd & " and id >='" & Combo5.Text & "'"
strcmd = strcmd & " and id <='" & Combo6.Text & "'"

Else
strcmd = "select count(*) as Net from Invoice_Dec" ' where invoice_no ='" & "InvCOR10305" & "'"
strcmd = strcmd & " where invoice_no in(select distinct trx_number from invimp)"
strcmd = strcmd & " and id >='" & Combo5.Text & "'"
strcmd = strcmd & " and id <='" & Combo6.Text & "'"
End If

Set comprec = con1.Execute(strcmd)
        If Not comprec.EOF Then
            MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
        End If
add_data
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Public Sub add_data()
Dim repcomprec As ADODB.Recordset
Dim repcomprec1 As ADODB.Recordset
c = 1
Dim comprec15 As ADODB.Recordset
'strcmd = "delete from RA_INTERFACE_LINES_ALL" 'where interface_status='" & "P" & "'"

strcmd = "delete from RA_INTERFACE_LINES_ALL where batch_source_name = 'CIPL Import2'"

Set comprec = con.Execute(strcmd)
'Set comprec = con.Execute("Delete from RA_INTERFACE_DISTRIBUTIONS_ALL")

Set repcomprec = New ADODB.Recordset
Set repcomprec1 = New ADODB.Recordset
countctr = 1
strcmd = "select * from ID_Code "
Set comprec = con1.Execute(strcmd)
ctr = comprec!Id '9909720

If Combo1.Text = "OL" Then
strcmd = "select * from Invoice_Dec_OL " 'where invoice_no ='" & "InvCOR10305" & "'"
strcmd = strcmd & " where invoice_no in(select distinct trx_number from invimp)"
strcmd = strcmd & " and id >='" & Combo5.Text & "'"
strcmd = strcmd & " and id <='" & Combo6.Text & "'"
Else
strcmd = "select * from Invoice_Dec " 'where invoice_no ='" & "InvCOR10305" & "'"
strcmd = strcmd & " where invoice_no in(select distinct trx_number from invimp)"
strcmd = strcmd & " and id >='" & Combo5.Text & "'"
strcmd = strcmd & " and id <='" & Combo6.Text & "'"

End If
 
Set comprec15 = con1.Execute(strcmd)
If Not comprec15.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "RA_INTERFACE_LINES_ALL", con, adOpenDynamic, adLockOptimistic
    Do While Not comprec15.EOF
            ctr = ctr + 1
            DoEvents
            status.Show
            DoEvents
                        
            'Code To Map Car No
            If comprec15!owned = True Then
                strcmd = "select * from new_car where vehicle_no='" & comprec15!vehicle_no & "'"
                Set newcomprec = con1.Execute(strcmd)
                If Not newcomprec.EOF Then
                    new_car = newcomprec!car_no
                Else
                    new_car = 0
                End If
            End If
            
            'Code For Vendor Car No
            If comprec15!owned = False Then
            'Added By Rahul
            If Combo1.Text = "OL" Then
            ' new_car = "Vendor Cateogory"
            'code added by Rahul
             strcmd = "select * from new_car where vehicle_no='" & CStr(comprec15!vehicle_no) & "'"
             strcmd = strcmd & " AND CITY='" & comprec15!CITY_NAME & "'"
             Set newcomprec = con1.Execute(strcmd)
               If Not newcomprec.EOF Then
                   new_car = newcomprec!car_no
            Else
                new_car = 0
                End If
                'Added By Rahul
                Else
                new_car = "Vendor Cateogory"
             End If
            End If
            strcmd = "select distinct(id) from auto_Import where RMS_Cust='" & Replace(comprec15!company, "'", "''") & "'"
            StatusBar1.Panels(1).Text = ctr
            Set comprec1 = con1.Execute(strcmd)
             If Not comprec1.EOF Then
                repcomprec1.AddNew
                
                'repcomprec1!INTERFACE_LINE_ID = ctr
                repcomprec1!LINE_TYPE = "LINE"
                repcomprec1!Description = "Standard Package"
                repcomprec1!Amount = -(comprec15!basic)
                repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                
                'Changed by Rahul
                
                'repcomprec1!TRX_DATE = "30-06-2008"
                'repcomprec1!gl_date = "30-06-2008"
                repcomprec1!TRX_DATE = DTPicker1
                repcomprec1!gl_DATE = DTPicker1
                
                'Original Code
                'repcomprec1!TRX_DATE = comprec15!VDATE
                'repcomprec1!gl_date = comprec15!VDATE
                
                repcomprec1!QUANTITY = -1
                repcomprec1!UNIT_SELLING_PRICE = comprec15!basic
                repcomprec1!UNIT_STANDARD_PRICE = comprec15!basic
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!UOM_CODE = "EA"
                repcomprec1!UOM_NAME = "Each"
                repcomprec1!ORG_ID = 101
                
                repcomprec1!CREDIT_METHOD_FOR_ACCT_RULE = "PRORATE"
                strcmd = "SELECT * FROM INVIMP WHERE TRX_NUMBER='" & comprec15!invoice_no & "'"
                strcmd = strcmd & " and description='" & "Standard Package" & "'"
                Debug.Print strcmd
                Set comprec2 = con1.Execute(strcmd)
                If Not comprec2.EOF Then
                    repcomprec1!REFERENCE_LINE_ID = comprec2!CUSTOMER_TRX_LINE_ID
                End If
                
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = 0
                repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                repcomprec1!SET_OF_BOOKS_ID = 1001
                repcomprec1!CURRENCY_CODE = "INR"
                repcomprec1!CUST_TRX_TYPE_NAME = "CIPL Import Credit"
                repcomprec1!trx_number = Left(comprec15!invoice_no, 20) & "-CMS"
                repcomprec1.Update
                ctr = ctr + 1
                
                If (comprec15!Parking_Toll + comprec15!other_taxes) > 0 Then
                repcomprec1.AddNew
                repcomprec1!LINE_TYPE = "LINE"
                repcomprec1!Description = "Parking & Toll Charges"
                repcomprec1!Amount = -(comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec1!ORIG_SYSTEM_SHIP_CUSTOMER_REF = comprec1!Id
                repcomprec1!ORIG_SYSTEM_SHIP_ADDRESS_REF = UCase(Trim(comprec15!ship_to)) & "-" & comprec1!Id  'comprec15!SHIP_TO & "-" & comprec1!ID
                repcomprec1!ORIG_SYSTEM_BILL_CUSTOMER_REF = comprec1!Id
                repcomprec1!ORIG_SYSTEM_BILL_ADDRESS_REF = UCase(Trim(comprec15!Branch_Booked)) & "-" & comprec1!Id
                repcomprec1!RECEIPT_METHOD_ID = comprec15!recipt_id
                repcomprec1!CONVERSION_TYPE = "User"
                repcomprec1!CONVERSION_RATE = 1
                
                'Changed by Rahul
                'repcomprec1!TRX_DATE = "30-06-2008"
                'repcomprec1!gl_date = "30-06-2008"
                            
                repcomprec1!TRX_DATE = DTPicker1
                repcomprec1!gl_DATE = DTPicker1
                'Original Code
                'repcomprec1!TRX_DATE = comprec15!VDATE
                'repcomprec1!gl_date = comprec15!VDATE
                'CDate("01-12-2005")
                repcomprec1!QUANTITY = -1
                repcomprec1!UNIT_SELLING_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec1!UNIT_STANDARD_PRICE = (comprec15!Parking_Toll + comprec15!other_taxes)
                repcomprec1!ATTRIBUTE1 = ctr
                repcomprec1!UOM_CODE = "EA"
                repcomprec1!UOM_NAME = "Each"
                repcomprec1!ORG_ID = 101
                repcomprec1!INTERFACE_LINE_CONTEXT = "CIPL Transaction Line"
                repcomprec1!INTERFACE_LINE_ATTRIBUTE1 = ctr
                repcomprec1!INTERFACE_LINE_ATTRIBUTE2 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE3 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE4 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE5 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE6 = 0
                repcomprec1!INTERFACE_LINE_ATTRIBUTE7 = 0
                repcomprec1!BATCH_SOURCE_NAME = "CIPL Import2"
                repcomprec1!SET_OF_BOOKS_ID = 1001
                repcomprec1!CURRENCY_CODE = "INR"
                repcomprec1!CUST_TRX_TYPE_NAME = "CIPL Import Credit"
                repcomprec1!trx_number = Left(comprec15!invoice_no, 20) & "-CMP"
                repcomprec1!CREDIT_METHOD_FOR_ACCT_RULE = "PRORATE"
                strcmd = "SELECT * FROM INVIMP WHERE TRX_NUMBER='" & comprec15!invoice_no & "'"
                strcmd = strcmd & " and description='" & "Parking & Toll Charges" & "'"
                Set comprec2 = con1.Execute(strcmd)
                Debug.Print "Parking " & strcmd
                If Not comprec2.EOF Then
                    repcomprec1!REFERENCE_LINE_ID = comprec2!CUSTOMER_TRX_LINE_ID
                End If
                p_Ctr = ctr
                repcomprec1.Update
                End If
        End If
        comprec15.MoveNext
    Loop
    
End If

strcmd = "update id_code set id='" & ctr & "'"
Set repcomprec = con1.Execute(strcmd)
comprec15.Close
'strcmd = "select count(*) as net from Invoice_Dec_OL where " 'invoice_no ='" & "022-26542INV" & "'"
'strcmd = strcmd & "  invoice_no in(select distinct trx_number from invimp)"
'strcmd = strcmd & " and id >='" & Combo5.Text & "'"
'strcmd = strcmd & " and id <='" & Combo6.Text & "'"
'Set comprec = con1.Execute(strcmd)
'If Not comprec.EOF Then
'    MsgBox "Total " & comprec!Net & " Invoices Uploaded", vbInformation, "Uploaded"
'End If

MsgBox "Done"
Unload status
Exit Sub
End Sub

Private Sub Form_Load()
Combo1.Clear
Combo1.AddItem "Rent A Car"
Combo1.AddItem "OL"

Combo1.Text = Combo1.List(0)
DTPicker1.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")

Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2
Combo5.Clear
Combo6.Clear
For i = 1 To 10000
    Combo5.AddItem i
    Combo6.AddItem i
Next
Combo5.Text = Combo5.List(0)
Combo6.Text = Combo6.List(0)
End Sub
