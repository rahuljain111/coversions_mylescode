VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Revenue 
   Caption         =   "Revenue Sharing"
   ClientHeight    =   6720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11595
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RevenueS.frx":0000
   LinkTopic       =   "Form4"
   ScaleHeight     =   6720
   ScaleWidth      =   11595
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ListView list1 
      Height          =   5895
      Left            =   6960
      TabIndex        =   9
      Top             =   540
      Visible         =   0   'False
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   10398
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483624
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Branch"
         Object.Width           =   2309
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Category"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.CheckBox Check1 
      Caption         =   "&View Revenue"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   6960
      TabIndex        =   8
      Top             =   240
      Width           =   2055
   End
   Begin VB.Frame frameeng 
      BorderStyle     =   0  'None
      Height          =   6015
      Left            =   0
      TabIndex        =   7
      Top             =   480
      Width           =   6975
      Begin VB.ComboBox Combo4 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         TabIndex        =   26
         Top             =   1560
         Width           =   3015
      End
      Begin VB.ComboBox Combo3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         TabIndex        =   24
         Top             =   2040
         Width           =   3015
      End
      Begin VB.TextBox txtFields 
         DataField       =   "Order ID"
         DataSource      =   "Data1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   325
         Index           =   0
         Left            =   1395
         TabIndex        =   22
         Top             =   2640
         Width           =   1815
      End
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         TabIndex        =   0
         Top             =   1042
         Width           =   3015
      End
      Begin VB.Frame frmadd 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   775
         Left            =   1080
         TabIndex        =   13
         Top             =   5040
         Width           =   4455
         Begin VB.CommandButton cmdexit 
            Cancel          =   -1  'True
            Caption         =   "E&xit"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   3100
            Style           =   1  'Graphical
            TabIndex        =   6
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton cmddele 
            Caption         =   "&Delete"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2146
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton cmdmodi 
            Caption         =   "&Modify"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1193
            Style           =   1  'Graphical
            TabIndex        =   4
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton cmdadd 
            Caption         =   "&Add"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   3
            Top             =   240
            Width           =   900
         End
      End
      Begin VB.Frame frmsave 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   775
         Left            =   2160
         TabIndex        =   14
         Top             =   5040
         Width           =   2055
         Begin VB.CommandButton cmdsave 
            Caption         =   "&Save"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            Style           =   1  'Graphical
            TabIndex        =   1
            Top             =   240
            Width           =   900
         End
         Begin VB.CommandButton cmdcancel 
            Caption         =   "&Cancel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   1000
            Style           =   1  'Graphical
            TabIndex        =   2
            Top             =   240
            Width           =   900
         End
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Vendor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   3
         Left            =   555
         TabIndex        =   25
         Tag             =   "Order ID:"
         Top             =   1560
         Width           =   540
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Per %"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   2
         Left            =   660
         TabIndex        =   23
         Tag             =   "Order ID:"
         Top             =   2640
         Width           =   435
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Category"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   1
         Left            =   435
         TabIndex        =   20
         Tag             =   "Order ID:"
         Top             =   2160
         Width           =   660
      End
      Begin VB.Label lblcode 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Code"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   1320
         TabIndex        =   17
         Tag             =   "Order ID:"
         Top             =   630
         Width           =   1035
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Code"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   4
         Left            =   720
         TabIndex        =   16
         Tag             =   "Order ID:"
         Top             =   630
         Width           =   375
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Branch"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Index           =   0
         Left            =   570
         TabIndex        =   15
         Tag             =   "Order ID:"
         Top             =   1095
         Width           =   525
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   0
      TabIndex        =   18
      Top             =   1920
      Width           =   6975
      Begin VB.CommandButton Command2 
         Caption         =   "&Cancel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5520
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   600
         Width           =   975
      End
      Begin VB.ComboBox Combo1 
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         Left            =   1800
         TabIndex        =   10
         Top             =   480
         Width           =   2895
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Ok"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5520
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Car Type"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   960
         TabIndex        =   19
         Top             =   480
         Width           =   660
      End
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Vendor Revenue"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000011&
      Height          =   270
      Index           =   12
      Left            =   240
      TabIndex        =   21
      Top             =   60
      Width           =   1830
   End
End
Attribute VB_Name = "Revenue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strcmd As String
Dim strcode As String
Dim match2(300) As Variant
Dim str4 As Variant
Dim txt As String

Public Function test1(s As String, Index As Integer)
Static prev As Variant
txt = ""
prev = 0
ST = 0
str1 = Trim(txtFields(Index))
len1 = Len(Trim(txtFields(Index)))
txtFields(Index) = txtFields(Index) & Space(1)
For i = 1 To len1 + 1
match2(i) = Mid(txtFields(Index), i, 1)
If match2(i) = Space(1) Then
    fil = Mid(txtFields(Index), prev + 1, i - prev - 1)
    f = Mid(fil, 1, 1)
    f = UCase(f)
    l = Mid(fil, 2, Len(fil))
    txt = txt & f & l & Space(1)
    prev = i
    i = i + 1
End If
Next
End Function

Public Function test2(s As String, Index As Integer)
Static prev As Variant
txt = ""
prev = 0
ST = 0
str1 = Trim(txtFields(Index))
len1 = Len(Trim(txtFields(Index)))
txtFields(Index) = txtFields(Index) & Space(1)
For i = 1 To len1 + 1
match2(i) = Mid(txtFields(Index), i, 1)
If match2(i) = Space(1) Then
    fil = Mid(txtFields(Index), prev + 1, i - prev - 1)
    txt = txt & UCase(fil) & Space(1)
    prev = i
    i = i + 1
End If
Next
End Function

Public Function test3(s As String, Index As Integer)
str2 = ""
str3 = ""
str1 = txtFields(Index)
str2 = Mid(txtFields(Index), 1, 1)
str3 = Mid(txtFields(Index), 2, Len(txtFields(Index)))
str4 = UCase(str2) & str3
End Function


'Private Sub txtfields_LostFocus(Index As Integer)
'If frmsetup.Option1.Value = True Then
'    a = test2(txtFields(Index), Index)
'    txtFields(Index) = Trim(txt)
'End If
'
'If frmsetup.Option2.Value = True Then
'    a = test1(txtFields(Index), Index)
'    txtFields(Index) = Trim(txt)
'End If
'
'If frmsetup.option3.Value = True Then
'    a = test3(txtFields(Index), Index)
'    txtFields(Index) = Trim(str4)
'End If
'txtFields(Index).BackColor = vbWhite
'End Sub



Private Sub Check1_Click()
'On Error GoTo errhandler
If Check1.Value = 1 Then
    addcust
    list1.Visible = True
Else
    list1.Visible = False
    clr
End If
'errhandler:
  '      If Err.Number <> 0 Then
 '           MsgBox Err.Number & Err.Description, vbCritical, "Error"
    '        Exit Sub
   '     End If
End Sub
 
Private Sub cmdadd_Click()
'On Error Resume Next
cmdcancel.Cancel = True
frmadd.Visible = False
frmsave.Visible = True
newflag = True
clr
Unlockctrl
Check1.Enabled = False
list1.Visible = False
addcat
autoId
addven
add_brn
txtFields(0).SetFocus
End Sub

Private Sub cmdCancel_Click()
'On Error GoTo errhandler
cmdexit.Cancel = True
frmadd.Visible = True
frmsave.Visible = False
clr
lockctrl
Check1.Enabled = True
If Check1.Value = 1 Then
    addcust
    list1.Visible = True
Else
    list1.Visible = False
End If
cmdexit.Cancel = True
newflag = False
editflag = False
'errhandler:
 '       If Err.Number <> 0 Then
  '          MsgBox Err.Number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Private Sub cmddele_Click()
'On Error Resume Next
If UCase(globeuser1) = "ADMIN" Then
Else
    MsgBox "Please Logged As Admin To Create A Company", vbInformation, "Admin"
Exit Sub
End If
addeditcombo
Command2.Cancel = True
delflag = True
frameeng.ZOrder 1
frameeng.Visible = False
Frame2.Visible = True
Frame2.ZOrder 0
If Check1.Value = 1 Then
    listtxt = list1.SelectedItem.Text
    Check1.Enabled = False
    list1.Visible = False
    Combo1.Text = listtxt
End If
Combo1.SetFocus
End Sub

Private Sub cmdexit_Click()
' proc to exit from the customer entry form
Unload Me
End Sub

Private Sub cmdmodi_Click()
'On Error GoTo errhandler
If UCase(globeuser1) = "ADMIN" Then
Else
    MsgBox "Please Logged As Admin To Create A Company", vbInformation, "Admin"
Exit Sub
End If
editflag = True
addeditcombo
frameeng.ZOrder 1
frameeng.Visible = False
Frame2.Visible = True
Frame2.ZOrder 0
If Check1.Value = 1 Then
    listtxt = list1.SelectedItem.Text
    Check1.Enabled = False
    list1.Visible = False
    Combo1.Text = listtxt
End If
Command2.Cancel = True
Combo1.SetFocus
'errhandler:
 '       If Err.Number <> 0 Then
  '          MsgBox Err.Number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Private Sub cmdsave_Click()
' proc to save the customer info.
'On Error GoTo errhandler
cmdexit.Cancel = True
If txtFields(0) = "" Then
    MsgBox "Category Name Should Not Be Blank ", vbCritical, "Error"
    txtFields(0).SetFocus
    Exit Sub
End If

Set comprec = New ADODB.Recordset
If newflag = True Then
    strcmd = "select * from H_Car_Type where category='" & txtFields(0) & "'"
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        MsgBox "Category " & txtFields(0) & " Already Exist", vbCritical, "Error"
        txtFields(0).SetFocus
        Exit Sub
    End If
    
    strcmd = "select * from H_Car_Type where Description='" & txtFields(0) & "'"
    strcmd = strcmd & " and category='" & Combo2.Text & "'"
    
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        MsgBox txtFields(0) & " Car " & " is Already Added To The Category  " & Combo2.Text & " !", vbCritical, "Error"
        txtFields(0).SetFocus
        Exit Sub
    End If
      
    strcmd = "select * from H_Car_Type where Description='" & txtFields(0) & "'"
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        MsgBox txtFields(0) & " Car " & " Is Already Added ", vbInformation, "Error"
        Exit Sub
    End If
      
    Set comprec = New ADODB.Recordset
    comprec.Open "Revenue", con2, adOpenKeyset, adLockPessimistic
    comprec.AddNew
    letrec
    comprec.Update
    MsgBox "Saved Successfully", vbExclamation, "Saved"
    chkrec
    newflag = False
    comprec.Close
    clr
    lockctrl
End If

If editflag = True Then
    strcmd = "select * from H_Car_Type where Code='" & Combo1.Text & "'"
    comprec.Open strcmd, con1, adOpenKeyset, adLockPessimistic
    letrec
    comprec.Update
    MsgBox "Saved Successfully", vbExclamation, "Saved"
    chkrec
    Combo2.Enabled = True
    editflag = False
    comprec.Close
    saveall
    clr
    lockctrl
End If
        frmadd.Visible = True
        frmsave.Visible = False
        Check1.Enabled = True
        If Check1.Value = 1 Then
            addcust
            list1.Visible = True
        Else
            addcust
            list1.Visible = False
        End If
'errhandler:
'        If Err.Number <> 0 Then
'            MsgBox Err.Number & Err.Description, vbCritical, "Error"
'            Exit Sub
'        End If
End Sub

Private Sub Combo1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    Command1.SetFocus
End If
End Sub

Private Sub Combo2_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
        If newflag = True Or editflag = True Then
            cmdsave.SetFocus
        Else
            Exit Sub
        End If
End If
End Sub

Private Sub Command1_Click()
'On Error GoTo errhandler
If editflag = True Then
        cmdexit.Cancel = True
        strcmd = "select * from H_Car_Type where code='" & Combo1.Text & "'"
        Set comprec = con1.Execute(strcmd)
        getrec
        frameeng.Visible = True
        Frame2.ZOrder 1
        frameeng.ZOrder 0
        a = MsgBox(" Want to Modify The Record ?", vbYesNo, "Modify")
        If a = vbYes Then
            oldcust = txtFields(0)
            frmadd.Visible = False
            frmsave.Visible = True
            Visible = True
            Frame2.Visible = False
            txtFields(0).SetFocus
            Unlockctrl
            frameeng.ZOrder 0
            Frame2.ZOrder 1
            Combo2.Enabled = False
        Else
            clr
            lockctrl
            editflag = False
            frameeng.Visible = True
            Frame2.Visible = False
            Check1.Enabled = True
            If Check1.Value = 1 Then
                addcust
                'getrec
                list1.Visible = True
            End If
        End If
End If
If delflag = True Then
    strcmd1 = "select * from H_Car_Type where Code='" & Combo1.Text & "'"
    Set comprec = New ADODB.Recordset
    comprec.Open strcmd1, con1, adOpenKeyset, adLockPessimistic
    getrec
    Frame2.ZOrder 1
    frameeng.ZOrder 0
    frameeng.Visible = True
    a = MsgBox(" Want to Delete The Record ?", vbYesNo, "Delete")
    If a = vbYes Then
        delflag = False
        strcmd1 = "Delete from H_Car_Type where Code='" & Combo1.Text & "'"
        'comprec.Delete
        Set comprec = con1.Execute(strcmd1)
        MsgBox "Deleted  Successfully !", vbExclamation, "Deleted"
        'comprec.Close
        chkrec
        clr
        lockctrl
        frameeng.ZOrder 0
        Frame2.ZOrder 1
        frameeng.Visible = True
        Check1.Enabled = True
        If Check1.Value = 1 Then
            addcust
            list1.Visible = True
        End If
        clr
    Else
        lockctrl
        comprec.Close
        delflag = False
        frameeng.ZOrder 0
        Frame2.ZOrder 1
        frameeng.Visible = True
        Check1.Enabled = True
        If Check1.Value = 1 Then
            addcust
            'getrec
            list1.Visible = True
        End If
        clr
    End If
End If

'errhandler:
 '       If Err.number <> 0 Then
  '          MsgBox Err.number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Private Sub Command1_KeyPress(KeyAscii As Integer)
If KeyPress = 13 Then
    Command1_Click
End If
End Sub

Private Sub Command2_Click()
'On Error GoTo errhandler
cmdexit.Cancel = True
Frame2.ZOrder 1
frameeng.ZOrder 0
frameeng.Visible = True
editflag = False
delflag = False
Check1.Enabled = True
If Check1.Value = 1 Then
    addeditcombo
    list1.Visible = True
End If
'errhandler:
 '       If Err.Number <> 0 Then
  '          MsgBox Err.Number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Private Sub Form_Activate()
'On Error GoTo errhandler
'addcat
lockctrl
addcust

'errhandler:
 '       If Err.Number <> 0 Then
  '       MsgBox Err.Number & Err.Description, vbCritical, "Error"
   '      Exit Sub
    '    End If
End Sub

' proc to show the data from recordset to txtboxes
Public Sub getrec()
lblcode.Caption = comprec!Code & ""
Combo2.Text = comprec!branch & ""
Combo3.Text = comprec!CATegory & ""
txtFields(0) = comprec!S_aMOUNT
Combo4.Text = comprec!Vendor & ""
End Sub

' proc to send the data from txtboxes to the recordset
Public Sub letrec()
comprec!Code = lblcode.Caption & ""
comprec!branch = Combo2.Text & ""
comprec!Vendor = Combo4.Text & ""
comprec!CATegory = Combo3.Text & ""
comprec!S_aMOUNT = txtFields(0) & ""
End Sub

Private Sub Form_Load()
'Me.Left = (Screen.Width - Me.Height) / 2
'Me.Top = (Screen.Height - Me.Height) / 2
'adjust Me
'Me.Width = Screen.Width
'Me.Left = frmmain.Picture1.Width
'Me.Height = Screen.Height - 10
'Me.Top = frmmain.tb1.Height - 200
chkrec
End Sub

Private Sub List1_Click()
'On Error GoTo errhandler
    If list1.ListItems.Count = 0 Then Exit Sub
    strcode = list1.SelectedItem.Text
    strcmd = "select * from revenue where code='" & list1.SelectedItem.Text & "'"
    Set comprec = con2.Execute(strcmd)
    If Not comprec.EOF Then
        getrec
    End If
'errhandler:
'        If Err.Number <> 0 Then
'            MsgBox Err.Number & Err.Description, vbCritical, "Error"
'            Exit Sub
'        End If
End Sub

Private Sub List1_DblClick()
List1_KeyPress 13
End Sub

Private Sub list1_KeyDown(KeyCode As Integer, Shift As Integer)
List1_Click
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    strcmd = "select * from H_Car_Type where code='" & strcode & "'"
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        cmdmodi_Click
        Combo1.Text = strcode
        Command1_Click
    End If
Else
    Exit Sub
End If
End Sub


Private Sub txtfields_GotFocus(Index As Integer)
Select Case txtFields(Index).Index
Case Index
        txtFields(Index).SelStart = 0
        txtFields(Index).SelLength = Len(txtFields(Index))
End Select
End Sub

Private Sub txtFields_KeyPress(Index As Integer, KeyAscii As Integer)
'On Error GoTo errhandler
If KeyAscii = 13 Then
Select Case txtFields(Index).Index
Case 0
'        Combo2.SetFocus
End Select
End If

'errhandler:
 '       If Err.Number <> 0 Then
  '          MsgBox Err.Number & Err.Description, vbCritical, "Error"
   '         Exit Sub
    '    End If
End Sub

Public Sub clr()
For i = 0 To 0
    txtFields(i) = ""
Next
lblcode.Caption = ""
Combo2.Text = ""
Combo3.Text = ""
End Sub

Public Sub lockctrl()
For i = 0 To 0
    txtFields(i).Locked = True
Next
Combo2.Locked = True
Combo3.Locked = True
End Sub

Public Sub Unlockctrl()
For i = 0 To 0
    txtFields(i).Locked = False
Next
Combo2.Locked = False
Combo3.Locked = False
End Sub

Public Sub addcust()
list1.ListItems.Clear
strcmd = "select * from Revenue order by Branch"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Set lt = list1.ListItems.Add(, , comprec!Code)
    lt.SubItems(1) = comprec!branch & ""
    lt.ListSubItems(1).Bold = True
    lt.SubItems(2) = comprec!CATegory & ""
    lt.ListSubItems(2).ForeColor = vbRed
    comprec.MoveNext
Loop
Else
    Exit Sub
End If
End Sub

Public Sub addeditcombo()
Combo1.Clear
strcmd = "select * from H_Car_Type order by Code"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
Do While Not comprec.EOF
    Combo1.AddItem comprec!Code & ""
    comprec.MoveNext
Loop
Else
'    MsgBox "No Car Type Available in The Car Type Master", vbCritical, "Error"
 '   Combo1.Visible = False
    Exit Sub
End If
Combo1.Text = Combo1.List(0)
End Sub

Private Sub txtFields_Validate(Index As Integer, Cancel As Boolean)
txtFields_KeyPress Index, 13
End Sub

Public Sub autoId()
Dim compauto As ADODB.Recordset
Dim strref As String
Set compauto = New ADODB.Recordset
strcmd = "select * from  REvenue order by code"
Set compauto = New ADODB.Recordset
compauto.Open strcmd, con2, adOpenStatic, adLockReadOnly

If Not compauto.EOF Then
    compauto.MoveLast
    strref = compauto!Code & ""
    strref = Mid(strref, 3, Len(strref))
    strref = Val(strref) + 1
    
    If strref >= 0 And strref <= 9 Then
    lblcode.Caption = "CT000" & strref
    End If
    
    If strref >= 10 And strref <= 99 Then
    lblcode.Caption = "CT00" & strref
    End If
    
    If strref >= 100 And strref <= 999 Then
    lblcode.Caption = "CT0" & strref
    End If
    If strref >= 1000 And strref <= 9999 Then
    lblcode.Caption = "CT" & strref
    End If
    Else
        strref = "CT0001"
        lblcode.Caption = strref
    End If
    compauto.Close
End Sub
                 
Public Sub chkrec()
'strcmd = "select count(*) as tot from H_Car_Type"
'Set comprec = con1.Execute(strcmd)
'If comprec!tot <= 0 Then
'    cmdmodi.Enabled = False
'    cmddele.Enabled = False
'Else
'    cmdmodi.Enabled = True
'    cmddele.Enabled = True
'End If
End Sub

Public Sub add_brn()
Combo2.Clear
strcmd = "select * from TAllyBranch order by BranchName"
Set comprec = con2.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo2.AddItem comprec!Branchname & ""
        comprec.MoveNext
    Loop
Else
    Exit Sub
End If
Combo2.Text = Combo2.List(0)
End Sub

Public Sub addcat()
Combo3.Clear
strcmd = "select * from H_Car_Master order by Category "
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo3.AddItem comprec!CATegory & ""
        comprec.MoveNext
    Loop
Else
    Exit Sub
End If
Combo3.Text = Combo3.List(0)
End Sub

Public Sub addven()
Combo4.Clear
strcmd = "select name from H_VEndor group by name"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo4.AddItem comprec!Name & ""
        comprec.MoveNext
    Loop
Else
    Exit Sub
End If
Combo4.Text = Combo4.List(0)
End Sub


Public Sub saveall()
Set comprecup = New ADODB.Recordset
strcmd = "update H_VEHICLE set MODEL='" & txtFields(0) & "'"
strcmd = strcmd & " where MODEL='" & oldcust & "'"
comprecup.Open strcmd, con1, adOpenKeyset, adLockPessimistic
End Sub

Public Sub saveall2()
'Set comprecup = New ADODB.Recordset
'strcmd = "update H_VEHICLE set COLOR='" & txtFields(0) & "'"
'strcmd = strcmd & " where COLOR='" & oldcust & "'"
'comprecup.Open strcmd, con1, adOpenKeyset, adLockPessimistic
End Sub


