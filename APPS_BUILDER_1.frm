VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form APPS_BUILDER 
   BackColor       =   &H00FF8080&
   Caption         =   "CIPL QUERY BUILDER"
   ClientHeight    =   11010
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   19785
   FillStyle       =   6  'Cross
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "APPS_BUILDER_1.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   11010
   ScaleWidth      =   19785
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame12 
      BackColor       =   &H00FF8080&
      Caption         =   "AP Account Wise Analysis - New"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   1455
      Left            =   4320
      TabIndex        =   84
      Top             =   8400
      Width           =   5655
      Begin VB.ComboBox Combo7 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   330
         Left            =   3360
         TabIndex        =   41
         Text            =   "Account Analysis"
         Top             =   240
         Width           =   2055
      End
      Begin VB.ComboBox Combo6 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   330
         Left            =   720
         TabIndex        =   40
         Text            =   "Account Analysis"
         Top             =   240
         Width           =   2055
      End
      Begin VB.CommandButton Command13 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Account Analysis Report"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   3960
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   720
         Width           =   1455
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   735
         Index           =   7
         Left            =   240
         TabIndex        =   85
         Top             =   600
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker14 
            Height          =   330
            Left            =   2160
            TabIndex        =   43
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin MSComCtl2.DTPicker DTPicker15 
            Height          =   330
            Left            =   600
            TabIndex        =   42
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   18
            Left            =   1905
            TabIndex        =   87
            Top             =   300
            Width           =   180
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   17
            Left            =   120
            TabIndex        =   86
            Top             =   300
            Width           =   360
         End
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   4
         Left            =   3000
         TabIndex        =   89
         Top             =   360
         Width           =   180
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   3
         Left            =   240
         TabIndex        =   88
         Top             =   360
         Width           =   360
      End
   End
   Begin VB.ComboBox Party_Name 
      Height          =   345
      Left            =   7800
      TabIndex        =   29
      Top             =   3360
      Width           =   2295
   End
   Begin VB.CommandButton Outstanding1 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Debtor Le&dger"
      Height          =   375
      Left            =   10200
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Command10"
      Enabled         =   0   'False
      Height          =   615
      Left            =   10200
      TabIndex        =   82
      Top             =   8280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Command7"
      Height          =   615
      Left            =   9720
      TabIndex        =   38
      Top             =   7560
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FF8080&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   855
      Index           =   6
      Left            =   6120
      TabIndex        =   79
      Top             =   7440
      Width           =   3495
      Begin MSComCtl2.DTPicker DTPicker12 
         Height          =   330
         Left            =   600
         TabIndex        =   36
         Top             =   255
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   58458115
         CurrentDate     =   37865
      End
      Begin MSComCtl2.DTPicker DTPicker13 
         Height          =   330
         Left            =   2160
         TabIndex        =   37
         Top             =   250
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   58458115
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   16
         Left            =   1905
         TabIndex        =   81
         Top             =   300
         Width           =   180
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   15
         Left            =   120
         TabIndex        =   80
         Top             =   300
         Width           =   360
      End
   End
   Begin VB.CommandButton Command12 
      Caption         =   "Command12"
      Height          =   615
      Left            =   4680
      TabIndex        =   39
      Top             =   7560
      Width           =   1335
   End
   Begin VB.Frame Frame10 
      BackColor       =   &H00FF8080&
      Caption         =   "Debtors Aging"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   3135
      Left            =   7560
      TabIndex        =   70
      Top             =   4200
      Width           =   4335
      Begin VB.CommandButton Command11 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&View"
         Height          =   375
         Left            =   1800
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   2640
         Width           =   1215
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   855
         Index           =   4
         Left            =   480
         TabIndex        =   71
         Top             =   360
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker8 
            Height          =   330
            Left            =   2160
            TabIndex        =   32
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   38807
         End
         Begin MSComCtl2.DTPicker DTPicker7 
            Height          =   330
            Left            =   600
            TabIndex        =   31
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   10
            Left            =   1905
            TabIndex        =   73
            Top             =   300
            Width           =   180
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   9
            Left            =   120
            TabIndex        =   72
            Top             =   300
            Width           =   360
         End
      End
      Begin VB.ComboBox Combo5 
         Height          =   345
         Left            =   1560
         TabIndex        =   34
         Top             =   1920
         Width           =   1455
      End
      Begin MSComCtl2.DTPicker DTPicker9 
         Height          =   330
         Left            =   1560
         TabIndex        =   33
         Top             =   1440
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MMM-yyyy"
         Format          =   58458115
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Cut Off Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   14
         Left            =   360
         TabIndex        =   75
         Top             =   1440
         Width           =   900
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Location"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   13
         Left            =   600
         TabIndex        =   74
         Top             =   1920
         Visible         =   0   'False
         Width           =   615
      End
   End
   Begin MSComDlg.CommonDialog cmd 
      Left            =   4080
      Top             =   7560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame9 
      BackColor       =   &H00FF8080&
      Caption         =   "AR Location Wise Outstanding"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   3495
      Left            =   7560
      TabIndex        =   63
      Top             =   600
      Width           =   4335
      Begin VB.Frame Frame11 
         BackColor       =   &H00FF8080&
         Caption         =   "Customer Ledger"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   735
         Left            =   120
         TabIndex        =   83
         Top             =   2520
         Width           =   4095
      End
      Begin VB.TextBox Text2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2880
         TabIndex        =   27
         Top             =   1560
         Width           =   1215
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1440
         TabIndex        =   26
         Top             =   1560
         Width           =   1335
      End
      Begin VB.ComboBox Combo4 
         Height          =   345
         Left            =   120
         TabIndex        =   25
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   855
         Index           =   3
         Left            =   120
         TabIndex        =   64
         Top             =   360
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker6 
            Height          =   330
            Left            =   2160
            TabIndex        =   24
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin MSComCtl2.DTPicker DTPicker5 
            Height          =   330
            Left            =   600
            TabIndex        =   23
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   5
            Left            =   120
            TabIndex        =   66
            Top             =   300
            Width           =   360
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   4
            Left            =   1905
            TabIndex        =   65
            Top             =   300
            Width           =   180
         End
      End
      Begin VB.CommandButton Command9 
         BackColor       =   &H00E0E0E0&
         Caption         =   "&View"
         Height          =   375
         Left            =   2640
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   2040
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Customer To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   8
         Left            =   2880
         TabIndex        =   69
         Top             =   1320
         Width           =   915
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Customer From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   7
         Left            =   1440
         TabIndex        =   68
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Location"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   6
         Left            =   240
         TabIndex        =   67
         Top             =   1320
         Width           =   615
      End
   End
   Begin VB.Frame Frame8 
      BackColor       =   &H00FF8080&
      Caption         =   "Future EMI Payment"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   1935
      Left            =   3480
      TabIndex        =   59
      Top             =   600
      Width           =   3975
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   735
         Index           =   2
         Left            =   240
         TabIndex        =   60
         Top             =   360
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker1 
            Height          =   330
            Left            =   600
            TabIndex        =   12
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin MSComCtl2.DTPicker DTPicker4 
            Height          =   330
            Left            =   2160
            TabIndex        =   13
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   3
            Left            =   1905
            TabIndex        =   62
            Top             =   300
            Width           =   180
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   0
            Left            =   120
            TabIndex        =   61
            Top             =   300
            Width           =   360
         End
      End
      Begin VB.CommandButton Command8 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Preview-Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1320
         Width           =   1335
      End
   End
   Begin VB.Frame Frame7 
      BackColor       =   &H00FF8080&
      Caption         =   "AR Debtors Ledger"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   1935
      Left            =   360
      TabIndex        =   58
      Top             =   7440
      Width           =   3615
      Begin VB.ComboBox Combo3 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   330
         Left            =   240
         TabIndex        =   10
         Text            =   "Account Analysis"
         Top             =   600
         Width           =   3135
      End
      Begin VB.CommandButton Command6 
         BackColor       =   &H00E0E0E0&
         Caption         =   "AR Customer Ledger"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   720
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1200
         Width           =   1815
      End
   End
   Begin VB.Frame Frame6 
      BackColor       =   &H00FF8080&
      Caption         =   "AP Account Wise Analysis - Old"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   2415
      Left            =   3480
      TabIndex        =   55
      Top             =   2640
      Width           =   3975
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   855
         Index           =   5
         Left            =   240
         TabIndex        =   76
         Top             =   840
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker10 
            Height          =   330
            Left            =   2160
            TabIndex        =   18
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin MSComCtl2.DTPicker DTPicker11 
            Height          =   330
            Left            =   600
            TabIndex        =   17
            Top             =   240
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   12
            Left            =   120
            TabIndex        =   78
            Top             =   300
            Width           =   360
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   11
            Left            =   1905
            TabIndex        =   77
            Top             =   300
            Width           =   180
         End
      End
      Begin VB.CommandButton Command5 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Account Analysis Report"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   960
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1800
         Width           =   1935
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   330
         Left            =   240
         TabIndex        =   15
         Text            =   "Account Analysis"
         Top             =   480
         Width           =   1695
      End
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   330
         Left            =   2040
         TabIndex        =   16
         Text            =   "Account Analysis"
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   0
         Left            =   360
         TabIndex        =   57
         Top             =   240
         Width           =   360
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   1
         Left            =   1920
         TabIndex        =   56
         Top             =   240
         Width           =   180
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00FF8080&
      Caption         =   "AP  Section"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   2055
      Left            =   3480
      TabIndex        =   51
      Top             =   5280
      Width           =   3975
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   855
         Index           =   1
         Left            =   240
         TabIndex        =   52
         Top             =   600
         Width           =   3495
         Begin MSComCtl2.DTPicker DTPicker2 
            Height          =   330
            Left            =   600
            TabIndex        =   20
            Top             =   255
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin MSComCtl2.DTPicker DTPicker3 
            Height          =   330
            Left            =   2160
            TabIndex        =   21
            Top             =   250
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   58458115
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   1
            Left            =   120
            TabIndex        =   54
            Top             =   300
            Width           =   360
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   2
            Left            =   1905
            TabIndex        =   53
            Top             =   300
            Width           =   180
         End
      End
      Begin VB.OptionButton Option4 
         BackColor       =   &H00FF8080&
         Caption         =   "AP Vendor Ledger"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Left            =   240
         TabIndex        =   47
         Top             =   360
         Value           =   -1  'True
         Width           =   2055
      End
      Begin VB.CommandButton Command4 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Preview-Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   1560
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   360
      Left            =   0
      TabIndex        =   48
      Top             =   10650
      Width           =   19785
      _ExtentX        =   34899
      _ExtentY        =   635
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   34369
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FF8080&
      Caption         =   "GENERAL ORACLE APPS SECTION"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   6735
      Left            =   360
      TabIndex        =   45
      Top             =   600
      Width           =   3015
      Begin VB.Frame Frame1 
         BackColor       =   &H00FF8080&
         Caption         =   "AP Section"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   1695
         Left            =   120
         TabIndex        =   50
         Top             =   4800
         Width           =   2535
         Begin VB.CommandButton Command3 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Preview-Print"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   960
            Width           =   1455
         End
         Begin VB.OptionButton Option2 
            BackColor       =   &H00FF8080&
            Caption         =   "AP VENDOR List"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Left            =   240
            TabIndex        =   8
            Top             =   480
            Width           =   1575
         End
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00FF8080&
         Caption         =   "AR Section"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   1575
         Index           =   0
         Left            =   120
         TabIndex        =   49
         Top             =   3120
         Width           =   2535
         Begin VB.CommandButton Command2 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Preview-Print"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   7
            Top             =   1080
            Width           =   1455
         End
         Begin VB.OptionButton Option3 
            BackColor       =   &H00FF8080&
            Caption         =   "AR Customer List"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Left            =   240
            TabIndex        =   6
            Top             =   600
            Width           =   1935
         End
      End
      Begin VB.Frame Frame4 
         BackColor       =   &H00FF8080&
         Caption         =   "FLEXFIELDS  Options"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   2655
         Left            =   120
         TabIndex        =   46
         Top             =   360
         Width           =   2535
         Begin VB.CommandButton Command1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Preview-Print"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   2040
            Width           =   1455
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00FF8080&
            Caption         =   "Apps Locations"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   255
            Index           =   3
            Left            =   240
            TabIndex        =   4
            Top             =   1560
            Width           =   1815
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00FF8080&
            Caption         =   "Apps Product"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   3
            Top             =   1185
            Width           =   1815
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00FF8080&
            Caption         =   "Apps Natural Acct."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   2
            Top             =   825
            Width           =   1815
         End
         Begin VB.OptionButton Option1 
            BackColor       =   &H00FF8080&
            Caption         =   "Apps Cars"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0000FFFF&
            Height          =   225
            Index           =   0
            Left            =   240
            TabIndex        =   1
            Top             =   480
            Width           =   1455
         End
      End
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   11160
      Top             =   7560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "CIPL QUERY BUILDER"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0080FFFF&
      Height          =   435
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4035
   End
End
Attribute VB_Name = "APPS_BUILDER"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim selind As Integer

Public Sub add_data()
strcmd = "select f1.flex_value_id,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
strcmd = strcmd & " and f1.flex_value_set_id='201356'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    Set repcomprec1 = New ADODB.Recordset
    con.CommandTimeout = 900
     repcomprec1.Open "APPS_CAR", con1, adOpenDynamic, adLockOptimistic
    If Not repcomprec1.EOF Then
        repcomprec1.AddNew
        repcomprec1!Id = comprec!flex_value_id
        repcomprec1!Description = comprec!Description
        DoEvents
        repcomprec1.Update
    End If
    comprec.MoveNext
    Loop
End If
End Sub


Private Sub Command1_Click()
strcmd = "truncate table APPs_CAR" ' where interface_status='" & "P" & "'"
Set comprec1 = con1.Execute(strcmd)
    
Select Case selind 'Option1(Index).Index
Case 0
    str1 = 1009932          ' Code For Cars
    CCODE = "Apps Car List "
Case 1
    str1 = 1009933          ' Code For Natuaral Accts
    CCODE = "Apps Natural Account List"
Case 2
    str1 = 1009934          ' Code For Product
    CCODE = "Apps Product List "
Case 3
    str1 = 1009931         ' Code For Locations
    CCODE = "Apps Location List "
End Select

strcmd = "select f1.flex_valuE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
strcmd = strcmd & " and f1.flex_value_set_id='" & str1 & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    Set repcomprec1 = New ADODB.Recordset
     con.CommandTimeout = 900
    
    repcomprec1.Open "APPS_CAR", con1, adOpenDynamic, adLockOptimistic
        repcomprec1.AddNew
        repcomprec1!Id = comprec!FLEX_VALUE
        repcomprec1!Description = comprec!Description
        repcomprec1!strlbl = CCODE
        DoEvents
        repcomprec1.Update
        StatusBar1.Panels(1).Text = " Loading : " & CCODE & "-----Wait---" & comprec!Description
        comprec.MoveNext
    Loop
End If
MsgBox CCODE & " Successfully Loaded in The System !"
StatusBar1.Panels(1).Text = ""

a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ADDREP
Else
    Exit Sub
End If
End Sub
Public Sub aaaa()
    Dim strSQL As String
    Dim lngRecsAff As Long
    Set cn = New ADODB.connection
    cmd.Action = 1
    a = cmd.FileName
    
    con.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & a & ";" & _
        "Extended Properties=Excel 8.0"
    
    'Import by using Jet Provider.
    strSQL = "SELECT * INTO [odbc;Driver={SQL Server};" & _
        "Server=Minhas;Database=apps;" & _
        "UID=sa;PWD=].XLImport9 " & _
        "FROM [sheet1$]"
    Debug.Print strSQL
    cn.Execute strSQL, lngRecsAff, adExecuteNoRecords
    Debug.Print "Records affected: " & lngRecsAff
    cn.Close
    Set cn = Nothing
                

End Sub


Private Sub Command10_Click()
strcmd = "select count(*) as ttt from invoice_dec where city_name='DELHI'"
'strcmd = strcmd & " and month(vdate)=12"
'strcmd = strcmd & " AND remark not like '1'"
Set comprec = con1.Execute(strcmd)
'MsgBox comprec!ttt
strcmd = "select * from invoice_dec where city_name='DELHI'"
'strcmd = strcmd & " and month(vdate)=12"
'strcmd = strcmd & " AND remark not like '1'"
'strcmd = strcmd & " and remark not like 1"
Set comprec = con1.Execute(strcmd)
Set repcomprec = New ADODB.Recordset
repcomprec.Open "Audit_Invoice", con1, adOpenDynamic, adLockOptimistic
If Not comprec.EOF Then
    Do While Not comprec.EOF
    repcomprec.AddNew
    strcmd = "select * from h_invoice where New_invoice='" & comprec!actual_Invoice & "'"
    Set comprec1 = con2.Execute(strcmd)
    If Not comprec1.EOF Then
        ds = comprec1!Duty_Slip_No
    Else
        ds = "A"
    End If
    strcmd = "select * from h_Dutyslip where Duty_slip_no='" & ds & "'"
    Set comprec2 = con2.Execute(strcmd)
    If Not comprec2.EOF Then
        rv = comprec2!reservation_no
    Else
        rv = "A"
    End If
    repcomprec!company = comprec!company & ""
    repcomprec!Duty_Slip_No = ds
    repcomprec!CITY = comprec!CITY_NAME
    repcomprec!actual_Invoice = comprec!actual_Invoice
    repcomprec!basic = comprec!basic
    repcomprec!Code = rv
    repcomprec.Update
    comprec.MoveNext
    Loop
End If
MsgBox "Done"
End Sub

Private Sub Command11_Click()
AGING
'MAIN_AGING
'add_aging
unapplied_Aging
a = MsgBox("Want to Print the aging as on " & DTPicker9.Value & " (Y/N)?", vbYesNo, "Print")
If a = vbYes Then
    prnagn
End If
End Sub

Private Sub Command12_Click()
Set comprec = con1.Execute("truncate table tax_cal")
p_start_date = Format(DTPicker12.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker13.Value, "dd-mmm-yyyy")
'add_vendor_Ledger
'STRCMD = "select TT.TAX_NAME from ap_invoice_distributions_all aa,JA_IN_TAX_CODES TT where AA.invoice_id=56985"
'STRCMD = STRCMD & " AND TT.TAX_ID=AA.ATTRIBUTE1"
strcmd = "SELECT * FROM TDS where ATTRIBUTE1 IS NOT NULL "
strcmd = strcmd & " AND gl_Date >='" & p_start_date & "'"
strcmd = strcmd & " AND gl_Date <='" & p_end_date & "'"

Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    strcmd = "select * from ap_invoice_distributions_all aa,JA_IN_TAX_CODES TT,po_vendors po"
    strcmd = strcmd & " ,ap_invoices_all api where AA.invoice_id=" & comprec!ATTRIBUTE1 & ""
    strcmd = strcmd & " AND TT.TAX_ID=AA.ATTRIBUTE1"
    strcmd = strcmd & " and aa.invoice_id=api.invoice_id"
    strcmd = strcmd & " and po.vendor_id=api.vendor_id"
    Set comprec1 = con.Execute(strcmd)
    If Not comprec1.EOF Then
        Set repcomprec = New ADODB.Recordset
        repcomprec.Open "Tax_Cal", con1, adOpenDynamic, adLockOptimistic
        repcomprec.AddNew
        repcomprec!Vendor_Name = comprec1!Vendor_Name
        repcomprec!Income_Tax = comprec!Vendor_Name
        repcomprec!tax_name = comprec1!tax_name
        repcomprec!invoice_AMOUNT = comprec1!invoice_AMOUNT
        repcomprec!INVOICE_DATE = comprec1!INVOICE_DATE
        repcomprec!invoice_num = comprec!invoice_num
        repcomprec!gl_DATE = comprec!gl_DATE
        repcomprec!tax_id = comprec1!tax_id
        repcomprec!tax_amount = comprec!invoice_AMOUNT
        Else
'        Set repcomprec = New ADODB.Recordset
'        repcomprec.Open "Tax_Cal", con1, adOpenDynamic, adLockOptimistic
'        repcomprec.AddNew
'
'        repcomprec!vendor_name = comprec1!vendor_name
'        repcomprec!Income_Tax = comprec!vendor_name
'        repcomprec!tax_name = comprec1!tax_name
'        repcomprec!invoice_amount = comprec1!invoice_amount
'        repcomprec!Invoice_date = comprec1!Invoice_date
'        repcomprec!Invoice_num = comprec!Invoice_num
'        repcomprec!tax_id = comprec1!tax_id
'        repcomprec!tax_amount = comprec!invoice_amount
    End If
    repcomprec.UpdateBatch
    comprec.MoveNext
    Loop
End If

a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ANATDS
Else
    Exit Sub
End If


End Sub

Private Sub Command13_Click()
strcmd = "truncate table ANALYSIS"
p_start_date = Format(DTPicker15.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker14.Value, "dd-mmm-yyyy")
Set comprec1 = con1.Execute(strcmd)
con1.CommandTimeout = 120000

'strcmd = "select gl.reference_5,gl.reference_4,NAME,gl.reference_1 AS party_name,gl.reference_4,gl.effective_date"
strcmd = "select gl.reference_5,gl.reference_4,NAME,gl.reference_1 AS party_name,arc.customer_name, gl.reference_4,gl.effective_date"
strcmd = strcmd & " , decode(gh.JE_SOURCE,'21','Lease',gh.JE_SOURCE) AS Source,gh.JE_CATEGORY "
'Add by BK sharma on date 24 Nov 2015 as Per Kaleem requirement
strcmd = strcmd & " , (case gh.JE_CATEGORY  when  'Purchase Invoices' then (select USSGL_TRANSACTION_CODE from ap_invoice_distributions_all apd where apd.invoice_id = gl.reference_2 "
strcmd = strcmd & " and apd.INVOICE_DISTRIBUTION_ID = (select SOURCE_ID from ap_ae_lines_all where GL_SL_LINK_ID = gl.GL_SL_LINK_ID)) Else ' ' end) DS_NUMBER "
'End of 24 Nov 2015
strcmd = strcmd & "  ,NVL(gl.ACCOUNTED_CR,0) as ACCOUNTED_CR ,NVL(gl.ACCOUNTED_dr,0) as ACCOUNTED_dr,gcc.segment1,gcc.segment3,gcc.segment2,gcc.segment4,"
'strcmd = " select gcc.segment3 ,sum(NVL(gl.ACCOUNTED_CR,0)) as Dr,sum(NVL(gl.ACCOUNTED_dr,0)) as Cr"
strcmd = strcmd & " gl.Description , gh.status, gh.period_name,gl.SUBLEDGER_DOC_SEQUENCE_VALUE,gh.default_effective_date" ' Rahul
'strcmd = strcmd & " gl.Description , gh.status, gh.period_name,gl.SUBLEDGER_DOC_SEQUENCE_VALUE,gh.default_effective_date,AP.INVOICE_DATE" ' Rahul
strcmd = strcmd & "  from gl_je_headers gh,gl_je_lines gl,gl_code_combinations gcc , ar_customers arc"
'strcmd = strcmd & "  from gl_je_headers gh,gl_je_lines gl,gl_code_combinations gcc"
'strcmd = strcmd & "  from gl_je_headers gh,gl_je_lines gl,gl_code_combinations gcc, AP_INVOICES_ALL AP"
'strcmd = strcmd & "  Where gh.JE_HEADER_ID = gl.JE_HEADER_ID"
strcmd = strcmd & "  Where gh.JE_HEADER_ID = gl.JE_HEADER_ID and gl.reference_7 = arc.customer_ID (+)"
strcmd = strcmd & "  and gh.ACTUAL_FLAG = 'A'"
strcmd = strcmd & "  and gl.code_combination_id = gcc.code_combination_id"

'Updated by Rahul---
'strcmd = strcmd & "  and gl.reference_5= AP.INVOICE_NUM(+)"
'strcmd = strcmd & "   and gl.Description=AP.Description(+)"
'strcmd = strcmd & "  AND gl.ENTERED_DR=AP.INVOICE_AMOUNT(+)"
'strcmd = strcmd & "  and gl.REFERENCE_2=AP.INVOICE_ID(+)"
'----------------------

strcmd = strcmd & "  and gcc.segment3 >='" & Combo6.Text & "'"
strcmd = strcmd & "  and gcc.segment3 <='" & Combo7.Text & "'"
strcmd = strcmd & " and gl.effective_date >='" & p_start_date & "'"
strcmd = strcmd & " AND gl.effective_date <='" & p_end_date & "'"
'--and gl.reference_2 = ai.INVOICE_ID
strcmd = strcmd & "  and gh.status='P'"
strcmd = strcmd & "  order by gh.period_name"
'Added by BK sharma to check quey
 '  Open "D:\mytextfile.txt" For Output As #1
 '  Write #1, strcmd
 '  Close #1
'End of check Query
Set comprec1 = con.Execute(strcmd)
con.CommandTimeout = 120000
If Not comprec1.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "ANALYSIS", con1, adOpenDynamic, adLockOptimistic
    
    Do While Not comprec1.EOF
    
    repcomprec1.AddNew
    repcomprec1!invoice_no = comprec1!reference_5
    repcomprec1!Name = comprec1!Name
    repcomprec1!Description = Left(comprec1!Description, 200)
    
    If comprec1!Source = "Receivables" Then
        repcomprec1!Party_Name = comprec1!CUSTOMER_NAME
    Else
        repcomprec1!Party_Name = comprec1!Party_Name
    End If
    
    'repcomprec1!Party_Name = comprec1!Party_Name
    repcomprec1!Voucher_No = comprec1!SUBLEDGER_DOC_SEQUENCE_VALUE
    repcomprec1!gl_DATE = comprec1!effective_date
   ' repcomprec1!accounting_date = comprec1!INVOICE_DATE
    repcomprec1!Source = comprec1!Source
    repcomprec1!ACCOUNTED_cr = comprec1!ACCOUNTED_cr
    repcomprec1!ACCOUNTED_dr = comprec1!ACCOUNTED_dr
    repcomprec1!period_name = comprec1!period_name
    repcomprec1!status = comprec1!status
    repcomprec1!Check_num = comprec1!reference_4 & ""
    repcomprec1!SEGMENT3 = comprec1!SEGMENT3
    repcomprec1!ACCOUNT = comprec1!segment1 & "." & comprec1!SEGMENT2 & "." & comprec1!SEGMENT3 & "." & comprec1!SEGMENT4
    'Add Booking Id
    If comprec1!DS_NUMBER <> " " Then
        repcomprec1!BookingID = comprec1!DS_NUMBER
    End If
    'End Booking ID
    StatusBar1.Panels(1).Text = "Loading---------" & comprec1!Party_Name & "--" & comprec1!period_name
    StatusBar1.Refresh
    repcomprec1.Update
    comprec1.MoveNext
    Loop
    Opn_bal 'Added By Rahul
MsgBox CCODE & " Successfully Loaded in The System !"
StatusBar1.Panels(1).Text = ""
a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ANAREP1
Else
    Exit Sub
End If
Exit Sub
Else
    MsgBox " NO TRANSATION IS THERE IN THE SYSTEM ", vbInformation, "NO TRANSACTION"
    Exit Sub
End If

End Sub

Private Sub Command2_Click()
If Option3.Value = True Then
strcmd = "select * from HZ_PARTIES WHERE STATUS ='A'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    Set repcomprec1 = New ADODB.Recordset
    
    con1.CommandTimeout = 900
    
    repcomprec1.Open "AR_CUSTOMER", con1, adOpenDynamic, adLockOptimistic
        repcomprec1.AddNew
        repcomprec1!Id = comprec!PARTY_ID
        repcomprec1!Name = comprec!Party_Name
        repcomprec1!Code = comprec!orig_system_reference
        DoEvents
        repcomprec1.Update
        StatusBar1.Panels(1).Text = " Loading : " & "CUSTOMER DETAILS----WAIT---" & comprec!Party_Name
        comprec.MoveNext
    Loop
End If
MsgBox "Customer Successfully Loaded in The System !"

StatusBar1.Panels(1).Text = ""
StatusBar1.Panels(1).Text = ""
a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ADDcust
Else
    Exit Sub
End If
End If
End Sub

Private Sub Command3_Click()
strcmd = "truncate table AP_VENDOR" ' where interface_status='" & "P" & "'"
Set comprec1 = con1.Execute(strcmd)
strcmd = "BEGIN"
strcmd = strcmd & " dbms_application_info.set_client_info(101)" & ";"
strcmd = strcmd & " END" & ";"

'JA_IN_VENDOR_TDS_INFO_HDR
Set comprec1 = con.Execute(strcmd)
If Option2.Value = True Then
strcmd = "select vendor_name,w.city,w.vendor_site_code,q.segment1 AS segment11, "
strcmd = strcmd & " vendor_type_lookup_code,vendor_name_alt, "
strcmd = strcmd & " w.VENDOR_SITE_CODE,w.CITY,w.ADDRESS_LINE1,  "
strcmd = strcmd & " w.ZIP,w.STATE,decode(w.COUNTRY,'IN','India',w.country), "
strcmd = strcmd & " w.ACCTS_PAY_CODE_COMBINATION_ID,w.PREPAY_CODE_COMBINATION_ID,   "
strcmd = strcmd & " cc.SEGMENT1,cc.segment2,cc.segment3,cc.segment4, CCC.SEGMENT1 AS SEG2_1 ,  "
strcmd = strcmd & " CCC.SEGMENT2 AS SEG2_2, CCC.SEGMENT3 AS SEG2_3, CCC.SEGMENT4 AS SEG2_4,q.end_date_active ,jv.Pan_no  "
strcmd = strcmd & " from po_vendors q,po_vendor_sites_all w,gl_code_combinations cc,gl_code_combinations ccc ,JA_IN_VENDOR_TDS_INFO_HDR jv "
strcmd = strcmd & " Where q.vendor_id = w.vendor_id  and cc.CODE_COMBINATION_ID = w.ACCTS_PAY_CODE_COMBINATION_ID "
strcmd = strcmd & " and jv.vendor_id(+)=q.vendor_id  "
strcmd = strcmd & " and ccc.CODE_COMBINATION_ID = w.PREPAY_CODE_COMBINATION_ID "

Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    Set repcomprec1 = New ADODB.Recordset
    con1.CommandTimeout = 900
    
    repcomprec1.Open "AP_VENDOR", con1, adOpenDynamic, adLockOptimistic
        repcomprec1.AddNew
        repcomprec1!Name = comprec!Vendor_Name
        repcomprec1!VENDOR_NO = comprec!segment11
        repcomprec1!vendor_Type = comprec!vendor_type_lookup_code
        repcomprec1!VENDOR_SITE = comprec!vendor_site_code
        repcomprec1!SEG1 = comprec!segment1
        repcomprec1!SEG2 = comprec!SEGMENT2
        repcomprec1!SEG3 = comprec!SEGMENT3
        repcomprec1!SEG4 = comprec!SEGMENT4
        repcomprec1!SEG2_1 = comprec!SEG2_1
        repcomprec1!SEG2_2 = comprec!SEG2_2
        repcomprec1!SEG2_3 = comprec!SEG2_3
        repcomprec1!SEG2_4 = comprec!SEG2_4
        repcomprec1!CITY_NAME = comprec!CITY
        repcomprec1!Site_Name = comprec!vendor_site_code
        repcomprec1!Site = comprec!Pan_no
        repcomprec1!InactiveDate = comprec!end_date_active
        DoEvents
        repcomprec1.Update
        StatusBar1.Panels(1).Text = " Loading : " & "VENDOR DETAILS----WAIT---" & comprec!Vendor_Name
        comprec.MoveNext
    Loop
End If
MsgBox CCODE & " Successfully Loaded in The System !"
End If
StatusBar1.Panels(1).Text = ""
StatusBar1.Panels(1).Text = ""
a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ADDVENDOR
Else
    Exit Sub
End If
End Sub

Private Sub Command4_Click()
strcmd = "truncate table ledger"
Set comprec1 = con1.Execute(strcmd)

If Option4.Value = True Then
p_start_date = Format(DTPicker2.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker3.Value, "dd-mmm-yyyy")

strcmd = "SELECT P1.VENDOR_NAME,a1.INVOICE_TYPE_LOOKUP_CODE, 0.00 debit_amount, a1.INVOICE_AMOUNT credit_amount, p1.VENDOR_TYPE_LOOKUP_CODE FROM AP_INVOICES_ALL A1, PO_VENDORS P1 WHERE P1.VENDOR_ID=A1.VENDOR_ID"
strcmd = strcmd & " and a1.INVOICE_TYPE_LOOKUP_CODE = 'STANDARD' "
strcmd = strcmd & " and Invoice_Date >='" & p_start_date & "'"
strcmd = strcmd & " AND Invoice_Date <='" & p_end_date & "'"
strcmd = strcmd & " Union"
strcmd = strcmd & " SELECT P1.VENDOR_NAME,a1.INVOICE_TYPE_LOOKUP_CODE, abs(a1.INVOICE_AMOUNT) debit_amount, 0.00 credit_amount, p1.VENDOR_TYPE_LOOKUP_CODE FROM AP_INVOICES_ALL A1, PO_VENDORS P1 WHERE P1.VENDOR_ID=A1.VENDOR_ID"
strcmd = strcmd & " and a1.INVOICE_TYPE_LOOKUP_CODE = 'CREDIT' "
strcmd = strcmd & " and Invoice_Date >='" & p_start_date & "'"
strcmd = strcmd & " AND Invoice_Date <='" & p_end_date & "'"
strcmd = strcmd & " Union"
strcmd = strcmd & " SELECT P1.VENDOR_NAME,a1.INVOICE_TYPE_LOOKUP_CODE, a1.INVOICE_AMOUNT debit_amount, 0.00 credit_amount, p1.VENDOR_TYPE_LOOKUP_CODE FROM AP_INVOICES_ALL A1, PO_VENDORS P1 WHERE P1.VENDOR_ID=A1.VENDOR_ID"
strcmd = strcmd & " and a1.INVOICE_TYPE_LOOKUP_CODE = 'PREPAYMENT'"
strcmd = strcmd & " and Invoice_Date >='" & p_start_date & "'"
strcmd = strcmd & " AND Invoice_Date <='" & p_end_date & "'"

Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Set repcomprec1 = New ADODB.Recordset

    'TimeOut
    con1.CommandTimeout = 900
    'con1.ConnectionTimeout = 900
    
    repcomprec1.Open "LEDGER", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
        repcomprec1.AddNew
        repcomprec1!Name = comprec!Vendor_Name
        repcomprec1!VTYPE = comprec!vendor_type_lookup_code
        repcomprec1!Debit_amt = comprec!debit_amount
        repcomprec1!Credit_amt = comprec!credit_amount
        repcomprec1!Inv_Type = comprec!invoice_type_lookup_code
        repcomprec1.Update
        comprec.MoveNext
    Loop
End If
MsgBox CCODE & " Successfully Loaded in The System !"

a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    LegREP
Else
    Exit Sub
End If
End If
End Sub

Private Sub Command5_Click()
strcmd = "truncate table ANALYSIS"
p_start_date = Format(DTPicker11.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker10.Value, "dd-mmm-yyyy")
Set comprec1 = con1.Execute(strcmd)
con1.CommandTimeout = 120000
'strcmd = "select gl.reference_5,gl.reference_4,NAME,gl.reference_1 AS party_name,gl.reference_4,gl.effective_date"
strcmd = "select gl.reference_5,gl.reference_4,NAME,gl.reference_1 AS party_name,arc.customer_name, gl.reference_4,gl.effective_date"
strcmd = strcmd & " , decode(gh.JE_SOURCE,'21','Lease',gh.JE_SOURCE) AS Source,"
strcmd = strcmd & "  gh.JE_CATEGORY,gl.ACCOUNTED_CR,gl.ACCOUNTED_dr,gcc.segment1,gcc.segment3,gcc.segment2,gcc.segment4,"
strcmd = strcmd & " gl.Description , gh.status, gh.period_name,gl.SUBLEDGER_DOC_SEQUENCE_VALUE,gh.default_effective_date" ' Rahul
'strcmd = strcmd & " gl.Description , gh.status, gh.period_name,gl.SUBLEDGER_DOC_SEQUENCE_VALUE,gh.default_effective_date,AP.INVOICE_DATE" ' Rahul
'strcmd = strcmd & "  from gl_je_headers gh,gl_je_lines gl,gl_code_combinations gcc"
strcmd = strcmd & "  from gl_je_headers gh, gl_je_lines gl, gl_code_combinations gcc, ar_customers arc"
'strcmd = strcmd & "  from gl_je_headers gh,gl_je_lines gl,gl_code_combinations gcc, ar_customers arc, AP_INVOICES_ALL AP"
strcmd = strcmd & "  Where gh.JE_HEADER_ID = gl.JE_HEADER_ID and gl.reference_7 = arc.customer_ID (+)"
strcmd = strcmd & "  and gh.ACTUAL_FLAG = 'A'"
strcmd = strcmd & "  and gl.code_combination_id = gcc.code_combination_id"
'Updated by Rahul---
'strcmd = strcmd & "  and gl.reference_5= AP.INVOICE_NUM(+)"
'strcmd = strcmd & "   and gl.Description=AP.Description(+)"
'strcmd = strcmd & "  AND gl.ENTERED_DR=AP.INVOICE_AMOUNT(+)"
  
'strcmd = strcmd & "  and gl.REFERENCE_2=AP.INVOICE_ID(+)"
'----------------------
strcmd = strcmd & "  and gcc.segment3 >='" & Combo1.Text & "'"
strcmd = strcmd & "  and gcc.segment3 <='" & Combo2.Text & "'"
strcmd = strcmd & " and gl.effective_date >='" & p_start_date & "'"
strcmd = strcmd & " AND gl.effective_date <='" & p_end_date & "'"
'--and gl.reference_2 = ai.INVOICE_ID
strcmd = strcmd & "  and gh.status='P'"
strcmd = strcmd & "  order by gh.period_name"
Set comprec1 = con.Execute(strcmd)
con.CommandTimeout = 120000
If Not comprec1.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "ANALYSIS", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec1.EOF
    repcomprec1.AddNew
    repcomprec1!invoice_no = comprec1!reference_5
    repcomprec1!Name = comprec1!Name
    repcomprec1!Description = Left(comprec1!Description, 200)
    
    If comprec1!Source = "Receivables" Then
        repcomprec1!Party_Name = comprec1!CUSTOMER_NAME
    Else
        repcomprec1!Party_Name = comprec1!Party_Name
    End If
    repcomprec1!Voucher_No = comprec1!SUBLEDGER_DOC_SEQUENCE_VALUE
    repcomprec1!gl_DATE = comprec1!effective_date
  '  repcomprec1!accounting_date = comprec1!INVOICE_DATE
    repcomprec1!Source = comprec1!Source
    repcomprec1!ACCOUNTED_cr = comprec1!ACCOUNTED_cr
    repcomprec1!ACCOUNTED_dr = comprec1!ACCOUNTED_dr
    repcomprec1!period_name = comprec1!period_name
    repcomprec1!status = comprec1!status
    repcomprec1!Check_num = comprec1!reference_4 & ""
    repcomprec1!SEGMENT3 = comprec1!SEGMENT3
    repcomprec1!ACCOUNT = comprec1!segment1 & "." & comprec1!SEGMENT2 & "." & comprec1!SEGMENT3 & "." & comprec1!SEGMENT4
    StatusBar1.Panels(1).Text = "Loading---------" & comprec1!Party_Name & "--" & comprec1!period_name
    StatusBar1.Refresh
    repcomprec1.Update
    comprec1.MoveNext
    Loop
MsgBox CCODE & " Successfully Loaded in The System !"
StatusBar1.Panels(1).Text = ""
a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    ANAREP
Else
    Exit Sub
End If
Exit Sub
Else
    MsgBox " NO TRANSATION IS THERE IN THE SYSTEM ", vbInformation, "NO TRANSACTION"
    Exit Sub
End If
End Sub

Private Sub Command6_Click()
addledger
End Sub

Private Sub Command7_Click()
p_start_date = Format(DTPicker12.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker13.Value, "dd-mmm-yyyy")
amt = 0
strcmd = " SELECT *   FROM AR_PAYMENT_SCHEDULES_ALL APS"
strcmd = strcmd & " Where gl_Date >='" & p_start_date & "'"
strcmd = strcmd & " and gl_Date <='" & p_end_date & "'"
strcmd = strcmd & " AND CLASS = 'INV'"
strcmd = strcmd & " AND APS.STATUS='OP'"
'strcmd = strcmd & " AND HP.PARTY_NAME='" & "AUTO DESK" & "'"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        If comprec!actual_date_closed >= "01-Mar-2008" Then
            amt = amt + comprec!AMOUNT_DUE_ORIGINAL
        End If
    comprec.MoveNext
    Loop
    MsgBox amt
End If
End Sub

'Private Sub Command7_Click()
''On Error Resume Next
'ctr = 0
'Dim comprec3 As ADODB.Recordset
'strcmd = "select * from CHANGE "
''STRCMD = STRCMD & " where vendor_name ='" & "SANJAY" & "'"
''STRCMD = STRCMD & " group by segment1,vendor_name,VENDOR_TYPE_LOOKUP_CODE,VENDOR_SITE_CODE"
'Set comprec = con1.Execute(strcmd)
'If Not comprec.EOF Then
'    Do While Not comprec.EOF
'        strcmd = "select * from po_vendors where segment1='" & comprec!SEGMENT1 & "'"
''       STRCMD = STRCMD & " and vendor_SITE_code='" & comprec!VENDOR_SITE_CODE & "'"
'        strcmd = strcmd & " and vendor_Name='" & comprec!Vendor_name & "'"
'        Set comprec1 = con.Execute(strcmd)
'        If Not comprec1.EOF Then
'        Do While Not comprec1.EOF
'            strcmd = "select * from po_vendor_sites_all where vendor_id='" & comprec1!vendor_id & "'"
'            Set comprec2 = con.Execute(strcmd)
'            If Not comprec2.EOF Then
'                strcmd = "select * from CHANGE where SEGMENT1='" & comprec!SEGMENT1 & "'"
'                strcmd = strcmd & " and OLD_CODE='" & comprec!old_CODE & "'"
'                strcmd = strcmd & " and vendor_Name='" & comprec!Vendor_name & "'"
'                Set comprec3 = con1.Execute(strcmd)
''                STRCMD = "SELECT * FROM CHANGE WHERE SEGMENT1='" & comprec!SEGMENT1 & "'"
''                STRCMD = STRCMD & " and VENDOR_SITE_CODE='" & comprec!VENDOR_SITE_CODE & "'"
''                Set comprec5 = con1.Execute(STRCMD)
''                If Not comprec5.EOF Then
''                    CNAME = comprec5!CITY
''                Else
''                    CNAME = ""
''                End If
'                If Not comprec3.EOF Then
'                strcmd1 = "Update po_vendor_SITES_ALL set state ='" & comprec3!State & "'"
'                strcmd1 = strcmd1 & " where VENDOR_SITE_CODE='" & comprec3!old_CODE & "'"
'                strcmd1 = strcmd1 & " AND vendor_id=" & comprec1!vendor_id & ""
'                Set comprec4 = con.Execute(strcmd1)
''                con.CommitTrans
'                End If
'                ctr = 1
'            End If
'            comprec1.MoveNext
'        Loop
'        End If
'    comprec.MoveNext
'    Loop
'End If
'MsgBox "Done"
'End Sub


Private Sub Command8_Click()
Set repcomprec = New ADODB.Recordset
Set comprec = con1.Execute("truncate table Lease")
p_start_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker4.Value, "dd-mmm-yyyy")
strcmd = "select  l2.lease_number,l3.payment_amount,l2.Description,l2.dist_code_combination_id,l3.interest , l3.payment_date from FA_LEASE_SCHEDULES l1,fa_leases l2,fa_amort_schedules l3"
strcmd = strcmd & "  Where l1.payment_schedule_id = l2.payment_schedule_id"
strcmd = strcmd & "  and l3.payment_schedule_id=l2.payment_schedule_id"
strcmd = strcmd & " and l3.payment_date >='" & p_start_date & "'"
strcmd = strcmd & " AND l3.payment_date <='" & p_end_date & "'"
'strcmd = strcmd & "  and l4.payment_schedule_id=l2.payment_schedule_id"
'strcmd = strcmd & "  and extract(year from l3.payment_date)='" & Combo5.Text & "'"
strcmd = strcmd & "  order by l2.lease_number,l3.payment_date"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "Lease", con1, adOpenDynamic, adLockOptimistic
    StatusBar1.Panels(1).Text = "Loading Lease No. " & comprec!Lease_number
    Do While Not comprec.EOF
    repcomprec.AddNew
    repcomprec!Lease_No = comprec!Lease_number
    repcomprec!payment_date = comprec!payment_date
    repcomprec!Interest = comprec!Interest
    repcomprec!Amount = comprec!payment_Amount
    repcomprec!Month_Name = MonthName(Month(comprec!payment_date)) & "-" & Year(comprec!payment_date)
    repcomprec!Year_Name = Year(comprec!payment_date)
    strcmd = "select * from gl_code_combinations where code_combination_id='" & comprec!dist_code_combination_id & "'"
    Set comprec1 = con.Execute(strcmd)
    If Not comprec1.EOF Then
    code_id = comprec1!SEGMENT2
    Else
    code_id = ""
    End If
    
    strcmd = "select f1.flex_valuE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
    strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id "
    strcmd = strcmd & " and f1.flex_value_set_id=201356 and flex_valuE='" & code_id & "'"
    Set comprec2 = con.Execute(strcmd)
    
    If Not comprec2.EOF Then
        car_no = comprec2!Description
    Else
        car_no = ""
    End If
    
    
    'a1 = comprec!Description
    
    'strcmd = "select f1.flex_valuE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
    'strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
    'strcmd = strcmd & "  and f1.flex_value_set_id=201356 and description ='" & a1 & "'"
    'Set comprec1 = con.Execute(strcmd)
    'If Not comprec1.EOF Then
    'CAR_ID = comprec1!flex_value_set_id
    'Else
    'CAR_ID = ""
    'End If
    repcomprec!CAR_ID = code_id
    repcomprec!car_no = car_no
    repcomprec.Update
    comprec.MoveNext
    Loop
Else
    MsgBox "No Transction is there in system", vbCritical, "Error"
    Exit Sub
End If

    MsgBox "Data Has been transfered for Reporting ", vbInformation, "Exported"
    
    a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
    If a = vbYes Then
        a = MsgBox("WANT TO PRINT THE DETAIL/CONCISE REPORT Y-Detail N-Concise--> (Y/N?)", vbYesNo, "PRINT")
        If a = vbYes Then
            leaseREP1
        Else
            leaseREP
        End If
    Else
        Exit Sub
    End If
End Sub

Public Sub Outstanding()
Set comprec = con1.Execute("truncate table outstanding")
Set comprec = con1.Execute("truncate table unapplied")

Dim newcomprec As ADODB.Recordset
p_start_date = Format(DTPicker5.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker6.Value, "dd-mmm-yyyy")
strcmd = "SELECT Distinct cc.SEGMENT1 Location,hp.PARTY_NAME,cc.SEGMENT4 product,RCT.TRX_DATE,"
strcmd = strcmd & " RCT.TRX_NUMBER INVOICE_NUM,RCT.PURCHASE_ORDER USER_NAME,aps.gl_date,"
strcmd = strcmd & " RCT.CT_REFERENCE DS_NUM,APS.AMOUNT_DUE_ORIGINAL ORIGINAL_AMOUNT,APS.AMOUNT_DUE_REMAINING BALANCE_AMOUNT"
strcmd = strcmd & " From RA_CUSTOMER_TRX_ALL RCT,HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP,HZ_CUST_SITE_USES_ALL HCSU,"
strcmd = strcmd & " AR_PAYMENT_SCHEDULES_ALL APS,"
strcmd = strcmd & " ra_cust_trx_line_gl_dist_all g,gl_code_combinations cc"
strcmd = strcmd & " Where RCT.CUSTOMER_TRX_ID = APS.CUSTOMER_TRX_ID "
strcmd = strcmd & " and rct.CUSTOMER_TRX_ID = g.CUSTOMER_TRX_ID "
strcmd = strcmd & " and cc.CODE_COMBINATION_ID = g.CODE_COMBINATION_ID"
'strcmd = strcmd & " AND APS.AMOUNT_LINE_ITEMS_REMAINING <> 0"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID AND HCSU.BILL_TO_SITE_USE_ID = RCT.BILL_TO_SITE_USE_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID"
'strcmd = strcmd & " and APS.AMOUNT_DUE_REMAINING > 0"
strcmd = strcmd & " and account_class = 'REC'"
strcmd = strcmd & " and cc.segment1 ='" & Combo4.Text & "'"
strcmd = strcmd & " and aps.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND aps.GL_DATE <='" & p_end_date & "'"
'strcmd = strcmd & "  and hp.PARTY_NAME like '" & "AUTO DESK" & "'"
'strcmd = strcmd & "  and hp.PARTY_NAME <='" & Text2 & "%'"
strcmd = strcmd & "  order by cc.segment1,hp.PARTY_NAME"
Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "Outstanding", con1, adOpenDynamic, adLockOptimistic
Set newcomprec = con.Execute(strcmd)
con.CommandTimeout = 900
If Not newcomprec.EOF Then
    status.Show
   ' status.LBL.Caption = "Loading Outstanding ..........."
    Do While Not newcomprec.EOF
    repcomprec.AddNew
    repcomprec!Location = newcomprec!Location
    repcomprec!product = newcomprec!product
    repcomprec!Party_Name = newcomprec!Party_Name
    repcomprec!TRX_DATE = newcomprec!TRX_DATE
    repcomprec!invoice_num = newcomprec!invoice_num
    repcomprec!USER_NAME = newcomprec!USER_NAME
    repcomprec!DS_NUM = newcomprec!DS_NUM
    repcomprec!ORIGINAL_AMOUNT = newcomprec!ORIGINAL_AMOUNT
    repcomprec!BALANCE_AMOUNT = newcomprec!BALANCE_AMOUNT
    
    repcomprec.Update
    newcomprec.MoveNext
    Loop
  ' newcomprec.UpdateBatch
   End If
'Unload Status
End Sub

Public Sub unapplied()
p_start_date = Format(DTPicker5.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker6.Value, "dd-mmm-yyyy")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)
strcmd = "select distinct cc.segment1 location,CUSTOMER_NAME,cc.segment4 product,receipt_number,receipt_Date,r.cash_receipt_id,r.amount, "
strcmd = strcmd & " (r.AMOUNT - r.APPLIED_AMOUNT) balance"
strcmd = strcmd & " from ar_cash_receipts_v r,ar_receivable_applications_all ars,"
strcmd = strcmd & " ar_cash_receipt_history_all arc,"
strcmd = strcmd & " gl_code_combinations cc"
strcmd = strcmd & " Where r.REVERSAL_Comments Is Null"
strcmd = strcmd & " and ars.CASH_RECEIPT_ID = r.CASH_RECEIPT_ID"
strcmd = strcmd & " and ars.CASH_RECEIPT_ID = arc.CASH_RECEIPT_ID"
strcmd = strcmd & " and r.CASH_RECEIPT_ID = arc.CASH_RECEIPT_ID"
strcmd = strcmd & " and r.RECEIPT_STATUS != 'REV'"
strcmd = strcmd & " and (r.AMOUNT - r.APPLIED_AMOUNT) != 0"
strcmd = strcmd & " and cc.CODE_COMBINATION_ID = arc.ACCOUNT_CODE_COMBINATION_ID"
strcmd = strcmd & "  and cc.segment1='" & Combo4.Text & "'"
'strcmd = strcmd & "  and customer_name ='" & "AUTO DESK" & "'"
'strcmd = strcmd & "  and customer_name <='" & Text2 & "%'"
strcmd = strcmd & " and r.receipt_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND r.receipt_DATE <='" & p_end_date & "'"
strcmd = strcmd & " AND ars.apply_DATE <='" & p_end_date & "'"
strcmd = strcmd & " order by cc.segment1,CUSTOMER_NAME,cc.segment4"
Set comprec = con.Execute(strcmd)

If Not comprec.EOF Then
Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "Unapplied", con1, adOpenDynamic, adLockOptimistic
Set newcomprec = con.Execute(strcmd)
If Not newcomprec.EOF Then
'    Status.Show
'    Status.lbl.Caption = "Loading Unapplied ..........."
    Do While Not newcomprec.EOF
    repcomprec.AddNew
    repcomprec!Location = newcomprec!Location
    repcomprec!CUSTOMER_NAME = newcomprec!CUSTOMER_NAME
    repcomprec!receipt_number = newcomprec!receipt_number
    repcomprec!receipt_date = newcomprec!receipt_date
    repcomprec!cash_receipt_id = newcomprec!cash_receipt_id
    repcomprec!Amount = newcomprec!Amount
    repcomprec!Balance = newcomprec!Balance
    'StatusBar1.Panels (1) ' = newcomprec!receipt_number
    newcomprec.MoveNext
    Loop
    repcomprec.UpdateBatch
End If
'MsgBox "No Data Is There For Outstanding !", vbCritical, "Error"
Exit Sub
End If
Unload status
End Sub

Public Sub ADDout()
        'cryctrl.WindowTitle = CCODE & " Wise Report"
        cryctrl.ReportFileName = App.Path & "\reportS\Outstanding.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub ADDappl()
        'cryctrl.WindowTitle = CCODE & " Wise Report"
        cryctrl.ReportFileName = App.Path & "\reportS\unapplied.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Private Sub Command9_Click()
Outstanding
unapplied
'MsgBox "done"
'Exit Sub

strcmd = "select customer_name,location from unapplied "
strcmd = strcmd & " where len(customer_name)>1"
strcmd = strcmd & "  and location='" & Combo4.Text & "'"
'strcmd = strcmd & "  and customer_name ='" & "AUTO DESK" & "'"
'strcmd = strcmd & "  and  customer_name <='" & Text2 & "%'"
'strcmd = strcmd & " and company  not like  '" & "COMP" & "%'"
strcmd = strcmd & "  group by customer_name,location"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
'    Status.Show
'    Status.lbl.Caption = "Updating Unapplied & Outstanding ..........."
    Do While Not comprec.EOF
    
    strcmd = "select * from outstanding where party_name='" & comprec!CUSTOMER_NAME & "'"
    strcmd = strcmd & " and location='" & comprec!Location & "'"
    'strcmd = strcmd & " and PARTY_NAME ='" & "AUTO DESK" & "'"
    Set comprec1 = con1.Execute(strcmd)
    
    strcmd = "select sum(balance)  as net from  unapplied where customer_name ='" & comprec!CUSTOMER_NAME & "'"
    strcmd = strcmd & " and location='" & comprec!Location & "'"
    'strcmd = strcmd & " and CUSTOMER_NAME ='" & "AUTO DESK" & "'"
    Set comprec2 = con1.Execute(strcmd)
    
    If Not comprec1.EOF Then
        netamt = comprec2!Net
    Else
        netamt = 0
        Set repcomprec1 = New ADODB.Recordset
    End If
    
    If IsNull(netamt) = True Then
        netamt = 0
    End If
    
    If netamt = 0 Then
    Else
        
        Set repcomprec1 = New ADODB.Recordset
        strcmd1 = "select * from outstanding where location='" & comprec!Location & "'"
        strcmd1 = strcmd1 & " and  party_name ='" & comprec!CUSTOMER_NAME & "'"
        
        con1.CommandTimeout = 900
        repcomprec1.Open strcmd1, con1, adOpenDynamic, adLockOptimistic
        If Not repcomprec1.EOF Then
                Do While Not repcomprec1.EOF
                    repcomprec1!unapplied = netamt
                    repcomprec1.MoveNext
                Loop
                repcomprec1.UpdateBatch
                repcomprec1.Close
        End If
End If
        Set comprec3 = con1.Execute(strcmd)
        StatusBar1.Panels(1).Text = "Loading ..................!"
        Me.Refresh
        comprec.MoveNext
    Loop
End If

    strcmd = "update outstanding set unapplied=0 where unapplied is null"
    Set comprec = con1.Execute(strcmd)

'-----------------------
        strcmd = "SELECT * FROM UNAPPLIED WHERE CUSTOMER_NAME NOT IN(SELECT PARTY_NAME FROM OUTSTANDING)"
        Set repcomprec1 = New ADODB.Recordset
        
        con1.CommandTimeout = 900
        
       repcomprec1.Open strcmd, con1, adOpenDynamic, adLockOptimistic
        If Not repcomprec1.EOF Then
                
'        Else
        Do While Not repcomprec1.EOF
            strcmd = "select * from  unapplied where customer_name ='" & repcomprec1!CUSTOMER_NAME & "'"
            strcmd = strcmd & " and location='" & repcomprec1!Location & "'"
            Set comprec4 = con1.Execute(strcmd)
                If Not comprec4.EOF Then
                    Set repcomprec2 = New ADODB.Recordset
                    repcomprec2.Open "OUTSTANDING", con1, adOpenDynamic, adLockOptimistic
                    repcomprec2.AddNew
                    repcomprec2!Party_Name = comprec4!CUSTOMER_NAME & ""
                    repcomprec2!Location = comprec4!Location & ""
                    
                    strcmd = "SELECT SUM(BALANCE) AS NETAMT FROM UNAPPLIED WHERE CUSTOMER_NAME='" & repcomprec1!CUSTOMER_NAME & "'"
                    strcmd = strcmd & " and location='" & repcomprec1!Location & "'"
                    'strcmd = strcmd & " and CUSTOMER_NAME ='" & "AUTO DESK" & "'"
                    Set REPcomprec3 = con1.Execute(strcmd)
                    repcomprec2!ORIGINAL_AMOUNT = 0 'REPcomprec3!NETAMT & ""
                    repcomprec2!BALANCE_AMOUNT = 0 & ""
                    repcomprec2!unapplied = REPcomprec3!netamt & ""
                    repcomprec2!REMARK = "NO OUTSTANDING BUT UNAPPLIED IS THERE !"
                    repcomprec2.Update
                End If
        repcomprec1.MoveNext
        Loop
        
    End If
'-----------------------
a = MsgBox("Data Imported ! Want to Print The Report (Y/N)?", vbYesNo, "Print")
If a = vbYes Then
ADDout
ADDappl
End If

'Unload Status
End Sub



Public Sub ADDloc()
Combo4.Clear
Combo5.Clear
strcmd = "select f1.flex_valuE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
strcmd = strcmd & " and f1.flex_value_set_id='" & 1009931 & "'"
strcmd = strcmd & " order by  f1.flex_valuE"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo5.AddItem comprec!FLEX_VALUE
        Combo4.AddItem comprec!FLEX_VALUE
        comprec.MoveNext
    Loop
End If
End Sub

Private Sub Form_Load()
Combo4.Clear
Combo5.Clear
strcmd = "select f1.flex_valuE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
strcmd = strcmd & " and f1.flex_value_set_id='" & 1009931 & "'"
strcmd = strcmd & " order by  f1.flex_valuE"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo5.AddItem comprec!FLEX_VALUE
        Combo4.AddItem comprec!FLEX_VALUE
        comprec.MoveNext
    Loop
End If



Party_Name.Clear  'ComboBox
strcmd = "select distinct Party_name from HZ_PARTIES "
strcmd = strcmd & " order by  Party_name"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Party_Name.AddItem comprec!Party_Name
         comprec.MoveNext
    Loop
End If


DTPicker2.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker3.Value = Format(Date, "dd-MMM-yyyy")
DTPicker1.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker4.Value = Format(Date, "dd-MMM-yyyy")
DTPicker9.Value = Format(Date, "dd-MMM-yyyy")
DTPicker5.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker6.Value = Format(Date, "dd-MMM-yyyy")
DTPicker7.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker8.Value = Format(Date, "dd-MMM-yyyy")
DTPicker10.Value = Format(Date, "dd-MMM-yyyy")
DTPicker11.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker12.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker13.Value = Format(Date, "dd-MMM-yyyy")
DTPicker15.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker14.Value = Format(Date, "dd-MMM-yyyy")
Combo1.Clear
Combo2.Clear
Combo6.Clear
Combo7.Clear
ADDparty
s = "1009933"
strcmd = "select f1.FLEX_VALUE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
strcmd = strcmd & " and f1.flex_value_set_id='" & s & "'"
strcmd = strcmd & " order by f1.FLEX_VALUE "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!FLEX_VALUE
        Combo2.AddItem comprec!FLEX_VALUE
        Combo6.AddItem comprec!FLEX_VALUE
        Combo7.AddItem comprec!FLEX_VALUE
        comprec.MoveNext
    Loop
End If
Combo1.Text = Combo1.List(0)
Combo2.Text = Combo2.List(0)
Combo6.Text = Combo6.List(0)
Combo7.Text = Combo7.List(0)
End Sub

Public Sub ADDVEN()
strcmd = "select DISTINCT vendor_name"
strcmd = strcmd & " from po_vendors "
'strcmd = strcmd & " Where q.vendor_id = w.vendor_id "
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo1.AddItem comprec!Vendor_Name
        comprec.MoveNext
    Loop
End If
Combo1.Text = Combo1.List(0)
End Sub

Public Sub ADDparty()
strcmd = "select DISTINCT party_name from hz_parties order by party_name"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo3.AddItem comprec!Party_Name
        comprec.MoveNext
    Loop
End If
Combo3.Text = Combo3.List(0)
End Sub
Public Sub Opn_bal()
p_start_date = Format(DTPicker15.Value, "dd-mmm-yyyy")
strcmd = "truncate table Analysis_opening"
Set comprec1 = con1.Execute(strcmd)
con1.CommandTimeout = 120000
strcmd = "select segment3 from Analysis group by segment3 order by segment3 "
Set comprec = con1.Execute(strcmd)
con1.CommandTimeout = 120000

Do While Not comprec.EOF
strcmd = " select gcc.segment3 ,sum(NVL(gl.ACCOUNTED_CR,0)) as Dr,sum(NVL(gl.ACCOUNTED_dr,0)) as Cr"
strcmd = strcmd & " ,sum(NVL(gl.ACCOUNTED_dr,0))-sum(NVL(gl.ACCOUNTED_CR,0)) as Opening"
strcmd = strcmd & " from gl_je_headers gh,gl_je_lines gl"
'strcmd = strcmd & " ,gl_code_combinations gcc, AP_INVOICES_ALL AP  Where gh.JE_HEADER_ID = gl.JE_HEADER_ID " ' Rahul
strcmd = strcmd & " ,gl_code_combinations gcc Where gh.JE_HEADER_ID = gl.JE_HEADER_ID " ' Rahul
strcmd = strcmd & " and gh.ACTUAL_FLAG = 'A'"
'strcmd = strcmd & " and gl.code_combination_id = gcc.code_combination_id  and gl.reference_5= AP.INVOICE_NUM(+) "
strcmd = strcmd & " and gl.code_combination_id = gcc.code_combination_id   "
'strcmd = strcmd & " and gl.Description=AP.Description(+)  AND gl.ENTERED_DR=AP.INVOICE_AMOUNT(+) "
'strcmd = strcmd & " and gl.reference_5= AP.INVOICE_NUM(+)"
'strcmd = strcmd & " and gl.Description=AP.Description(+)"
'strcmd = strcmd & " AND gl.ENTERED_DR=AP.INVOICE_AMOUNT(+)"
strcmd = strcmd & " and gcc.segment3 ='" & comprec!SEGMENT3 & "'"
'strcmd = strcmd & " and gcc.segment3 >='" & comprec!segment3 & "'"
'strcmd = strcmd & " and gcc.segment3 <='" & comprec!segment3 & "'"
strcmd = strcmd & " and gl.effective_date <'" & p_start_date & "'"
strcmd = strcmd & " and gh.status='P'"
strcmd = strcmd & " group by gcc.segment3"
strcmd = strcmd & " order by gcc.segment3"

Set comprec1 = con.Execute(strcmd)
con.CommandTimeout = 120000
If Not comprec1.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "Analysis_opening", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec1.EOF
    repcomprec1.AddNew
    repcomprec1!ACCOUNTED_cr = comprec1!cr
    repcomprec1!ACCOUNTED_dr = comprec1!dr
    repcomprec1!Opening = comprec1!Opening
    repcomprec1!SEGMENT3 = comprec1!SEGMENT3
    repcomprec1.Update
    comprec1.MoveNext
    Loop
End If

comprec.MoveNext
Loop

End Sub

Private Sub Option1_Click(Index As Integer)
    selind = Option1(Index).Index
End Sub

Public Sub ADDREP()
        cryctrl.WindowTitle = CCODE & " Wise Report"
        cryctrl.ReportFileName = App.Path & "\reportS\APPSLIST.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub leaseREP()
                 cryctrl.ReportFileName = App.Path & "\reportS\Concise.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Public Sub leaseREP1()
                 cryctrl.ReportFileName = App.Path & "\reportS\LEASE_DETAIL.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub




Public Sub LegREP()
        cryctrl.WindowTitle = "AR VENDOR LEDGER REPORT"
        cryctrl.ReportFileName = App.Path & "\reportS\LEdGer.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub ANAREP()
        cryctrl.WindowTitle = "ACCOUNT ANALYSIS REPORT"
        cryctrl.ReportFileName = App.Path & "\REPORTS\ANALYSIS.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                     .Action = 1
                End With
End Sub
Public Sub ANAREP1()
        cryctrl.WindowTitle = "ACCOUNT ANALYSIS REPORT"
        cryctrl.ReportFileName = App.Path & "\REPORTS\ANALYSIS2.rpt"
                     With cryctrl
                     '.Connect = con1
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                     .Action = 1
                End With
End Sub

Public Sub ANATDS()
        'cryctrl.WindowTitle = "ACCOUNT ANALYSIS REPORT"
        cryctrl.ReportFileName = App.Path & "\reportS\TDS.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub



Public Sub ADDVENDOR()
        cryctrl.WindowTitle = "AP VENDOR " & " MASTER Report"
        cryctrl.ReportFileName = App.Path & "\reportS\REPVENDOR.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Public Sub ADDcust()
        cryctrl.WindowTitle = "AR CUSTOMER " & " MASTER Report"
        cryctrl.ReportFileName = App.Path & "\reportS\ARCUST.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub

Public Sub prnagn()
        cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\Agn.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnagn11()
        cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\Outstanding_Inv_Report.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub
Public Sub prnagn112()
        cryctrl.WindowTitle = "Location Wise " & " Agieng Report"
        cryctrl.ReportFileName = App.Path & "\reportS\Customer_Ledger.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub




Public Sub addled()
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)
Set comprec = con1.Execute("truncate table all_data")

strcmd = "SELECT  povs.vendor_site_code,"
strcmd = strcmd & " povs.accts_pay_code_combination_id,"
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " apd.dist_code_combination_id ccid,"
strcmd = strcmd & " api.invoice_currency_code,"
strcmd = strcmd & " api.exchange_rate,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',abs(z.amt_val) - abs(nvl(api.discount_amount_taken,0) ) , 0)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',0,z.amt_val - nvl(api.discount_amount_taken,0) ) cr_val,"
strcmd = strcmd & " 0 acct_dr, 0 acct_cr,to_char(api.doc_sequence_value) payment_num,"
strcmd = strcmd & " to_char(apd.accounting_date, 'dd-MON-yyyy') pay_accounting_date,"
strcmd = strcmd & "  NULL check_number,pov.segment1,pov.vendor_name,pov.vendor_type_lookup_code,"
strcmd = strcmd & "  apd.po_distribution_id,api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id, api.exchange_date,api.invoice_id,"
strcmd = strcmd & " apd.accounting_date FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,po_vendors pov,po_vendor_sites_all povs,"
strcmd = strcmd & " (SELECT NVL(SUM(apd.amount),0) amt_val,API.invoice_id"
strcmd = strcmd & " FROM ap_invoices_all api,ap_invoice_distributions_all apd,po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs Where API.invoice_id = apd.invoice_id "
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id AND apd.match_status_flag='A'"
strcmd = strcmd & " AND apd.line_type_lookup_code <> 'PREPAY' GROUP BY api.invoice_id) z "
strcmd = strcmd & " Where API.invoice_id = Z.invoice_id and api.invoice_id = apd.invoice_id "
strcmd = strcmd & " AND api.VENDOR_ID=418"
strcmd = strcmd & " AND apd.rowid = ( select  rowid From ap_invoice_distributions_all "
strcmd = strcmd & " Where rownum = 1 and invoice_id=apd.invoice_id AND match_status_flag='A')"
strcmd = strcmd & "  AND api.vendor_id = pov.vendor_id "
strcmd = strcmd & "  AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & "  AND apd.match_status_flag = 'A'"
strcmd = strcmd & "  AND api.vendor_site_id = povs.vendor_site_id "
strcmd = strcmd & "  AND ((api.invoice_type_lookup_code  <> 'DEBIT')"
strcmd = strcmd & " or ( (api.invoice_type_lookup_code  = 'DEBIT') "
strcmd = strcmd & " and ( not exists "
strcmd = strcmd & " (Select '1' "
strcmd = strcmd & " from   ap_invoice_payments_all  app,"
strcmd = strcmd & " ap_checks_all apc Where App.check_id = apc.check_id "
strcmd = strcmd & " and app.invoice_id = api.invoice_id"
strcmd = strcmd & " and apc.payment_type_flag = 'R'"
strcmd = strcmd & " ))))"
strcmd = strcmd & " Union All "
strcmd = strcmd & " SELECT povs.vendor_site_code,povs.accts_pay_code_combination_id,"
strcmd = strcmd & " api.invoice_type_lookup_code,api.invoice_num,api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " app.accts_pay_code_combination_id ccid,api.payment_currency_code,"
strcmd = strcmd & " apc.exchange_rate , Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',0,0),app.amount)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',app.amount,abs(app.amount)),0)  cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.doc_sequence_value), 'P',  to_char(apc.doc_sequence_value),  to_char(apc.doc_sequence_value), 'N', NULL) payment_num,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(app.accounting_date, 'dd-MON-yyyy'), 'P', to_char(app.accounting_date, 'dd-MON-yyyy')) pay_accounting_date,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.check_number), 'P', to_char(apc.check_number)) check_number,"
strcmd = strcmd & " pov.segment1,pov.vendor_name,pov.vendor_type_lookup_code,apd.po_distribution_id,"
strcmd = strcmd & " apc.exchange_rate_type,api.org_id,api.batch_id,apc.exchange_date,"
strcmd = strcmd & " api.invoice_id,App.accounting_date FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,ap_invoice_payments_all app,"
strcmd = strcmd & " ap_checks_all apc,po_vendor_sites_all povs"
strcmd = strcmd & " Where API.invoice_id = apd.invoice_id AND apd.rowid = (select rowid from ap_invoice_distributions_all where rownum=1 and invoice_id=apd.invoice_id and match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id "
strcmd = strcmd & " AND app.invoice_id = api.invoice_id"
strcmd = strcmd & " AND app.check_id = apc.check_id "
strcmd = strcmd & " AND apc.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')"
strcmd = strcmd & " AND apd.match_status_flag='A' "
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id "
strcmd = strcmd & " AND api.VENDOR_ID=418"
strcmd = strcmd & " and app.reversal_flag='N'"
strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code, povs.accts_pay_code_combination_id, "
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & "  api.invoice_num, d. ACCOUNTING_DATE INVOICE_DATE , "
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val, 0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " 0 distribution_id,api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id,sysdate exchange_date,"
strcmd = strcmd & " api.invoice_id,D.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,ap_invoices_all api,"
strcmd = strcmd & " po_vendor_sites_all povs where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND "
strcmd = strcmd & " a.vendor_site_id = api.vendor_site_id AND "
strcmd = strcmd & " b.vendor_id = api.vendor_id AND "
strcmd = strcmd & " c.ae_header_id =  d.ae_header_id AND "
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND "
strcmd = strcmd & " c.source_table = 'AP_INVOICES' AND "
strcmd = strcmd & " C.source_id = API.invoice_id "
strcmd = strcmd & " and b.vendor_id=418"
strcmd = strcmd & " Union All "
strcmd = strcmd & " select a.vendor_site_code,a.accts_pay_code_combination_id,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,"
strcmd = strcmd & " a.accts_pay_code_combination_id ccid, "
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & " D.accounting_date from   po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b,ap_ae_lines_all c,"
strcmd = strcmd & " ap_ae_headers_all d,ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd"
strcmd = strcmd & " where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND"
strcmd = strcmd & " a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND"
strcmd = strcmd & " c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICE_DISTRIBUTIONS' AND"
strcmd = strcmd & " C.source_id = apd.invoice_distribution_id"
strcmd = strcmd & " and api.vendor_id=418"
strcmd = strcmd & " Union All "
strcmd = strcmd & " select a.vendor_site_code,povs.accts_pay_code_combination_id,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code, "
strcmd = strcmd & " api.invoice_num, "
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,"
strcmd = strcmd & " a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,"
strcmd = strcmd & " 0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "Null  payment_num,"
strcmd = strcmd & " to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " to_char(ac.check_number) check_number,"
strcmd = strcmd & " b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,0 distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id,sysdate exchange_date,api.invoice_id,D.accounting_date from   po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b, ap_ae_lines_all c,ap_ae_headers_all d,ap_invoices_all api,"
strcmd = strcmd & " ap_checks_all ac ,ap_invoice_payments_all app  ,po_vendor_sites_all povs"
strcmd = strcmd & " where  a.vendor_id = b.vendor_id AND a.vendor_id = api.vendor_id AND a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & " app.check_id = ac.check_id  AND "
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_CHECKS' AND c.source_id = ac.check_id  AND"
strcmd = strcmd & " ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED') "  '-- AND"
strcmd = strcmd & " and api.vendor_id=418"
strcmd = strcmd & " and app.reversal_flag='N' Union All"
strcmd = strcmd & " select a.vendor_site_code,povs.accts_pay_code_combination_id,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,d. ACCOUNTING_DATE INVOICE_DATE ,null description,"
strcmd = strcmd & " a.accts_pay_code_combination_id ccid,c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val, 0 cr_val,DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " to_char(ac.check_number) check_number,"
strcmd = strcmd & " b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " 0 distribution_id,api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,sysdate exchange_date,"
strcmd = strcmd & " api.invoice_id,D.accounting_date"
strcmd = strcmd & " from po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & " ap_invoices_all api,"
strcmd = strcmd & " ap_checks_all ac ,ap_invoice_payments_all app,po_vendor_sites_all povs"
strcmd = strcmd & " where  a.vendor_id = b.vendor_id AND a.vendor_id = api.vendor_id AND "
strcmd = strcmd & " a.vendor_site_id = api.vendor_site_id AND "
strcmd = strcmd & " b.vendor_id = api.vendor_id AND "
strcmd = strcmd & " c.ae_header_id =  d.ae_header_id AND api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & " app.check_id = ac.check_id  AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND "
strcmd = strcmd & " c.source_table = 'AP_INVOICE_PAYMENTS' AND "
strcmd = strcmd & " c.source_id = app.invoice_payment_id  AND"
strcmd = strcmd & " ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED') " ' --AND"
strcmd = strcmd & " and api.vendor_id=418"
strcmd = strcmd & " and app.reversal_flag='N'"
strcmd = strcmd & " Union All"
strcmd = strcmd & " select  DISTINCT a.vendor_site_code,povs.accts_pay_code_combination_id,NULL invoice_type_lookup_code,"
strcmd = strcmd & " null invoice_num,SYSDATE INVOICE_DATE ,null description,a.accts_pay_code_combination_id ccid, "
strcmd = strcmd & " null invoice_currency_code, 0  rate , 0 dr_val, 0  cr_val,"
strcmd = strcmd & " 0 acct_dr, 0 acct_cr,null payment_num, null  pay_accounting_date,"
strcmd = strcmd & " null check_number, b.segment1 ,b.vendor_name,"
strcmd = strcmd & " null, 0 distribution_id,null,a.org_id, 0 batch_id,"
strcmd = strcmd & " sysdate exchange_date, 0 invoice_id,sysdate accounting_date"
strcmd = strcmd & " from po_vendor_sites_all a,po_vendors b,"
strcmd = strcmd & " ap_invoices_all c, "
strcmd = strcmd & " AP_INVOICE_DISTRIBUTIONS_ALL D ,po_vendor_sites_all povs"
strcmd = strcmd & " where a.vendor_id = b.vendor_id and "
strcmd = strcmd & " a.vendor_id = c.vendor_id"
strcmd = strcmd & " AND A.VENDOR_SITE_ID = C.VENDOR_SITE_ID"
strcmd = strcmd & " AND C.INVOICE_ID = D.INVOICE_ID "
strcmd = strcmd & " and c.vendor_id=418"
strcmd = strcmd & "  ORDER BY 24,25 "

Set comprec2 = con.Execute(strcmd)
If Not comprec2.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "All_Data", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec2.EOF
    repcomprec.AddNew
    repcomprec!vendor_site_code = comprec2!vendor_site_code
    repcomprec!invoice_type_lookup_code = comprec2!invoice_type_lookup_code
    repcomprec!invoice_num = comprec2!invoice_num
    repcomprec!INVOICE_DATE = comprec2!INVOICE_DATE
    repcomprec!Description = comprec2!Description
    repcomprec!dr_val = comprec2!dr_val
    repcomprec!cr_val = comprec2!cr_val
    repcomprec!segment1 = comprec2!segment1
    repcomprec!Vendor_Name = comprec2!Vendor_Name
    repcomprec!vendor_type_lookup_code = comprec2!vendor_type_lookup_code
    repcomprec!INVOICE_ID = comprec2!INVOICE_ID
    repcomprec!accounting_date = comprec2!accounting_date
    comprec2.MoveNext
'    MsgBox "Yes"
    Loop
    repcomprec.UpdateBatch
Else
    MsgBox "No Transaction Is There is the Database !", vbInformation, "No Data"
    Exit Sub
End If
MsgBox "Done"
End Sub


Private Sub Period_Change()

End Sub

Public Sub AGING()
Set comprec = con1.Execute("truncate table Aging")
Set comprec = con1.Execute("truncate table unapplied_amount")
Dim newcomprec As ADODB.Recordset

p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")

strcmd = "SELECT Distinct cc.SEGMENT1 Location,hp.PARTY_NAME,cc.SEGMENT4 product,RCT.TRX_DATE,"
strcmd = strcmd & " RCT.TRX_NUMBER INVOICE_NUM,RCT.PURCHASE_ORDER USER_NAME,"
strcmd = strcmd & " RCT.CT_REFERENCE DS_NUM,APS.AMOUNT_DUE_ORIGINAL ORIGINAL_AMOUNT,APS.AMOUNT_DUE_REMAINING BALANCE_AMOUNT"
strcmd = strcmd & " From RA_CUSTOMER_TRX_ALL RCT,HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP,HZ_CUST_SITE_USES_ALL HCSU,"
strcmd = strcmd & " AR_PAYMENT_SCHEDULES_ALL APS,"
strcmd = strcmd & " ra_cust_trx_line_gl_dist_all g,gl_code_combinations cc"
strcmd = strcmd & " Where RCT.CUSTOMER_TRX_ID = APS.CUSTOMER_TRX_ID "
strcmd = strcmd & " and rct.CUSTOMER_TRX_ID = g.CUSTOMER_TRX_ID "
strcmd = strcmd & " and cc.CODE_COMBINATION_ID = g.CODE_COMBINATION_ID"
strcmd = strcmd & " AND APS.AMOUNT_LINE_ITEMS_REMAINING <> 0"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID AND HCSU.BILL_TO_SITE_USE_ID = RCT.BILL_TO_SITE_USE_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " and APS.AMOUNT_DUE_REMAINING <> 0"
'strcmd = strcmd & " and account_class = 'REC'"
'strcmd = strcmd & " and hp.PARTY_NAME like  '%" & "ANAND TRANSPORT" & "'%"
strcmd = strcmd & " and cc.segment1 ='" & Combo5.Text & "'"
'strcmd = strcmd & " and RCT.TRX_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND RCT.TRX_DATE <='" & p_end_date & "'"
strcmd = strcmd & "  order by cc.segment1,hp.PARTY_NAME"

Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "aging", con1, adOpenDynamic, adLockOptimistic
Set newcomprec = con.Execute(strcmd)
If Not newcomprec.EOF Then
    
        status.LBL.Caption = "Loading Aging ..........."
    Do While Not newcomprec.EOF
        status.Show
       a1 = newcomprec!TRX_DATE
       a2 = DTPicker9.Value
       total_days = DateDiff("d", a1, a2)
       'MsgBox total_days
        repcomprec.AddNew
       
       If total_days >= 0 And total_days <= 30 Then
            netamt = newcomprec!BALANCE_AMOUNT
            repcomprec!up_30 = netamt
            repcomprec!up_60 = 0
            repcomprec!up_90 = 0
            repcomprec!up_180 = 0
            repcomprec!MORE_180 = 0
       End If
       
       If total_days > 30 And total_days <= 60 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = netamt
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
       
       If total_days > 60 And total_days <= 90 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = netamt
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 91 And total_days <= 180 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = netamt
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 181 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = netamt
       End If
      
       repcomprec!Location = newcomprec!Location
       repcomprec!customer = newcomprec!Party_Name
       repcomprec!TRX_DATE = newcomprec!TRX_DATE
       repcomprec!CUT_DATE = DTPicker9.Value
       repcomprec!trx_number = newcomprec!invoice_num
       'repcomprec!user_name = newcomprec!user_name
       'repcomprec!DS_NUM = newcomprec!DS_NUM
       'repcomprec!ORIGINAL_AMOUNT = newcomprec!ORIGINAL_AMOUNT
       'repcomprec!BALANCE_AMOUNT = newcomprec!BALANCE_AMOUNT
        repcomprec.UpdateBatch
        StatusBar1.Panels(1).Text = "Loading ..................!" & newcomprec!invoice_num
        newcomprec.MoveNext
    Loop
   
Else
'Exit Sub
End If
Unload status
MsgBox "Data for Outstanding !", vbCritical, "Error"
End Sub


Public Sub unapplied_Aging()
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)
DT = Format(DTPicker9.Value, "dd-MMM-yyyy")
strcmd = "select customer_name, cc.segment1,"
strcmd = strcmd & " sum((r.AMOUNT - r.APPLIED_AMOUNT)) balance"
strcmd = strcmd & " from ar_cash_receipts_v r,"
strcmd = strcmd & " ar_cash_receipt_history_all arc,"
strcmd = strcmd & " gl_code_combinations cc"
strcmd = strcmd & " Where r.REVERSAL_Comments Is Null"
strcmd = strcmd & " and r.CASH_RECEIPT_ID = arc.CASH_RECEIPT_ID"
strcmd = strcmd & " and r.RECEIPT_STATUS != 'REV'"
strcmd = strcmd & " and (r.AMOUNT - r.APPLIED_AMOUNT) != 0"
strcmd = strcmd & " and cc.CODE_COMBINATION_ID = arc.ACCOUNT_CODE_COMBINATION_ID"
'strcmd = strcmd & " and customer_name ='" & "%ERNST AND YOUNG" & "'"
strcmd = strcmd & " and cc.segment1='" & Combo5.Text & "'"
strcmd = strcmd & " and r.receipt_DATE <='" & DT & "'"
'strcmd = strcmd & " and r.receipt_DATE >='" & p_start_date & "'"
'strcmd = strcmd & " AND r.receipt_DATE <='" & p_end_date & "'"
strcmd = strcmd & " group  by CUSTOMER_NAME,cc.segment1"
strcmd = strcmd & " order by cc.segment1,CUSTOMER_NAME"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    status.Show
    status.LBL.Caption = "Loading Unapplied ..........."
'    repcomprec.Open "Aging", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
'    strcmd = "select sum((r.AMOUNT - r.APPLIED_AMOUNT)) balance"
'    strcmd = strcmd & " from ar_cash_receipts_v r,"
'    strcmd = strcmd & " ar_cash_receipt_history_all arc,"
'    strcmd = strcmd & " gl_code_combinations cc"
'    strcmd = strcmd & " Where r.REVERSAL_Comments Is Null"
'    strcmd = strcmd & " and r.CASH_RECEIPT_ID = arc.CASH_RECEIPT_ID"
'    strcmd = strcmd & " and r.RECEIPT_STATUS != 'REV'"
'    strcmd = strcmd & " and (r.AMOUNT - r.APPLIED_AMOUNT) != 0"
'    strcmd = strcmd & " and cc.CODE_COMBINATION_ID = arc.ACCOUNT_CODE_COMBINATION_ID"
'    strcmd = strcmd & " and cc.segment1='" & Combo5.Text & "'"
'    strcmd = strcmd & " and r.receipt_DATE >='" & p_start_date & "'"
'    strcmd = strcmd & " AND r.receipt_DATE <='" & p_end_date & "'"
'    strcmd = strcmd & " AND customer_name='" & comprec!customer_name & "'"
    
    'Set comprec2 = con.Execute(strcmd)
    'If Not comprec2.EOF Then
     '   strcmd = "select * from aging where customer='" & comprec!customer_name & "'"
      '  Set comprec3 = New ADODB.Recordset
       ' comprec3.Open strcmd, con1, adOpenDynamic, adLockOptimistic
        'If Not comprec3.EOF Then
        Set comprec4 = New ADODB.Recordset
        comprec4.Open "Unapplied_Amount", con1, adOpenDynamic, adLockOptimistic
        comprec4.AddNew
        comprec4!Amount = comprec!Balance
        comprec4!customer = comprec!CUSTOMER_NAME
        comprec4!Location = comprec!segment1
        comprec4.Update
        'Do While Not comprec3.EOF
        'strcmd = "select * from aging where customer='" & comprec!customer_name & "'"
        'comprec4.Open strcmd, con1, adOpenDynamic, adLockOptimistic
        'comprec3!unapplied_balance = comprec2!balance
        'comprec3.MoveNext
        'Loop
        'comprec3.UpdateBatch
'    End If
'    StatusBar1.Panels (1) ' = newcomprec!receipt_number
    comprec.MoveNext
    Loop
End If

strcmd = "SELECT * From Unapplied_Amount WHERE customer NOT IN (SELECT customer FROM aging)"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Set comprec1 = New ADODB.Recordset
        con1.CommandTimeout = 900
        comprec1.Open "Aging", con1, adOpenDynamic, adLockOptimistic
        comprec1.AddNew
        comprec1!customer = comprec!customer
        comprec1!Location = comprec!Location
       ' comprec1!up_30 = 0
       ' comprec1!up_60 = 0
       ' comprec1!up_90 = 0
       ' comprec1!up_180 = 0
       ' comprec1!MORE_180 = 0
        comprec1.Update
        comprec.MoveNext
    Loop
End If
MsgBox "Data For Unapplied Completed !", vbCritical, "Error"
1 Unload status
End Sub

Public Sub addledger()
Set comprec = con1.Execute("truncate table AR_LEDGER")
strcmd = "Select a.customer_id ,B.Bill_To_Site_Use_Id,a.customer_number,a.customer_name ,d.gl_date,B.CUSTOMER_TRX_ID,"
strcmd = strcmd & " b.trx_number,TO_CHAR(b.trx_date, 'DD-MM-YYYY') trx_date,NULL receipt_number,"
strcmd = strcmd & " NULL receipt_date ,SUBSTR(b.comments,1,50) remarks,d.code_combination_id account_id,"
strcmd = strcmd & "b.invoice_currency_code currency_code,b.exchange_rate ,"
strcmd = strcmd & "d.amount amount,(d.amount * NVL(b.exchange_rate,-1)) amount_other_currency,"
strcmd = strcmd & "f.type,b.customer_trx_id,d.customer_trx_line_id  ,b.rowid From    ra_customers    a,   ra_customer_trx_ALL  B, RA_CUST_TRX_LINE_GL_DIST_ALL D,"
strcmd = strcmd & "GL_CODE_COMBINATIONS E,RA_CUST_TRX_TYPES_ALL F,   ar_payment_schedules_all G"
strcmd = strcmd & " Where   b.bill_to_customer_id = a.customer_id AND   b.complete_flag = 'Y'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " AND  d.customer_trx_id = b.customer_trx_id"
strcmd = strcmd & " AND d.account_class = 'REC' AND e.code_combination_id = d.code_combination_id"
strcmd = strcmd & " AND f.cust_trx_type_id =b.cust_trx_type_id AND  f.type in ('INV','CM','DM','DEP')"
strcmd = strcmd & " AND d.latest_rec_flag = 'Y' AND g.customer_trx_id= b.customer_trx_id"
strcmd = strcmd & " and g.payment_schedule_id in (select min(payment_schedule_id)"
strcmd = strcmd & " from   ar_payment_schedules_all where  customer_trx_id = g.customer_trx_id)"
strcmd = strcmd & " Union"
strcmd = strcmd & " Select a.customer_id,aa1.Bill_To_Site_Use_Id ,a.customer_number, a.customer_name ,e.gl_date,0 ,"
strcmd = strcmd & " NULL,NULL,b.receipt_number,TO_CHAR(b.receipt_date,  'DD-MM-YYYY'),"
strcmd = strcmd & " NULL ,d.cash_ccid account_id ,b.currency_code,b.exchange_rate,b.amount  amount,"
strcmd = strcmd & " (b.amount * NVL(b.exchange_rate,1)) amount_other_currency,'REC' type ,0 ,0 ,"
strcmd = strcmd & " b.rowid From    ra_customers A, ra_customer_trx_all  aa1,ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,"
strcmd = strcmd & " ar_receipt_method_accounts_all D,"
strcmd = strcmd & " ar_cash_receipt_history_all  E,ar_payment_schedules_all    F,  ("
strcmd = strcmd & " SELECT     MIN(cash_receipt_history_id) cash_receipt_history_id,"
strcmd = strcmd & " cash_receipt_id     FROM    ar_cash_receipt_history_all"
strcmd = strcmd & " GROUP BY cash_receipt_id) G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " and aa1.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_id = g.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_history_id   = g.cash_receipt_history_id"
strcmd = strcmd & " AND  f.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " Union "
strcmd = strcmd & " select a.customer_id ,aa2.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name,e.gl_date ,0 ,"
strcmd = strcmd & " NULL,NULL,b.receipt_number,TO_CHAR(b.receipt_date,  'DD-MM-YYYY')  ,"
strcmd = strcmd & " NULL,d.cash_ccid account_id  ,b.currency_code  ,b.exchange_rate  ,g.amount_applied  amount                                         ,"
strcmd = strcmd & " (g.amount_applied * NVL(b.exchange_rate,1)) amount_other_currency,"
strcmd = strcmd & " 'W/O' type ,0 ,0 ,b.rowid From  ra_customers A, ar_cash_receipts_all B,"
strcmd = strcmd & " gl_code_combinations C, ar_receipt_method_accounts_all D,   ar_cash_receipt_history_all E,"
strcmd = strcmd & " ar_payment_schedules_all F,ra_customer_trx_all aa2,ar_receivable_applications_all G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id And g.applied_payment_schedule_id = -3"
strcmd = strcmd & " AND g.cash_receipt_id = b.cash_receipt_id and g.cash_receipt_history_id = e.cash_receipt_history_id"
strcmd = strcmd & " AND g.status = 'ACTIVITY' "
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND AA2.BILL_TO_CUSTOMER_ID = a.customer_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and not exists (select 1"
strcmd = strcmd & " From ar_cash_receipt_history_all"
 strcmd = strcmd & " Where cash_receipt_id = b.cash_receipt_id"
 strcmd = strcmd & " and    status = 'REVERSED' )"
strcmd = strcmd & " UNION Select a.customer_id ,aa3.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name ,e.gl_date gl_date,0  ,"
strcmd = strcmd & " NULL ,to_char(e.trx_date,'DD-MM-YYYY')  trx_date ,b.receipt_number,"
strcmd = strcmd & " TO_CHAR(b.receipt_date,  'DD-MM-YYYY'),NULL,c.code_combination_id account_id ,"
strcmd = strcmd & " b.currency_code,b.exchange_rate ,b.amount amount  ,(b.amount * NVL(b.exchange_rate,1)) amount_other_currency,"
strcmd = strcmd & " 'REV' type ,0 ,0  ,b.rowid From     ra_customers A, ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,    ar_receipt_method_accounts_all  D,"
strcmd = strcmd & " ar_cash_receipt_history_all E , ra_customer_trx_all  aa3,ar_payment_schedules_all F"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id = d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and aa3.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND e.status = 'REVERSED'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " and b.reversal_date is not null UNION"
strcmd = strcmd & " SELECT A.CUSTOMER_ID,c.bill_to_customer_id,A.CUSTOMER_NUMBER  ,A.CUSTOMER_NAME ,B.GL_DATE  ,0  ,"
strcmd = strcmd & " B.ADJUSTMENT_NUMBER ,TO_CHAR(B.APPLY_DATE,'DD-MM-YYYY') trx_date ,NULL receipt_number,"
strcmd = strcmd & " NULL receipt_date ,SUBSTR(b.comments,1,50) remarks ,b.code_combination_id account_id ,c.invoice_currency_code currency_code ,"
strcmd = strcmd & " c.exchange_rate   ,b.amount amount  ,(b.amount*NVL(c.exchange_rate,1)) amount_other_currency ,"
strcmd = strcmd & " 'ADJ' type,0  ,0  ,b.rowid FROM ra_customers a,"
strcmd = strcmd & " ar_adjustments_all  b,ra_customer_trx_all  c,ar_payment_schedules_all  d,gl_code_combinations   e"
strcmd = strcmd & " Where b.customer_trx_id = c.customer_trx_id And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & " AND b.status = 'A' AND  e.code_combination_id = b.code_combination_id"
strcmd = strcmd & " AND b.payment_schedule_id   = d.payment_schedule_id AND b.customer_trx_id    = d.customer_trx_id"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & " union "
strcmd = strcmd & " Select a.customer_id,b.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name ,  d.gl_date,"
strcmd = strcmd & " B.CUSTOMER_TRX_ID,"
strcmd = strcmd & " b.trx_number,"
strcmd = strcmd & " TO_CHAR(b.trx_date, 'DD-MM-YYYY') trx_date ,NULL receipt_number  ,NULL receipt_date  ,"
strcmd = strcmd & " SUBSTR(b.comments,1,50) remarks,earned_discount_ccid account_id  ,"
 strcmd = strcmd & " b.invoice_currency_code currency_code  ,b.exchange_rate,d.EARNED_discount_taken amount  ,"
strcmd = strcmd & " d.ACCTD_EARNED_DISCOUNT_TAKEN  amount_other_currency ,"
strcmd = strcmd & " 'DSC' type,b.customer_trx_id,0 ,b.rowid From    ra_customers  a,"
strcmd = strcmd & " ra_customer_trx_all   B,    ar_receivable_applications_all  D Where"
strcmd = strcmd & " b.bill_to_customer_id = a.customer_id AND   b.complete_flag   = 'Y'"
strcmd = strcmd & " AND D.EARNED_DISCOUNT_TAKEN  is not null"
strcmd = strcmd & " and D.EARNED_DISCOUNT_TAKEN <> 0"
strcmd = strcmd & " and b.customer_trx_id  = d.applied_customer_trx_id"
strcmd = strcmd & " and d.application_type = 'CASH'"
strcmd = strcmd & " and d.display   = 'Y'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " Union All SELECT"
strcmd = strcmd & " a.customer_id ,b.Bill_To_Site_Use_Id,a.customer_number,a.customer_name,d.gl_date,b.customer_trx_id,b.trx_number,"
strcmd = strcmd & " to_char(b.trx_date,'DD-MM-YYYY') trx_date  ,c.receipt_number  ,"
strcmd = strcmd & " to_char(c.receipt_date,'DD-MM-yyyy') receipt_date,"
strcmd = strcmd & " decode(e.amount_dr, null, 'CR','DR')   comments  ,"
strcmd = strcmd & " e.code_combination_id ,b.INVOICE_CURRENCY_CODE ,"
strcmd = strcmd & " b.exchange_rate  ,nvl(e.AMOUNT_DR, e.AMOUNT_CR)  amount  ,"
strcmd = strcmd & " nvl(e.ACCTD_AMOUNT_DR,e.ACCTD_AMOUNT_CR)     acctd_amount ,"
strcmd = strcmd & " e.source_type,0 customer_trx_id  ,"
strcmd = strcmd & " 0 customer_trx_line_id , b.ROWID"
strcmd = strcmd & " FROM ra_customers a,ra_customer_trx_all b,ar_cash_receipts_all c,ar_receivable_applications_all  d,"
strcmd = strcmd & " ar_distributions_all  e WHERE    a.customer_id  =  b.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND b.customer_trx_id=  d.APPLIED_CUSTOMER_TRX_ID"
strcmd = strcmd & " AND c.cash_receipt_id  =  d.cash_receipt_id"
strcmd = strcmd & " AND e.SOURCE_ID =  d.receivable_application_id"
strcmd = strcmd & " AND e.source_Type  IN ('EXCH_LOSS', 'EXCH_GAIN')"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & "ORDER BY 1,4"
Set comprec = con.Execute(strcmd)
'If Not comprec.EOF Then
     strcmd = " begin "
    strcmd = strcmd & " dbms_application_info.set_client_info(101);"
    strcmd = strcmd & " end;"
    Set comprec1 = con.Execute(strcmd)
    
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "Ar_Ledger", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
    'MsgBox comprec!trx_number
    repcomprec.AddNew
    repcomprec!CUSTOMER_NAME = comprec!CUSTOMER_NAME
    repcomprec!CUSTOMER_NUMBER = comprec!CUSTOMER_NUMBER
    repcomprec!gl_DATE = comprec!gl_DATE
    repcomprec!Inv_Type = comprec!Type
    repcomprec!loc_id = comprec!Bill_To_Site_Use_Id
    If comprec!Type = "REC" Then
    repcomprec!amount_CR = comprec!amount_other_currency
    repcomprec!amount_DR = 0
    repcomprec!TRX_DATE = comprec!receipt_date
    repcomprec!trx_number = comprec!receipt_number
    Else
    repcomprec!amount_DR = comprec!Amount
    repcomprec!amount_CR = 0
    repcomprec!TRX_DATE = comprec!TRX_DATE
    repcomprec!trx_number = comprec!trx_number
    End If
    
   

    
    strcmd = "select * from RA_CUSTOMER_TRX_PARTIAL_V where su_bill_to_Location='" & comprec!Bill_To_Site_Use_Id & "'"
    Set comprec1 = con.Execute(strcmd)
    If Not comprec1.EOF Then
        Location = comprec1!RAA_bill_to_City
    Else
        Location = "-----"
    End If
    repcomprec!location_Name = Location
    comprec.MoveNext
    Loop
    repcomprec.UpdateBatch


a = MsgBox("Want to Print The Aging (Y/N)? ", vbYesNo, "Print")
If a = vbYes Then
    prnarled
End If
End Sub

Public Sub prnarled()
        cryctrl.WindowTitle = "AR Customer LedgerReport"
        cryctrl.ReportFileName = App.Path & "\reportS\Ar_Customer.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Public Sub crled()
        'cryctrl.WindowTitle = CCODE & " Wise Report"
                    cryctrl.ReportFileName = App.Path & "\reportS\Ap_Out.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Public Sub add_vendor_Ledger()
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)
Set comprec = con1.Execute("truncate table all_data")


strcmd = "SELECT  povs.vendor_site_code, "
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " apd.dist_code_combination_id ccid,"
strcmd = strcmd & " api.invoice_currency_code,"
strcmd = strcmd & " api.exchange_rate,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',abs(z.amt_val) - abs(nvl(api.discount_amount_taken,0) ) , 0)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',0,z.amt_val - nvl(api.discount_amount_taken,0) ) cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " to_char(api.doc_sequence_value) payment_num,"
strcmd = strcmd & " to_char(apd.accounting_date, 'dd-MON-yyyy') pay_accounting_date,"
strcmd = strcmd & " NULL check_number,"
strcmd = strcmd & " pov.segment1,"
strcmd = strcmd & " pov.vendor_name,"
strcmd = strcmd & " pov.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,"
strcmd = strcmd & " api.batch_id,"
strcmd = strcmd & " api.exchange_date,"
strcmd = strcmd & " api.invoice_id,"
strcmd = strcmd & " apd.accounting_date"
strcmd = strcmd & " FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs,"
strcmd = strcmd & " (SELECT NVL(SUM(apd.amount),0) amt_val,"
strcmd = strcmd & " api.invoice_id"
strcmd = strcmd & " FROM ap_invoices_all api,"
strcmd = strcmd & " ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,"
strcmd = strcmd & " po_vendor_sites_all povs"
strcmd = strcmd & " Where api.invoice_id = apd.invoice_id"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " AND apd.match_status_flag='A'"
strcmd = strcmd & " AND apd.line_type_lookup_code <> 'PREPAY'"
strcmd = strcmd & " GROUP BY api.invoice_id) z"
strcmd = strcmd & " Where api.invoice_id = Z.invoice_id"
strcmd = strcmd & " and api.invoice_id = apd.invoice_id"
strcmd = strcmd & " --and api.vendor_id=418"
'strcmd = strcmd & " --and povs.vendor_site_code ='DELHI'"
strcmd = strcmd & " AND apd.rowid = ( select  rowid"
strcmd = strcmd & " From ap_invoice_distributions_all"
strcmd = strcmd & " Where rownum = 1"
strcmd = strcmd & " and invoice_id=apd.invoice_id"
strcmd = strcmd & " AND match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND api.invoice_type_lookup_code <> 'PREPAYMENT'"
strcmd = strcmd & " AND apd.match_status_flag = 'A'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " AND ((api.invoice_type_lookup_code  <> 'DEBIT')"
strcmd = strcmd & " or"
strcmd = strcmd & " ("
strcmd = strcmd & " (api.invoice_type_lookup_code  = 'DEBIT')"
strcmd = strcmd & " and"
strcmd = strcmd & " ( not exists"
strcmd = strcmd & " (Select '1'"
strcmd = strcmd & " from ap_invoice_payments_all  app,"
strcmd = strcmd & " ap_checks_all apc"
strcmd = strcmd & " Where App.check_id = apc.check_id"
strcmd = strcmd & " and app.invoice_id = api.invoice_id"
strcmd = strcmd & " and apc.payment_type_flag = 'R'"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
strcmd = strcmd & " )"
'----------------IInd

strcmd = strcmd & " Union All"
strcmd = strcmd & " SELECT povs.vendor_site_code,"
strcmd = strcmd & " api.invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " api.invoice_date,"
strcmd = strcmd & " apd.description description,"
strcmd = strcmd & " app.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " api.payment_currency_code,"
strcmd = strcmd & " apc.exchange_rate ,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',0,0),app.amount)  dr_val,"
strcmd = strcmd & " Decode(api.invoice_type_lookup_code,'CREDIT',decode(status_lookup_code,'VOIDED',app.amount,abs(app.amount)),0)  cr_val,"
strcmd = strcmd & " 0 acct_dr,"
strcmd = strcmd & " 0 acct_cr,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.doc_sequence_value), 'P',  to_char(apc.doc_sequence_value),  to_char(apc.doc_sequence_value), 'N', NULL) payment_num,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(app.accounting_date, 'dd-MON-yyyy'), 'P', to_char(app.accounting_date, 'dd-MON-yyyy')) pay_accounting_date,"
strcmd = strcmd & " DECODE(api.payment_status_flag, 'Y', to_char(apc.check_number), 'P', to_char(apc.check_number)) check_number,"
strcmd = strcmd & " pov.segment1,"
strcmd = strcmd & " pov.vendor_name,"
strcmd = strcmd & " pov.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id,"
strcmd = strcmd & " apc.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,apc.exchange_date,api.invoice_id,App.accounting_date"
strcmd = strcmd & " FROM ap_invoices_all api,ap_invoice_distributions_all apd,"
strcmd = strcmd & " po_vendors pov,ap_invoice_payments_all app,ap_checks_all apc,"
strcmd = strcmd & " po_vendor_sites_all povs Where api.invoice_id = apd.invoice_id"
strcmd = strcmd & " AND apd.rowid = (select rowid from ap_invoice_distributions_all where rownum=1 and invoice_id=apd.invoice_id and match_status_flag='A')"
strcmd = strcmd & " AND api.vendor_id = pov.vendor_id"
strcmd = strcmd & " AND app.invoice_id = api.invoice_id"
strcmd = strcmd & " AND app.check_id = apc.check_id"
strcmd = strcmd & " AND apc.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')"
strcmd = strcmd & " AND apd.match_status_flag='A'"
strcmd = strcmd & " AND api.vendor_site_id = povs.vendor_site_id"
strcmd = strcmd & " --AND API.VENDOR_ID=418"
'strcmd = strcmd & " --and app.reversal_flag='N'"
'strcmd = strcmd & "--d povs.vendor_site_code='"

'''--------------------------IIIrd
strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,d. ACCOUNTING_DATE INVOICE_DATE ,null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " 0 distribution_id,api.exchange_rate_type,api.org_id,"
strcmd = strcmd & " api.batch_id,sysdate exchange_date,"
strcmd = strcmd & " api.invoice_id,d.ACCOUNTING_DATE from po_vendor_sites_all a,po_vendors b,"
strcmd = strcmd & " ap_ae_lines_all c,ap_ae_headers_all d,ap_invoices_all api,"
strcmd = strcmd & " po_vendor_sites_all povs where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICES' AND"
strcmd = strcmd & " c.source_id = api.invoice_id"
strcmd = strcmd & " and api.vendor_id=418"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"

''''--------------------4th
strcmd = strcmd & " Union All"
strcmd = strcmd & " select"
strcmd = strcmd & " a.vendor_site_code,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " c.currency_code invoice_currency_code,"
strcmd = strcmd & " c.currency_conversion_rate  rate ,"
strcmd = strcmd & " 0 dr_val,0 cr_val,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & " DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & " null payment_num,"
strcmd = strcmd & " to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & " null check_number,"
strcmd = strcmd & " b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & " apd.po_distribution_id po_distribution_id,"
strcmd = strcmd & " api.exchange_rate_type,"
strcmd = strcmd & " api.org_id,api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & " d.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & " po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & " ap_invoices_all api,ap_invoice_distributions_all apd"
strcmd = strcmd & " where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & " a.vendor_id = api.vendor_id AND"
strcmd = strcmd & " a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & " b.vendor_id = api.vendor_id AND"
strcmd = strcmd & " c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & " c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & " c.source_table = 'AP_INVOICE_DISTRIBUTIONS' AND"
strcmd = strcmd & " c.source_id = apd.invoice_distribution_id"
strcmd = strcmd & " and api.vendor_id=418"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
'-------------------------5th

strcmd = strcmd & " Union All"
strcmd = strcmd & " select a.vendor_site_code,"
strcmd = strcmd & " 'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & " api.invoice_num,"
strcmd = strcmd & " d. ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description, a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & "  c.currency_code invoice_currency_code,"
strcmd = strcmd & "  c.currency_conversion_rate  rate ,"
strcmd = strcmd & "  0 dr_val,0 cr_val,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "  null payment_num,"
strcmd = strcmd & "  to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & "  to_char(ac.check_number) check_number,"
strcmd = strcmd & "  b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,"
strcmd = strcmd & "  0 distribution_id,api.exchange_rate_type,"
strcmd = strcmd & "  api.org_id,api.batch_id,sysdate exchange_date,api.invoice_id,"
strcmd = strcmd & "  d.accounting_date from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & "  ap_invoices_all api,"
strcmd = strcmd & "  ap_checks_all ac ,ap_invoice_payments_all App"
strcmd = strcmd & " po_vendor_sites_all povs"
strcmd = strcmd & "  where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & "  a.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & "   b.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & "  api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & "  app.check_id = ac.check_id  AND"
strcmd = strcmd & "  c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & "  c.source_table = 'AP_CHECKS' AND"
strcmd = strcmd & "  c.source_id = ac.check_id  AND"
strcmd = strcmd & "  ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED') -- AND"
strcmd = strcmd & " and api.vendor_id=418"
'strcmd = strcmd & "  and a.vendor_site_code='DELHI'"
' and app.reversal_flag='N'
'--------------------------------------------6th

 strcmd = strcmd & "  Union All"
strcmd = strcmd & "  select a.vendor_site_code,'GAIN/LOSS' invoice_type_lookup_code,"
strcmd = strcmd & "  api.invoice_num,d.ACCOUNTING_DATE INVOICE_DATE ,"
strcmd = strcmd & " null description,a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & "  c.currency_code invoice_currency_code,"
strcmd = strcmd & "  c.currency_conversion_rate  rate ,"
strcmd = strcmd & "  0 dr_val,0 cr_val,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'GAIN', accounted_cr,0) acct_dr,"
strcmd = strcmd & "  DECODE(c.ae_line_type_code,'LOSS',accounted_dr,0)  acct_cr,"
strcmd = strcmd & "  null payment_num,to_char(d.accounting_date, 'dd-MON-yyyy')  pay_accounting_date,"
strcmd = strcmd & "  to_char(ac.check_number) check_number,"
strcmd = strcmd & "  b.segment1 ,b.vendor_name,b.vendor_type_lookup_code,0 distribution_id,"
strcmd = strcmd & "  api.exchange_rate_type,api.org_id,api.batch_id,sysdate exchange_date,"
strcmd = strcmd & "  api.invoice_id,d.accounting_date"
strcmd = strcmd & "  from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_ae_lines_all c,ap_ae_headers_all d,"
strcmd = strcmd & "  ap_invoices_all api,"
strcmd = strcmd & "  ap_checks_all ac ,ap_invoice_payments_all App"
strcmd = strcmd & "  where  a.vendor_id = b.vendor_id AND"
strcmd = strcmd & "  a.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  a.vendor_site_id = api.vendor_site_id AND"
strcmd = strcmd & "  b.vendor_id = api.vendor_id AND"
strcmd = strcmd & "  c.ae_header_id =  d.ae_header_id AND"
strcmd = strcmd & "  api.invoice_id =  app.invoice_id AND"
strcmd = strcmd & "  app.check_id = ac.check_id  AND"
strcmd = strcmd & "  c.ae_line_type_code in ( 'GAIN','LOSS') AND"
strcmd = strcmd & "  c.source_table = 'AP_INVOICE_PAYMENTS' AND"
strcmd = strcmd & "  c.source_id = app.invoice_payment_id  AND"
strcmd = strcmd & "  ac.status_lookup_code IN ('CLEARED', 'NEGOTIABLE','VOIDED','RECONCILED UNACCOUNTED', 'RECONCILED', 'CLEARED BUT UNACCOUNTED')  --AND"
strcmd = strcmd & "  AND API.VENDOR_ID=418"
'strcmd = strcmd & " and a.vendor_site_code='DELHI'"
'and app.reversal_flag='N'

'-----------------------7th
 strcmd = strcmd & "  Union All"
strcmd = strcmd & "  select DISTINCT a.vendor_site_code,"
strcmd = strcmd & "  NULL invoice_type_lookup_code,"
strcmd = strcmd & " null invoice_num,"
strcmd = strcmd & " SYSDATE INVOICE_DATE ,"
strcmd = strcmd & "  null description,"
strcmd = strcmd & "  a.accts_pay_code_combination_id ccid,"
strcmd = strcmd & " Null  invoice_currency_code,"
strcmd = strcmd & "  0  rate ,0 dr_val,0  cr_val,0 acct_dr,"
strcmd = strcmd & "  0 acct_cr,null payment_num,null  pay_accounting_date,"
strcmd = strcmd & "  null check_number,"
strcmd = strcmd & "  b.segment1 ,"
strcmd = strcmd & "  b.vendor_name,"
strcmd = strcmd & "  null,"
strcmd = strcmd & "  0 distribution_id,"
strcmd = strcmd & "  null,"
strcmd = strcmd & " a.org_id,"
strcmd = strcmd & "  0 batch_id,"
strcmd = strcmd & "  sysdate exchange_date,"
strcmd = strcmd & "  0 invoice_id,"
strcmd = strcmd & " sysdate accounting_date"
strcmd = strcmd & "  from po_vendor_sites_all a,"
strcmd = strcmd & "  po_vendors b,ap_invoices_all c,"
strcmd = strcmd & "  AP_INVOICE_DISTRIBUTIONS_ALL D"
strcmd = strcmd & "  where a.vendor_id = b.vendor_id and"
strcmd = strcmd & "  a.vendor_id = C.vendor_id"
strcmd = strcmd & " AND A.VENDOR_SITE_ID = C.VENDOR_SITE_ID"
strcmd = strcmd & "  AND C.INVOICE_ID = D.INVOICE_ID"
'strcmd = strcmd & "  AND a.vendor_site_code='DELHI'"
strcmd = strcmd & "  and  a.vendor_id=418"
strcmd = strcmd & "  ORDER BY 24,25"
 
Set comprec2 = con.Execute(strcmd)
If Not comprec2.EOF Then
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "All_Data", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec2.EOF
    repcomprec.AddNew
    repcomprec!vendor_site_code = comprec2!vendor_site_code
    repcomprec!invoice_type_lookup_code = comprec2!invoice_type_lookup_code
    repcomprec!invoice_num = comprec2!invoice_num
    repcomprec!INVOICE_DATE = comprec2!INVOICE_DATE
    repcomprec!Description = comprec2!Description
    repcomprec!dr_val = comprec2!dr_val
    repcomprec!cr_val = comprec2!cr_val
    repcomprec!segment1 = comprec2!segment1
    repcomprec!Vendor_Name = comprec2!Vendor_Name
    repcomprec!vendor_type_lookup_code = comprec2!vendor_type_lookup_code
    repcomprec!INVOICE_ID = comprec2!INVOICE_ID
    repcomprec!accounting_date = comprec2!accounting_date
    comprec2.MoveNext
'    MsgBox "Yes"
    Loop
    repcomprec.UpdateBatch
Else
    MsgBox "No Transaction Is There is the Database !", vbInformation, "No Data"
    Exit Sub
End If
MsgBox "Done"
End Sub


Public Sub add_aging()
Set comprec = con1.Execute("truncate table Aging")
Set comprec = con1.Execute("truncate table unapplied_amount")
Dim newcomprec As ADODB.Recordset

p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")

strcmd = "SELECT Distinct cc.SEGMENT1 Location,hp.PARTY_NAME,RCT.TRX_NUMBER INVOICE_NUM,rct.trx_date,to_char(RCT.TRX_DATE,'dd-MON-yy'),"
strcmd = strcmd & " APS.AMOUNT_DUE_ORIGINAL ORIGINAL_AMOUNT,"
strcmd = strcmd & " APS.AMOUNT_DUE_REMAINING BALANCE_AMOUNT"
 strcmd = strcmd & " From RA_CUSTOMER_TRX_ALL RCT,"
strcmd = strcmd & " HZ_CUST_ACCOUNTS hca ,"
strcmd = strcmd & " HZ_PARTIES  HP,HZ_CUST_SITE_USES_ALL HCSU,"
strcmd = strcmd & " AR_PAYMENT_SCHEDULES_ALL APS,"
strcmd = strcmd & " ra_cust_trx_line_gl_dist_all g,"
strcmd = strcmd & " gl_code_combinations cc"
strcmd = strcmd & " Where RCT.CUSTOMER_TRX_ID = APS.CUSTOMER_TRX_ID"
strcmd = strcmd & "   and rct.CUSTOMER_TRX_ID = g.CUSTOMER_TRX_ID"
 strcmd = strcmd & "   and cc.CODE_COMBINATION_ID =APS.CUSTOMER_SITE_USE_ID"
strcmd = strcmd & "   AND HCA.PARTY_ID = HP.PARTY_ID"
strcmd = strcmd & "   AND HCSU.BILL_TO_SITE_USE_ID = RCT.BILL_TO_SITE_USE_ID"
strcmd = strcmd & "   AND HCA.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID"
'strcmd = strcmd & "   and hp.PARTY_NAME like '%ERNST AND YOUNG%'"
'strcmd = strcmd & " and  AND RCT.TRX_DATE <= '31-Mar-2006'"
'strcmd = strcmd & " and hp.PARTY_NAME like  '%" & "ERNST AND YOUNG" & "%'"
strcmd = strcmd & " AND RCT.TRX_DATE <='" & p_end_date & "'"
'strcmd = strcmd & " AND RCT.TRX_number like  '%" & "040-19212INV" & "%'"
strcmd = strcmd & "   order by  hp.PARTY_NAME"
 
Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "aging", con1, adOpenDynamic, adLockOptimistic
Set newcomprec = con.Execute(strcmd)
If Not newcomprec.EOF Then
    
    status.LBL.Caption = "Loading Aging ..........."
    Do While Not newcomprec.EOF
        status.Show
       a1 = newcomprec!TRX_DATE
       a2 = DTPicker9.Value
       total_days = DateDiff("d", a1, a2)
       'MsgBox total_days
        repcomprec.AddNew
       
       If total_days >= 0 And total_days <= 30 Then
            netamt = newcomprec!BALANCE_AMOUNT
            repcomprec!up_30 = netamt
            repcomprec!up_60 = 0
            repcomprec!up_90 = 0
            repcomprec!up_180 = 0
            repcomprec!MORE_180 = 0
       End If
       
       If total_days > 30 And total_days <= 60 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = netamt
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
       
       If total_days > 60 And total_days <= 90 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = netamt
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 91 And total_days <= 180 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = netamt
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 181 Then
              netamt = newcomprec!BALANCE_AMOUNT
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = netamt
       End If
      
       repcomprec!Location = newcomprec!Location
       repcomprec!customer = newcomprec!Party_Name
       repcomprec!TRX_DATE = newcomprec!TRX_DATE
       repcomprec!CUT_DATE = DTPicker9.Value
       repcomprec!trx_number = newcomprec!invoice_num
       'repcomprec!user_name = newcomprec!user_name
       'repcomprec!DS_NUM = newcomprec!DS_NUM
       'repcomprec!ORIGINAL_AMOUNT = newcomprec!ORIGINAL_AMOUNT
       'repcomprec!BALANCE_AMOUNT = newcomprec!BALANCE_AMOUNT
        repcomprec.UpdateBatch
'        StatusBar1.Panels(1).Text = "Loading ..................!" & newcomprec!INVOICE_NUM
        newcomprec.MoveNext
    Loop
   
Else
'Exit Sub
End If
Unload status
MsgBox "Data for Outstanding !", vbCritical, "Error"
 
 
End Sub


Public Sub MAIN_AGING()
Set comprec = con1.Execute("truncate table Aging")
'Set comprec = con1.Execute("truncate table unapplied_amount")
Dim newcomprec As ADODB.Recordset
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")

strcmd = " SELECT APSA.status,APSA.AMOUNT_DUE_ORIGINAL,APSA.GL_DATE,APSA.TRX_NUMBER,"
strcmd = strcmd & " APSA.TRX_DATE,HP.PARTY_NAME,APSA.CLASS FROM AR_PAYMENT_SCHEDULES_ALL APSA,AR_CASH_RECEIPTS_ALL ACRA,"
strcmd = strcmd & " HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP Where APSA.CASH_RECEIPT_ID = ACRA.CASH_RECEIPT_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = APSA.CUSTOMER_ID"
strcmd = strcmd & " AND APSA.CUSTOMER_ID = ACRA.PAY_FROM_CUSTOMER"
strcmd = strcmd & " AND HP.PARTY_ID = HCA.PARTY_ID"
strcmd = strcmd & " AND APSA.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND APSA.GL_DATE <='" & p_end_date & "'"
'strcmd = strcmd & " AND HP.PARTYP_NAME=' & ""AUTO DESK" & "'"
strcmd = strcmd & " AND APSA.CLASS = 'PMT'"
strcmd = strcmd & " AND ACRA.Status<> 'REV'"
strcmd = strcmd & " Union All"

strcmd = strcmd & " SELECT APSA.status,APSA.AMOUNT_DUE_ORIGINAL,APSA.GL_DATE,APSA.TRX_NUMBER,APSA.TRX_DATE,HP.PARTY_NAME,APSA.CLASS"
strcmd = strcmd & " From AR_PAYMENT_SCHEDULES_ALL APSA,RA_CUSTOMER_TRX_ALL RCTL,"
strcmd = strcmd & " HZ_CUST_ACCOUNTS HCA,"
strcmd = strcmd & " HZ_PARTIES HP"
strcmd = strcmd & " Where APSA.CUSTOMER_TRX_ID = RCTL.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCTL.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND HP.PARTY_ID = HCA.PARTY_ID"
strcmd = strcmd & " AND APSA.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND APSA.GL_DATE <='" & p_end_date & "'"
strcmd = strcmd & " AND APSA.CLASS = 'INV'"

strcmd = strcmd & " Union All"
strcmd = strcmd & " SELECT APSA.status,APSA.AMOUNT_DUE_ORIGINAL,APSA.GL_DATE,APSA.TRX_NUMBER,APSA.TRX_DATE, HP.PARTY_NAME,APSA.CLASS From AR_PAYMENT_SCHEDULES_ALL APSA,"
strcmd = strcmd & " RA_CUSTOMER_TRX_ALL RCTL,HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP"
strcmd = strcmd & " Where APSA.CUSTOMER_TRX_ID = RCTL.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCTL.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND HP.PARTY_ID = HCA.PARTY_ID"
strcmd = strcmd & " AND APSA.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND APSA.GL_DATE <='" & p_end_date & "'"
strcmd = strcmd & " AND APSA.CLASS = 'CM'"

Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "aging", con1, adOpenDynamic, adLockOptimistic
Set newcomprec = con.Execute(strcmd)
If Not newcomprec.EOF Then
    status.LBL.Caption = "Loading Aging ..........."
    Do While Not newcomprec.EOF
    '   status.Show
        Dim d1 As Date
        d1 = "01-Dec-2005"
       If newcomprec!TRX_DATE <= d1 Then
        a1 = newcomprec!TRX_DATE
       Else
        a1 = newcomprec!gl_DATE
       End If
       
       a2 = DTPicker9.Value
       total_days = DateDiff("d", a1, a2)
       
        repcomprec.AddNew
       If newcomprec!TRX_DATE = "12-Nov-2005" Then
        MsgBox ""
       End If
       If total_days >= 0 And total_days <= 30 Then
            If newcomprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = newcomprec!AMOUNT_DUE_ORIGINAL
            End If
            repcomprec!up_30 = netamt
            repcomprec!up_60 = 0
            repcomprec!up_90 = 0
            repcomprec!up_180 = 0
            repcomprec!MORE_180 = 0
       End If
       If total_days > 30 And total_days <= 60 Then
              If newcomprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = newcomprec!AMOUNT_DUE_ORIGINAL
               End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = netamt
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
       
       If total_days > 60 And total_days <= 90 Then
              If newcomprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = newcomprec!AMOUNT_DUE_ORIGINAL
               End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = netamt
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 91 And total_days <= 180 Then
              If newcomprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = newcomprec!AMOUNT_DUE_ORIGINAL
               End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = netamt
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 181 Then
              If newcomprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = newcomprec!AMOUNT_DUE_ORIGINAL
              End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = netamt
       End If
      
        If newcomprec!Class = "PMT" Then
              repcomprec!rec_amount = newcomprec!AMOUNT_DUE_ORIGINAL
        Else
              repcomprec!rec_amount = 0
        End If
        
       'repcomprec!Location = newcomprec!Location
       repcomprec!customer = newcomprec!Party_Name
       repcomprec!TRX_DATE = newcomprec!TRX_DATE
       repcomprec!CUT_DATE = DTPicker9.Value
       repcomprec!trx_number = newcomprec!trx_number
       repcomprec!CLASS_name = newcomprec!Class
       repcomprec!status = newcomprec!status
       repcomprec!total_days = total_days
       
       repcomprec.UpdateBatch
       StatusBar1.Panels(1).Text = "Loading ..................!" & newcomprec!trx_number
        newcomprec.MoveNext
    Loop
   
Else
'Exit Sub
End If
Unload status
'MsgBox "Data for Outstanding !", vbCritical, "Error"
End Sub

Public Sub add_all_aging()
strcmd = " SELECT * FROM AR_PAYMENT_SCHEDULES_ALL APSA,AR_CASH_RECEIPTS_ALL ACRA,"
strcmd = strcmd & " HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP Where APSA.CASH_RECEIPT_ID = ACRA.CASH_RECEIPT_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = APSA.CUSTOMER_ID"
strcmd = strcmd & " AND APSA.CUSTOMER_ID = ACRA.PAY_FROM_CUSTOMER"
strcmd = strcmd & " AND HP.PARTY_ID = HCA.PARTY_ID"
strcmd = strcmd & " AND APSA.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND APSA.GL_DATE <='" & p_end_date & "'"
'STRCMD = STRCMD & " AND APSA.GL_DATE BETWEEN '1-DEC-05' AND '31-MAR-06'"
strcmd = strcmd & " AND APSA.CLASS = 'PMT'"
strcmd = strcmd & " AND ACRA.Status<> 'REV'"

strcmd = strcmd & " Union All"
strcmd = strcmd & " SELECT NVL(SUM(APSA.AMOUNT_DUE_ORIGINAL),0)"
strcmd = strcmd & " From AR_PAYMENT_SCHEDULES_ALL APSA,RA_CUSTOMER_TRX_ALL RCTL,"
strcmd = strcmd & " HZ_CUST_ACCOUNTS HCA,"
strcmd = strcmd & " HZ_PARTIES HP"
strcmd = strcmd & " Where APSA.CUSTOMER_TRX_ID = RCTL.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCTL.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND HP.PARTY_ID = HCA.PARTY_ID"
strcmd = strcmd & " AND APSA.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND APSA.GL_DATE <='" & p_end_date & "'"
'STRCMD = STRCMD & " AND APSA.GL_DATE BETWEEN '1-DEC-05' AND '31-MAR-06'"
strcmd = strcmd & " AND APSA.CLASS = 'INV'"
strcmd = strcmd & " Union All"
strcmd = strcmd & " SELECT * From AR_PAYMENT_SCHEDULES_ALL APSA,"
strcmd = strcmd & " RA_CUSTOMER_TRX_ALL RCTL,HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP"
strcmd = strcmd & " Where APSA.CUSTOMER_TRX_ID = RCTL.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCTL.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND HP.PARTY_ID = HCA.PARTY_ID"
'STRCMD = STRCMD & " AND APSA.GL_DATE BETWEEN '1-DEC-05' AND '31-MAR-06'"
strcmd = strcmd & " AND APSA.GL_DATE >='" & p_start_date & "'"
strcmd = strcmd & " AND APSA.GL_DATE <='" & p_end_date & "'"
strcmd = strcmd & " AND APSA.CLASS = 'CM'"
End Sub


Private Sub Outstanding1_Click()
'Outstanding
Set comprec = con1.Execute("truncate table AGN")
strcmd = " SELECT HP.PARTY_NAME PARTY_NAME,'Invoice' INVOICE_RECEIPT,"
strcmd = strcmd & " FVTA.DESCRIPTION LOCATION,"
strcmd = strcmd & " RCT.TRX_DATE INVOICE_DATE,"
strcmd = strcmd & " RCT.TRX_NUMBER INVOICE_NUMBER,"
strcmd = strcmd & " RCT.PURCHASE_ORDER USER_NAME ,"
strcmd = strcmd & " RCT.CT_REFERENCE DS_NUMBER,"
strcmd = strcmd & " APS.AMOUNT_DUE_ORIGINAL ORIGINAL_AMOUNT_DUE,"
strcmd = strcmd & " APS.AMOUNT_DUE_REMAINING BALANCE_AMOUNT_DUE,"
strcmd = strcmd & " NULL RECEIPT_DATE,"
strcmd = strcmd & " NULL RECEIPT_NUMBER,"
strcmd = strcmd & " NULL RECEIPT_AMOUNT,"
strcmd = strcmd & " NULL UNAPPLIED_AMOUNT"
strcmd = strcmd & " From RA_CUSTOMER_TRX_ALL RCT, RA_CUST_TRX_LINE_GL_DIST_ALL RCTLGD,GL_CODE_COMBINATIONS GCC,"
strcmd = strcmd & " FND_FLEX_VALUES FVA,FND_FLEX_VALUES_TL FVTA,FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB"
strcmd = strcmd & " ,FND_FLEX_VALUES FVC,FND_FLEX_VALUES_TL FVTC,FND_FLEX_VALUES FVD,FND_FLEX_VALUES_TL FVTD,"
strcmd = strcmd & " HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP,AR_PAYMENT_SCHEDULES_ALL APS,RA_TERMS_TL RT"
strcmd = strcmd & " Where RCT.CUSTOMER_TRX_ID = APS.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID"
'strcmd = strcmd & "AND HP.PARTY_NAME = NVL(:P_CUSTOMER_NAME, HP.PARTY_NAME)"
'strcmd = strcmd & " AND HP.PARTY_NAME = 'HEWLETT PACKARD INDIA SALES PRIVATE LIMITED'"
strcmd = strcmd & " AND HP.PARTY_NAME = '" & Party_Name.Text & "'"
'strcmd = strcmd & "AND RT.TERM_ID = nvl(RCT.TERM_ID,rt.term_id)"
strcmd = strcmd & " and rct.status_trx != 'CL'"
strcmd = strcmd & " AND RCT.CUSTOMER_TRX_ID = RCTLGD.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND GCC.CODE_COMBINATION_ID = RCTLGD.CODE_COMBINATION_ID"
strcmd = strcmd & " AND RCTLGD.ACCOUNT_CLASS = 'REC'"
strcmd = strcmd & " AND FVA.FLEX_VALUE_SET_ID = '1009931' AND FVB.FLEX_VALUE_SET_ID = '1009932'"
strcmd = strcmd & " AND FVC.FLEX_VALUE_SET_ID = '1009933' AND FVD.FLEX_VALUE_SET_ID = '1009934'"
strcmd = strcmd & " AND FVA.FLEX_VALUE = GCC.SEGMENT1 AND FVB.FLEX_VALUE = GCC.SEGMENT2"
strcmd = strcmd & " AND FVC.FLEX_VALUE = GCC.SEGMENT3 AND FVD.FLEX_VALUE = GCC.SEGMENT4"
strcmd = strcmd & " AND FVA.FLEX_VALUE_ID = FVTA.FLEX_VALUE_ID AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVC.FLEX_VALUE_ID = FVTC.FLEX_VALUE_ID AND FVD.FLEX_VALUE_ID = FVTD.FLEX_VALUE_ID"
strcmd = strcmd & " Union SELECT HP.PARTY_NAME,'Receipt',FVTA.DESCRIPTION LOCATION,"
strcmd = strcmd & " NULL TRX_DATE,"
strcmd = strcmd & " NULL INVOICE_NUM,NULL USER_NAME,NULL DS_NUM,NULL ORIGINAL_AMOUNT,"
strcmd = strcmd & " NULL BALANCE_AMOUNT,ACR.RECEIPT_DATE ,ACR.RECEIPT_NUMBER,ACR.AMOUNT,"
strcmd = strcmd & " TO_NUMBER(DECODE(ACR.TYPE, 'MISC', NULL, NVL(SUM(DECODE(ARA.STATUS, 'UNAPP', NVL(ARA.AMOUNT_APPLIED, 0), 0)), 0))) UNAPPLIED_AMOUNT"
strcmd = strcmd & " From AR_CASH_RECEIPTS_ALL ACR,AR_RECEIVABLE_APPLICATIONS_ALL ARA,"
strcmd = strcmd & " AR_CASH_RECEIPT_HISTORY_ALL ACRH,GL_CODE_COMBINATIONS GCC,FND_FLEX_VALUES FVA,"
strcmd = strcmd & " FND_FLEX_VALUES_TL FVTA,FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB,"
strcmd = strcmd & " FND_FLEX_VALUES FVC,FND_FLEX_VALUES_TL FVTC,FND_FLEX_VALUES FVD,"
strcmd = strcmd & " FND_FLEX_VALUES_TL FVTD,HZ_CUST_ACCOUNTS HCA,HZ_CUST_PROFILE_CLASSES HCPC,HZ_CUSTOMER_PROFILES HCP,"
strcmd = strcmd & " HZ_PARTIES HP,RA_TERMS_TL RT WHERE ACRH.CURRENT_RECORD_FLAG = 'Y'"
strcmd = strcmd & " AND ACR.CASH_RECEIPT_ID = ACRH.CASH_RECEIPT_ID AND ACR.CASH_RECEIPT_ID = ARA.CASH_RECEIPT_ID"
strcmd = strcmd & " AND ARA.STATUS IN ('ACC', 'UNAPP') and ACR.status<>'REV'"
strcmd = strcmd & " AND ACRH.ACCOUNT_CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = ACR.PAY_FROM_CUSTOMER"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID"
'strcmd = strcmd & " AND HP.PARTY_NAME = NVL(:P_CUSTOMER_NAME, HP.PARTY_NAME)"
'strcmd = strcmd & " AND HP.PARTY_NAME = 'HEWLETT PACKARD INDIA SALES PRIVATE LIMITED'"
strcmd = strcmd & " AND HP.PARTY_NAME = '" & Party_Name.Text & "'"
strcmd = strcmd & " AND RT.TERM_ID = HCPC.STANDARD_TERMS AND HCP.SITE_USE_ID IS NULL"
strcmd = strcmd & " AND HCP.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID AND HCP.PROFILE_CLASS_ID = HCPC.PROFILE_CLASS_ID"
strcmd = strcmd & " AND FVA.FLEX_VALUE_SET_ID = '1009931' AND FVB.FLEX_VALUE_SET_ID = '1009932'"
strcmd = strcmd & " AND FVC.FLEX_VALUE_SET_ID = '1009933' AND FVD.FLEX_VALUE_SET_ID = '1009934'"
strcmd = strcmd & " AND FVA.FLEX_VALUE = GCC.SEGMENT1 AND FVB.FLEX_VALUE = GCC.SEGMENT2"
strcmd = strcmd & " AND FVC.FLEX_VALUE = GCC.SEGMENT3 AND FVD.FLEX_VALUE = GCC.SEGMENT4"
strcmd = strcmd & " AND FVA.FLEX_VALUE_ID = FVTA.FLEX_VALUE_ID AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVC.FLEX_VALUE_ID = FVTC.FLEX_VALUE_ID AND FVD.FLEX_VALUE_ID = FVTD.FLEX_VALUE_ID"
strcmd = strcmd & " GROUP BY HP.PARTY_NAME, FVTA.DESCRIPTION,ACR.RECEIPT_DATE ,"
strcmd = strcmd & " ACR.RECEIPT_NUMBER , ACR.Type, ACR.AMOUNT, FVTB.Description, FVTC.Description, FVTD.Description"

 'repcomprec1.Update
Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
'repcomprec.Open "Outstanding", con1, adOpenDynamic, adLockOptimistic
repcomprec.Open "AGN", con1, adOpenDynamic, adLockOptimistic
Set newcomprec = con.Execute(strcmd)
con.CommandTimeout = 900
If Not newcomprec.EOF Then
    'Status.Show
    'status.LBL.Caption = "Loading Customer Ledger ..........."
     StatusBar1.Panels(1).Text = "Loading Customer Ledger ..........."
    Do While Not newcomprec.EOF
    repcomprec.AddNew

   repcomprec!Party_Name = newcomprec!Party_Name
 If newcomprec!INVOICE_RECEIPT = "Invoice" Then
    repcomprec!INVOICE_RECEIPT = newcomprec!INVOICE_RECEIPT
    repcomprec!Location = newcomprec!Location
    repcomprec!INVOICE_DATE = newcomprec!INVOICE_DATE
    repcomprec!INVOICE_NUMBER = newcomprec!INVOICE_NUMBER
    repcomprec!USER_NAME = newcomprec!USER_NAME
    repcomprec!DS_NUMBER = newcomprec!DS_NUMBER
    repcomprec!ORIGINAL_AMOUNT_DUE = newcomprec!ORIGINAL_AMOUNT_DUE
    repcomprec!BALANCE_AMOUNT_DUE = newcomprec!BALANCE_AMOUNT_DUE
    repcomprec!receipt_date = newcomprec!receipt_date
    repcomprec!receipt_number = ""
    repcomprec!Receipt_Amount = 0
    repcomprec!UNAPPLIED_AMOUNT = 0
 Else
    repcomprec!INVOICE_RECEIPT = newcomprec!INVOICE_RECEIPT
    repcomprec!Location = newcomprec!Location
    repcomprec!INVOICE_DATE = newcomprec!INVOICE_DATE
    repcomprec!INVOICE_NUMBER = ""
    repcomprec!USER_NAME = newcomprec!USER_NAME
    repcomprec!DS_NUMBER = ""
    repcomprec!ORIGINAL_AMOUNT_DUE = 0
    repcomprec!BALANCE_AMOUNT_DUE = 0
    repcomprec!receipt_date = newcomprec!receipt_date
    repcomprec!receipt_number = newcomprec!receipt_number
    repcomprec!Receipt_Amount = newcomprec!Receipt_Amount
    repcomprec!UNAPPLIED_AMOUNT = newcomprec!UNAPPLIED_AMOUNT
 End If
    
    repcomprec.Update
    StatusBar1.Panels(1).Text = "Loading ..................!" & newcomprec!INVOICE_NUMBER
    newcomprec.MoveNext
    Loop
    repcomprec.UpdateBatch
    StatusBar1.Panels(1).Text = " "
Else
MsgBox "No Data Is There For Customer Ledger !", vbCritical, "Error"
Exit Sub
End If
a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
If a = vbYes Then
    prnagn112
Else
    Exit Sub
End If
Exit Sub
End Sub
