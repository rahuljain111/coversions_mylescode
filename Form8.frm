VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form Form8 
   BackColor       =   &H00FF8080&
   Caption         =   "AP Auto-Invoice Interface"
   ClientHeight    =   7740
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8625
   Icon            =   "Form8.frx":0000
   LinkTopic       =   "Form8"
   ScaleHeight     =   7740
   ScaleWidth      =   8625
   StartUpPosition =   1  'CenterOwner
   Visible         =   0   'False
   Begin VB.CommandButton VendorMapping 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Vendor Mapping"
      Height          =   375
      Left            =   5400
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Browse-----"
      Height          =   375
      Left            =   4680
      TabIndex        =   11
      Top             =   360
      Visible         =   0   'False
      Width           =   1215
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6480
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.ComboBox Combo2 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   3480
      TabIndex        =   1
      Top             =   960
      Width           =   1215
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1920
      TabIndex        =   0
      Top             =   960
      Width           =   1215
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   7365
      Visible         =   0   'False
      Width           =   8625
      _ExtentX        =   15214
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12118
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "3:15 PM"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Command2 
      BackColor       =   &H00FFFFFF&
      Cancel          =   -1  'True
      Caption         =   "Exit"
      Height          =   375
      Left            =   3960
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   6720
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Upload Invoices"
      Height          =   375
      Left            =   2400
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   6720
      Width           =   1575
   End
   Begin MSComctlLib.ListView ListView1 
      Height          =   4695
      Left            =   0
      TabIndex        =   5
      Top             =   1800
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   8281
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HotTracking     =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   8404992
      BackColor       =   16771515
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "TRX_NO"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "CASH ID"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "AMOUNT"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "DATE"
         Object.Width           =   2540
      EndProperty
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   2880
      TabIndex        =   12
      Top             =   6240
      Visible         =   0   'False
      Width           =   45
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   6000
      TabIndex        =   10
      Top             =   1320
      Visible         =   0   'False
      Width           =   45
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Current"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5160
      TabIndex        =   9
      Top             =   1320
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5760
      TabIndex        =   8
      Top             =   960
      Visible         =   0   'False
      Width           =   45
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   5160
      TabIndex        =   7
      Top             =   960
      Visible         =   0   'False
      Width           =   405
   End
End
Attribute VB_Name = "Form8"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    strcmd = "select count(*) as net from AP_invoices_Header where id is not null "
    strcmd = strcmd & " and status = 0 and vendor_name is not null and id >= '" & Combo1.Text & "'"
    strcmd = strcmd & " and id <= '" & Combo2.Text & "'"
    
    Set comprec = con1.Execute(strcmd)
    If Not comprec.EOF Then
        Label2.Caption = comprec!Net
    End If
    add_data
    If Not comprec.EOF Then
    MsgBox "Total Invoices Uploaded : " & Label2.Caption '"DONE"
    Else
    MsgBox "Total Invoices Uploaded : " & comprec!Net '"DONE"
    End If
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Public Sub add_data()
c = 1
Dim repcomprec As ADODB.Recordset
Dim repcomprec1 As ADODB.Recordset
c = 1
Dim comprec15 As ADODB.Recordset
ListView1.ListItems.Clear
Counter = 1
strcmd = "delete from ap_invoice_lines_interface where INVOICE_ID in (SELECT INVOICE_ID FROM AP_INVOICES_INTERFACE WHERE SOURCE = 'IMPORTED')" ' where interface_status='" & "P" & "'"
Set comprec = con.Execute(strcmd)
Set comprec = con.Execute("Delete from AP_INVOICES_INTERFACE where SOURCE = 'IMPORTED'")
Set repcomprec = New ADODB.Recordset
Set repcomprec1 = New ADODB.Recordset
Set repcomprec2 = New ADODB.Recordset

countctr = 1
strcmd = "select * from ID_Code "
Set comprec = con1.Execute(strcmd)
ctr = comprec!ID1 '9909720
ajay = 100
strcmd = "select * from ID_Code "
Set comprec = con1.Execute(strcmd)
ctr1 = comprec!ID2 '9909720


'If Option1.Value = True Then
    strcmd = "select distinct id from AP_invoices_Header where id is not null "
    strcmd = strcmd & " and status = 0 and vendor_name is not null and id >= '" & Combo1.Text & "'"
    strcmd = strcmd & " and id <= '" & Combo2.Text & "'"
    'strcmd = strcmd & " and id=9"
    'strcmd = strcmd & " AND vdate >    =Convert(DateTime, '" & "28-12-2005" & "', 103)"
    'strcmd = strcmd & " AND vdate <=Convert(DateTime, '" & "31-12-2005" & "', 103)"
'Else
    'strcmd = "select * from invoice where vdate >=Convert(DateTime, '" & DTPicker2.Value & "', 103)"
    'strcmd = strcmd & " AND vDate <=Convert(DateTime, '" & DTPicker3.Value & "', 103)"
'End If
Set comprec1 = con1.Execute(strcmd)
If Not comprec1.EOF Then
    Set repcomprec1 = New ADODB.Recordset
    repcomprec1.Open "AP_INVOICES_INTERFACE", con, adOpenDynamic, adLockOptimistic
    Set repcomprec2 = New ADODB.Recordset
    repcomprec2.Open "ap_invoice_lines_interface", con, adOpenDynamic, adLockOptimistic
    Do While Not comprec1.EOF
            ctr = ctr + 1
            ajay = ajay + 1
            ctr1 = ctr1 + 1
            DoEvents
            'Status.Show
            lbl.Caption = "Generating Id Number :--> " & comprec1!Id
            DoEvents
           
             If Not comprec1.EOF Then
                repcomprec1.AddNew
                strcmd = "select * from AP_invoices_Header where id='" & comprec1!Id & "'"
                Set comprec3 = con1.Execute(strcmd)
                If Not comprec3.EOF Then
                    'c = c + 1
                    'Label4.Caption = c
                    ListView1.Refresh
                    l_amt = comprec3!INV_AMOUNT
                    repcomprec1!INVOICE_ID = ctr
                    repcomprec1!invoice_num = Mid(comprec3!invoice_no, 1, Len(comprec3!invoice_no) - 1)  '-& "C-" & ajay
                    
                    repcomprec1!VOUCHER_NUM = comprec3!Voucher_No
                    
                    repcomprec1!Source = "IMPORTED"
                    repcomprec1!Vendor_Name = comprec3!Vendor_Name
                    'repcomprec1!gl_date = comprec3!inv_DATE
                    repcomprec1!gl_DATE = comprec3!gl_DATE
                    repcomprec1!ORG_ID = 101
                    repcomprec1!INVOICE_CURRENCY_CODE = "INR"
                    repcomprec1!vendor_site_code = Trim(comprec3!Site)
                    repcomprec1!invoice_AMOUNT = comprec3!INV_AMOUNT
                    repcomprec1!TERMS_ID = comprec3!TERMS_NAME
                    repcomprec1!invoice_type_lookup_code = Trim(comprec3!Invoice_Type)
                    repcomprec1!INVOICE_DATE = comprec3!inv_DATE
                    repcomprec1!Description = Left(comprec3!Description, 100)
                    
                    'added by rahul on 30-Aug-2017
                    repcomprec1!ATTRIBUTE_CATEGORY = comprec3!Context
                    repcomprec1!ATTRIBUTE4 = comprec3!GSTRegistration
                    repcomprec1!ATTRIBUTE5 = comprec3!GSTClassification
                    repcomprec1!ATTRIBUTE6 = comprec3!VStateCode 'renamed column from GSTState to VStateCode
                    repcomprec1!ATTRIBUTE7 = comprec3!product
                    repcomprec1!ATTRIBUTE8 = comprec3!CStateCode 'renamed column from StateCode to CStateCode
                    If comprec3!PeriodFrom <> "" Then
                        repcomprec1!ATTRIBUTE11 = UCase(Format(comprec3!PeriodFrom, "dd-MMM-yyyy"))  'added new column
                    End If
                    If comprec3!PeriodTo <> "" Then
                        repcomprec1!ATTRIBUTE12 = UCase(Format(comprec3!PeriodTo, "dd-MMM-yyyy")) 'added new column
                    End If
                    repcomprec1!ATTRIBUTE9 = comprec3!GSTDescription 'added new column
                    
                    'repcomprec1!ACCTS_PAY_CODE_COMBINATION_ID = "CHECK"
                    repcomprec1.Update
                End If
                
                    'gldate = comprec3!inv_DATE
                    gldate = comprec3!gl_DATE
                    strcmd = "select * from ap_invoices_Lines where id='" & comprec1!Id & "'"
                    Set comprec2 = con1.Execute(strcmd)
                    If Not comprec2.EOF Then
                        strcmd = "select * from new_car where vehicle_no='" & comprec2!car_no & "'"
                        Set newcomprec = con1.Execute(strcmd)
                            If Not newcomprec.EOF Then
                                new_car = newcomprec!car_no
                            Else
                                new_car = "000000"
                            End If
                    
                            'new_car = "Common"
                        new_car = comprec2!car_no
                        If comprec2!car_no = "0" Then
                            new_car = "000000"
                        End If
                        Do While Not comprec2.EOF
                      
                        '****************Location Code Combination*****************
                        strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                        strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID=1009931" '1008036"
                        strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                        strcmd2 = strcmd2 & " AND FVTB.Description='" & Trim(comprec2!branch_name) & "'"
                        Set newcomprec = con.Execute(strcmd2)
                        'If comprec2!branch_name = "Jaipur Hub" Then
                         '   MsgBox ""
                        'End If
                        'if product is drive from the system
                        '****************Product Code Combination*****************
                        strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                        strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009934" '1008038"
                        strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                        strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec2!product & "'"
                        Set comprec5 = con.Execute(strcmd2)
                        
                        '****************Car Code Combination*****************
                        strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                        strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009932" '1008037"
                        strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                        strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec2!car_no & "'"
                        Set comprec4 = con.Execute(strcmd2)
                        
                        '***************Natural Account Code Combination************
                        strcmd2 = " Select DISTINCT FVB.FLEX_VALUE From FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB "
                        strcmd2 = strcmd2 & " Where  FVB.FLEX_VALUE_SET_ID = 1009933"
                        strcmd2 = strcmd2 & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
                        strcmd2 = strcmd2 & " AND FVTB.Description='" & comprec2!NATURAL_ACCOUNT & "'"
                        Set comprec6 = con.Execute(strcmd2)
                                
                        'If Not newcomprec.EOF Then
                        '    'branch = newcomprec!FLEX_VALUE
                        'End If
                                
                        branch = comprec2!Location
                        
                        If Not comprec4.EOF Then
                            'new_car = comprec4!FLEX_VALUE
                            new_car = comprec2!car_no
                        End If
                        new_car = comprec2!car_no
                        
                        If comprec2!car_no = "0" Then
                            new_car = "000000"
                        End If
                        
                        'If Not comprec6.EOF Then
                        '    'NATURAL_ACCOUNT = comprec6!FLEX_VALUE
                        'End If
                        
                        NATURAL_ACCOUNT = comprec2!NATURAL_ACCOUNT
                        
                        If Not comprec5.EOF Then
                            product = comprec5!FLEX_VALUE
                        End If
                        
                        strcmd2 = "SELECT SUM(AMOUNT) as h_amt  from ap_invoices_Lines where id='" & comprec1!Id & "'"
                        Set comprec3 = con1.Execute(strcmd2)
                        If Not comprec3.EOF Then
                            If Val(l_amt) = Val(comprec3!h_amt) Then
                            Else
                                MsgBox "Amount Problem"
                                Exit Sub
                            End If
                        End If
                        '----------------------
                        site_Code = branch & "." & new_car & "." & NATURAL_ACCOUNT & "." & product
                        If Not comprec4.EOF Then
                            new_car = comprec4!FLEX_VALUE
                        End If
                        'MsgBox site_code
                        '------------------------------------------
                        repcomprec2.AddNew
                        repcomprec2!INVOICE_ID = ctr
                        repcomprec2!INVOICE_LINE_ID = ctr1
                        repcomprec2!LINE_NUMBER = Counter  'INVOICE WHICH SPECIFIES THE INVOICE LINES
                        repcomprec2!LINE_TYPE_LOOKUP_CODE = "ITEM"
                        repcomprec2!Amount = comprec2!Amount
                        repcomprec2!accounting_date = gldate
                        'repcomprec2!accounting_date = accountingdate
                        repcomprec2!Description = Left(comprec2!Description, 100)
                        repcomprec2!DIST_CODE_CONCATENATED = Trim(site_Code)
                        'repcomprec2!DIST_CODE_CONCATENATED = Trim(comprec2!SITE_CODE) & "." & Trim(comprec2!car_no) & "." & Trim(comprec2!NA_CODE) & "." & Trim(comprec2!P_CODE)
                        repcomprec2!ORG_ID = 101
                        
                        repcomprec2!ATTRIBUTE_CATEGORY = "India Distributions"
                        
                        If comprec2!tax_id = 0 Then
                        Else
                             repcomprec2!ATTRIBUTE1 = comprec2!tax_id
                        End If
                        repcomprec2!USSGL_TRANSACTION_CODE = comprec2!BookingID 'code added by rahul on 20-Oct-2015
                        
                        'added by rahul on 30-Aug-2017
                        repcomprec2!ATTRIBUTE4 = comprec2!LineType
                        repcomprec2!ATTRIBUTE5 = comprec2!HSN_SAC
                        repcomprec2!ATTRIBUTE6 = comprec2!VendorName 'changed values in attribute from GSTTax to VendorName
                        repcomprec2!ATTRIBUTE7 = comprec2!InvoiceNo 'added new attribute
                        repcomprec2!ATTRIBUTE8 = comprec2!ServiceType
                        repcomprec2!ATTRIBUTE9 = comprec2!SupplyType 'added new attribute
                        repcomprec2!ATTRIBUTE10 = comprec2!GSTTax 'changed from attribute 6 to 10
                        repcomprec2!ATTRIBUTE11 = comprec2!Goods_Service 'added new attribute
                        repcomprec2!ATTRIBUTE12 = comprec2!CessRate 'added new attribute
                        repcomprec2!ATTRIBUTE13 = comprec2!Grouping 'added new attribute
                        If comprec2!InvoiceDate <> "" Then
                            repcomprec2!ATTRIBUTE14 = UCase(Format(comprec2!InvoiceDate, "dd-MMM-yyyy"))  'added new attribute
                        End If
                        repcomprec2!ATTRIBUTE15 = comprec2!VendorGSTIN 'added new attribute
                        
                        ctr1 = ctr1 + 1
                        Counter = Counter + 1
                        repcomprec2.Update
                        comprec2.MoveNext
                        site_Code = 0
                        'l_amt = 0
                        Loop
            End If
                        'Unload Status
        End If
               
                        comprec1.MoveNext
                        Counter = 1
  Loop
                        strcmd = "update id_code set id='" & ctr & "'"
                        Set repcomprec = con1.Execute(strcmd)
        
                        strcmd = "update id_code set id1='" & ctr1 & "'"
                        Set repcomprec = con1.Execute(strcmd)
End If
'MsgBox "DONE"
End Sub



Private Sub Form_Load()
    'Me.Left = frmmain.ListView1.Width + 2000
    'Me.Height = Screen.Height - 100
    'Me.Top = frmmain.ListView1.Height - 7500
    Combo1.Clear
    Combo2.Clear
    For i = 1 To 10000
        Combo1.AddItem i
        Combo2.AddItem i
    Next
    Combo1.Text = Combo1.List(0)
    Combo2.Text = Combo2.List(0)
End Sub


Public Sub VendorDetails()
        cryctrl.WindowTitle = "Vendor Mapping REPORT"
        cryctrl.ReportFileName = App.Path & "\reportS\vendorMapping.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                     .Action = 1
                End With
End Sub

Private Sub VendorMapping_Click()

    a = MsgBox("WANT TO PRINT THE REPORT" & Code & "(Y/N?)", vbYesNo, "PRINT")
    If a = vbYes Then
        VendorDetails
    Else
        Exit Sub
    End If

End Sub
