VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Begin VB.Form Form4 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   ClientHeight    =   7800
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8175
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   8175
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   495
      Left            =   3720
      TabIndex        =   13
      Top             =   5640
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      BackColor       =   &H00C0FFFF&
      Cancel          =   -1  'True
      Caption         =   "&Exit"
      Height          =   375
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   6840
      Width           =   1335
   End
   Begin VB.Frame Frame10 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      Caption         =   "Location Wise Debtors Aging"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   3495
      Left            =   2040
      TabIndex        =   0
      Top             =   1320
      Width           =   4695
      Begin VB.ComboBox Combo5 
         BackColor       =   &H00C0FFFF&
         Height          =   315
         Left            =   1320
         TabIndex        =   6
         Top             =   1920
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00000000&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   855
         Index           =   4
         Left            =   480
         TabIndex        =   1
         Top             =   360
         Width           =   3975
         Begin MSComCtl2.DTPicker DTPicker8 
            Height          =   330
            Left            =   2520
            TabIndex        =   2
            Top             =   240
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   20709379
            CurrentDate     =   37865
         End
         Begin MSComCtl2.DTPicker DTPicker7 
            Height          =   330
            Left            =   840
            TabIndex        =   3
            Top             =   240
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   582
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            CalendarBackColor=   16777215
            CustomFormat    =   "dd-MM-yyyy"
            Format          =   20709379
            CurrentDate     =   37865
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "From"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   9
            Left            =   120
            TabIndex        =   5
            Top             =   300
            Width           =   360
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00E0E0E0&
            BackStyle       =   0  'Transparent
            Caption         =   "To"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H0080FFFF&
            Height          =   210
            Index           =   10
            Left            =   2265
            TabIndex        =   4
            Top             =   300
            Width           =   180
         End
      End
      Begin MSComCtl2.DTPicker DTPicker9 
         Height          =   330
         Left            =   1320
         TabIndex        =   7
         Top             =   1320
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   16777215
         CustomFormat    =   "dd-MM-yyyy"
         Format          =   20709379
         CurrentDate     =   37865
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Location"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   13
         Left            =   360
         TabIndex        =   9
         Top             =   1920
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "Cut Off Date"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   210
         Index           =   14
         Left            =   120
         TabIndex        =   8
         Top             =   1320
         Width           =   900
      End
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   240
      Top             =   1560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton command1 
      BackColor       =   &H00C0FFFF&
      Caption         =   "&Preview"
      Height          =   375
      Left            =   2760
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   6840
      Width           =   1695
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   2520
      TabIndex        =   12
      Top             =   5040
      Width           =   45
   End
End
Attribute VB_Name = "Form4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Command11_Click()

End Sub

Private Sub Debtors_Click()

End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub command1_Click()
'Dim k As New cExcelEB
'Dim oExcelApplication As Excel.Application
'Dim oExcelWorkbook As Excel.Workbook
'Dim o As New cExcelEB
'Dim sFileName As String
'sFileName = App.Path & "\" & "aging.xls"
'With o
'
'    If .StartFile(oExcelApplication, oExcelWorkbook, sFileName, False) Then
'
'    End If
'End With
'add_new_aging
MAIN_AGING
a = MsgBox("Want to Print the aging as on " & DTPicker9.Value & " (Y/N)?", vbYesNo, "Print")
If a = vbYes Then
    prnagn
End If
'a = MsgBox("Want to Export the aging (Y/N)?", vbYesNo, "Export to Excel")

'If a = vbYes Then
 '    a = Shell(App.Path & "\" & "aging.xls", 3)
'End If
End Sub


Public Sub AGING()
Set comprec = con1.Execute("truncate table Aging")
Set comprec = con1.Execute("truncate table unapplied_amount")
Dim newcomprec As ADODB.Recordset
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")
strcmd = "SELECT Distinct cc.SEGMENT1 Location,hp.PARTY_NAME,cc.SEGMENT4 product,RCT.TRX_DATE,"
strcmd = strcmd & " RCT.TRX_NUMBER INVOICE_NUM,RCT.PURCHASE_ORDER USER_NAME,"
strcmd = strcmd & " RCT.CT_REFERENCE DS_NUM,APS.AMOUNT_DUE_ORIGINAL ORIGINAL_AMOUNT,APS.AMOUNT_DUE_REMAINING BALANCE_AMOUNT_DUE"
strcmd = strcmd & " From RA_CUSTOMER_TRX_ALL RCT,HZ_CUST_ACCOUNTS HCA,HZ_PARTIES HP,HZ_CUST_SITE_USES_ALL HCSU,"
strcmd = strcmd & " AR_PAYMENT_SCHEDULES_ALL APS,"
strcmd = strcmd & " ra_cust_trx_line_gl_dist_all g,gl_code_combinations cc"
strcmd = strcmd & " Where RCT.CUSTOMER_TRX_ID = APS.CUSTOMER_TRX_ID "
strcmd = strcmd & " and rct.CUSTOMER_TRX_ID = g.CUSTOMER_TRX_ID "
strcmd = strcmd & " and cc.CODE_COMBINATION_ID = g.CODE_COMBINATION_ID"
strcmd = strcmd & " AND APS.AMOUNT_LINE_ITEMS_REMAINING <> 0"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID AND HCSU.BILL_TO_SITE_USE_ID = RCT.BILL_TO_SITE_USE_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID"
'STRCMD = STRCMD & " and APS.AMOUNT_DUE_REMAINING > 0"
strcmd = strcmd & " and account_class = 'REC'"
strcmd = strcmd & " and cc.segment1 ='" & Combo5.Text & "'"
'strcmd = strcmd & " and RCT.TRX_DATE >='" & p_start_date & "'"
'strcmd = strcmd & " AND RCT.TRX_DATE <='" & p_end_date & "'"
strcmd = strcmd & "  order by cc.segment1,hp.PARTY_NAME"

Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "aging", con1, adOpenKeyset, adLockPessimistic
Set newcomprec = con.Execute(strcmd)
If Not newcomprec.EOF Then
    
    lbl.Caption = "Loading Aging ..........."
    Do While Not newcomprec.EOF
       a1 = newcomprec!TRX_DATE
       a2 = DTPicker9.Value
       total_days = DateDiff("d", a1, a2)
       'MsgBox total_days
        repcomprec.AddNew
       
       If total_days >= 0 And total_days <= 30 Then
            netamt = newcomprec!BALANCE_AMOUNT_DUE
            repcomprec!up_30 = netamt
            repcomprec!up_60 = 0
            repcomprec!up_90 = 0
            repcomprec!up_180 = 0
            repcomprec!MORE_180 = 0
       End If
       
       If total_days > 30 And total_days <= 60 Then
              netamt = newcomprec!BALANCE_AMOUNT_DUE
              repcomprec!up_30 = 0
              repcomprec!up_60 = netamt
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
       
       If total_days > 60 And total_days <= 90 Then
              netamt = newcomprec!BALANCE_AMOUNT_DUE
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = netamt
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 91 And total_days <= 180 Then
              netamt = newcomprec!BALANCE_AMOUNT_DUE
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = netamt
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 181 Then
              netamt = newcomprec!BALANCE_AMOUNT_DUE
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = netamt
       End If
      
       repcomprec!Location = newcomprec!Location
       repcomprec!customer = newcomprec!PARTY_NAME
       repcomprec!TRX_DATE = newcomprec!TRX_DATE
       repcomprec!CUT_DATE = DTPicker9.Value
       'repcomprec!INVOICE_NUM = newcomprec!INVOICE_NUM
       'repcomprec!user_name = newcomprec!user_name
       'repcomprec!DS_NUM = newcomprec!DS_NUM
       'repcomprec!ORIGINAL_AMOUNT = newcomprec!ORIGINAL_AMOUNT
       'repcomprec!BALANCE_AMOUNT_DUE = newcomprec!BALANCE_AMOUNT_DUE
        repcomprec.UpdateBatch
        lbl.Caption = "Loading ..................!" & newcomprec!INVOICE_NUM
        newcomprec.MoveNext
    Loop
   
Else
'Exit Sub
End If
MsgBox "Data for Outstanding !", vbCritical, "Error"
End Sub


Public Sub unapplied_Aging()
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(82);"
strcmd = strcmd & " end;"
Set comprec1 = con.Execute(strcmd)
DT = Format(DTPicker9.Value, "dd-MMM-yyyy")
strcmd = "select customer_name, cc.segment1,"
strcmd = strcmd & " sum((r.AMOUNT - r.APPLIED_AMOUNT)) balance"
strcmd = strcmd & " from ar_cash_receipts_v r,"
strcmd = strcmd & " ar_cash_receipt_history_all arc,"
strcmd = strcmd & " gl_code_combinations cc"
strcmd = strcmd & " Where r.REVERSAL_Comments Is Null"
strcmd = strcmd & " and r.CASH_RECEIPT_ID = arc.CASH_RECEIPT_ID"
strcmd = strcmd & " and r.RECEIPT_STATUS != 'REV'"
strcmd = strcmd & " and (r.AMOUNT - r.APPLIED_AMOUNT) != 0"
strcmd = strcmd & " and cc.CODE_COMBINATION_ID = arc.ACCOUNT_CODE_COMBINATION_ID"
strcmd = strcmd & " and cc.segment1='" & Combo5.Text & "'"
strcmd = strcmd & " and r.receipt_DATE <='" & DT & "'"
'strcmd = strcmd & " and r.receipt_DATE >='" & p_start_date & "'"
'strcmd = strcmd & " AND r.receipt_DATE <='" & p_end_date & "'"
strcmd = strcmd & " group  by CUSTOMER_NAME,cc.segment1"
'strcmd = strcmd & " order by cc.segment1,CUSTOMER_NAME,cc.segment4"
Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    lbl.Caption = "Loading Unapplied ..........."
'    repcomprec.Open "Aging", con1, adOpenKeyset, adLockPessimistic
    Do While Not comprec.EOF
'    strcmd = "select sum((r.AMOUNT - r.APPLIED_AMOUNT)) balance"
'    strcmd = strcmd & " from ar_cash_receipts_v r,"
'    strcmd = strcmd & " ar_cash_receipt_history_all arc,"
'    strcmd = strcmd & " gl_code_combinations cc"
'    strcmd = strcmd & " Where r.REVERSAL_Comments Is Null"
'    strcmd = strcmd & " and r.CASH_RECEIPT_ID = arc.CASH_RECEIPT_ID"
'    strcmd = strcmd & " and r.RECEIPT_STATUS != 'REV'"
'    strcmd = strcmd & " and (r.AMOUNT - r.APPLIED_AMOUNT) != 0"
'    strcmd = strcmd & " and cc.CODE_COMBINATION_ID = arc.ACCOUNT_CODE_COMBINATION_ID"
'    strcmd = strcmd & " and cc.segment1='" & Combo5.Text & "'"
'    strcmd = strcmd & " and r.receipt_DATE >='" & p_start_date & "'"
'    strcmd = strcmd & " AND r.receipt_DATE <='" & p_end_date & "'"
'    strcmd = strcmd & " AND customer_name='" & comprec!customer_name & "'"
    
    'Set comprec2 = con.Execute(strcmd)
    'If Not comprec2.EOF Then
     '   strcmd = "select * from aging where customer='" & comprec!customer_name & "'"
      '  Set comprec3 = New ADODB.Recordset
       ' comprec3.Open strcmd, con1, adOpenKeyset, adLockPessimistic
        'If Not comprec3.EOF Then
        Set comprec4 = New ADODB.Recordset
        comprec4.Open "Unapplied_Amount", con1, adOpenKeyset, adLockPessimistic
        comprec4.AddNew
        comprec4!AMOUNT = comprec!balance
        comprec4!customer = comprec!customer_name
        comprec4!Location = comprec!segment1
        comprec4.Update
        'Do While Not comprec3.EOF
        'strcmd = "select * from aging where customer='" & comprec!customer_name & "'"
        'comprec4.Open strcmd, con1, adOpenKeyset, adLockPessimistic
        'comprec3!unapplied_balance = comprec2!balance
        'comprec3.MoveNext
        'Loop
        'comprec3.UpdateBatch
'    End If
'    StatusBar1.Panels (1) ' = newcomprec!receipt_number
    comprec.MoveNext
    Loop
End If

strcmd = "SELECT * From Unapplied_Amount WHERE customer NOT IN (SELECT customer FROM aging)"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Set comprec1 = New ADODB.Recordset
        comprec1.Open "Aging", con1, adOpenKeyset, adLockPessimistic
        comprec1.AddNew
        comprec1!customer = comprec!customer
        comprec1!Location = comprec!Location
        comprec1!up_30 = 0
        comprec1!up_60 = 0
        comprec1!up_90 = 0
        comprec1!up_180 = 0
        comprec1!MORE_180 = 0
        comprec1.Update
        comprec.MoveNext
    Loop
End If
MsgBox "Data For Unapplied Completed !", vbCritical, "Error"
End Sub


Public Sub prnagn()
        cryctrl.WindowTitle = "Location Wise " & "Customer Wise Outstanding Report"
        cryctrl.ReportFileName = App.Path & "\reportS\Outstanding.rpt"
                     With cryctrl
                     '.PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub



Private Sub Command3_Click()
ageme
End Sub

Private Sub Form_Load()
DTPicker7.Value = "01-09-2000"
DTPicker8.Value = "31-03-2006"
DTPicker9.Value = "31-03-2006"
'ADDloc
Me.Left = frmmain.ListView1.Width + 2000
Me.Top = frmmain.ListView1.Height - 7500
msAppPath = AddBackslash(App.Path)
End Sub

'Public Sub ADDloc()
''Combo4.Clear
'Combo5.Clear
'strcmd = "select f1.flex_valuE,f2.description from fnd_flex_values f1 ,fnd_flex_values_tl f2"
'strcmd = strcmd & " Where f1.flex_value_id = f2.flex_value_id"
'strcmd = strcmd & " and f1.flex_value_set_id='" & 1008074 & "'"
'strcmd = strcmd & " order by  f1.flex_valuE"
'Set comprec = con.Execute(strcmd)
'If Not comprec.EOF Then
'    Do While Not comprec.EOF
'        Combo5.AddItem comprec!FLEX_VALUE
'        'Combo4.AddItem comprec!FLEX_VALUE
'        comprec.MoveNext
'    Loop
'End If
'End Sub

Public Sub MAIN_AGING()
Set comprec = con1.Execute("truncate table Aging")
'Set comprec = con1.Execute("truncate table unapplied_amount")
Dim newcomprec As ADODB.Recordset
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")

strcmd = "select * from agn where invoice_date >=Convert(DateTime, '" & DTPicker7.Value & "', 103)"
strcmd = strcmd & " and invoice_date <=Convert(DateTime, '" & DTPicker8.Value & "', 103)"
Set comprec = con1.Execute(strcmd)
If Not comprec.EOF Then

Set repcomprec = New ADODB.Recordset
repcomprec.Open "aging", con1, adOpenKeyset, adLockPessimistic
Set newcomprec = con1.Execute(strcmd)
'If Not newcomprec.EOF Then
    lbl.Caption = "Loading Aging ..........."
    Do While Not comprec.EOF
        
       Dim d1 As Date
       a1 = comprec!Invoice_date
       a2 = DTPicker9.Value
       total_days = DateDiff("d", a1, a2)
       repcomprec.AddNew
       If total_days >= 0 And total_days <= 30 Then
            If comprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = comprec!BALANCE_AMOUNT_DUE
            End If
            repcomprec!up_30 = netamt
            repcomprec!up_60 = 0
            repcomprec!up_90 = 0
            repcomprec!up_180 = 0
            repcomprec!MORE_180 = 0
       End If
       If total_days > 30 And total_days <= 60 Then
              If comprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = comprec!BALANCE_AMOUNT_DUE
               End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = netamt
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
       
       If total_days > 60 And total_days <= 90 Then
              If comprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = comprec!BALANCE_AMOUNT_DUE
               End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = netamt
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 91 And total_days <= 180 Then
              If comprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = comprec!BALANCE_AMOUNT_DUE
               End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = netamt
              repcomprec!MORE_180 = 0
       End If
              
       If total_days >= 181 Then
              If comprec!Class = "PMT" Then
                netamt = 0
              Else
                netamt = comprec!BALANCE_AMOUNT_DUE
              End If
              repcomprec!up_30 = 0
              repcomprec!up_60 = 0
              repcomprec!up_90 = 0
              repcomprec!up_180 = 0
              repcomprec!MORE_180 = netamt
       End If
      
       repcomprec!Location = comprec!Location
       repcomprec!customer = comprec!PARTY_NAME
       
       repcomprec!CUT_DATE = DTPicker9.Value
       
       If comprec!Class = "PMT" Then
            repcomprec!TRX_NUMBER = comprec!RECEIPT_NUMBER
            repcomprec!TRX_DATE = comprec!RECEIPT_DATE
            repcomprec!unapplied_balance = comprec!UNAPPLIED_AMOUNT
       Else
            repcomprec!TRX_NUMBER = comprec!invoice_number
            repcomprec!TRX_DATE = comprec!Invoice_date
            repcomprec!unapplied_balance = 0
       End If
       repcomprec!CLASS_name = comprec!Class
       'repcomprec!Status = newcomprec!Status
       repcomprec!total_days = total_days
       repcomprec.UpdateBatch
       lbl.Caption = "Loading ..................!" & comprec!invoice_number
       comprec.MoveNext
    Loop
   
Else
End If
'End If
'Unload Status
End Sub

Private Function DoStartFile() As Boolean
'    Dim oExcelApplication As Object
'    Dim oExcelWorkbook As Object
'    Dim o As New cExcelLB
'    Dim sFileName As String
'
'    sFileName = Me.Text2.Text
'    With o
'        If .StartFile(oExcelApplication, oExcelWorkbook, sFileName, False) Then
'            DoStartFile = True
'        End If
'    End With
End Function

Public Sub add_new_aging()
Set comprec = con1.Execute("truncate table outstanding")
'Set comprec = con1.Execute("truncate table unapplied_amount")
Dim newcomprec As ADODB.Recordset
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker8.Value, "dd-mmm-yyyy")

strcmd = "select  r.customer_name, amount_due_remaining,trx_date,gl_date,trx_number from ar_payment_schedules_all h,ra_customers r"
strcmd = strcmd & " Where r.customer_id = h.customer_id"
strcmd = strcmd & " AND h.gl_date <='" & p_end_date & "'"

Set newcomprec = New ADODB.Recordset
Set repcomprec = New ADODB.Recordset
repcomprec.Open "Outstanding", con1, adOpenKeyset, adLockPessimistic
Set newcomprec = con.Execute(strcmd)
If Not newcomprec.EOF Then
'    Status.Show
    lbl.Caption = "Loading Outstanding ..........."
    Do While Not newcomprec.EOF
    repcomprec.AddNew
'   repcomprec!Location = newcomprec!Location
    repcomprec!PARTY_NAME = newcomprec!customer_name
    repcomprec!TRX_DATE = newcomprec!TRX_DATE
    repcomprec!gl_date = newcomprec!gl_date
    repcomprec!INVOICE_NUM = newcomprec!TRX_NUMBER
    'repcomprec!user_name = newcomprec!user_name
    'repcomprec!DS_NUM = newcomprec!DS_NUM
    'repcomprec!ORIGINAL_AMOUNT = newcomprec!ORIGINAL_AMOUNT
    
    repcomprec!BALANCE_AMOUNT_DUE = newcomprec!AMOUNT_DUE_REMAINING
    lbl.Caption = "Loading ..................!" & newcomprec!TRX_NUMBER
    newcomprec.MoveNext
    Loop
    repcomprec.UpdateBatch
Else
'MsgBox "No Data Is There For Outstanding !", vbCritical, "Error"
'Exit Sub
End If
End Sub

Public Sub ageme()
strcmd = "SELECT HP.PARTY_NAME ,'Invoice' ,"
strcmd = strcmd & " FVTA.DESCRIPTION,RCT.TRX_DATE,"
strcmd = strcmd & " RCT.TRX_NUMBER,RCT.PURCHASE_ORDER ,RCT.CT_REFERENCE ,"
strcmd = strcmd & " APS.AMOUNT_DUE_ORIGINAL,APS.AMOUNT_DUE_REMAINING,"
strcmd = strcmd & " NULL RECEIPT_DATE,NULL RECEIPT_NUMBER,NULL RECEIPT_AMOUNT,NULL UNAPPLIED_AMOUNT"
strcmd = strcmd & " From RA_CUSTOMER_TRX_ALL RCT,RA_CUST_TRX_LINE_GL_DIST_ALL RCTLGD,"
strcmd = strcmd & " GL_CODE_COMBINATIONS GCC,FND_FLEX_VALUES FVA,FND_FLEX_VALUES_TL FVTA,"
strcmd = strcmd & " FND_FLEX_VALUES FVB,FND_FLEX_VALUES_TL FVTB,FND_FLEX_VALUES FVC,FND_FLEX_VALUES_TL FVTC,"
strcmd = strcmd & " FND_FLEX_VALUES FVD,FND_FLEX_VALUES_TL FVTD,HZ_CUST_ACCOUNTS HCA,"
strcmd = strcmd & " HZ_PARTIES HP,AR_PAYMENT_SCHEDULES_ALL APS,RA_TERMS_TL RT"
strcmd = strcmd & " Where RCT.CUSTOMER_TRX_ID = APS.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND APS.AMOUNT_DUE_REMAINING <> 0"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = RCT.BILL_TO_CUSTOMER_ID"
'AND HP.PARTY_NAME = NVL(:P_CUSTOMER_NAME, HP.PARTY_NAME)
'AND FVA.FLEX_VALUE BETWEEN :P_LOCATION_LOW AND :P_LOCATION_HIGH
'AND FVD.FLEX_VALUE = NVL(:P_PRODUCT, FVD.FLEX_VALUE)
'AND RCT.TRX_DATE BETWEEN :P_START_DATE AND :P_END_DATE
strcmd = strcmd & " AND RT.TERM_ID = RCT.TERM_ID"
strcmd = strcmd & " and rct.status_trx != 'CL'"
strcmd = strcmd & " AND RCT.CUSTOMER_TRX_ID = RCTLGD.CUSTOMER_TRX_ID"
strcmd = strcmd & " AND GCC.CODE_COMBINATION_ID = RCTLGD.CODE_COMBINATION_ID"
strcmd = strcmd & " AND RCTLGD.ACCOUNT_CLASS = 'REC'"
'AND RT.NAME = NVL(:P_TERMS_NAME, RT.NAME)
strcmd = strcmd & " AND FVA.FLEX_VALUE_SET_ID = '1008074'"
strcmd = strcmd & " AND FVB.FLEX_VALUE_SET_ID = '1008075'"
strcmd = strcmd & " AND FVC.FLEX_VALUE_SET_ID = '1008076'"
strcmd = strcmd & " AND FVD.FLEX_VALUE_SET_ID = '1008077'"
strcmd = strcmd & " AND FVA.FLEX_VALUE = GCC.SEGMENT1"
strcmd = strcmd & " AND FVB.FLEX_VALUE = GCC.SEGMENT2"
strcmd = strcmd & " AND FVC.FLEX_VALUE = GCC.SEGMENT3"
strcmd = strcmd & " AND FVD.FLEX_VALUE = GCC.SEGMENT4"
strcmd = strcmd & " AND FVA.FLEX_VALUE_ID = FVTA.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVC.FLEX_VALUE_ID = FVTC.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVD.FLEX_VALUE_ID = FVTD.FLEX_VALUE_ID"
strcmd = strcmd & " Union"
strcmd = strcmd & " SELECT HP.PARTY_NAME,"
'Receipt',
strcmd = strcmd & " FVTA.DESCRIPTION LOCATION,NULL TRX_DATE,NULL INVOICE_NUM,"
strcmd = strcmd & " NULL USER_NAME,NULL DS_NUM,NULL ORIGINAL_AMOUNT,NULL BALANCE_AMOUNT,ACR.RECEIPT_DATE ,"
strcmd = strcmd & " ACR.RECEIPT_NUMBER,ACR.AMOUNT,TO_NUMBER(DECODE(ACR.TYPE, 'MISC', NULL, NVL(SUM(DECODE(ARA.STATUS, 'UNAPP', NVL(ARA.AMOUNT_APPLIED, 0), 0)), 0))) UNAPPLIED_AMOUNT"
strcmd = strcmd & " From AR_CASH_RECEIPTS_ALL ACR,AR_RECEIVABLE_APPLICATIONS_ALL ARA,"
strcmd = strcmd & " AR_CASH_RECEIPT_HISTORY_ALL ACRH,"
strcmd = strcmd & " GL_CODE_COMBINATIONS GCC,"
strcmd = strcmd & " FND_FLEX_VALUES FVA,"
strcmd = strcmd & " FND_FLEX_VALUES_TL FVTA,"
strcmd = strcmd & " FND_FLEX_VALUES FVB,"
strcmd = strcmd & " FND_FLEX_VALUES_TL FVTB,FND_FLEX_VALUES FVC,"
strcmd = strcmd & " FND_FLEX_VALUES_TL FVTC,FND_FLEX_VALUES FVD,"
strcmd = strcmd & " FND_FLEX_VALUES_TL FVTD,HZ_CUST_ACCOUNTS HCA,"
strcmd = strcmd & " HZ_CUST_PROFILE_CLASSES HCPC,HZ_CUSTOMER_PROFILES HCP,"
strcmd = strcmd & " HZ_PARTIES HP,RA_TERMS_TL RT"
strcmd = strcmd & " WHERE ACRH.CURRENT_RECORD_FLAG = 'Y'"
strcmd = strcmd & " AND ACR.CASH_RECEIPT_ID = ACRH.CASH_RECEIPT_ID"
strcmd = strcmd & " AND ACR.CASH_RECEIPT_ID = ARA.CASH_RECEIPT_ID"
'AND ACR.STATUS &lt;&gt; 'REV'
strcmd = strcmd & " AND ARA.STATUS IN ('ACC', 'UNAPP')"
strcmd = strcmd & " AND ACRH.ACCOUNT_CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID"
strcmd = strcmd & " AND HCA.CUST_ACCOUNT_ID = ACR.PAY_FROM_CUSTOMER"
strcmd = strcmd & " AND HCA.PARTY_ID = HP.PARTY_ID"
'AND HP.PARTY_NAME = NVL(:P_CUSTOMER_NAME, HP.PARTY_NAME)
'AND FVA.FLEX_VALUE BETWEEN :P_LOCATION_LOW AND :P_LOCATION_HIGH
'AND FVD.FLEX_VALUE = NVL(:P_PRODUCT, FVD.FLEX_VALUE)
'AND ACR.RECEIPT_DATE BETWEEN :P_START_DATE AND :P_END_DATE
strcmd = strcmd & " AND RT.TERM_ID = HCPC.STANDARD_TERMS"
strcmd = strcmd & " AND HCP.SITE_USE_ID IS NULL"
strcmd = strcmd & " AND HCP.CUST_ACCOUNT_ID = HCA.CUST_ACCOUNT_ID"
strcmd = strcmd & " AND HCP.PROFILE_CLASS_ID = HCPC.PROFILE_CLASS_ID"
'AND RT.NAME = NVL(:P_TERMS_NAME, RT.NAME)
strcmd = strcmd & " AND FVA.FLEX_VALUE_SET_ID = '1008074'"
strcmd = strcmd & " AND FVB.FLEX_VALUE_SET_ID = '1008075'"
strcmd = strcmd & " AND FVC.FLEX_VALUE_SET_ID = '1008076'"
strcmd = strcmd & " AND FVD.FLEX_VALUE_SET_ID = '1008077'"
strcmd = strcmd & " AND FVA.FLEX_VALUE = GCC.SEGMENT1"
strcmd = strcmd & " AND FVB.FLEX_VALUE = GCC.SEGMENT2"
strcmd = strcmd & " AND FVC.FLEX_VALUE = GCC.SEGMENT3"
strcmd = strcmd & " AND FVD.FLEX_VALUE = GCC.SEGMENT4"
strcmd = strcmd & " AND FVA.FLEX_VALUE_ID = FVTA.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVB.FLEX_VALUE_ID = FVTB.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVC.FLEX_VALUE_ID = FVTC.FLEX_VALUE_ID"
strcmd = strcmd & " AND FVD.FLEX_VALUE_ID = FVTD.FLEX_VALUE_ID"
strcmd = strcmd & " GROUP BY HP.PARTY_NAME, FVTA.DESCRIPTION, ACR.RECEIPT_DATE, ACR.RECEIPT_NUMBER, ACR.TYPE, ACR.AMOUNT, FVTB.DESCRIPTION, FVTC.DESCRIPTION, FVTD.DESCRIPTION"

Set comprec = con.Execute(strcmd)
If Not comprec.EOF Then
    Do While Not comprec.EOF
    MsgBox "Done"
    comprec.MoveNext
    Loop
End If



End Sub
