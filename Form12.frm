VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form12 
   BackColor       =   &H00FF8080&
   Caption         =   "Customer Wise Ledger - New"
   ClientHeight    =   8160
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7125
   LinkTopic       =   "Form12"
   MDIChild        =   -1  'True
   ScaleHeight     =   8175.027
   ScaleMode       =   0  'User
   ScaleWidth      =   7125
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command6 
      BackColor       =   &H00FFFFFF&
      Caption         =   "AR Customer Ledger"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   6960
      Width           =   1935
   End
   Begin VB.CommandButton Command1 
      BackColor       =   &H00FFFFFF&
      Cancel          =   -1  'True
      Caption         =   "E&xit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3720
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   6960
      Width           =   1335
   End
   Begin VB.ComboBox Combo3 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   1920
      TabIndex        =   0
      Text            =   "Account Analysis"
      Top             =   1320
      Width           =   3735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   495
      Left            =   2760
      TabIndex        =   3
      Top             =   4560
      Width           =   2055
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   6
      Top             =   7665
      Width           =   7125
      _ExtentX        =   12568
      _ExtentY        =   873
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9472
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "4:12 PM"
         EndProperty
      EndProperty
   End
   Begin Crystal.CrystalReport cryctrl 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      PrintFileLinesPerPage=   60
   End
   Begin MSComCtl2.DTPicker DTPicker7 
      Height          =   330
      Left            =   1920
      TabIndex        =   1
      Top             =   1920
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   16777215
      CustomFormat    =   "dd-MM-yyyy"
      Format          =   16646147
      CurrentDate     =   37865
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   330
      Left            =   4320
      TabIndex        =   2
      Top             =   1920
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   16777215
      CustomFormat    =   "dd-MM-yyyy"
      Format          =   16646147
      CurrentDate     =   37865
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   0
      Left            =   3840
      TabIndex        =   9
      Top             =   1920
      Width           =   180
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "Customer"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   2
      Left            =   960
      TabIndex        =   8
      Top             =   1320
      Width           =   690
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00E0E0E0&
      BackStyle       =   0  'Transparent
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Index           =   9
      Left            =   960
      TabIndex        =   7
      Top             =   1920
      Width           =   360
   End
End
Attribute VB_Name = "Form12"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Unload Me
End Sub

Private Sub Command2_Click()
Set comprec = con1.Execute("truncate table apps_unapplied")
strcmd = " begin "
strcmd = strcmd & " dbms_application_info.set_client_info(101);"
strcmd = strcmd & " end;"
Set comprec2 = con.Execute(strcmd)
con.CommandTimeout = 1200000

p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
strcmd = " select distinct san.receipt_number,san.receipt_date,CUSTOMER_NAME,san.amount,san.cash_receipt_id"
strcmd = strcmd & " from AR_CASH_RECEIPTS_V sam ,"
strcmd = strcmd & " ar_cash_receipts_all san"
strcmd = strcmd & " Where sam.cash_receipt_id = san.cash_receipt_id"
strcmd = strcmd & "  and san.receipt_date >='" & p_start_date & "'"
strcmd = strcmd & "  and san.receipt_date <='" & p_end_date & "'"
'strcmd = strcmd & "  and san.status like 'UN%'"

'strcmd = strcmd & "  and san.receipt_number ='" & "25499" & "'"  '----Commented by Rahul
'strcmd = strcmd & "  and customer_name like '" & "ALCATEL INDIA LIMITED" & "'"
strcmd = strcmd & "  and customer_name like '" & Combo3.Text & "'" 'Line Added by Rahul
strcmd = strcmd & "  group by san.receipt_number,san.receipt_date,CUSTOMER_NAME,san.amount,san.cash_receipt_id"

Set comprec = con.Execute(strcmd)
con.CommandTimeout = 1200000
If Not comprec.EOF Then
    Do While Not comprec.EOF
    rct_amt = 0
    rct = 0
    
        strcmd = "select * from AR_CASH_RECEIPTS_V  where receipt_number = '" & comprec!receipt_number & "'"
        'strcmd = strcmd & "  and CUSTOMER_NAME <='" & comprec!CUSTOMER_NAME & "'"
        strcmd = strcmd & "  and receipt_date <='" & Format(comprec!receipt_date, "DD-MMM-YYYY") & "'"
        strcmd = strcmd & "  and cash_receipt_id ='" & comprec!cash_receipt_id & "'"
        Set comprec3 = con.Execute(strcmd)
        con.CommandTimeout = 120000
        If Not comprec.EOF Then
                R_NET = comprec3!Amount '- comprec3!applied_amount
        Else
            R_NET = 0
        End If
        
        strcmd = " begin "
        strcmd = strcmd & " dbms_application_info.set_client_info(101);"
        strcmd = strcmd & " end;"
        Set comprec2 = con.Execute(strcmd)
        con.CommandTimeout = 120000
        strcmd = "select * from AR_RECEIVABLE_APPLICATIONS_V " 'WHERE RECEIPT_date ='" & comprec!RECEIPT_date & "'"
        strcmd = strcmd & "  where receipt_number = '" & comprec!receipt_number & "'"
        strcmd = strcmd & "  and CUSTOMER_NAME <='" & comprec!CUSTOMER_NAME & "'"
        strcmd = strcmd & "  and cash_receipt_id='" & comprec!cash_receipt_id & "'"
        'strcmd = strcmd & "  and gl_date <= '" & p_start_date & "'"
        Set comprec1 = con.Execute(strcmd)
        con.CommandTimeout = 1200000
        If Not comprec1.EOF Then

        'If comprec1! <= DTPicker7.Value Then
                'R_NET = comprec3!amount
                'R_NET = comprec3!amount
        'Else
          Do While Not comprec1.EOF
                If comprec1!amount_applied < 0 Then
                    R_NET = R_NET + -(comprec1!amount_applied)
                Else
                    R_NET = R_NET - comprec1!amount_applied
                End If
            comprec1.MoveNext
           Loop
        
                R_NET = comprec3!Amount
        End If
        'End If
        Set repcomprec = New ADODB.Recordset
        repcomprec.Open "Apps_Unapplied", con1, adOpenDynamic, adLockOptimistic
        repcomprec.AddNew
        repcomprec!customer = comprec!CUSTOMER_NAME & ""
        repcomprec!RECEIPT_NO = comprec!receipt_number
        repcomprec!R_Date = comprec!receipt_date
        repcomprec!Receipt_Amount = comprec3!Amount
        repcomprec!CASH_ID = comprec!cash_receipt_id & ""
        repcomprec!Unaplied = R_NET
        repcomprec.Update
        R_NET = 0
        cust = ""
        comprec.MoveNext
    Loop
End If
MsgBox "DONE"
End Sub

Private Sub Command6_Click()
addledger
End Sub

Public Sub total()
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
Set comprec = con1.Execute("truncate table AR_LEDGER_Total")
con1.CommandTimeout = 120000
strcmd = "Select a.customer_name,B.Bill_To_Site_Use_Id, sum(d.amount) amount,sum((d.amount * NVL(b.exchange_rate,-1))) amount_other_currency,"
strcmd = strcmd & " f.type From ra_customers a, ra_customer_trx_ALL B, RA_CUST_TRX_LINE_GL_DIST_ALL D,"
strcmd = strcmd & " GL_CODE_COMBINATIONS E,RA_CUST_TRX_TYPES_ALL F,   ar_payment_schedules_all G"
strcmd = strcmd & " Where   b.bill_to_customer_id = a.customer_id AND   b.complete_flag = 'Y'"
strcmd = strcmd & " AND d.account_class = 'REC'  and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " and b.trx_date<'" & p_start_date & "'"
strcmd = strcmd & " AND  d.customer_trx_id = b.customer_trx_id"
strcmd = strcmd & " AND e.code_combination_id = d.code_combination_id"
strcmd = strcmd & " AND f.cust_trx_type_id =b.cust_trx_type_id AND  f.type in ('INV','CM','DM','DEP')"
strcmd = strcmd & " AND d.latest_rec_flag = 'Y' AND g.customer_trx_id= b.customer_trx_id"
strcmd = strcmd & " and g.payment_schedule_id in (select min(payment_schedule_id)"
strcmd = strcmd & " from   ar_payment_schedules_all where  customer_trx_id = g.customer_trx_id)"
strcmd = strcmd & " group by a.customer_name,B.Bill_To_Site_Use_Id, f.type "
strcmd = strcmd & " Union"
strcmd = strcmd & " Select a.customer_name ,aa1.Bill_To_Site_Use_Id,sum(b.amount) amount,"
strcmd = strcmd & " sum((b.amount * NVL(b.exchange_rate,1))) amount_other_currency,'REC' type "
strcmd = strcmd & " From ra_customers A, ra_customer_trx_all  aa1,ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,"
strcmd = strcmd & " ar_receipt_method_accounts_all D,"
strcmd = strcmd & " ar_cash_receipt_history_all  E,ar_payment_schedules_all    F,  ("
strcmd = strcmd & " SELECT     MIN(cash_receipt_history_id) cash_receipt_history_id,"
strcmd = strcmd & " cash_receipt_id     FROM    ar_cash_receipt_history_all"
strcmd = strcmd & " GROUP BY cash_receipt_id) G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " and aa1.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_id = g.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_history_id   = g.cash_receipt_history_id"
strcmd = strcmd & " AND  f.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " And b.receipt_date<'" & p_start_date & "'"
strcmd = strcmd & "  and aa1.trx_date<'" & p_start_date & "'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " group by a.customer_name ,aa1.Bill_To_Site_Use_Id "
strcmd = strcmd & " Union "
strcmd = strcmd & " select a.customer_name,aa2.Bill_To_Site_Use_Id, sum(g.amount_applied)  amount, "
strcmd = strcmd & " sum((g.amount_applied * NVL(b.exchange_rate,1))) amount_other_currency,"
strcmd = strcmd & " 'W/O' type From  ra_customers A, ar_cash_receipts_all B,"
strcmd = strcmd & " gl_code_combinations C, ar_receipt_method_accounts_all D,   ar_cash_receipt_history_all E,"
strcmd = strcmd & " ar_payment_schedules_all F,ra_customer_trx_all aa2,ar_receivable_applications_all G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id And g.applied_payment_schedule_id = -3"
strcmd = strcmd & " AND g.cash_receipt_id = b.cash_receipt_id and g.cash_receipt_history_id = e.cash_receipt_history_id"
strcmd = strcmd & " AND g.status = 'ACTIVITY' "
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND AA2.BILL_TO_CUSTOMER_ID = a.customer_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND b.receipt_date<'" & p_start_date & "'"
strcmd = strcmd & " and aa2.trx_date<'" & p_start_date & "'"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and not exists (select 1"
strcmd = strcmd & " From ar_cash_receipt_history_all"
strcmd = strcmd & " Where cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and    status = 'REVERSED' )"
strcmd = strcmd & " group by a.customer_name,aa2.Bill_To_Site_Use_Id"
strcmd = strcmd & " UNION"
strcmd = strcmd & " Select a.customer_name ,aa3.Bill_To_Site_Use_Id,"
strcmd = strcmd & " sum(b.amount) amount ,sum((b.amount * NVL(b.exchange_rate,1))) amount_other_currency,"
strcmd = strcmd & " 'REV' type From ra_customers A, ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C, ar_receipt_method_accounts_all  D,"
strcmd = strcmd & " ar_cash_receipt_history_all E , ra_customer_trx_all  aa3,ar_payment_schedules_all F"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id = d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and aa3.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND b.receipt_date<'" & p_start_date & "'"
strcmd = strcmd & "  and aa3.trx_date<'" & p_start_date & "'"
strcmd = strcmd & " AND e.status = 'REVERSED'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " and b.reversal_date is not null "
strcmd = strcmd & " group by  a.customer_name ,aa3.Bill_To_Site_Use_Id "
strcmd = strcmd & " Union "
strcmd = strcmd & " SELECT A.CUSTOMER_NAME ,c.bill_to_customer_id,"
strcmd = strcmd & " sum(b.amount) amount  ,sum((b.amount*NVL(c.exchange_rate,1))) amount_other_currency ,"
strcmd = strcmd & " 'ADJ' type FROM ra_customers a,"
strcmd = strcmd & " ar_adjustments_all  b,ra_customer_trx_all  c,ar_payment_schedules_all  d,gl_code_combinations   e"
strcmd = strcmd & " Where b.customer_trx_id = c.customer_trx_id And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & " AND b.status = 'A' AND  e.code_combination_id = b.code_combination_id"
strcmd = strcmd & " AND b.payment_schedule_id   = d.payment_schedule_id AND b.customer_trx_id    = d.customer_trx_id"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & "  and c.trx_date<'" & p_start_date & "'"
strcmd = strcmd & "  group by A.CUSTOMER_NAME ,c.bill_to_customer_id "
strcmd = strcmd & " union "
strcmd = strcmd & " Select a.customer_name ,b.Bill_To_Site_Use_Id,"
 strcmd = strcmd & " sum(d.EARNED_discount_taken) amount, sum(d.ACCTD_EARNED_DISCOUNT_TAKEN)  amount_other_currency ,"
strcmd = strcmd & " 'DSC' type From    ra_customers  a,"
strcmd = strcmd & " ra_customer_trx_all   B,    ar_receivable_applications_all  D Where"
strcmd = strcmd & " b.bill_to_customer_id = a.customer_id AND   b.complete_flag   = 'Y'"
strcmd = strcmd & " AND D.EARNED_DISCOUNT_TAKEN  is not null"
strcmd = strcmd & " and D.EARNED_DISCOUNT_TAKEN <> 0"
strcmd = strcmd & " and b.customer_trx_id  = d.applied_customer_trx_id"
strcmd = strcmd & " and d.application_type = 'CASH'"
strcmd = strcmd & " and d.display   = 'Y'"
strcmd = strcmd & "  and b.trx_date<'" & p_start_date & "'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " group by a.customer_name ,b.Bill_To_Site_Use_Id "
strcmd = strcmd & " Union All "
strcmd = strcmd & " SELECT a.customer_name,b.Bill_To_Site_Use_Id, sum(nvl(e.AMOUNT_DR, e.AMOUNT_CR))  amount  ,"
strcmd = strcmd & " sum(nvl(e.ACCTD_AMOUNT_DR,e.ACCTD_AMOUNT_CR)) acctd_amount ,"
strcmd = strcmd & " e.source_type "
strcmd = strcmd & " FROM ra_customers a,ra_customer_trx_all b,ar_cash_receipts_all c,ar_receivable_applications_all  d,"
strcmd = strcmd & " ar_distributions_all  e WHERE    a.customer_id  =  b.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND b.customer_trx_id=  d.APPLIED_CUSTOMER_TRX_ID"
strcmd = strcmd & " AND c.cash_receipt_id  =  d.cash_receipt_id"
strcmd = strcmd & " AND c.receipt_date<'" & p_start_date & "'"
strcmd = strcmd & "  and b.trx_date<'" & p_start_date & "'"
strcmd = strcmd & " AND e.SOURCE_ID =  d.receivable_application_id"
strcmd = strcmd & " AND e.source_Type  IN ('EXCH_LOSS', 'EXCH_GAIN')"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " group by a.customer_name,b.Bill_To_Site_Use_Id,e.source_type "
strcmd = strcmd & " ORDER BY 1,4"

Set comprec = con.Execute(strcmd)
con.CommandTimeout = 1200000
If Not comprec.EOF Then
    strcmd = " begin "
    strcmd = strcmd & " dbms_application_info.set_client_info(101);"
    strcmd = strcmd & " end;"
    Set comprec1 = con.Execute(strcmd)
    
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "AR_LEDGER_Total", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
    'MsgBox comprec!trx_number
    repcomprec.AddNew
    repcomprec!CUSTOMER_NAME = comprec!CUSTOMER_NAME
    repcomprec!Inv_Type = comprec!Type
    repcomprec!loc_id = comprec!Bill_To_Site_Use_Id
    If comprec!Type = "REC" Then
        repcomprec!amount_CR = comprec!amount_other_currency
        repcomprec!amount_DR = 0
    Else
        repcomprec!amount_DR = comprec!Amount
        repcomprec!amount_CR = 0
    End If
    
    strcmd = "select * from RA_CUSTOMER_TRX_PARTIAL_V where su_bill_to_Location='" & comprec!Bill_To_Site_Use_Id & "'"
    Set comprec1 = con.Execute(strcmd)
    con.CommandTimeout = 12000
    If Not comprec1.EOF Then
        Location = comprec1!RAA_bill_to_City
    Else
        Location = "-----"
    End If
    repcomprec!location_Name = Location
    'StatusBar1.Panels(1).Text = "Loading ......." & comprec!trx_number
    
    comprec.MoveNext
    Loop
    repcomprec.UpdateBatch
End If

End Sub
Public Sub addledger()
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
Set comprec = con1.Execute("truncate table AR_LEDGER")
con1.CommandTimeout = 120000
strcmd = "Select a.customer_id ,B.Bill_To_Site_Use_Id,a.customer_number,a.customer_name ,d.gl_date,B.CUSTOMER_TRX_ID,"
strcmd = strcmd & " b.trx_number,TO_CHAR(b.trx_date, 'DD-MM-YYYY') trx_date,NULL receipt_number,"
strcmd = strcmd & " NULL receipt_date ,SUBSTR(b.comments,1,50) remarks,d.code_combination_id account_id,"
strcmd = strcmd & "b.invoice_currency_code currency_code,b.exchange_rate ,"
strcmd = strcmd & "d.amount amount,(d.amount * NVL(b.exchange_rate,-1)) amount_other_currency,"
strcmd = strcmd & "f.type,b.customer_trx_id,d.customer_trx_line_id  ,b.rowid From    ra_customers    a,   ra_customer_trx_ALL  B, RA_CUST_TRX_LINE_GL_DIST_ALL D,"
strcmd = strcmd & "GL_CODE_COMBINATIONS E,RA_CUST_TRX_TYPES_ALL F,   ar_payment_schedules_all G"
strcmd = strcmd & " Where   b.bill_to_customer_id = a.customer_id AND   b.complete_flag = 'Y'"
strcmd = strcmd & " AND d.account_class = 'REC'  and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & "  and b.trx_date>='" & p_start_date & "'"
strcmd = strcmd & "  and b.trx_date<='" & p_end_date & "'"
strcmd = strcmd & " AND  d.customer_trx_id = b.customer_trx_id"
strcmd = strcmd & " AND e.code_combination_id = d.code_combination_id"
strcmd = strcmd & " AND f.cust_trx_type_id =b.cust_trx_type_id AND  f.type in ('INV','CM','DM','DEP')"
strcmd = strcmd & " AND d.latest_rec_flag = 'Y' AND g.customer_trx_id= b.customer_trx_id"
strcmd = strcmd & " and g.payment_schedule_id in (select min(payment_schedule_id)"
strcmd = strcmd & " from   ar_payment_schedules_all where  customer_trx_id = g.customer_trx_id)"
strcmd = strcmd & " Union"
strcmd = strcmd & " Select a.customer_id,aa1.Bill_To_Site_Use_Id ,a.customer_number, a.customer_name ,e.gl_date,0 ,"
strcmd = strcmd & " NULL,NULL,b.receipt_number,TO_CHAR(b.receipt_date,  'DD-MM-YYYY'),"
strcmd = strcmd & " NULL ,d.cash_ccid account_id ,b.currency_code,b.exchange_rate,b.amount  amount,"
strcmd = strcmd & " (b.amount * NVL(b.exchange_rate,1)) amount_other_currency,'REC' type ,0 ,0 ,"
strcmd = strcmd & " b.rowid From    ra_customers A, ra_customer_trx_all  aa1,ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,"
strcmd = strcmd & " ar_receipt_method_accounts_all D,"
strcmd = strcmd & " ar_cash_receipt_history_all  E,ar_payment_schedules_all    F,  ("
strcmd = strcmd & " SELECT     MIN(cash_receipt_history_id) cash_receipt_history_id,"
strcmd = strcmd & " cash_receipt_id     FROM    ar_cash_receipt_history_all"
strcmd = strcmd & " GROUP BY cash_receipt_id) G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " and aa1.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_id = g.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_history_id   = g.cash_receipt_history_id"
strcmd = strcmd & " AND  f.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " And b.receipt_date>='" & p_start_date & "'"
strcmd = strcmd & " And b.receipt_date<='" & p_end_date & "'"
strcmd = strcmd & "  and aa1.trx_date>='" & p_start_date & "'"
strcmd = strcmd & "  and aa1.trx_date<='" & p_end_date & "'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " Union "
strcmd = strcmd & " select a.customer_id ,aa2.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name,e.gl_date ,0 ,"
strcmd = strcmd & " NULL,NULL,b.receipt_number,TO_CHAR(b.receipt_date,  'DD-MM-YYYY')  ,"
strcmd = strcmd & " NULL,d.cash_ccid account_id  ,b.currency_code  ,b.exchange_rate  ,g.amount_applied  amount                                         ,"
strcmd = strcmd & " (g.amount_applied * NVL(b.exchange_rate,1)) amount_other_currency,"
strcmd = strcmd & " 'W/O' type ,0 ,0 ,b.rowid From  ra_customers A, ar_cash_receipts_all B,"
strcmd = strcmd & " gl_code_combinations C, ar_receipt_method_accounts_all D,   ar_cash_receipt_history_all E,"
strcmd = strcmd & " ar_payment_schedules_all F,ra_customer_trx_all aa2,ar_receivable_applications_all G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id And g.applied_payment_schedule_id = -3"
strcmd = strcmd & " AND g.cash_receipt_id = b.cash_receipt_id and g.cash_receipt_history_id = e.cash_receipt_history_id"
strcmd = strcmd & " AND g.status = 'ACTIVITY' "
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND AA2.BILL_TO_CUSTOMER_ID = a.customer_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND b.receipt_date>='" & p_start_date & "'"
strcmd = strcmd & " AND b.receipt_date<='" & p_end_date & "'"
strcmd = strcmd & " and aa2.trx_date>='" & p_start_date & "'"
strcmd = strcmd & " and aa2.trx_date<='" & p_end_date & "'"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and not exists (select 1"
strcmd = strcmd & " From ar_cash_receipt_history_all"
strcmd = strcmd & " Where cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and    status = 'REVERSED' )"
strcmd = strcmd & " UNION Select a.customer_id ,aa3.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name ,e.gl_date gl_date,0  ,"
strcmd = strcmd & " NULL ,to_char(e.trx_date,'DD-MM-YYYY')  trx_date ,b.receipt_number,"
strcmd = strcmd & " TO_CHAR(b.receipt_date,  'DD-MM-YYYY'),NULL,c.code_combination_id account_id ,"
strcmd = strcmd & " b.currency_code,b.exchange_rate ,b.amount amount  ,(b.amount * NVL(b.exchange_rate,1)) amount_other_currency,"
strcmd = strcmd & " 'REV' type ,0 ,0  ,b.rowid From     ra_customers A, ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,    ar_receipt_method_accounts_all  D,"
strcmd = strcmd & " ar_cash_receipt_history_all E , ra_customer_trx_all  aa3,ar_payment_schedules_all F"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id = d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and aa3.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND b.receipt_date>='" & p_start_date & "'"
strcmd = strcmd & " AND b.receipt_date<='" & p_end_date & "'"
strcmd = strcmd & "  and aa3.trx_date>='" & p_start_date & "'"
strcmd = strcmd & "  and aa3.trx_date<='" & p_end_date & "'"
strcmd = strcmd & " AND e.status = 'REVERSED'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " and b.reversal_date is not null UNION"
strcmd = strcmd & " SELECT A.CUSTOMER_ID,c.bill_to_customer_id,A.CUSTOMER_NUMBER  ,A.CUSTOMER_NAME ,B.GL_DATE  ,0  ,"
strcmd = strcmd & " B.ADJUSTMENT_NUMBER ,TO_CHAR(B.APPLY_DATE,'DD-MM-YYYY') trx_date ,NULL receipt_number,"
strcmd = strcmd & " NULL receipt_date ,SUBSTR(b.comments,1,50) remarks ,b.code_combination_id account_id ,c.invoice_currency_code currency_code ,"
strcmd = strcmd & " c.exchange_rate   ,b.amount amount  ,(b.amount*NVL(c.exchange_rate,1)) amount_other_currency ,"
strcmd = strcmd & " 'ADJ' type,0  ,0  ,b.rowid FROM ra_customers a,"
strcmd = strcmd & " ar_adjustments_all  b,ra_customer_trx_all  c,ar_payment_schedules_all  d,gl_code_combinations   e"
strcmd = strcmd & " Where b.customer_trx_id = c.customer_trx_id And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & " AND b.status = 'A' AND  e.code_combination_id = b.code_combination_id"
strcmd = strcmd & " AND b.payment_schedule_id   = d.payment_schedule_id AND b.customer_trx_id    = d.customer_trx_id"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & "  and c.trx_date>='" & p_start_date & "'"
strcmd = strcmd & "  and c.trx_date<='" & p_end_date & "'"

strcmd = strcmd & " union "
strcmd = strcmd & " Select a.customer_id,b.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name ,  d.gl_date,"
strcmd = strcmd & " B.CUSTOMER_TRX_ID,"
strcmd = strcmd & " b.trx_number,"
strcmd = strcmd & " TO_CHAR(b.trx_date, 'DD-MM-YYYY') trx_date ,NULL receipt_number  ,NULL receipt_date  ,"
strcmd = strcmd & " SUBSTR(b.comments,1,50) remarks,earned_discount_ccid account_id  ,"
 strcmd = strcmd & " b.invoice_currency_code currency_code  ,b.exchange_rate,d.EARNED_discount_taken amount  ,"
strcmd = strcmd & " d.ACCTD_EARNED_DISCOUNT_TAKEN  amount_other_currency ,"
strcmd = strcmd & " 'DSC' type,b.customer_trx_id,0 ,b.rowid From    ra_customers  a,"
strcmd = strcmd & " ra_customer_trx_all   B,    ar_receivable_applications_all  D Where"
strcmd = strcmd & " b.bill_to_customer_id = a.customer_id AND   b.complete_flag   = 'Y'"
strcmd = strcmd & " AND D.EARNED_DISCOUNT_TAKEN  is not null"
strcmd = strcmd & " and D.EARNED_DISCOUNT_TAKEN <> 0"
strcmd = strcmd & " and b.customer_trx_id  = d.applied_customer_trx_id"
strcmd = strcmd & " and d.application_type = 'CASH'"
strcmd = strcmd & " and d.display   = 'Y'"
strcmd = strcmd & "  and b.trx_date>='" & p_start_date & "'"
strcmd = strcmd & "  and b.trx_date<='" & p_end_date & "'"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"

strcmd = strcmd & " Union All SELECT"
strcmd = strcmd & " a.customer_id ,b.Bill_To_Site_Use_Id,a.customer_number,a.customer_name,d.gl_date,b.customer_trx_id,b.trx_number,"
strcmd = strcmd & " to_char(b.trx_date,'DD-MM-YYYY') trx_date  ,c.receipt_number  ,"
strcmd = strcmd & " to_char(c.receipt_date,'DD-MM-yyyy') receipt_date,"
strcmd = strcmd & " decode(e.amount_dr, null, 'CR','DR')   comments  ,"
strcmd = strcmd & " e.code_combination_id ,b.INVOICE_CURRENCY_CODE ,"
strcmd = strcmd & " b.exchange_rate  ,nvl(e.AMOUNT_DR, e.AMOUNT_CR)  amount  ,"
strcmd = strcmd & " nvl(e.ACCTD_AMOUNT_DR,e.ACCTD_AMOUNT_CR)     acctd_amount ,"
strcmd = strcmd & " e.source_type,0 customer_trx_id  ,"
strcmd = strcmd & " 0 customer_trx_line_id , b.ROWID"
strcmd = strcmd & " FROM ra_customers a,ra_customer_trx_all b,ar_cash_receipts_all c,ar_receivable_applications_all  d,"
strcmd = strcmd & " ar_distributions_all  e WHERE    a.customer_id  =  b.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND b.customer_trx_id=  d.APPLIED_CUSTOMER_TRX_ID"
strcmd = strcmd & " AND c.cash_receipt_id  =  d.cash_receipt_id"
strcmd = strcmd & " AND c.receipt_date>='" & p_start_date & "'"
strcmd = strcmd & " AND c.receipt_date<='" & p_end_date & "'"
strcmd = strcmd & "  and b.trx_date>='" & p_start_date & "'"
strcmd = strcmd & "  and b.trx_date<='" & p_end_date & "'"
strcmd = strcmd & " AND e.SOURCE_ID =  d.receivable_application_id"
strcmd = strcmd & " AND e.source_Type  IN ('EXCH_LOSS', 'EXCH_GAIN')"
strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " ORDER BY 1,4"
'strcmd = strcmd

Set comprec = con.Execute(strcmd)
con.CommandTimeout = 1200000
If Not comprec.EOF Then
    strcmd = " begin "
    strcmd = strcmd & " dbms_application_info.set_client_info(101);"
    strcmd = strcmd & " end;"
    Set comprec1 = con.Execute(strcmd)
    
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "Ar_Ledger", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
    'MsgBox comprec!trx_number
    repcomprec.AddNew
    repcomprec!CUSTOMER_NAME = comprec!CUSTOMER_NAME
    repcomprec!CUSTOMER_NUMBER = comprec!CUSTOMER_NUMBER
    repcomprec!gl_DATE = comprec!gl_DATE
    repcomprec!Inv_Type = comprec!Type
    repcomprec!loc_id = comprec!Bill_To_Site_Use_Id
    If comprec!Type = "REC" Then
    repcomprec!amount_CR = comprec!amount_other_currency
    repcomprec!amount_DR = 0
    repcomprec!TRX_DATE = comprec!receipt_date
    repcomprec!trx_number = comprec!receipt_number
    Else
    repcomprec!amount_DR = comprec!Amount
    repcomprec!amount_CR = 0
    repcomprec!TRX_DATE = comprec!TRX_DATE
    repcomprec!trx_number = comprec!trx_number
    End If
    
    strcmd = "select * from RA_CUSTOMER_TRX_PARTIAL_V where su_bill_to_Location='" & comprec!Bill_To_Site_Use_Id & "'"
    Set comprec1 = con.Execute(strcmd)
    con.CommandTimeout = 120000
    If Not comprec1.EOF Then
        Location = comprec1!RAA_bill_to_City
    Else
        Location = "-----"
    End If
    repcomprec!location_Name = Location
    StatusBar1.Panels(1).Text = "Loading ......." & comprec!trx_number
    
    comprec.MoveNext
    Loop
    repcomprec.UpdateBatch
End If

total
a = MsgBox("Want to Print The Aging (Y/N)? ", vbYesNo, "Print")
If a = vbYes Then
    prnarled
End If
End Sub

Public Sub prnarled()
        cryctrl.WindowTitle = "AR Customer LedgerReport"
        'cryctrl.ReportFileName = App.Path & "\reportS\Ar_Customer.rpt"
        cryctrl.ReportFileName = App.Path & "\reportS\Ar_Customer1.rpt"
                     With cryctrl
                     .PageZoom "35"
                     .WindowState = crptMaximized
                     .Destination = crptToWindow
                     .WindowShowRefreshBtn = True
                     .WindowShowExportBtn = True
                     .WindowShowPrintSetupBtn = True
                     .WindowShowPrintBtn = True
                     .WindowShowGroupTree = False
                     .WindowShowNavigationCtls = True
                     .WindowShowProgressCtls = False
                    .Action = 1
                End With
End Sub


Public Sub ADDparty()
strcmd = "select DISTINCT party_name from hz_parties order by party_name"
Set comprec = con.Execute(strcmd)
con.CommandTimeout = 120000
If Not comprec.EOF Then
    Do While Not comprec.EOF
        Combo3.AddItem comprec!Party_Name
        comprec.MoveNext
    Loop
End If
Combo3.Text = Combo3.List(0)
End Sub

Private Sub Form_Load()
DTPicker7.Value = Format("01-" & Month(Date) & "-" & Year(Date), "dd-mm-yyyy")
DTPicker1.Value = Format(Date, "dd-MMM-yyyy")
Me.Left = (Screen.Width - Me.Width) / 2
Me.Top = (Screen.Height - Me.Height) / 2
ADDparty
End Sub


Public Sub addledger2()
p_start_date = Format(DTPicker7.Value, "dd-mmm-yyyy")
p_end_date = Format(DTPicker1.Value, "dd-mmm-yyyy")
Set comprec = con1.Execute("truncate table AR_LEDGER")
con1.CommandTimeout = 120000
strcmd = "Select a.customer_id ,B.Bill_To_Site_Use_Id,a.customer_number,a.customer_name ,d.gl_date,B.CUSTOMER_TRX_ID,"
strcmd = strcmd & " b.trx_number,TO_CHAR(b.trx_date, 'DD-MM-YYYY') trx_date,NULL receipt_number,"
strcmd = strcmd & " NULL receipt_date ,SUBSTR(b.comments,1,50) remarks,d.code_combination_id account_id,"
strcmd = strcmd & "b.invoice_currency_code currency_code,b.exchange_rate ,"
strcmd = strcmd & "d.amount amount,(d.amount * NVL(b.exchange_rate,-1)) amount_other_currency,"
strcmd = strcmd & "f.type,b.customer_trx_id,d.customer_trx_line_id  ,b.rowid From    ra_customers    a,   ra_customer_trx_ALL  B, RA_CUST_TRX_LINE_GL_DIST_ALL D,"
strcmd = strcmd & "GL_CODE_COMBINATIONS E,RA_CUST_TRX_TYPES_ALL F,   ar_payment_schedules_all G"
strcmd = strcmd & " Where   b.bill_to_customer_id = a.customer_id AND   b.complete_flag = 'Y'"
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & "  and b.trx_date<='" & p_start_date & "'"
strcmd = strcmd & " AND  d.customer_trx_id = b.customer_trx_id"
strcmd = strcmd & " AND d.account_class = 'REC' AND e.code_combination_id = d.code_combination_id"
strcmd = strcmd & " AND f.cust_trx_type_id =b.cust_trx_type_id AND  f.type in ('INV','CM','DM','DEP')"
strcmd = strcmd & " AND d.latest_rec_flag = 'Y' AND g.customer_trx_id= b.customer_trx_id"
strcmd = strcmd & " and g.payment_schedule_id in (select min(payment_schedule_id)"
strcmd = strcmd & " from   ar_payment_schedules_all where  customer_trx_id = g.customer_trx_id)"
strcmd = strcmd & " Union"
strcmd = strcmd & " Select a.customer_id,aa1.Bill_To_Site_Use_Id ,a.customer_number, a.customer_name ,e.gl_date,0 ,"
strcmd = strcmd & " NULL,NULL,b.receipt_number,TO_CHAR(b.receipt_date,  'DD-MM-YYYY'),"
strcmd = strcmd & " NULL ,d.cash_ccid account_id ,b.currency_code,b.exchange_rate,b.amount  amount,"
strcmd = strcmd & " (b.amount * NVL(b.exchange_rate,1)) amount_other_currency,'REC' type ,0 ,0 ,"
strcmd = strcmd & " b.rowid From    ra_customers A, ra_customer_trx_all  aa1,ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,"
strcmd = strcmd & " ar_receipt_method_accounts_all D,"
strcmd = strcmd & " ar_cash_receipt_history_all  E,ar_payment_schedules_all    F,  ("
strcmd = strcmd & " SELECT     MIN(cash_receipt_history_id) cash_receipt_history_id,"
strcmd = strcmd & " cash_receipt_id     FROM    ar_cash_receipt_history_all"
strcmd = strcmd & " GROUP BY cash_receipt_id) G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " and aa1.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_id = g.cash_receipt_id"
strcmd = strcmd & " AND e.cash_receipt_history_id   = g.cash_receipt_history_id"
strcmd = strcmd & " AND  f.cash_receipt_id  = b.cash_receipt_id"
strcmd = strcmd & " And b.receipt_date<='" & p_start_date & "'"
strcmd = strcmd & "  and aa1.trx_date<='" & p_start_date & "'"
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " Union "
strcmd = strcmd & " select a.customer_id ,aa2.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name,e.gl_date ,0 ,"
strcmd = strcmd & " NULL,NULL,b.receipt_number,TO_CHAR(b.receipt_date,  'DD-MM-YYYY')  ,"
strcmd = strcmd & " NULL,d.cash_ccid account_id  ,b.currency_code  ,b.exchange_rate  ,g.amount_applied  amount                                         ,"
strcmd = strcmd & " (g.amount_applied * NVL(b.exchange_rate,1)) amount_other_currency,"
strcmd = strcmd & " 'W/O' type ,0 ,0 ,b.rowid From  ra_customers A, ar_cash_receipts_all B,"
strcmd = strcmd & " gl_code_combinations C, ar_receipt_method_accounts_all D,   ar_cash_receipt_history_all E,"
strcmd = strcmd & " ar_payment_schedules_all F,ra_customer_trx_all aa2,ar_receivable_applications_all G"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id And g.applied_payment_schedule_id = -3"
strcmd = strcmd & " AND g.cash_receipt_id = b.cash_receipt_id and g.cash_receipt_history_id = e.cash_receipt_history_id"
strcmd = strcmd & " AND g.status = 'ACTIVITY' "
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " AND b.remittance_bank_account_id= d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND AA2.BILL_TO_CUSTOMER_ID = a.customer_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND b.receipt_date<='" & p_start_date & "'"
strcmd = strcmd & " and aa2.trx_date<='" & p_start_date & "'"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and not exists (select 1"
strcmd = strcmd & " From ar_cash_receipt_history_all"
strcmd = strcmd & " Where cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and    status = 'REVERSED' )"
strcmd = strcmd & " UNION Select a.customer_id ,aa3.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name ,e.gl_date gl_date,0  ,"
strcmd = strcmd & " NULL ,to_char(e.trx_date,'DD-MM-YYYY')  trx_date ,b.receipt_number,"
strcmd = strcmd & " TO_CHAR(b.receipt_date,  'DD-MM-YYYY'),NULL,c.code_combination_id account_id ,"
strcmd = strcmd & " b.currency_code,b.exchange_rate ,b.amount amount  ,(b.amount * NVL(b.exchange_rate,1)) amount_other_currency,"
strcmd = strcmd & " 'REV' type ,0 ,0  ,b.rowid From     ra_customers A, ar_cash_receipts_all  B,"
strcmd = strcmd & " gl_code_combinations  C,    ar_receipt_method_accounts_all  D,"
strcmd = strcmd & " ar_cash_receipt_history_all E , ra_customer_trx_all  aa3,ar_payment_schedules_all F"
strcmd = strcmd & " Where b.pay_from_customer = a.customer_id"
strcmd = strcmd & " AND b.remittance_bank_account_id = d.bank_account_id"
strcmd = strcmd & " AND d.receipt_method_id = b.receipt_method_id"
strcmd = strcmd & " AND d.cash_ccid = c.code_combination_id"
strcmd = strcmd & " AND e.cash_receipt_id= b.cash_receipt_id"
strcmd = strcmd & " AND f.cash_receipt_id = b.cash_receipt_id"
strcmd = strcmd & " and aa3.bill_to_customer_id=a.customer_id"
strcmd = strcmd & " AND b.receipt_date<='" & p_start_date & "'"
strcmd = strcmd & "  and aa3.trx_date<='" & p_start_date & "'"
strcmd = strcmd & " AND e.status = 'REVERSED'"
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " and b.reversal_date is not null UNION"
strcmd = strcmd & " SELECT A.CUSTOMER_ID,c.bill_to_customer_id,A.CUSTOMER_NUMBER  ,A.CUSTOMER_NAME ,B.GL_DATE  ,0  ,"
strcmd = strcmd & " B.ADJUSTMENT_NUMBER ,TO_CHAR(B.APPLY_DATE,'DD-MM-YYYY') trx_date ,NULL receipt_number,"
strcmd = strcmd & " NULL receipt_date ,SUBSTR(b.comments,1,50) remarks ,b.code_combination_id account_id ,c.invoice_currency_code currency_code ,"
strcmd = strcmd & " c.exchange_rate   ,b.amount amount  ,(b.amount*NVL(c.exchange_rate,1)) amount_other_currency ,"
strcmd = strcmd & " 'ADJ' type,0  ,0  ,b.rowid FROM ra_customers a,"
strcmd = strcmd & " ar_adjustments_all  b,ra_customer_trx_all  c,ar_payment_schedules_all  d,gl_code_combinations   e"
strcmd = strcmd & " Where b.customer_trx_id = c.customer_trx_id And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & " AND b.status = 'A' AND  e.code_combination_id = b.code_combination_id"
strcmd = strcmd & " AND b.payment_schedule_id   = d.payment_schedule_id AND b.customer_trx_id    = d.customer_trx_id"
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " And c.bill_to_customer_id = a.customer_id"
strcmd = strcmd & "  and c.trx_date<='" & p_start_date & "'"
strcmd = strcmd & " union "
strcmd = strcmd & " Select a.customer_id,b.Bill_To_Site_Use_Id,a.customer_number ,a.customer_name ,  d.gl_date,"
strcmd = strcmd & " B.CUSTOMER_TRX_ID,"
strcmd = strcmd & " b.trx_number,"
strcmd = strcmd & " TO_CHAR(b.trx_date, 'DD-MM-YYYY') trx_date ,NULL receipt_number  ,NULL receipt_date  ,"
strcmd = strcmd & " SUBSTR(b.comments,1,50) remarks,earned_discount_ccid account_id  ,"
 strcmd = strcmd & " b.invoice_currency_code currency_code  ,b.exchange_rate,d.EARNED_discount_taken amount  ,"
strcmd = strcmd & " d.ACCTD_EARNED_DISCOUNT_TAKEN  amount_other_currency ,"
strcmd = strcmd & " 'DSC' type,b.customer_trx_id,0 ,b.rowid From    ra_customers  a,"
strcmd = strcmd & " ra_customer_trx_all   B,    ar_receivable_applications_all  D Where"
strcmd = strcmd & " b.bill_to_customer_id = a.customer_id AND   b.complete_flag   = 'Y'"
strcmd = strcmd & " AND D.EARNED_DISCOUNT_TAKEN  is not null"
strcmd = strcmd & " and D.EARNED_DISCOUNT_TAKEN <> 0"
strcmd = strcmd & " and b.customer_trx_id  = d.applied_customer_trx_id"
strcmd = strcmd & " and d.application_type = 'CASH'"
strcmd = strcmd & " and d.display   = 'Y'"
strcmd = strcmd & "  and b.trx_date<='" & p_start_date & "'"
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & " Union All SELECT"
strcmd = strcmd & " a.customer_id ,b.Bill_To_Site_Use_Id,a.customer_number,a.customer_name,d.gl_date,b.customer_trx_id,b.trx_number,"
strcmd = strcmd & " to_char(b.trx_date,'DD-MM-YYYY') trx_date  ,c.receipt_number  ,"
strcmd = strcmd & " to_char(c.receipt_date,'DD-MM-yyyy') receipt_date,"
strcmd = strcmd & " decode(e.amount_dr, null, 'CR','DR')   comments  ,"
strcmd = strcmd & " e.code_combination_id ,b.INVOICE_CURRENCY_CODE ,"
strcmd = strcmd & " b.exchange_rate  ,nvl(e.AMOUNT_DR, e.AMOUNT_CR)  amount  ,"
strcmd = strcmd & " nvl(e.ACCTD_AMOUNT_DR,e.ACCTD_AMOUNT_CR)     acctd_amount ,"
strcmd = strcmd & " e.source_type,0 customer_trx_id  ,"
strcmd = strcmd & " 0 customer_trx_line_id , b.ROWID"
strcmd = strcmd & " FROM ra_customers a,ra_customer_trx_all b,ar_cash_receipts_all c,ar_receivable_applications_all  d,"
strcmd = strcmd & " ar_distributions_all  e WHERE    a.customer_id  =  b.BILL_TO_CUSTOMER_ID"
strcmd = strcmd & " AND b.customer_trx_id=  d.APPLIED_CUSTOMER_TRX_ID"
strcmd = strcmd & " AND c.cash_receipt_id  =  d.cash_receipt_id"
strcmd = strcmd & " AND c.receipt_date<='" & p_start_date & "'"
strcmd = strcmd & "  and b.trx_date<='" & p_start_date & "'"
strcmd = strcmd & " AND e.SOURCE_ID =  d.receivable_application_id"
strcmd = strcmd & " AND e.source_Type  IN ('EXCH_LOSS', 'EXCH_GAIN')"
'strcmd = strcmd & " and a.customer_name='" & Combo3.Text & "'"
strcmd = strcmd & "ORDER BY 1,4"


Set comprec = con.Execute(strcmd)
con.CommandTimeout = 120000
If Not comprec.EOF Then
     strcmd = " begin "
    strcmd = strcmd & " dbms_application_info.set_client_info(101);"
    strcmd = strcmd & " end;"
    Set comprec1 = con.Execute(strcmd)
    con.CommandTimeout = 120000
    Set repcomprec = New ADODB.Recordset
    repcomprec.Open "Ar_Ledger", con1, adOpenDynamic, adLockOptimistic
    Do While Not comprec.EOF
    'MsgBox comprec!trx_number
    repcomprec.AddNew
    repcomprec!CUSTOMER_NAME = comprec!CUSTOMER_NAME
    repcomprec!CUSTOMER_NUMBER = comprec!CUSTOMER_NUMBER
    repcomprec!gl_DATE = comprec!gl_DATE
    repcomprec!Inv_Type = comprec!Type
    repcomprec!loc_id = comprec!Bill_To_Site_Use_Id
    If comprec!Type = "REC" Then
    repcomprec!amount_CR = comprec!amount_other_currency
    repcomprec!amount_DR = 0
    repcomprec!TRX_DATE = comprec!receipt_date
    repcomprec!trx_number = comprec!receipt_number
    Else
    repcomprec!amount_DR = comprec!Amount
    repcomprec!amount_CR = 0
    repcomprec!TRX_DATE = comprec!TRX_DATE
    repcomprec!trx_number = comprec!trx_number
    End If
    
    strcmd = "select * from RA_CUSTOMER_TRX_PARTIAL_V where su_bill_to_Location='" & comprec!Bill_To_Site_Use_Id & "'"
    Set comprec1 = con.Execute(strcmd)
    con.CommandTimeout = 120000
    If Not comprec1.EOF Then
        Location = comprec1!RAA_bill_to_City
    Else
        Location = "-----"
    End If
    repcomprec!location_Name = Location
    StatusBar1.Panels(1).Text = "Loading ......." & comprec!trx_number
    
    comprec.MoveNext
    Loop
    repcomprec.UpdateBatch
End If


a = MsgBox("Want to Print The Aging (Y/N)? ", vbYesNo, "Print")
If a = vbYes Then
    prnarled
End If
End Sub
