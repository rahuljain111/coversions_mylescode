Attribute VB_Name = "Module22"
      Public Xtwips As Integer, Ytwips As Integer
      Public Xpixels As Integer, Ypixels As Integer
      Type FRMSIZE
         Height As Long
         Width As Long
      End Type
    Public RePosForm As Boolean
    Public DoResize As Boolean
    Dim MyForm As FRMSIZE
    Dim DesignX As Integer
    Dim DesignY As Integer
    Dim i As Long
    Dim item As ListItem
  
'      Public m_cHdrIcons As New cLVHeaderSortIcons
      Sub Resize_For_Resolution(ByVal SFX As Single, _
       ByVal SFY As Single, MyForm As Form)
      Dim i As Integer
      Dim SFFont As Single

      SFFont = (SFX + SFY) / 2  ' average scale
      ' Size the Controls for the new resolution
    On Error Resume Next  ' for read-only or nonexistent properties
      With MyForm
        For i = 0 To .Count - 1
         If TypeOf .Controls(i) Is ComboBox Then   ' cannot change Height
           .Controls(i).Left = .Controls(i).Left * SFX
           .Controls(i).Top = .Controls(i).Top * SFY
           .Controls(i).Width = .Controls(i).Width * SFX
         Else
           .Controls(i).Move .Controls(i).Left * SFX, _
            .Controls(i).Top * SFY, _
            .Controls(i).Width * SFX, _
            .Controls(i).Height * SFY
         End If
           ' Be sure to resize and reposition before changing the FontSize
           .Controls(i).FontSize = .Controls(i).FontSize * SFFont
        Next i
        If RePosForm Then
          ' Now size the Form
          .Move .Left * SFX, .Top * SFY, .Width * SFX, .Height * SFY
        End If
      End With
      End Sub
 

Public Sub adjust(ByVal a As Form)

      Dim ScaleFactorX As Single, ScaleFactorY As Single  ' Scaling factors
      ' Size of Form in Pixels at design resolution
      DesignX = 800
      DesignY = 600
      RePosForm = True   ' Flag for positioning Form
      DoResize = False   ' Flag for Resize Event
      ' Set up the screen values
      Xtwips = Screen.TwipsPerPixelX
      Ytwips = Screen.TwipsPerPixelY
      Ypixels = Screen.Height / Ytwips ' Y Pixel Resolution
      Xpixels = Screen.Width / Xtwips  ' X Pixel Resolution

      ' Determine scaling factors
      ScaleFactorX = (Xpixels / DesignX)
      ScaleFactorY = (Ypixels / DesignY)
      ScaleMode = 1  ' twips
      'Exit Sub  ' uncomment to see how Form1 looks without resizing
      Resize_For_Resolution ScaleFactorX, ScaleFactorY, a
      'Label1.Caption = "Current resolution is " & Str$(Xpixels) + _
       "  by " + Str$(Ypixels)
      MyForm.Height = a.Height ' Remember the current size
      MyForm.Width = a.Width
End Sub



Private Sub dk(ByVal a As Form)
'      Dim ScaleFactorX As Single, ScaleFactorY As Single
'
'      'If Not DoResize Then  ' To avoid infinite loop
'       '  DoResize = True
'        ' Exit Sub
'      'End If
'
'      RePosForm = False
''      ScaleFactorX = Me.Width / MyForm.Width   ' How much change?
''      ScaleFactorY = Me.Height / MyForm.Height
'      'Resize_For_Resolution ScaleFactorX, ScaleFactorY,
'      'MyForm.Height = Me.Height ' Remember the current size
'      'MyForm.Width = Me.Width
      End Sub

